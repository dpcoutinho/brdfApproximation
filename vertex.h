#ifndef VERTEX_H
#define VERTEX_H

#include "utils.h"

struct LAVertex
{
    Point position;
    Point normal;
    float index;
    float adjacencyStart;
    float numAdjacents;
public:
    LAVertex()
    {
        position = Point(0, 0, 0);
        normal = Point(0, 0, 0);
        index = 0;
    }
    static int stride()
    {
        return 2*sizeof(Point) + 3*sizeof(float);
    }
};

struct Line
{
    Point a;
    float index1;
    Point baseColor;
    Point b;
    float index2;
    Point tipColor;

    static int stride()
    {
        return 2*sizeof(Point)+sizeof(float);
    }
};




#endif // VERTEX_H
