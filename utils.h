#ifndef UTILS_H
#define UTILS_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <QGLWidget>

#include <Eigen/Dense>
#include <Eigen/StdVector>

/// vcg imports
#include <vcg/complex/complex.h>
#include <vcg/complex/algorithms/update/bounding.h>
#include <vcg/complex/algorithms/update/color.h>
#include <vcg/complex/algorithms/update/normal.h>
#include <vcg/complex/algorithms/create/platonic.h>

///// wrapper imports
#include <wrap/io_trimesh/import.h>
#include <wrap/gl/trimesh.h>
#include <wrap/gui/trackball.h>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#include <QLabel>
#else
#include <QKeyEvent>
#include <QMouseEvent>
#endif

#include "CImg.h"

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <stdio.h>

#define PI 3.14159265358979323846
#define invPI 0.31830988618379067154


using namespace std;
using namespace Eigen;
using namespace cimg_library;
class CFace;
class CVertex;
struct MyUsedTypes : public vcg::UsedTypes<vcg::Use<CVertex>::AsVertexType, vcg::Use<CFace>::AsFaceType>{};

/// compositing wanted proprieties
class CVertex : public vcg::Vertex< MyUsedTypes, vcg::vertex::Coord3f, vcg::vertex::Color4b, vcg::vertex::Normal3f, vcg::vertex::BitFlags>{};
class CFace   : public vcg::Face<  MyUsedTypes, vcg::face::VertexRef, vcg::face::Color4b ,vcg::face::Normal3f, vcg::face::BitFlags > {};
class CMesh   : public vcg::tri::TriMesh< std::vector<CVertex>, std::vector<CFace> > {};

typedef vcg::Point3f				Point;
//typedef vcg::Point3f                Point3f;
typedef vcg::Point2f                Point2f;

enum RenderingMode {Combined = 0, Normal_Map, Color_per_vertex, Specular, Silhouette, Specular_combined};

struct Program
{
    GLuint id;
    GLuint vertexShader, fragmentShader, geometryShader, tessellationControlShader, tessellationEvaluationShader, computeShader;

    Program()
    {
        vertexShader = 0;
        fragmentShader = 0;
        geometryShader = 0;
        tessellationControlShader = 0;
        tessellationEvaluationShader = 0;
        computeShader = 0;
    }
};

struct OptEnvironment
{
    RenderingMode renderingMode;
    vcg::Shot<float> shot;
    bool estimateFocal;
    bool fineAlignment;
    float expectedVariance;
    float tolerance;
    int numIterations;
    int backgroundWeight;
    float mIweight;

    OptEnvironment()
    {
        mIweight = -1.0;
    }
};

struct Axis
{
    Point pos;
    Point color;
};

Vector3f vcgToEigen(const Point&);
Point eigenToVcg(const Vector3f&);
Vector2f vcgToEigen2f(const Point2f&);
Point2f eigenToVcg2f(const Vector2f&);

bool vectorOperator(tuple<vector<Vector3f>, float > a, tuple<vector<Vector3f>, float >b);

bool triangleIntersection(const Point&, const Point&, const Point&, const Vector3f&, const Vector3f&, double&);
Point* sphere(const float& radius, const unsigned int& vSlices, const unsigned int& hSlices,  GLuint*& indices);
Axis* createAxis(unsigned int axis, float radius, unsigned int vlices, unsigned int hSlices, GLuint*& indices);
Axis* createAxes(float radius, unsigned int vSlices, unsigned int hSlices, GLuint*& indices);


GLuint createShader(const char *filename, GLenum type);
void AddShader(Program& ShaderProgram, const char* filename, GLenum ShaderType);
void compileShaders(Program& shaderProgram, const char* vertexFile, const char* fragmentFile, const char* geometryFile = 0, const char* tesselationFile = 0);
void compileShaders(Program& shaderProgram, const char* computeFile);
void detachShaders(Program& shaderProgram);
char* parseShader(const char* filename, int& length);


void getLocation(GLuint, initializer_list<tuple<GLuint&, const char*> >);
void getUniformLocation(GLuint, initializer_list<tuple<GLuint&, const char*> >);
void enable(initializer_list<GLuint>, bool disable = false);
void bindBufferBase(GLuint, initializer_list<tuple<int, GLuint&> > );
void unbindBufferBase(GLuint, initializer_list<int>);


void makeTexture(GLubyte* , QImage , int , int , int );
void makeResizedTexture(GLubyte*, GLubyte*, int, int, int, int, int, int);
void clamp(float&, float, float);

double dot(const Point& vector1, const Point& vector2);
double dot(const Vector3f& vector1, const Vector3f& vector2);
Point cross(const Point& vector1, const Point& vector2);
Vector3f cross(const Vector3f& vector1, const Vector3f& vector2);
float distance(const Vector3f& vector1, const Vector3f& vector2);

Matrix4f lookAt(const Vector3f& eye, const Vector3f& look, const Vector3f& up);
Matrix4f scale(double sx, double sy, double sz);
Matrix4f translation(double tx, double ty, double tz);
Matrix4f rotate(float angle, const Vector3f& axis);

float angle(const Vector3f&, const Vector3f&);
float stableAngle(const Vector3f&, const Vector3f&);
float normAndAngle(const Vector3f&, const Vector3f&);

Vector2f convertToNormalizedDeviceCoordinates(const Vector2i& position, int width, int height);

Matrix4f perspective(double fovy, double aspectRatio, double near, double far);
Matrix4f orthographic(float left, float right, float bottom, float top, float near, float far);

tuple<GLuint&, const char*> tUIC(GLuint&, const char*);
tuple<int, GLuint&> tIUI(int, GLuint&);

void genSSBO(int bufferID, GLuint& bufferName, unsigned long int size);

cv::Mat qImageToMat(const QImage&);
QImage matToQImage(const cv::Mat&);
vector<Vector3f> detectSphereHough(const cv::Mat&);
Vector3f vecToEigen(const cv::Vec3f&);
float diffColor(const Vector3f&, const Vector3f&);

Vector3f rgbToXyz(const Vector3f& c);
Vector3f xyzToRgb(const Vector3f& c);
Vector3f xyzToLab(const Vector3f& c);
Vector3f labToXyz(const Vector3f& c);
Vector3f rgbToLab(const Vector3f& c);
Vector3f labToRgb(const Vector3f& c);
Vector3f rgbToHsv(const Vector3f& c);
Vector3f hsvToRgb(const Vector3f& c);
float f(float t);
float fi(float t);

QImage blur(const QImage&, float, int);
QImage correctDistortion(const QImage&, const Vector2f&, const Vector2f&,  Vector2f);

float vArea(float x);

#endif // UTILS_H
