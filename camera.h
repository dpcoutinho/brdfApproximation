#ifndef PHOTOCAMERA_H
#define PHOTOCAMERA_H

#include "vertex.h"

#include <iostream>
#include <fstream>


class PhotoCamera
{
    Matrix4f intrinsicMatrix, extrinsicMatrix;
    Matrix4f rotationMatrix,translationMatrix;
    Vector2i principalPoint;
    Vector2f pixelSize;
    Vector4f cameraCenter;

    Vector3d eulerAngles;

    int width;
    int height;
    float focalLength;
    float oneOverDx;
    float oneOverDy;
    Vector2f radialDistortion;
    Vector2f tangentialDistortion;

    float phi;
    float theta;

    Vector4f center(const MatrixXf& camera);

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    PhotoCamera();
    ~PhotoCamera();
    bool applyDistortion();
    void buildIntrinsic();
    void buildExtrinsic();
    void build(const PhotoCamera&);
    void setDefault(const int&, const int&, const int&);
    const Matrix4f& getIntrinsic() const;
    const Matrix4f& getExtrinsic() const;
    MatrixXf getPseudoInverse();
    double intrinsic(int i, int j);
    double extrinsic(int i, int j);
    const int& getHeight() const;
    const int& getWidth() const;
    float getFocalLength() const;
    Vector2f getFocalInPixels();
    const Vector2f& getPixelSize() const;
    const Vector2i& getPrincipalPoint() const;
    Vector2f getRadialDistortion() const;
    Vector2f getTangentialDistortion() const;
    double lambda();
    const Matrix4f& getRotation() const;
    const Matrix4f& getTranslation() const;
    void setFov(const float&);
    void setHeight(const int&);
    void setWidth(const int&);
    void setWidthHeight(const int&, const int&);
    void setFocalLength(const float&);
    void setPixelSize(const float&, const float&);
    void setRadialDistortion(float, float);
    void setTangentialDistortion(float, float);
    void setRotation(const Matrix4f&);
    void setTranslation(const Matrix4f&);
    void setPrincipalPoint(const Vector2i&);
    void setIntrinsic(const Matrix3f);
    void setExtrinsic(const Matrix4f);
    Vector3f unproject(const Vector2d&);
    Vector2f project(const Vector3d&);
    const Vector3d& getEulerAngles();
    const Vector4f& getCenter() const;
    void applyScale(float s);
    bool operator==(const PhotoCamera&);
    bool operator!=(const PhotoCamera&);
    void setPhi(float);
    void setTheta(float);
    float getPhi();
    float getTheta();

};

#endif // PHOTOCAMERA_H
