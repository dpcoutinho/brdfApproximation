#ifndef PHOTO_H
#define PHOTO_H

#include "camera.h"


class Photo
{
    PhotoCamera camera;
    QImage photo;
    Vector3f light;
    string file;
    Matrix4f lightViewMatrix;
public:
    Photo(const char* filename);
    Photo(const QImage &image, const PhotoCamera& newCamera, const Vector3f& newLight);
    ~Photo();
    double pixel(int i, int j);
    void buildCamera(const PhotoCamera&);
    void setCamera(const PhotoCamera&);
    const PhotoCamera& getCamera();
    QImage getPhoto();
    const Vector3f& getLight() const;
    void setLight(const Vector3f &value);
    const char* getFile();
    void applyScale(float s);
    bool hasLight();
    void setLightViewMatrix(const Matrix4f&);
    const Matrix4f& getLightViewMatrix();
};

#endif // PHOTO_H
