#include "model.h"

//void Model::setVertices()
//{
//    //cmesh.vert.size()
//    for (int i = 0; i < cmesh.vert.size(); i++)
//    {
//        LAVertex newVertex;
//        Point p = Point(cmesh.vert[i].P()[0], cmesh.vert[i].P()[1], cmesh.vert[i].P()[2]);
//        Point n = Point(cmesh.vert[i].N()[0], cmesh.vert[i].N()[1], cmesh.vert[i].N()[2]);


//        newVertex.position = p;
//        newVertex.normal = n;
//        newVertex.index = i;
//        newVertex.indexFace = i;


////        cout << "vert: " << endl << p.X() << endl << p.Y() << endl << p.Z() << endl;
////        cout << "norm: " << endl << n.X() << endl << n.Y() << endl << n.Z() << endl;


//        vertices.push_back(newVertex);

//        vector<GLuint> newAdjacencyList;
//        adjacencyList.push_back(newAdjacencyList);

//    }
//    numberVertices = vertices.size();
//}

void Model::setVertices(const vector<Vector3f>& vert, const vector<Vector3f>& norm)
{
//    cout << "set vertices: " << vert.size() << endl;
//    cout << "norm size: " << norm.size() << endl;
    for (unsigned int i = 0; i < vert.size()  ; i++)
    {
        LAVertex newVertex;
        Point p = Point(vert[i](0), vert[i](1), vert[i](2));
        Point n = Point(norm[i](0), norm[i](1), norm[i](2));

//        cout << "norm before: " << n.Norm() << endl;

        n /= n.Norm();

//        cout << "norm after: " << n.Norm() << endl;

        newVertex.position = p;
        newVertex.normal = n;
        newVertex.index = i;

//        cout << "vert: " << endl << vert[i] << endl;
//        cout << "norm: " << endl << norm[i] << endl;


        vertices.push_back(newVertex);

        vector<GLuint> newAdjacencyList;
        adjacencyList.push_back(newAdjacencyList);
    }
    numberVertices = vertices.size();
//    cout << "vertices set" << endl;
}

void Model::createAdjacencyList()
{
//    ofstream file("adjacency");
//    cout << "vertices size: " << vertices.size() << endl;
//    cout << "face indices size: " << faceIndices.size() << endl;
    for (unsigned int i = 0; i < faceIndices.size(); i+= 3)
    {
        int index = faceIndices[i];
        if (find(adjacencyList[index].begin(), adjacencyList[index].end(), faceIndices[i+1]) == adjacencyList[index].end())
            adjacencyList[index].push_back(faceIndices[i+1]);
        if (find(adjacencyList[index].begin(), adjacencyList[index].end(), faceIndices[i+2]) == adjacencyList[index].end())
            adjacencyList[index].push_back(faceIndices[i+2]);

        index = faceIndices[i+1];
        if (find(adjacencyList[index].begin(), adjacencyList[index].end(), faceIndices[i]) == adjacencyList[index].end())
            adjacencyList[index].push_back(faceIndices[i]);
        if (find(adjacencyList[index].begin(), adjacencyList[index].end(), faceIndices[i+2]) == adjacencyList[index].end())
            adjacencyList[index].push_back(faceIndices[i+2]);

        index = faceIndices[i+2];
        if (find(adjacencyList[index].begin(), adjacencyList[index].end(), faceIndices[i]) == adjacencyList[index].end())
            adjacencyList[index].push_back(faceIndices[i]);
        if (find(adjacencyList[index].begin(), adjacencyList[index].end(), faceIndices[i+1]) == adjacencyList[index].end())
            adjacencyList[index].push_back(faceIndices[i+1]);
    }
    int start = 0;
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        vertices[i].numAdjacents = adjacencyList[i].size();
        vertices[i].adjacencyStart = start;
        start += adjacencyList[i].size();
    }
}


void Model::setVertices()
{
    for (unsigned int i = 0; i < cmesh.face.size(); i++)
    {
        LAVertex newVertex0, newVertex1, newVertex2;
        Point p0 = Point(cmesh.face[i].V(0)->P()[0], cmesh.face[i].V(0)->P()[1], cmesh.face[i].V(0)->P()[2]);
        Point p1 = Point(cmesh.face[i].V(1)->P()[0], cmesh.face[i].V(1)->P()[1], cmesh.face[i].V(1)->P()[2]);
        Point p2 = Point(cmesh.face[i].V(2)->P()[0], cmesh.face[i].V(2)->P()[1], cmesh.face[i].V(2)->P()[2]);

        Point n0 = Point(cmesh.face[i].V(0)->N()[0], cmesh.face[i].V(0)->N()[1], cmesh.face[i].V(0)->N()[2]);
        Point n1 = Point(cmesh.face[i].V(1)->N()[0], cmesh.face[i].V(1)->N()[1], cmesh.face[i].V(1)->N()[2]);
        Point n2 = Point(cmesh.face[i].V(2)->N()[0], cmesh.face[i].V(2)->N()[1], cmesh.face[i].V(2)->N()[2]);

        n0 /= n0.Norm();
        n1 /= n1.Norm();
        n2 /= n2.Norm();

        newVertex0.position = p0;
        newVertex0.normal = n0;
        newVertex0.index = 3*i;
        newVertex1.position = p1;
        newVertex1.normal = n1;
        newVertex1.index = 3*i + 1;
        newVertex2.position = p2;
        newVertex2.normal = n2;
        newVertex2.index = 3*i + 2;

        vertices.push_back(newVertex0);
        vertices.push_back(newVertex1);
        vertices.push_back(newVertex2);
    }
    numberVertices = vertices.size();
}

Model::Model()
{
    numberVertices = 0;
}

void Model::loadMesh(const char *filename)
{
    loadMeshRply(filename);
    int err=vcg::tri::io::ImporterPLY<CMesh>::Open(cmesh,(char*)filename);if(err==vcg::ply::E_NOERROR)
    {
      vcg::tri::UpdateBounding<CMesh>::Box(cmesh);
      vcg::tri::UpdateNormal<CMesh>::PerVertexNormalizedPerFace(cmesh);
      vcg::tri::UpdateNormal<CMesh>::PerFaceNormalized(cmesh);

      vcg::tri::UpdateColor<CMesh>::PerVertexConstant(cmesh,vcg::Color4b::White);
      vcg::tri::UpdateColor<CMesh>::PerFaceConstant(cmesh, vcg::Color4b::White);

      vcg::tri::UpdateColor<CMesh>::PerVertexPerlinNoise(cmesh,Point(0.5,0.75,1.0));
      vcg::tri::UpdateColor<CMesh>::PerFaceFromVertex(cmesh);
    }

    diagonal = cmesh.bbox.Diag();
    vcg::Point3<float> c = cmesh.bbox.Center();
    center << c.X(), c.Y(), c.Z();
    bbox << cmesh.bbox.DimX(), cmesh.bbox.DimY(), cmesh.bbox.DimZ();

    scaledCenter = center;
    scaledBbox = bbox;

//    cout << "Model: " << endl;
    cout << "Num faces: " << cmesh.face.size() << endl;
    cout << "Num vertices: " << cmesh.vert.size() << endl;
//    setVertices();


}

static int vertex_cb( p_ply_argument argument )
{
    static Eigen::Vector3f v;

    void* data;
    long coord;

    ply_get_argument_user_data( argument, &data, &coord );

    vector<Eigen::Vector3f>* vec = static_cast< vector<Eigen::Vector3f>* >(data);

    switch( coord )
    {
        case 0:
        case 1:
            v[coord] = ply_get_argument_value( argument );
            break;

        case 2:
            v[2] = ply_get_argument_value( argument );
            vec->push_back( v );
            break;
    }

    return 1;
}

static int face_cb(p_ply_argument argument) {
    long length, value_index;
    void* data;
    GLuint index;

    ply_get_argument_property(argument, NULL, &length, &value_index);
    ply_get_argument_user_data( argument, &data, NULL );
    vector<GLuint>* vec = static_cast< vector<GLuint>* >(data);


    switch (value_index) {
        case 0:
        case 1:
            index = ply_get_argument_value(argument);
            //cout << index << " ";
            vec->push_back(index);
            break;
        case 2:
//            printf("%f\n", ply_get_argument_value(argument));
            index = ply_get_argument_value(argument);
            //cout << index << endl;
            vec->push_back(index);
            break;
        default:
            break;
    }
    return 1;
}

void Model::loadMeshRply(const char* filename)
{
    p_ply ply = ply_open( filename, NULL, 0, NULL );
    if( !ply || !ply_read_header(ply) )
    {
        cerr << "Cannot open " << filename << endl;
        exit(1);
    }
//    int success = ply_read_header(ply);

    vector<Vector3f> vert;
    vector<Vector3f> norm;

    long nvertices, nnormals, ntriangles;

    nvertices = ply_set_read_cb( ply, "vertex", "x", vertex_cb, (void*)&vert, 0 );
                ply_set_read_cb( ply, "vertex", "y", vertex_cb, (void*)&vert, 1 );
                ply_set_read_cb( ply, "vertex", "z", vertex_cb, (void*)&vert, 2 );

    nnormals = ply_set_read_cb( ply, "vertex", "nx", vertex_cb, (void*)&norm, 0 );
               ply_set_read_cb( ply, "vertex", "ny", vertex_cb, (void*)&norm, 1 );
               ply_set_read_cb( ply, "vertex", "nz", vertex_cb, (void*)&norm, 2 );


    ntriangles = ply_set_read_cb(ply, "face", "vertex_indices", face_cb, (void*) &faceIndices, 0);
                 ply_set_read_cb(ply, "face", "vertex_indices", face_cb, (void*) &faceIndices, 1);
                 ply_set_read_cb(ply, "face", "vertex_indices", face_cb, (void*) &faceIndices, 2);

    if( !ply_read( ply ) )
        return;

    if (nnormals == 0);
        // TODO use vcglib's create normal

    setVertices(vert, norm);
    createAdjacencyList();
}

CMesh& Model::mesh()
{
    return cmesh;
}

const Vector3f& Model::bbCenter()
{
    return center;
    //return cmesh.bbox.Center();
}

const Vector3f& Model::bBox()
{
    return bbox;
}

const Vector3f& Model::scaledBBCenter()
{
    return scaledCenter;
}

const Vector3f& Model::scaledBBox()
{
    return scaledBbox;
}

float Model::bbDiag()
{
    return diagonal;
    //return cmesh.bbox.Diag();
}

int Model::numVertices()
{
    return numberVertices;
}

Matrix4f Model::applyScale()
{
    float invBBDiag = 2.0f/bbDiag();
    Matrix4f s = scale(invBBDiag, invBBDiag, invBBDiag);
//    cout << "scale: " << invBBDiag << endl;
//    s = Matrix4f::Identity();
    Matrix4f t = translation(-bbCenter()(0), -bbCenter()(1), -bbCenter()(2));
    Vector4f center, newCenter;
    center << bbCenter(), 1;
    newCenter = s*center;
    scaledCenter << newCenter(0), newCenter(1), newCenter(2);
    scaledBbox = s.topLeftCorner(3, 3)* bbox;
//    cout << "new center.w: " << newCenter(3) << endl;
    Matrix4f ti = translation(newCenter(0), newCenter(1), newCenter(2));
//    cout << "translation" << endl << t << endl;
//    cout << "inverse translation" << endl << ti << endl;
    Matrix4f meshScale = ti*s*t;
    return meshScale;
}

void Model::clear()
{
    numberVertices = 0;
    cmesh.Clear();
}

vector<LAVertex>& Model::getVertices()
{
    return vertices;
}

vector<GLuint> &Model::getFaceIndices()
{
    return faceIndices;
}

vector<vector<GLuint> >& Model::getAdjacencies()
{
    return adjacencyList;
}

void Model::cleanVertices()
{
    vertices.clear();
    faceIndices.clear();
    adjacencyList.clear();
}
