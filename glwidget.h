    /****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "model.h"
#include <QKeyEvent>

#include <tuple>

#include "trackball.hpp"
#include "Mutualinfo/filter_mutualinfo.h"
#include "Mutualinfo/solver.h"

using namespace ShaderLib;


class GLWidget : public QGLWidget
{
    Q_OBJECT

    Model model;
    QImage presentPhoto;
    PhotoCamera currentCamera;

    vector<Photo> photos;
    vector<bool> usedPhotos;
    vector<int> chosenPhotos;

    vector<int> cameras;
    vector<GLubyte*> textures;

    vector<tuple<Vector3f, int> >  allLights;
    vector< tuple< vector<Vector3f>, float> > centralLights;

    vector<float> timeCalculateLights;
    vector< vector< GLfloat> > errorsPerPhoto;

    vector<tuple<Vector3f, Vector3f>> correspondences;
    vector<Vector2f> imageCorrespondences;

    vector<vector<GLuint> > adjacencyList;

    GLuint vbo;
    GLuint tbo;
    GLuint fbo;
    GLuint cbo;
    GLuint sbo;
    GLuint abo;
    GLuint vertexRbo;
    GLuint textureBuffer;
    GLuint faceBuffer;
    GLuint correspondenceBuffer;
    GLuint axisBuffer;
    GLuint sphereBuffer;
    GLuint depth;
    GLuint photoTexture;
    GLuint meshTexture;
    GLuint depthTexture;
    GLuint lightTexture;
    GLuint testTexture;
    GLuint textureArray;
    Program getPixelProgram;
    Program fittingProgram;
    Program suggestLightProgram;
    Program coordProgram;
    Program textureProgram;
    Program mixProgram;
    Program clearProgram;
    Program renderProgram;
    Program lightProgram;
    Program correspondenceProgram;
    Program sphereProgram;
    Program neighborsProgram;
    Program axisProgram;
    Program renderBonnProgram;
    GLuint colorSsbo;
    GLuint positionSsbo;
    GLuint normalSsbo;
    GLuint curveSsbo;
    GLuint polynomialSsbo;
    GLuint feedbackSsbo;
    GLuint lightSsbo;
    GLuint angleSsbo;
    GLuint photosSsbo;
    GLuint lightPosSsbo;
    GLuint qualitySsbo;
    GLuint printSsbo;
    GLuint adjacencySsbo;
    GLuint adjInfoSsbo;
    GLuint fillSsbo;
    GLuint numPhotos;
    GLuint sizeAdjacencyList;
    GLint texIndex;
    GLubyte* photoAlignment;
    int numVertices;
    int numIndices;
    int numIndicesCorrespondences;
    int indexPhoto;
    int currentPhoto;
    int sizePoint3f;
    int sizeFloat;
    int stride;
    int bestLight;
    int bestPhoto;
    int numNonCovered;
    int xSize;
    int ySize;
    int xStep;
    int yStep;
    int numXSteps;
    int numYSteps;
    int ak;
    int startingLight;
    int finalLight;
    int currentLight;
    int mostLights;
    int previousNumNegatives;
    int thresholdNormalAngle;
    int numNewPhotos;
    unsigned int hSlices;
    unsigned int vSlices;
    bool initialRender;
    bool getPixel;
    bool fitCurves;
    bool fillNeighbors;
    bool renderDifferP;
    bool renderDifferM;
    bool renderPixel;
    bool renderLight;
    bool readDiffer;
    bool calculatingNewLight;
    bool analyzingLight;
    bool showError;
    bool initializedGL;
    bool aligning;
    bool aligningRendering;
    bool aligned;
    bool getClickedPosition;
    bool printBuffer;
    bool renderTrackball;
    bool showHemisphereCoverage;
    bool drawSuggestedLight;
    bool useColorWeights;
    bool changedView;
    bool changeTextureBlock;
    bool startedOptimization;
    bool finishedOptimization;
    bool discardBadVertices;
    bool save;
    bool testing;
    bool renderAxis;
    bool deletePhoto;
    bool renderingBonn;
    float performance;
    float cameraFov;
    float _near;
    float _far;
    float greatestWeight;
    float* samplesVertices;
    double averageCoverage;
    double standardDeviation;
    double timePhotos;
    double timeFirstCycle;
    double timeOtherCycles;
    double timeNeighbors;
    LAVertex* vertices;
    GLuint* faceIndices;
    GLuint* correspondenceIndices;
    GLuint* hemisphereIndices;
    GLuint* axisIndices;
    Point* tokens;
    Point* hemisphere;
    Axis* axis;
    Vector3f center;
    Vector3f newLight;
    Vector3f webcamLightDirection;
    Vector3f eye;
    Vector3f look;
    Vector3f colorWeights;
    Vector3f currentNormal;
    Vector2f initialRotation;
    Vector2f initialTranslation;
    Vector2f* drDataPoints;
    Vector2f* dgDataPoints;
    Vector2f* dbDataPoints;
    Vector2f* dlDataPoints;
    Vector2i photoSize;
    float* drDataCurve;
    float* dgDataCurve;
    float* dbDataCurve;
    float currentAngle;
    int numDataPoints;
    int numDataSPoints;
    float initialScale;
    QPoint clickedPoint;
    clock_t neighborsStart;

    Trackball* cameraTrackball;
    Trackball* lightTrackball;

    float blendFactor;
    bool mixTextures;
    bool showMesh;

    Matrix4f modelMatrix, viewMatrix, projectionMatrix, modelviewMatrix, lightProjectionMatrix, lightViewMatrix, meshScaleMatrix, cameraViewMatrix, translate, iTranslate, backupModelMatrix;
    Matrix3f lightModelMatrix;
    Affine3f cvToGl;

    MutualInfoPlugin mutualInfo;

    void setGlProjectionMatrix();
    void setWorldTransformation();

    void loadTexture();
    void createFbo();

    Vector3f readPixel(int x, int y);
    void acquireCoefficients(int index, GLfloat &kd, GLfloat &ks, GLfloat &shininess);
    void acquireColor(int index, GLfloat &r, GLfloat &g, GLfloat &b);
    void acquireQuality(int index, GLfloat &qkd, GLfloat &qks);
    void acquirePolynomial(int index, Vector4f &pR, Vector4f &pG, Vector4f &pB);
    void addLightSSBO(const Vector3f& lightPos);
    void removeLightSSBO();
    void addCorrespondence(const Vector3f&, const Vector3f&);

    void getPixelRender();
    void suggestLightRender();
    void fittingRender();
    void fillNeighborsRender();
    void renderMesh();
    void renderTexture();
    void renderMeshTexture();
    void coordRendering(bool, bool renderPosition = false, bool renderNormal = false);
    void lightRendering();
    void normalRendering();
    void correspondenceRendering();
    void axisRendering();
    void sphereRendering();
    void renderBonn();

    void createSSBO();
    void clearSSBO();
    GLfloat readBuffer(GLuint, unsigned int, unsigned int);
    void readCurveBuffer(unsigned int);
    void readPrintBuffer(unsigned int);

    void calculateAverageCoverage();
    void calculateStandardDeviation();
    void averageTimeLights();

    void changeLight();
    void analyzeLight();

    bool analyzeNeighbors();

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    GLWidget(QWidget *parent = 0);
    void initialSetup();
    ~GLWidget();

    void createPhotoTexture();
    void createCamera(const PhotoCamera&);
    void createCamera(const PhotoCamera&, const char*, const Vector3f&);
    void createCamera(const PhotoCamera&, const char*, const Vector3f&, const Matrix4f&);
    void loadMesh(const char*);
    void nextView(bool getCamera = true);
    void previousView();

    const PhotoCamera& getCurrentCamera();
    const PhotoCamera& getNextCamera();
    const PhotoCamera& getPreviousCamera();
    void createPhoto(const PhotoCamera&, const char*, const Vector3f& = Vector3f(0, 0, 0));
    void createPhoto(const PhotoCamera&, const char*, const Vector3f&, const Matrix4f&);
    bool hasPhoto();
    const Vector3f& getLight();
    const Vector3f& getCurrentLight();
    void updateCameraMatrices();

    void update();
    void start();
    void fitBRDF();
    void useNeighbors();
    void reset();
    void renderDiffer();
    void printError();
    void reloadShaders();
    void calculateNewLight();
    void loadBestPhoto();
    void acceptBestPhoto();
    void chooseBestPhoto();
    void calculateNewPhotos();
    void renderNormal();
    void addPhoto(const char*, const Vector3f&);
    void testPhoto(const char*, const Vector3f&);
    void testPhoto(const PhotoCamera&, const char*, const Vector3f&);
    void setAlign(const bool&, const QImage& = QImage());
    void startAlign(const QImage& = QImage(), const OptEnvironment& = OptEnvironment());
    void setCamera();
    void restartCamera();
    void clearCorrespondences();
    void removeCorrespondence();
    void undoScale();
    void undoRotation();
    void undoTranslation();
    bool hasAligned();
    void drawTrackball();
    void clear();
    void drawHemisphereCoverage();
    void setWebcamLightDirection(const Vector3f&);
    int exportMesh(const char*);
    void setColorWeights(const Vector3f&);
    void setUseColorWeights();
    void deleteTextures();
    void moveTextureBlock();
    void getPlotData();
    void printStatistics();
    void printDebugData();
    vector<int> getChosenPhotos();
    void setSave();
    void renderAxes();
    void removePhoto();
    void changeThresholdAngle(int);
    void setRenderingBonn(bool);

    QSize minimumSizeHint() const;

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseDoubleClickEvent ( QMouseEvent * event );
    void wheelEvent(QWheelEvent *event);

signals:
    void correspondence(QPoint);
    void operation(unsigned int);
    void plot(Vector2f*, int, float*, int);
    void plotLine(float);
    void changedFov(float);
    void message(const char*);
private slots:
    void updateCamera(const vcg::Shot<float>&);
};

#endif
