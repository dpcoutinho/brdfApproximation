#ifndef MODEL_H
#define MODEL_H

#include "photo.h"
#include "rply.h"


class Model
{
    CMesh cmesh;
    vector<LAVertex> vertices;
    vector<GLuint> faceIndices;
    vector<vector<GLuint> > adjacencyList;
    int numberVertices;
    float diagonal;
    Vector3f center, scaledCenter, bbox, scaledBbox;

    void setVertices();
    void setVertices(const vector<Vector3f>&, const vector<Vector3f>&);
    void createAdjacencyList();
public:
    Model();
    void loadMesh(const char*);
    void loadMeshRply(const char*);
    CMesh& mesh();
    const Vector3f& bbCenter();
    const Vector3f& bBox();
    const Vector3f& scaledBBCenter();
    const Vector3f& scaledBBox();
    float bbDiag();
    int numVertices();
    Matrix4f applyScale();
    void clear();

    vector<LAVertex>& getVertices();
    vector<GLuint>& getFaceIndices();
    vector<vector<GLuint> >& getAdjacencies();
    void cleanVertices();

};

#endif // MODEL_H
