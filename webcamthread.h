#ifndef WEBCAMTHREAD_H
#define WEBCAMTHREAD_H

#include <QThread>
#include <QMutex>
#include <QImage>
#include <queue>

#include "utils.h"

#include <iostream>
using namespace std;

class WebcamThread : public QThread
{
    Q_OBJECT

    QMutex stopMutex;
    QMutex sendMutex;
    QMutex captureMutex;
    QMutex releaseMutex;
    QImage img;
    bool doStop;
    cv::VideoCapture capture;
    queue<QImage> images;
    bool send;
    bool detecting;
    cv::Vec3f sphereCenter;
    QSize frameSize;
    cv::Vec3f lightDirection;
    cv::Mat currentFrame;
    bool paused;
    bool hidden;
    int zoom;
    cv::Vec2i zoomCenter;
    bool firstSave;

    QPoint convertSize(const QPoint&, const QSize&);

    cv::Vec3f detectLightDirection(cv::Vec3i c, cv::Mat img, cv::Vec2i& lightCenter);
public:
    explicit WebcamThread(QObject *parent = 0);
    void stop();
    void start(Priority);
    void setCameraCircle(const QPoint&, const QSize&);
    void detectSphere(const bool&);
    void pause();
    void resume();
    cv::Vec3f getLightDirection();
    const cv::Mat& getFrame();
    bool isPaused();
    bool isHidden();
    void hide();
    void setCenter(const cv::Vec3f&);
    void clear();

protected:
    void run();

signals:
    void newFrame(const QImage& frame);
    void drawLightCenter(int, int);
    void renderLightDirection(float, float, float);

public slots:
    void release();
    void zoomed(int, cv::Vec2f);

};

#endif // WEBCAMTHREAD_H
