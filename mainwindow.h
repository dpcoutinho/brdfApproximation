#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <QtGlobal>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSlider>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QAction>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#else
#include <QtGui>
#endif

#include "webcamthread.h"
#include "glwidget.h"
#include "cameralabel.h"
#include "digitalcamera.h"
#include "curveglwidget.h"

#include <QMainWindow>
#include <QStackedWidget>
#include <qxmlstream.h>
#include <QtXml/QDomNode>
#include <QtXml/QDomElement>
#include <QtXml/QDomDocument>


class MainWindow : public QMainWindow
{
    Q_OBJECT

    GLWidget* meshView;
    CameraLabel* photo;
    WebcamThread* webcam;
    DigitalCamera* digitalCamera;
    QImage currentPhoto;
    OptEnvironment environment;
    CurveGLWidget* curveGraph;

    stack<unsigned int> operations;
    vector<vector<QString> > photos;
    vector<vector<Vector3f> > lightDirections;
    vector<PhotoCamera, aligned_allocator<PhotoCamera> > cameras;
    vector<Vector3f, aligned_allocator<Vector3f> > centers;
    vector< vector<Vector3f> , aligned_allocator<vector<Vector3f> > > colorCharts;

    vector<vector<QString> > testingPhotos;
    vector<vector<Vector3f> > testingLightDirections;
    vector<PhotoCamera, aligned_allocator<PhotoCamera> > testingCameras;

    QStackedWidget* centralWidget;

    QMenu* fileMenu;
    QMenu* toolsMenu;

    QToolBar* fileToolBar;
    QToolBar* calibrationToolBar;

    QStatusBar* messageStatusBar;

    QPushButton* startCalibrationButton;
    QPushButton* suggestLightButton;
    QPushButton* fitCurvesButton;
    QPushButton* deletePhotoButton;
    QPushButton* detectSphereButton;
    QPushButton* acceptSpherebutton;
    QPushButton* delimitAreaButton;
    QPushButton* finishSphereButton;
    QPushButton* detectChartButton;
    QPushButton* clearChartButton;
    QPushButton* acceptChartButton;
    QPushButton* finishChartButton;
    QPushButton* skipChartButton;
    QPushButton* alignButton;
    QPushButton* setAlignButton;
    QPushButton* clearCorrespondencesButton;
    QPushButton* finishAlignButton;
    QPushButton* alignmentParametersButton;
    QPushButton* updateAlignmentButton;
    QPushButton* cancelAlignmentButton;
    QPushButton* acceptSettingsButton;
    QPushButton* cancelSettingsButton;
    QPushButton* acceptUploadButton;
    QPushButton* saveGraphButton;
    QPushButton* testPhotosButton;

    QAction* newProjectAction;
    QAction* loadMeshAction;
    QAction* saveAction;
    QAction* readFileAction;
    QAction* undoAction;
    QAction* takePhotoAction;
    QAction* showAxisAction;
    QAction* showHemisphereAction;
    QAction* startAction;
    QAction* exportMeshAction;
    QAction* startCalibrationAction;
    QAction* showCameraAction;
    QAction* suggestLightAction;
    QAction* fitCurvesAction;
    QAction* deletePhotoAction;
    QAction* fillNeighborsAction;
    QAction* detectSphereAction;
    QAction* acceptSphereAction;
    QAction* delimitAreaAction;
    QAction* finishSphereAction;
    QAction* detectChartAction;
    QAction* clearChartAction;
    QAction* acceptChartAction;
    QAction* finishChartAction;
    QAction* skipChartAction;
    QAction* alignAction;
    QAction* setAlignAction;
    QAction* clearCorrespondencesAction;
    QAction* finishAlignAction;
    QAction* alignmentParametersAction;
    QAction* optionsAction;
    QAction* uploadChartAction;
    QAction* acceptUploadAction;
    QAction* testPhotosAction;

    QDialog* optionsMessage;
    QDialog* alignmentOptionsMessage;
    QDialog* curveDialog;
    QDialog* testPhotosMessage;

    QGroupBox* alignmentOptionsGroup;

    QCheckBox* estimateFocalCheck;
    QCheckBox* fineAlignmentCheck;
    QCheckBox* rCheckBox;
    QCheckBox* gCheckBox;
    QCheckBox* bCheckBox;
    QCheckBox* lumCheckBox;
    QCheckBox* hueCheckBox;
    QCheckBox* saturationCheckBox;
    QCheckBox* valueCheckBox;
    QCheckBox* pointBox;
    QCheckBox* curveBox;
    QCheckBox* diffuseBox;
    QCheckBox* specularBox;

    QSpinBox* iterationsSpinBox;
    QSpinBox* backgroundWeightSpinBox;
    QDoubleSpinBox* toleranceSpinBox;
    QDoubleSpinBox* varianceSpinBox;
    QDoubleSpinBox* mutCorrSpinBox;

    QSlider* mutCorrSlider;
    QSlider* curveResolutionSlider;

    QLabel* estimateFocalLabel;
    QLabel* fineAlignmentLabel;
    QLabel* numIterationsLabel;
    QLabel* backgroundLabel;
    QLabel* toleranceLabel;
    QLabel* varianceLabel;
    QLabel* mutCorrLabel;
    QLabel* cameraNameLabel;
    QLabel* curveResolutionLabel;
    QLabel* colorThresholdLabel;

    QRadioButton* webcamButton;
    QRadioButton* DSLRCameraButton;
    QRadioButton* takePhotosRadioButton;
    QRadioButton* uploadPhotosRadioButton;

    QTextEdit* cameraName;
    QTextEdit* colorThreshold;

    bool calibrating;
    bool testing;
    bool cameraOpen;
    bool changedSettings;

    int indexPhoto;
    int chosenCamera;

    QString meshFile;

    OptEnvironment readOptimizationXML(const char*);
    void saveChoices();
    void passTestPhoto();

    string fileDir;
    string photoDir;
    vector<char*> photoFiles;
    vector<Vector3f> lightPositions;

    Vector2i testingIndex;

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void keyReleaseEvent(QKeyEvent *);

signals:
    void updated();

public slots:
    void newProject();
    void loadMesh();
    void save();
    void readFile();
    void addOperation(unsigned int);
    void undoOperation();
    void takePhoto();
    void showAxis();
    void showHemisphere();
    void start();
    void exportMesh();
    void startCalibration();
    void showCamera();
    void suggestLight();
    void fitCurves();
    void deletePhoto();
    void detectSphere();
    void delimitArea();
//    void acceptSphere();
    void finishSphere();
    void detectChart();
    void clearChart();
    void acceptChart();
    void finishChart(bool accepted = true);
    void skipChart();
    void align();
    void takePhotoAlign();
    void setAlign();
    void clearCorrespondences();
    void finishAlign();
    void updateFrame(const QImage& frame);
    void drawCorrespondence(QPoint);
    void showParametersMessageBox();
    void updateMutCorrSpinBox(int);
    void updateMutCorrSlider(double);
    void updateAlignmentConfiguration();
    void cancelAlignment();
    void acceptSettings();
    void cancelSettings();
    void setLightCenter(int, int);
    void setLightDirection(float, float, float);
    void showSettings();
    void plotGraph();
    void saveGraph();
    void uploadChart();
    void acceptUpload();
    void changedFov(float);
    void message(const char*);
    void fillNeighbors();
    void testPhotos();
    void acceptTestPhotos();
    void readBonnXML();
};

#endif // MAINWINDOW_H
