#include "digitalcamera.h"

DigitalCamera::DigitalCamera(QObject *parent) : QObject(parent)
{
    indexPhoto = 1;

#ifdef WIN32 || _WIN32 || WIN64 || _WIN64
    onLinux = false;
#else
    onLinux = true;
#endif
}

void DigitalCamera::clear()
{

}

QImage DigitalCamera::getPhoto(const char* newName)
{
    string filename("DSC_");
    if (indexPhoto < 100)
        if (indexPhoto <10)
            filename.append("000");
        else
            filename.append("00");
    else
        filename.append("0");
    filename += to_string(indexPhoto) + ".JPG";

    if (onLinux)
    {
    //    filename.append(to_string(indexPhoto));
    //    filename.append(".JPG");
        system("gphoto2 --folder /store_00010001 --port=usb --camera \"Nikon DSC D80 (PTP mode)\" --capture-image --wait-event=10");

        string command("gphoto2 --folder /store_00010001/DCIM/100NCD80 --port=usb --camera \"Nikon DSC D80 (PTP mode)\" --get-file=");
        command.append(filename);
        system(command.c_str());

        if (newName != 0)
        {
            string newFilename(newName);
            string command2("mv ");
            command2 += filename + " " + newFilename;
            system(command2.c_str());
            filename = newFilename;
            cout << "filename: " << filename << endl;
        }

        string command3("mv ");
        command3 += filename + " photos/";
        system(command3.c_str());
    }
    else
    {
        string commandaux("C:\\\"Program Files (x86)\"\\digiCamControl\\CameraControlCmd.exe /filename ");
//        string waitCommand(" && timeout 3");
        QDir currentDir = QDir::currentPath();
        string dir = string("photos\\")/* currentDir.dirName().toStdString()+ */;
        if (newName != 0)
        {
            string newFilename(newName);
            filename = newFilename;
            cout << newFilename << endl;
            //dir += "\\";
        }
        string command = commandaux + dir + filename + string(" /capture") /* IF NECESSARY, use wait command:  + waitCommand */;
        system(command.c_str());
       /* if (newName != 0)
        {
            string newFilename(newName);
            string changeNameCommand("ren ");
            changeNameCommand += filename + " " + newFilename;
            cout << changeNameCommand << endl;
            system(changeNameCommand.c_str());
            system("pause");
            filename = newFilename;
        }*/

        /*string moveCommand("move ");
        moveCommand += filename + " photos\\";
        system(moveCommand.c_str());*/
    }


    indexPhoto++;

    // TODO CHANGE FILENAME TO SAVE AS PHOTO_INDEXCAMERAPOSITION_INDEXPHOTO

    string photoDir("photos/");
    photoDir += filename;
//    cout << "photo dir: " << photoDir << endl;
    QImage newPhoto(photoDir.c_str());
    cout << "new photo: " << photoDir << endl;
    currentPhoto = newPhoto;
    photoWidth = currentPhoto.width();
    photoHeight = currentPhoto.height();
    return newPhoto;
}

void DigitalCamera::setCenter(const Vector3f &newCenter)
{
    sphereCenter = newCenter;
//    Vector2i lightCenter;
//    lightDirection = detectLightDirection(sphereCenter, lightCenter);
//    cout << "LIGHT CENTER: " << lightCenter.transpose() << endl;
//    emit drawLightCenter(lightCenter[0], lightCenter[1]);
//    emit renderLightDirection(lightDirection[0], lightDirection[1], lightDirection[2]);
}

void DigitalCamera::start(const char* cameraName)
{
    FILE* listFiles;
    bool finishedReading = false;
    if (onLinux == false)
        return;
    while(!finishedReading)
    {
        string openCommand = "gphoto2 --folder /store_00010001 --port=usb --camera  --list-files";
        size_t pos = openCommand.find("--list");
        char name[100];
        sprintf(name, "\"%s\"", cameraName);
        openCommand.insert(pos-1, name);
        // \"Nikon DSC D80 (PTP mode)\"
        #ifndef WIN32 || _WIN32 || WIN64 || _WIN64
            listFiles = popen(openCommand.c_str(), "r");
        #endif
        if (!listFiles)
        {
            cout << "ERROR" << endl;
            return;
        }
        char buffer[128];
        std::string result = "";
        while(!feof(listFiles)) {
            if(fgets(buffer, 128, listFiles) != NULL)
                result = buffer;
        }
        size_t posDash = result.find("_");
        size_t posPoint = result.find(".JPG");
        if (posDash != string::npos && posPoint != string::npos)
        {
            size_t size = posPoint - posDash - 1;
            string number =  result.substr(posDash+1, size);
            indexPhoto = stoi(number)+1;
        }

        finishedReading = true;
    }
}

Vector3f DigitalCamera::getLightDirection()
{
    Vector2i lightCenter;
    return detectLightDirection(sphereCenter, lightCenter);
}

Vector3f DigitalCamera::detectLightDirection(Vector3f c, Vector2i& lightCenter)
{
//    cout << "image dimensions: " << currentPhoto.width() << " " << currentPhoto.height() << endl;
//    cout << "c: " << c.transpose() << endl;
    Vector2i current;
    Vector3f bigger(0, 0, 0);
    int radius = c[2];
    for (int i = -radius; i <= radius; i++)
    {
        for (int j = -radius; j <= radius; j++)
        {
            int r, g, b;
            QColor pixelColor = currentPhoto.pixel(c[0]+i, c[1]+j);
            pixelColor.getRgb(&r, &g, &b);
            Vector3f colorN = Vector3f(r, g, b);
            float distance = pow(i, 2) + pow(j, 2);
            if (colorN.norm() > bigger.norm() && distance <= (c[2]*c[2]))
            {
                current = Vector2i(c[0]+i, c[1]+j);
                bigger = colorN;
            }
        }
    }
    // Maybe look a region of around 10 pixels in around the brightest pixel and catch the center. Look at that later

    lightCenter = current;

    Vector3f r;
    r[0] = current[0] - c[0];
    r[1] = current[1] - c[1];
    r[1] *= -1;
    r[0] /= radius;
    r[1] /= radius;
    r[2] = sqrt(1 - pow(r[0], 2) - pow(r[1], 2));

    float phi = acos(r[2]);
    float phiL = 2*phi;
    float thetaL = asin(r[1]/sin(phi));

    Vector3f l2;
    l2[0] = sin(phiL)*cos(thetaL);
    l2[1] = sin(phiL)*sin(thetaL);
    l2[2] = cos(phiL);

    Vector3f v(0, 0, -1);
    float dot = r[0]*v[0] + r[1]*v[1] + r[2]*v[2];

    Vector3f l = v - 2*dot*r;
//    cout << "l1: " << l[0] << " " << l[1] << " " << l[2] << endl;
//    cout << "l2: " << l2[0] << " " << l2[1] << " " << l2[2] << endl;
    return l;
}

void DigitalCamera::formatCamera()
{
    indexPhoto = 1;
}

int DigitalCamera::width()
{
    return photoWidth;
}

int DigitalCamera::height()
{
    return photoHeight;
}
