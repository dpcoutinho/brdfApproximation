#include "curveglwidget.h"

#define BUFFER_OFFSET(bytes)  ((GLubyte*) NULL + (bytes))

float colors[14][3] = {1, 0, 0, 0, 1, 0, 0, 0, 1, 0.3, 0.3, 0.3, 0.7, 0.2, 0, 0.6, 0.65, 0.7, 0.7, 0.7, 0 , 1, 0.5, 0.5, 0.5, 1, 0.8, 0.8, 0.5, 1, 0.7, 0.7, 0.7, 1, 0.5, 0.15, 0.8, 0.85, 1, 1, 1, 0};
Point2f image2[] = {Point2f(-1.0, -1.0), Point2f(1.0, -1.0), Point2f(-1.0, 1.0), Point2f(1.0, 1.0)};
GLuint elementData2[] = {0, 1, 2, 3};

void errorCheckFunc(string file, int line, string message = ""){
    //OpenGL Error Handling Function:
    GLenum ErrorCheckValue = glGetError();
    if (ErrorCheckValue != GL_NO_ERROR)
    {
        cerr << ">> GL error in " << file << "  line " << line << " : " << gluErrorString(ErrorCheckValue) << "  ::  " << message << endl;
    }
}

void CurveGLWidget::createGraph(int index)
{
    Vector2f* newCurveData = new Vector2f[curveResolution];
    float step = (/*1.2**/(maxX - minX))/(float)curveResolution;
    for (int i = 0; i < curveResolution; i++)
    {
        float x = minX/* - 0.1*(maxX-minX)*/ + i*step;
        float y = polynomial[index](0)*pow(x, 3) + polynomial[index](1)*pow(x, 2) + polynomial[index](2)*x + polynomial[index](3);
//        float y = polynomial[index](0)*exp(-(x*x)/(2*pow(polynomial[index](1),2)));
        newCurveData[i] = Vector2f(x, y);
    }
    dataCurve.push_back(newCurveData);
}

void CurveGLWidget::createGraph(initializer_list<int>indexes)
{
    for (unsigned int i = 0; i < dataCurve.size(); i++)
        delete[] dataCurve[i];
    dataCurve.clear();
    for (auto &a: indexes)
        createGraph(a);
}

void CurveGLWidget::drawCurve(unsigned int index)
{
    glClear(GL_DEPTH_BUFFER_BIT);
    glUseProgram(curveProgram.id);

    GLuint projectionLocation, positionLocation, colorLocation;
    getLocation(curveProgram.id, {tUIC(positionLocation, "position")});
    getUniformLocation(curveProgram.id, {tUIC(projectionLocation, "projection"), tUIC(colorLocation, "color")});

    enable({positionLocation});
    glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniform3fv(colorLocation, 1, colors[index]);

    glBindBuffer(GL_ARRAY_BUFFER, cbo);
    glBufferData(GL_ARRAY_BUFFER, curveResolution*sizeof(Vector2f), dataCurve[index], GL_STATIC_DRAW);

    glVertexAttribPointer(positionLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_LINE_STRIP, 0, curveResolution);
    glFinish();

    enable({positionLocation}, true);
    glUseProgram(0);
    errorCheckFunc(__FILE__, __LINE__);
}

void CurveGLWidget::drawCurve(initializer_list<bool> indexes)
{
    int i = 0;
    for (initializer_list<bool>::iterator it = indexes.begin(); it != indexes.end(); it++, i++)
    {
        if (*it)
            drawCurve(i);
    }
}

void CurveGLWidget::drawPoints(unsigned int index)
{
    glClear(GL_DEPTH_BUFFER_BIT);
    glUseProgram(pointsProgram.id);

    GLuint projectionLocation, positionLocation, colorLocation;
    getLocation(pointsProgram.id, {tUIC(positionLocation, "position")});
    getUniformLocation(pointsProgram.id, {tUIC(projectionLocation, "projection"), tUIC(colorLocation, "color")});

    enable({positionLocation});
    glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniform3fv(colorLocation, 1, colors[index]);

//    for (unsigned int i = 0; i < numPoints; i++)
//        cout << dataPoints[index][i](1) << endl;
//    cout << endl;

    glBindBuffer(GL_ARRAY_BUFFER, pbo);
    glBufferData(GL_ARRAY_BUFFER, numPoints*sizeof(Vector2f), dataPoints[index], GL_STATIC_DRAW);

    glVertexAttribPointer(positionLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_POINTS, 0, numPoints);
    glFinish();

    enable({positionLocation}, true);
    glUseProgram(0);
    errorCheckFunc(__FILE__, __LINE__);
}

void CurveGLWidget::drawPoints(initializer_list<bool> indexes)
{
    int i = 0;
    for (initializer_list<bool>::iterator it = indexes.begin(); it != indexes.end(); it++, i++)
    {
        if (*it)
            drawPoints(i);
    }
}

void CurveGLWidget::clearCurves()
{
    for (unsigned int i = 0; i < dataCurve.size(); i++)
        delete[] dataCurve[i];
    for (unsigned int i = 0; i < dataPoints.size(); i++)
        delete[] dataPoints[i];
    dataCurve.clear();
    dataPoints.clear();
    polynomial.clear();

    maxX = FLT_MIN;
    minX = FLT_MAX;
    maxY = FLT_MIN;
    minY = FLT_MAX;
}

void CurveGLWidget::buildAxisImage()
{
    QImage axisImage = QImage(this->width(), this->height(), QImage::Format_RGB32);
    axisImage.fill(QColor::fromRgb(255, 255, 255));
    QPainter paint(&axisImage);

    float xHeight, yWidth;
    xHeight = (-minY)*this->height()/(/*1.2**/(maxY-minY));
    if (xHeight < 0)
        xHeight = 1;
    xHeight = this->height() - xHeight;

    yWidth = (-minX)*this->width()/(/*1.2**/(maxX - minX));
    if (yWidth < 0)
        yWidth = 1;

    paint.drawLine(0, xHeight, this->width(), xHeight);
    paint.drawLine(yWidth, 0, yWidth, this->height());

    float step = (maxX - minX)/10;
    float imageStep = this->width()/10;
    for (int i = 0; i < 11; i++)
        paint.drawText(i*imageStep-5, xHeight, 200, 200, Qt::AlignLeft, QString::number(minX + i*step, 'g', 4));

    step = (maxY - minY)/10;
    imageStep = this->height()/10;
    for (int i = 0; i < 11; i++)
        paint.drawText(yWidth, (10-i)*imageStep, 200, 200, Qt::AlignLeft, QString::number(minY + i*step, 'g', 4));

    if (currentAngle >= 0)
    {
        QPen pen;
        pen.setColor(QColor::fromRgb(255, 200, 10));
        paint.setPen(pen);
        float perc = (currentAngle - minX)/(maxX - minX);
        int x = perc*this->width();
//        if (x >= 0 && x <= this->width())
//            paint.drawLine(x, 0, x, this->height());
    }

    QImage interleaved = axisImage.mirrored();
    GLubyte* axisTextureBytes = new GLubyte[interleaved.height()*interleaved.width()*4];
    makeTexture(axisTextureBytes, interleaved, interleaved.width(), interleaved.height(), 4);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D,axisTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, interleaved.width(), interleaved.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, axisTextureBytes );
    delete[] axisTextureBytes;
    errorCheckFunc(__FILE__, __LINE__);
}

void CurveGLWidget::renderAxisImage()
{
    // render texture
    glUseProgram(textureProgram.id);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, axisTexture);

    GLuint position, imageTextureLocation, viewportLocation;
    getUniformLocation(textureProgram.id, {tUIC(imageTextureLocation, "imageTexture"), tUIC(viewportLocation, "viewportSize")});

    getLocation(textureProgram.id, {tUIC(position, "position")});

    glUniform1i(imageTextureLocation, 5);
    glUniform2i(viewportLocation, this->width(), this->height());

    glBindBuffer(GL_ARRAY_BUFFER, tbo);

    glEnableVertexAttribArray(position);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textureBuffer);

    glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, (GLvoid*)0);

    glDisableVertexAttribArray(position);
    glUseProgram(0);
    errorCheckFunc(__FILE__, __LINE__);
}

CurveGLWidget::CurveGLWidget(QWidget *parent)
{
    curveResolution = 100;
    numPoints = 0;
    currentAngle = -1;
    mouseClick = QPoint(-1, -1);
    plotRed = true;
    plotGreen = false;
    plotBlue = false;
    plotPoints = true;
    plotCurves = true;
    plotDiffuse = true;
    plotSpecular = false;
}

CurveGLWidget::~CurveGLWidget()
{
    clearCurves();
}

void CurveGLWidget::plot(Vector2f *points, int newNumPoints, float *newPolynomial, int curveIndex)
{
    if (curveIndex == 0)
        clearCurves();

    numPoints = newNumPoints;
    Vector2f* newCurvePoints = new Vector2f[numPoints];

    for (int i = 0; i < numPoints; i++)
        newCurvePoints[i] = points[i];
    dataPoints.push_back(newCurvePoints);

    for (int i = 0; i < numPoints; i++)
    {
        if (points[i](0) > maxX)
            maxX = points[i](0);
        if (points[i](0) < minX)
            minX = points[i](0);
        if (points[i](1) > maxY)
            maxY = points[i](1);
        if (points[i](1) < minY)
            minY = points[i](1);
    }
    xInterval << minX, maxX;
    yInterval << minY, maxY;

    cout << "x interval: " << xInterval.transpose() << endl;
    cout << "y interval: " << yInterval.transpose() << endl;

    polynomial.push_back(Vector4f(newPolynomial[0], newPolynomial[1], newPolynomial[2], newPolynomial[3]));
    createGraph(curveIndex);

    if (curveIndex == 3)
    {
        Vector2f* huePoints = new Vector2f[numPoints];
        Vector2f* saturationPoints = new Vector2f[numPoints];
        Vector2f* valuePoints = new Vector2f[numPoints];

        for (int i = 0; i < numPoints; i++)
        {
            Vector3f c = Vector3f(dataPoints[0][i](1), dataPoints[1][i](1), dataPoints[2][i](1));
            Vector3f cHsv = rgbToHsv(c);
            huePoints[i] = Vector2f(dataPoints[0][i](0), cHsv(0));
            saturationPoints[i] = Vector2f(dataPoints[0][i](0), cHsv(1));
            valuePoints[i] = Vector2f(dataPoints[0][i](0), cHsv(2));
        }

        dataPoints.push_back(huePoints);
        dataPoints.push_back(saturationPoints);
        dataPoints.push_back(valuePoints);
    }

    updateGL();
}



void CurveGLWidget::plotLine(float newCurrentAngle)
{
    currentAngle = newCurrentAngle;
//    cout << "current angle: " << currentAngle << endl;
//    for (int i = 0; i < numPoints; i++)
//    {
//        cout << dataPoints[0][i](0) << " == " << currentAngle << endl;
//        if (dataPoints[0][i](0) == currentAngle)
//            cout << "point: " << dataPoints[0][i] << endl;
//    }
}

void CurveGLWidget::changeResolution(const int &scale)
{
    curveResolution = 10 + scale;
    createGraph({0, 1, 2});
    updateGL();
}

void CurveGLWidget::reloadShaders()
{
    detachShaders(curveProgram);
    detachShaders(pointsProgram);
    detachShaders(textureProgram);

    compileShaders(curveProgram, "shaders/curveShader.vert", "shaders/curveShader.frag");
    compileShaders(pointsProgram, "shaders/pointShader.vert", "shaders/pointShader.frag");
    compileShaders(textureProgram, "shaders/axistexture.vert", "shaders/axistexture.frag");
}

void CurveGLWidget::plot(bool newPlotR, bool newPlotG, bool newPlotB, bool newPlotLum, bool newPlotPoints, bool newPlotCurves, bool newPlotDiffuse, bool newPlotSpecular, bool newPlotHue, bool newPlotSaturation, bool newPlotValue)
{
    plotRed = newPlotR;
    plotGreen = newPlotG;
    plotBlue = newPlotB;
    plotLum = newPlotLum;
    plotPoints = newPlotPoints;
    plotCurves = newPlotCurves;
    plotDiffuse = newPlotDiffuse;
    plotSpecular = newPlotSpecular;
    plotHue = newPlotHue;
    plotSaturation = newPlotSaturation;
    plotValue = newPlotValue;
    updateGL();
}

void CurveGLWidget::reset()
{
    minX = xInterval(0);
    maxX = xInterval(1);
    minY = yInterval(0);
    maxY = yInterval(1);
    createGraph({0, 1, 2});

    updateGL();
}

void CurveGLWidget::save()
{
    this->grabFrameBuffer().save("graph.png");
}

void CurveGLWidget::initializeGL()
{
    setMouseTracking(true);
    glClearColor(1, 1, 1, 1);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(4);

    glGenBuffers(1, &pbo);
    glBindBuffer(GL_ARRAY_BUFFER, pbo);

    glGenBuffers(1, &cbo);
    glBindBuffer(GL_ARRAY_BUFFER, cbo);

    glGenBuffers(1, &tbo);
    glBindBuffer(GL_ARRAY_BUFFER, tbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(image2), image2, GL_STATIC_DRAW);

    glActiveTexture(GL_TEXTURE5);
    glGenTextures(1, &axisTexture);
    glBindTexture(GL_TEXTURE_2D, axisTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);

    glGenBuffers(1, &textureBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textureBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementData2), elementData2, GL_STATIC_DRAW);


    compileShaders(curveProgram, "shaders/curveShader.vert", "shaders/curveShader.frag");
    compileShaders(pointsProgram, "shaders/pointShader.vert", "shaders/pointShader.frag");
    compileShaders(textureProgram, "shaders/axistexture.vert", "shaders/axistexture.frag");
}

void CurveGLWidget::resizeGL (int w, int h)
{
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
}

void CurveGLWidget::wheelEvent(QWheelEvent *e)
{
    const int WHEEL_STEP = 120;
    float pos = e->delta() / WHEEL_STEP;

    float diffX = maxX - minX;
    float diffY = maxY - minY;
    if (pos > 0)
    {
        maxX += 0.25*diffX;
        minX -= 0.25*diffX;
        maxY += 0.25*diffY;
        minY -= 0.25*diffY;
    }
    else
    {
        maxX -= 0.25*diffX;
        minX += 0.25*diffX;
        maxY -= 0.25*diffY;
        minY += 0.25*diffY;
    }
    createGraph({0, 1, 2});
    updateGL();
}

void CurveGLWidget::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
        mouseClick = e->pos();
}

void CurveGLWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
        mouseClick = QPoint(-1, -1);
}

void CurveGLWidget::mouseMoveEvent(QMouseEvent *e)
{
    if (mouseClick.x() == -1)
        return;
    QPoint diffP = e->pos() - mouseClick;
    Vector2f diff = Vector2f(diffP.x(), diffP.y());
    diff(0) *= -1;
    diff(0) /= this->width();
    diff(1) /= this->height();
    float diffX = maxX - minX;
    float diffY = maxY - minY;

    maxX += diff(0)*diffX;
    minX += diff(0)*diffX;
    maxY += diff(1)*diffY;
    minY += diff(1)*diffY;
    mouseClick = e->pos();

    createGraph({0, 1, 2});
    updateGL();
}

void CurveGLWidget::keyReleaseEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_R)
        reset();
}

QSize CurveGLWidget::minimumSizeHint() const
{
    return QSize(640, 480);
}

void CurveGLWidget::paintGL()
{
    makeCurrent();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    projectionMatrix = orthographic(minX/* - 0.1*(maxX-minX)*/, maxX /*+ 0.1*(maxX-minX)*/, minY /*- 0.1*(maxY-minY)*/, maxY /*+ 0.1*(maxY-minY)*/, 1, 10);

    buildAxisImage();
    renderAxisImage();

    if (plotPoints)
    {
        if (plotDiffuse)
            drawPoints({plotRed, plotGreen, plotBlue, plotLum, plotHue, plotSaturation, plotValue});
    }
    if (plotCurves)
    {
        if (plotDiffuse)
            drawCurve({plotRed, plotGreen, plotBlue});
    }

}
