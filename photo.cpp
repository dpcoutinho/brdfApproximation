#include "photo.h"

Photo::Photo(const char* filename)
{
    file = string(filename);
//    photo.load(filename);
    light << 0, 0, 0;
}

Photo::Photo(const QImage &image, const PhotoCamera& newCamera, const Vector3f& newLight)
{
    photo = image;
    setCamera(newCamera);
    setLight(newLight);
}

Photo::~Photo()
{
}


double Photo::pixel(int i, int j)
{
    return photo.pixel(i, j);
}

void Photo::buildCamera(const PhotoCamera& newCamera)
{
    camera.build(newCamera);
}

void Photo::setCamera(const PhotoCamera& newCamera)
{
    camera = newCamera;
}

const PhotoCamera& Photo::getCamera()
{
    return camera;
}

QImage Photo::getPhoto()
{
    return photo;
}

const Vector3f& Photo::getLight() const
{
    return light;
}

void Photo::setLight(const Vector3f &value)
{
    light = value;
    if (fabs(light.norm() - 1) > 1e-6 && fabs(light.norm()) > 1e-6)
        light /= light.norm();
}

const char* Photo::getFile()
{
    return file.c_str();
}

void Photo::applyScale(float s)
{
    camera.applyScale(s);
}

bool Photo::hasLight()
{
    return (light != Vector3f(0, 0, 0));
}

void Photo::setLightViewMatrix(const Matrix4f& newLightViewMatrix)
{
    lightViewMatrix = newLightViewMatrix;
}

const Matrix4f& Photo::getLightViewMatrix()
{
    return lightViewMatrix;
}
