#include "utils.h"


Vector3f vcgToEigen(const Point& vcgPoint)
{
    Vector3f eigenPoint;
    eigenPoint << vcgPoint.X(), vcgPoint.Y(), vcgPoint.Z();
    return eigenPoint;
}


Point eigenToVcg(const Vector3f &eigenPoint)
{
    Point vcgPoint(eigenPoint(0), eigenPoint(1), eigenPoint(2));
    return vcgPoint;
}


Vector2f vcgToEigen2f(const Point2f &vcgPoint)
{
    Vector2f eigenPoint;
    eigenPoint << vcgPoint.X(), vcgPoint.Y();
    return eigenPoint;
}


Point2f eigenToVcg2f(const Vector2f &eigenPoint)
{
    Point2f vcgPoint(eigenPoint(0), eigenPoint(1));
    return vcgPoint;
}

bool vectorOperator(tuple<vector<Vector3f>, float > a, tuple<vector<Vector3f>, float >b)
{
    return get<1>(a) > get<1>(b);
}

bool triangleIntersection(const Point &p0, const Point &p1, const Point &p2, const Vector3f &origin, const Vector3f &ray , double& t)
{
    Vector3f e1 = vcgToEigen(p1 - p0);
    Vector3f e2 = vcgToEigen(p2 - p0);
    Matrix3f M;
    M.col(0) << -ray;
    M.col(1) << e1;
    M.col(2) << e2;
    Vector3f b = origin - vcgToEigen(p0);
    Vector3f solution = M.fullPivLu().solve(b);
    t = solution(0);
    if (solution(1) < 0 || solution(2) < 0 || (solution(1) + solution(2)) > 1)
        return false;
    return true;
}


GLuint createShader(const char *filename, GLenum type)
{
    int length;
    char* source = parseShader(filename, length);

//    ifstream shaderFile(filename, ifstream::binary);
//    shaderFile.seekg(0, shaderFile.end);
//    int length = shaderFile.tellg();
//    char* source = new char[length];
//    shaderFile.seekg(0, shaderFile.beg);
//    shaderFile.read(source, length);


    GLuint shader;
    GLint shader_ok;

    if (!source)
        return 0;   

    shader = glCreateShader(type);

    if (shader == 0) {
            fprintf(stderr, "Error creating shader type %d\n", type);
            exit(0);
        }

    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);

    if (!shader_ok)
    {
        GLchar InfoLog[1024];
        glGetShaderInfoLog(shader, 1024, NULL, InfoLog);
        fprintf(stderr, "Error compiling shader %s type %d: '%s'\n", filename, type, InfoLog);
        exit(1);
    }
    delete[] source;
    return shader;
}

char* parseShader(const char *filename, int& length)
{
    ifstream shaderFile(filename, /*ifstream::binary*/std::ios::in);

    string sourceStr;
    string line = "";
    string includeFilename;
    int insertPosition;
    while (getline(shaderFile, line))
    {
        bool willInclude = false;
        for (int i = 0; i < line.size(); i++)
            if (line[i] == '#')
            {
                if (line.substr(i+1, 7).compare("include") == 0)
                {
                    insertPosition = sourceStr.length()+2;
                    includeFilename = line.substr(i+9, line.size()-(i+9));

                    willInclude = true;
                    break;
                }
            }
        if (willInclude)
        {
            ifstream includeFile(includeFilename, ifstream::binary);

            //Check if file exists
            if (includeFile.fail())
                cout << "File doesn't exist - " << includeFilename << endl;
            else
            {
                string newLine = "";
                string newSourceStr;
                while(getline(includeFile, newLine))
                    newSourceStr += "\n" + newLine;
                sourceStr += "\n\n";
                sourceStr.insert(insertPosition, newSourceStr);
            }
            includeFilename.clear();
        }
        else
            sourceStr += "\n" + line;
    }


    length = sourceStr.length();
    char* source = new char[length+1];
    strcpy(source, sourceStr.c_str());

//    delete[] source;
    return source;
}

void AddShader(Program& shaderProgram, const char *filename, GLenum shaderType)
{
    GLuint shaderObj = createShader(filename, shaderType);
    if (shaderType == GL_VERTEX_SHADER)
        shaderProgram.vertexShader = shaderObj;
    if (shaderType == GL_FRAGMENT_SHADER)
        shaderProgram.fragmentShader = shaderObj;
    if (shaderType == GL_GEOMETRY_SHADER)
        shaderProgram.geometryShader = shaderObj;
    if (shaderType == GL_TESS_CONTROL_SHADER)
        shaderProgram.tessellationControlShader = shaderObj;
    if (shaderType == GL_TESS_EVALUATION_SHADER)
        shaderProgram.tessellationEvaluationShader = shaderObj;
    if (shaderType == GL_COMPUTE_SHADER)
        shaderProgram.computeShader = shaderObj;
    glAttachShader(shaderProgram.id, shaderObj);
}

void compileShaders(Program& shaderProgram, const char* vertexFile, const char* fragmentFile, const char* geometryFile, const char* tessellationFile)
{
    shaderProgram.id = glCreateProgram();

    if (shaderProgram.id == 0)
    {
        fprintf(stderr, "Error creating shader program\n");
        exit(1);
    }

    AddShader(shaderProgram, vertexFile, GL_VERTEX_SHADER);
    AddShader(shaderProgram, fragmentFile, GL_FRAGMENT_SHADER);
    if (geometryFile != 0)
    {
        AddShader(shaderProgram, geometryFile, GL_GEOMETRY_SHADER);
    }
    if (tessellationFile != 0)
    {
        AddShader(shaderProgram, tessellationFile, GL_TESS_CONTROL_SHADER);
    }

    GLint success = 0;
    GLchar ErrorLog[1024] = { 0 };

    glLinkProgram(shaderProgram.id);
    glGetProgramiv(shaderProgram.id, GL_LINK_STATUS, &success);
    if (success == 0)
    {
        glGetProgramInfoLog(shaderProgram.id, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    glValidateProgram(shaderProgram.id);
    glGetProgramiv(shaderProgram.id, GL_VALIDATE_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shaderProgram.id, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
        exit(1);
    }
}

void compileShaders(Program &shaderProgram, const char *computeFile)
{
    shaderProgram.id = glCreateProgram();

    if (shaderProgram.id == 0)
    {
        fprintf(stderr, "Error creating shader program\n");
        exit(1);
    }

    AddShader(shaderProgram, computeFile, GL_COMPUTE_SHADER);

    GLint success = 0;
    GLchar ErrorLog[1024] = { 0 };

    glLinkProgram(shaderProgram.id);
    glGetProgramiv(shaderProgram.id, GL_LINK_STATUS, &success);
    if (success == 0)
    {
        glGetProgramInfoLog(shaderProgram.id, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    glValidateProgram(shaderProgram.id);
    glGetProgramiv(shaderProgram.id, GL_VALIDATE_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shaderProgram.id, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
        exit(1);
    }
}

void makeTexture(GLubyte* newMappedTextures, QImage image, int width, int height, int spectrum)
{
    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
            QRgb pixel = image.pixel(j, i);
            newMappedTextures[(i* width + j)*spectrum + 0] = qRed(pixel);
            newMappedTextures[(i* width + j)*spectrum + 1] = qGreen(pixel);
            newMappedTextures[(i* width + j)*spectrum + 2] = qBlue(pixel);
            newMappedTextures[(i* width + j)*spectrum + 3] = (GLubyte) 1;
        }
}

void makeResizedTexture(GLubyte *source, GLubyte *dest, int srcWidth, int destWidth, int destHeight, int spectrum, int xIndex, int yIndex)
{
    for (int i = 0; i < destHeight; i++)
    {
        for (int j = 0; j < destWidth; j++)
        {
            dest[(i* destWidth + j)*spectrum + 0] = source[((i+yIndex)*srcWidth + (j+xIndex))*spectrum + 0];
            dest[(i* destWidth + j)*spectrum + 1] = source[((i+yIndex)*srcWidth + (j+xIndex))*spectrum + 1];
            dest[(i* destWidth + j)*spectrum + 2] = source[((i+yIndex)*srcWidth + (j+xIndex))*spectrum + 2];
            dest[(i* destWidth + j)*spectrum + 3] = (GLubyte) 1;
        }
    }
}

void clamp(float& value, float minBound, float maxBound)
{
    if (value < minBound)
        value = minBound;
    if (value > maxBound)
        value = maxBound;
}

double dot(const Point& vector1, const Point& vector2)
{
    return (vector1.X()*vector2.X()) + (vector1.Y()*vector2.Y()) + (vector1.Z()*vector2.Z());
}


double dot(const Vector3f& vector1, const Vector3f& vector2)
{
    float sum = 0;
    for (int i = 0; i < 3; i++)
        sum += vector1(i) * vector2(i);
    return sum;
}

Point cross(const Point& vector1, const Point& vector2)
{
    Point normal;
    normal.X() = vector1.Y()*vector2.Z() - vector2.Y()*vector1.Z();
    normal.Y() = vector2.X()*vector1.Z() - vector1.X()*vector2.Z();
    normal.Z() = vector1.X()*vector2.Y() - vector2.X()*vector1.Y();
    return normal;
}


Vector3f cross(const Vector3f& vector1, const Vector3f& vector2)
{
    Point crossProduct = cross(eigenToVcg(vector1), eigenToVcg(vector2));
    return vcgToEigen(crossProduct);
}



Matrix4f lookAt(const Vector3f& eye, const Vector3f& look, const Vector3f& up)
{
    Matrix4f M;
    Vector3f forward = look - eye;
//    cout << "forward: " << forward.X() << " " << forward.Y() <<  " " << forward.Z() << endl;
    if (forward.norm() != 0)
        forward /= forward.norm();
    Vector3f u = up;
    Vector3f side = cross(forward, u);
//    cout << "side: " << side.X() << " " << side.Y() << " " << side.Z() << endl;
    if (side.norm() != 0)
        side /= side.norm();
    else
        side = Vector3f(0, 0, 0);

    u = cross(side, forward);
//    cout << "u: " << u.X() << " " << u.Y() << " " << u.Z() << endl;
    if (u.norm() == 0)
        u = Vector3f(0, 0, 0);

    M <<  side(0),  side(1),  side(2), 0,
          u(0),  u(1),  u(2), 0,
         -forward(0), -forward(1), -forward(2), 0,
              0,      0,      0,  1;
//    cout << "M: " << endl << M << endl;

    double tx, ty, tz;
    tx = dot(eye*-1, Vector3f(M(0, 0), M(0, 1), M(0, 2)));
    ty = dot(eye*-1, Vector3f(M(1, 0), M(1, 1), M(1, 2)));
    tz = dot(eye*-1, Vector3f(M(2, 0), M(2, 1), M(2, 2)));
//    cout << "t: " << tx << " " << ty << " " << tz << endl;

    if (fabs(tx) < 1e-6)
        tx = 0;
    if (fabs(ty) < 1e-6)
        ty = 0;
    if (fabs(tz) < 1e-6)
        tz = 0;

    M.col(3) << tx, ty, tz, 1;

    return M;
}



Matrix4f scale(double sx, double sy, double sz)
{
    Matrix4f s = Matrix4f::Identity();
    s(0, 0) *= sx;
    s(1, 1) *= sy;
    s(2, 2) *= sz;
    return s;
}


Matrix4f translation(double tx, double ty, double tz)
{
    Matrix4f t = Matrix4f::Identity();
    t(0, 3) = tx;
    t(1, 3) = ty;
    t(2, 3) = tz;
    return t;
}

Matrix4f rotate(float angle, const Vector3f& axis)
{
    Vector3f axisN = axis/axis.norm();

    float c = cos(angle);
    float s = sin(angle);
    float cI = (1-c);

    float rx, ry, rz;
    rx = axisN(0);
    ry = axisN(1);
    rz = axisN(2);

    Vector4f c1, c2, c3, c4;
    c1 << c+cI*rx*rx, cI*rx*ry+rz*s, cI*rx*rz-ry*s, 0;
    c2 << cI*rx*ry-rz*s, c+cI*ry*ry, cI*ry*rz+rx*s, 0;
    c3 << cI*rx*rz+ry*s, cI*ry*rz-rx*s, c+cI*rz*rz, 0;
    c4 << 0, 0, 0, 1;
    Matrix4f r;
    r.col(0) << c1;
    r.col(1) << c2;
    r.col(2) << c3;
    r.col(3) << c4;


    return r;
}

void detachShaders(Program &shaderProgram)
{
    if (shaderProgram.vertexShader)
        glDetachShader(shaderProgram.id, shaderProgram.vertexShader);
    if (shaderProgram.fragmentShader)
        glDetachShader(shaderProgram.id, shaderProgram.fragmentShader);
    if (shaderProgram.geometryShader)
        glDetachShader(shaderProgram.id, shaderProgram.geometryShader);
    if (shaderProgram.tessellationControlShader)
        glDetachShader(shaderProgram.id, shaderProgram.tessellationControlShader);
    if (shaderProgram.tessellationEvaluationShader)
        glDetachShader(shaderProgram.id, shaderProgram.tessellationEvaluationShader);
    if (shaderProgram.computeShader)
        glDetachShader(shaderProgram.id, shaderProgram.computeShader);
}


float distance(const Vector3f& vector1, const Vector3f& vector2)
{
    float sum = pow(vector2(0) - vector1(0), 2) + pow(vector2(1) - vector1(1), 2) + pow(vector2(2) - vector1(2), 2);
    return sqrt(sum);
}

//Implement later the more stable formula:
// angle = atan2(norm(cross(a,b)), dot(a,b))
float angle(const Vector3f& vector1, const Vector3f& vector2)
{
    float cosine = dot(vector1, vector2);
    if (cosine <= -1)
        cosine = -1;
    if (cosine >= 1)
        cosine = 1;
    float ang = acos(cosine)*180.0/PI;
    return ang;
}

float normAndAngle(const Vector3f& vector1, const Vector3f& vector2)
{
    float norm1 = vector1.norm();
    float norm2 = vector2.norm();
    Vector3f v1N = vector1 / norm1;
    Vector3f v2N = vector2 / norm2;

    float cosine = dot(v1N, v2N);
    if (cosine <= -1)
        cosine = -1;
    if (cosine >= 1)
        cosine = 1;
    float ang = acos(cosine)*180.0/PI;
    return ang;

}



// angle = atan2(norm(cross(a,b)), dot(a,b))
float stableAngle(const Vector3f& vector1, const Vector3f& vector2)
{
    Vector3f crossProduct = cross(vector1, vector2);
    float dotProduct = dot(vector1, vector2);
    float angle = atan2(crossProduct.norm(), dotProduct);
    angle = angle*180.0/PI;
    return fabs(angle);
}

Vector2f convertToNormalizedDeviceCoordinates(const Vector2i& position, int width, int height)
{
    float x = (float)position[0]/(float)(width/2.0);
    x = x-1;

    float y = (float)position[1]/(float)(height/2.0);
    y = 1-y;

    Eigen::Vector2f ret(x,y);
    return ret;
}

Matrix4f perspective(double fovy, double aspectRatio, double _near, double _far)
{
    Matrix4f M;
    double angleRadian = (PI/180.0) * fovy;
    double c = 1/tan(angleRadian/2);
    M << c/aspectRatio, 0, 0, 0,
         0, c, 0, 0,
         0, 0, (_far + _near)/(_near - _far), (2*_far*_near)/(_near - _far),
         0, 0, -1, 0;
    return M;
}

Matrix4f orthographic(float left, float right, float bottom, float top, float _near, float _far)
{
    Matrix4f O;
    float dx = right - left;
    float dy = top - bottom;
    float dz = _far - _near;
    O << 2/dx, 0, 0, -((right+left)/dx),
            0, 2/dy, 0, -((top+bottom)/dy),
            0, 0, 2/dz, -((_far+_near)/dz),
            0, 0, 0, 1;
    return O;
}


void getLocation(GLuint program, initializer_list<tuple<GLuint&, const char *> > variables)
{
    for (auto &variable: variables)
    {
        GLuint newLocation = glGetAttribLocation(program, get<1>(variable));
        get<0>(variable) = newLocation;
    }
}

void getUniformLocation(GLuint shaderId, initializer_list<tuple<GLuint&, const char*> > variables)
{
    for (auto variable: variables)
    {
        get<0>(variable) = glGetUniformLocation(shaderId, get<1>(variable));
//        cout << "location no method: " << get<0>(variable) << endl;
    }
}

void enable(initializer_list<GLuint> variables, bool disable)
{
    for (auto &variable: variables)
    {
        if (!disable)
            glEnableVertexAttribArray(variable);
        else
            glDisableVertexAttribArray(variable);
    }
}


void bindBufferBase(GLuint bindType, initializer_list<tuple<int, GLuint &> > variables)
{
    for (auto variable: variables)
        glBindBufferBase(bindType, get<0>(variable), get<1>(variable));
}

void unbindBufferBase(GLuint bindType,  initializer_list<int> variables)
{
    for (auto variable: variables)
        glBindBufferBase(bindType, variable, 0);
}


tuple<GLuint &, const char *> tUIC(GLuint & integer, const char * string)
{
    return tuple<GLuint &, const char *>(integer, string);
}


tuple<int, GLuint &> tIUI(int integer, GLuint & uInteger)
{
    return tuple<int, GLuint&>(integer, uInteger);
}

void genSSBO(int bufferID, GLuint &bufferName, unsigned long int size)
{
//    cout << "size: " << size << endl;
    glGenBuffers(1, &bufferName);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferName);
    glBindBufferBase( GL_SHADER_STORAGE_BUFFER, bufferID, bufferName);
    glBufferData(GL_SHADER_STORAGE_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
}


vector<Vector3f> detectSphereHough(const cv::Mat& frame)
{
    int startingRadius = 95;
    startingRadius *= frame.cols/1350.0;
    cv::Mat gray;
    cv::cvtColor(frame, gray, CV_BGR2GRAY);
    medianBlur(gray, gray, 5);
    bool notFound = true;

    vector<Vector3f> result;
    int i = 0;
    while(notFound)
    {
        vector<cv::Vec3f> circles;
        cv::HoughCircles(gray, circles, CV_HOUGH_GRADIENT, 1, 10, 100, 30, startingRadius - i, startingRadius + i);
        for (unsigned int i = 0; i < circles.size(); i++)
            result.push_back(vecToEigen(circles[i]));
        if (circles.size() > 0)
            notFound = false;
        i += 2;
    }
    return result;
}


cv::Mat qImageToMat(const QImage &image)
{
    QImage swapped = image.rgbSwapped();
    cv::Mat  mat( swapped.height(), swapped.width(), CV_8UC4, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() );
    return mat.clone();
}


QImage matToQImage(const cv::Mat &image)
{
    QImage result;
    const uchar* qImageBuffer = (const uchar*) image.data;
    result = QImage(qImageBuffer, image.cols, image.rows, image.step, QImage::Format_RGB888);
    result = result.rgbSwapped();
    return result;
}


Vector3f vecToEigen(const cv::Vec3f& v)
{
    return Vector3f(v[0], v[1], v[2]);
}


float diffColor(const Vector3f &v1, const Vector3f &v2)
{
    Vector3f v3I = v1 - v2;
    Vector3f v3F;
    v3F << v3I[0], v3I[1], v3I[2];
    return v3F.norm();
}

Vector3f rgbToXyz(const Vector3f& c)
{
    Vector3f convertedColor;
    Matrix3f matrixConversion;
    float constant = 1/0.17697;

    matrixConversion(0,0) = 0.49;
    matrixConversion(0,1) = 0.31;
    matrixConversion(0,2) = 0.20;
    matrixConversion(1,0) = 0.17697;
    matrixConversion(1,1) = 0.81240;
    matrixConversion(1,2) = 0.01063;
    matrixConversion(2,0) = 0.00;
    matrixConversion(2,1) = 0.01;
    matrixConversion(2,2) = 0.99;

    matrixConversion *= constant;
    convertedColor = matrixConversion*c;
    return convertedColor;
}

Vector3f xyzToRgb(const Vector3f& c)
{
    Vector3f convertedColor;
    Matrix3f matrixConversion;

    matrixConversion(0,0) = 0.41847;
    matrixConversion(0,1) = -0.15866;
    matrixConversion(0,2) = -0.082835;
    matrixConversion(1,0) = -0.091169;
    matrixConversion(1,1) = 0.25243;
    matrixConversion(1,2) = 0.015708;
    matrixConversion(2,0) = 0.00092090;
    matrixConversion(2,1) = -0.0025498;
    matrixConversion(2,2) = 0.17860;

    convertedColor = matrixConversion*c;
    return convertedColor;

}

Vector3f xyzToLab(const Vector3f& c)
{
    Vector3f convertedColor;
    float xw, yw, zw;
    xw = 0.9642;
    yw = 1.0;
    zw = 0.8249;

    convertedColor(0) = 116*f(c(1)/yw) - 16;
    convertedColor(1) = 500*(f(c(0)/xw) - f(c(1)/yw));
    convertedColor(2) = 200*(f(c(1)/yw) - f(c(2)/zw));

    return convertedColor;
}

Vector3f labToXyz(const Vector3f& c)
{
    Vector3f convertedColor;
    float xw, yw, zw;
    xw = 0.9642;
    yw = 1.0;
    zw = 0.8249;
    float constant = (1.0/116.0) *(c(0) + 16);

    convertedColor(1) = yw*fi(constant);
    convertedColor(0) = xw*fi(constant + c(1)/500.0);
    convertedColor(2) = zw*fi(constant - c(2)/200.0);

    return convertedColor;
}

Vector3f rgbToLab(const Vector3f& c)
{
    Vector3f colorXYZ = rgbToXyz(c);
    return xyzToLab(colorXYZ);
}

Vector3f labToRgb(const Vector3f& c)
{
    Vector3f colorXYZ = labToXyz(c);
    return xyzToRgb(colorXYZ);
}

float f(float t)
{
    if (t > 0.0088564516790)
        return pow(t, 1.0/3.0);
    else
        return (1.0/3.0*pow(29.0/6.0, 2)*t + 4.0/29.0);
}

float fi(float t)
{
    if (t > 0.2068965517241)
        return pow(t, 3);
    else
        return (3*pow(6.0/29.0, 2)*(t-4.0/29.0));
}



Point *sphere(const float &radius, const unsigned int& vSlices, const unsigned int& hSlices, GLuint*& indices)
{
    Point* points = new Point[vSlices*hSlices + 2];
    float theta = 0;
    float gamma = 0;
    float dTheta = 360.0/hSlices;
    float dGamma = 180.0/vSlices;
    gamma = dGamma;
    points[0] = Point(0, radius, 0);

    for (unsigned int i = 0; i < vSlices; i++)
        for (unsigned int j = 0; j < hSlices; j++)
        {
            float cTheta = theta + j*dTheta;
            float cGamma = gamma + i*dGamma;
            cTheta *= (PI/180);
            cGamma *= (PI/180);
            Point newPoint;
            newPoint.X() = radius*cos(cTheta)*sin(cGamma);
            newPoint.Z() = radius*sin(cTheta)*sin(cGamma);
            newPoint.Y() = radius*cos(cGamma);
            points[i*hSlices + j + 1] = newPoint;
        }
    points[vSlices*hSlices+1] = Point(0, -radius, 0);

    indices = new GLuint[2*hSlices*(3*vSlices + 1)];
    int index = 0;
    for (unsigned int j = 0; j < hSlices; j++)
    {
        indices[index] = 0;
        indices[index+1] = (j+1)%hSlices+1;
        indices[index+2] = j+1;
        index += 3;
    }
    for (unsigned int i = 0; i < vSlices-1; i++)
        for (unsigned int j = 0; j < hSlices; j++)
        {
            indices[index] = i*hSlices + j +1;
            indices[index+1] = i*hSlices + (j+1)%hSlices +1;
            indices[index+2] = (i+1)*hSlices + (j+1)%hSlices +1;
            index+=3;
            indices[index] = i*hSlices + j +1;
            indices[index+1] = (i+1)*hSlices + (j+1)%hSlices +1;
            indices[index+2] = (i+1)*hSlices + j +1;
            index+=3;
        }

    for (unsigned int j = 0; j < hSlices; j++)
    {
        indices[index] = (vSlices-1)*hSlices + j+1;
        indices[index+1] = (vSlices-1)*hSlices + j+1+1;
        indices[index+2] = vSlices*hSlices+1;
        index+=3;
    }

    return points;
}


Axis *createAxis(unsigned int axis, float radius, unsigned int vSlices, unsigned int hSlices, GLuint *&indices)
{
    int vertSlices = vSlices + 1;
    Axis* points = new Axis[vertSlices*hSlices /*+ hSlices + 1*/];
    float dTheta = (2*PI)/hSlices;
    float dHeight = 0.4/vSlices;

    Matrix4f rotation = Matrix4f::Identity();

    Point color = Point(0, 1, 0);
    Vector3f rotAxis;
    if (axis == 0)
    {
        color= Point(1, 0, 0);
        rotAxis << 0, 0, 1;
        rotation = rotate(-PI/2, rotAxis);
    }
    if (axis == 2)
    {
        color = Point(0, 0, 1);
        rotAxis << 1, 0, 0;
        rotation = rotate(-PI/2, rotAxis);
    }

    int index = 0;
    for (int i = 0; i < vertSlices; i++)
        for (int j = 0; j < hSlices; j++)
        {
            Point newPoint;
            newPoint.X() = radius*cos(j*dTheta);
            newPoint.Y() = i*dHeight;
            newPoint.Z() = radius*sin(j*dTheta);

            Vector3f newPointE = vcgToEigen(newPoint);
            newPointE = rotation.topLeftCorner(3, 3)*newPointE;
            newPoint = eigenToVcg(newPointE);

            points[index].pos = newPoint;
            points[index].color = color;
            index++;
        }

//    for (int j = 0; j < hSlices; j++)
//    {
//        Point newPoint;
//        newPoint.X() = radius*sin(j*dTheta);
//        newPoint.Y() = 0.1;
//        newPoint.Z() = radius*cos(j*dTheta);

//        points[vSlices*hSlices+ j] = newPoint;
//    }

//    Point newPoint(0.0, 0.2, 0.0);
//    points[vSlices*hSlices+ hSlices] = newPoint;

    indices = new GLuint[hSlices*3*2*vertSlices];

    index = 0;
    for (int i = 0; i < vertSlices-1; i++)
        for (int j = 0; j < hSlices; j++)
        {
            indices[index] = i*hSlices + j;
            indices[index+1] = i*hSlices + (j+1)%hSlices;
            indices[index+2] = (i+1)*hSlices + (j+1)%hSlices;
            index+=3;

            indices[index] = i*hSlices + j;
            indices[index+1] = (i+1)*hSlices + (j+1)%hSlices;
            indices[index+2] = (i+1)*hSlices + j;
            index+=3;
        }

    return points;
}


Axis *createAxes(float radius, unsigned int vSlices, unsigned int hSlices, GLuint*& indices)
{
    Axis* axes = new Axis[3*(vSlices+1)*hSlices];
    indices = new GLuint[3*hSlices*3*2*(vSlices+1)];

    GLuint *xInd, *yInd, *zInd;
    Axis* xAxis = createAxis(0, radius, vSlices, hSlices, xInd);
    Axis* yAxis = createAxis(1, radius, vSlices, hSlices, yInd);
    Axis* zAxis = createAxis(2, radius, vSlices, hSlices, zInd);

    for (unsigned int i = 0; i < (vSlices+1)*hSlices; i++)
    {
        axes[i] = xAxis[i];
        axes[(vSlices+1)*hSlices + i] = yAxis[i];
        axes[2*(vSlices+1)*hSlices + i] = zAxis[i];
    }

    for (unsigned int i = 0; i < hSlices*3*2*(vSlices+1); i++)
    {
        indices[i] = xInd[i];
        indices[hSlices*3*2*(vSlices+1)+i] = yInd[i];
        indices[2*hSlices*3*2*(vSlices+1) +i] = zInd[i];
    }


    delete[] xAxis;
    delete[] yAxis;
    delete[] zAxis;
    delete[] xInd;
    delete[] yInd;
    delete[] zInd;

    return axes;
}



QImage blur(const QImage &image, float sigma, int newSize)
{
    int sizeWindow;
    if (newSize == 0)
        sizeWindow = ceil(6.0*sigma - 1.0);
    else
        sizeWindow = newSize;
    if (sizeWindow%2 == 0)
        sizeWindow++;
    float* weights = new float[sizeWindow];
    int numCells = ceil(sizeWindow/2.0);
    float constant = 1.0/(sqrt(2*PI)*sigma);
//    float constant = 1;
    //cout << sizeWindow << " e " << numCells << endl;
    for (int i = 0; i < numCells; i++)
    {
        float gaussianWeight;
        float x = i - numCells + 1;
        gaussianWeight = constant*exp(-(x*x)/(2*sigma*sigma));
        weights[i] = gaussianWeight;
        weights[sizeWindow-i-1] = gaussianWeight;
        //cout << i << " - " << (sizeWindow-i-1) << endl;
    }
    float length = 0;
    for (int i = 0; i < sizeWindow; i++)
        length += weights[i];
    for (int i = 0; i < sizeWindow; i++)
        weights[i] /= length;


    int width = image.width(), height = image.height();
    int spectrum = 3;
    //if (image.isGrayscale())
      //  spectrum = 1;
    float*** blurred = new float**[width];
    float*** auxBlurred = new float**[width];
    for (int i = 0; i < width; i++)
    {
        blurred[i] = new float*[height];
        auxBlurred[i] = new float*[height];
        for (int j = 0; j < height; j++)
        {
            blurred[i][j] = new float[spectrum];
            auxBlurred[i][j] = new float[spectrum];
        }
    }

    int halfWindow = ceil(sizeWindow/2);

    for (int j = 0; j < height; j++)
    {
        for (int i = 0; i < width; i++)
        {
            float aux[3];
            for (int l = 0; l < spectrum; l++)
                aux[l] = 0;
            for (int k = -halfWindow, filterPos = 0; k <= halfWindow; k++, filterPos++)
            {
                QRgb pixel;
                if ((i + k) >= 0 && (i + k) < width)
                    pixel = image.pixel(i+k, j);
                else
                    pixel = image.pixel(i-k, j);
                aux[0] += qRed(pixel)*weights[filterPos];
                aux[1] += qGreen(pixel)*weights[filterPos];
                aux[2] += qBlue(pixel)*weights[filterPos];
            }
            for (int l = 0; l < spectrum; l++)
                auxBlurred[i][j][l] = aux[l];
        }
    }
    for (int i = 0; i < width; i++)
        for (int j = 0; j < height; j++)
        {
            double aux[3];
            for (int l = 0; l < spectrum; l++)
                aux[l] = 0;
            for (int k = -halfWindow, filterPos = 0; k <= halfWindow; k++, filterPos++)
                for (int l = 0; l < spectrum; l++)
                {
                    if ((j+k) >= 0 && (j+k) < height)
                        aux[l] += auxBlurred[i][j+k][l]*weights[filterPos];
                    else
                        aux[l] += auxBlurred[i][j-k][l]*weights[filterPos];

                }
            for (int l = 0; l < spectrum; l++)
                blurred[i][j][l] = aux[l];
        }


    QImage blurredImage(width, height, image.format());

    for (int i = 0; i < width; i++)
        for (int j = 0; j < height; j++)
        {
//            QRgb pixel = image.pixel(i, j);
            QColor color;
            color.setRedF(blurred[i][j][0]/255.0);
            color.setGreenF(blurred[i][j][1]/255.0);
            color.setBlueF(blurred[i][j][2]/255.0);
//            cout << "blurred: " << blurred[i][j][0] << " " << blurred[i][j][1] << " " << blurred[i][j][2] << " x " << color.red() << " " << color.green() << " " << color.blue() << endl;
//            cout << "change: " << qRed(pixel) << " " << qGreen(pixel) << " " << qBlue(pixel) << " x " << color.red() << " " << color.green() << " " << color.blue() << endl << endl;
            blurredImage.setPixel(i, j, color.rgb());
        }

    for (int i = 0; i < width; i++)
        for (int j = 0; j < height; j++)
        {
            delete[] blurred[i][j];
            delete[] auxBlurred[i][j];
        }
    for (int i = 0; i < width; i++)
    {
        delete[] blurred[i];
        delete[] auxBlurred[i];
    }
    delete[] blurred;
    delete[] auxBlurred;


    return blurredImage;
}




Vector3f rgbToHsv(const Vector3f &c)
{
    Vector3f cHsv;
    float min, max, delta;
    int index = 0;
    min = qMin(qMin(c(0), c(1)), c(2));
    if (c(0) > c(1) && c(0) > c(2))
    {
        max = c(0);
        index = 0;
    }
    if (c(1) > c(0) && c(1) > c(2))
    {
        max = c(1);
        index = 1;
    }
    if (c(2) > c(0) && c(2) > c(1))
    {
        max = c(2);
        index = 2;
    }
    delta = max - min;
    cHsv(2) = max;

    if (max != 0)
    {
        cHsv(1) = delta/max;
    }
    else
    {
        cHsv(1) = 0;
        cHsv(0) = -1;
        return cHsv;
    }

    if (index == 0)
    {
        cHsv(0) = fmod((c(1) - c(2))/delta, 6);
    }
    else
    {
        if (index == 1)
        {
            cHsv(0) = 2 + (c(2) - c(0))/delta;
        }
        else
        {
            cHsv(0) = 4 + (c(0) - c(1))/delta;
        }
    }

    return cHsv;
}


Vector3f hsvToRgb(const Vector3f &c)
{
    Vector3f cRgb;
    float vs = c(2)*c(1);
    float m = c(2) - vs;
    float x = vs*(1 - fabs(fmod(c(0),2) - 1));

    Vector3f rgb1;
    int h = c(0);
    switch(h)
    {
        case 0:
            rgb1 = Vector3f(vs, x, 0);
            break;
        case 1:
            rgb1 = Vector3f(x, vs, 0);
            break;
        case 2:
            rgb1 = Vector3f(0, vs, x);
            break;
        case 3:
            rgb1 = Vector3f(0, x, vs);
            break;
        case 4:
            rgb1 = Vector3f(x, 0, vs);
            break;
        case 5:
            rgb1 = Vector3f(vs, 0, x);
            break;
    }

    cRgb = rgb1;
    cRgb(0) += m;
    cRgb(1) += m;
    cRgb(2) += m;

    return cRgb;
}


float vArea(float x)
{
    return 4*PI*pow(sin(x/2), 2);
}



QImage correctDistortion(const QImage & image, const Vector2f &k, const Vector2f& p,  Vector2f f)
{
    QImage corrected(image.width(), image.height(), image.format());
    Vector2f center = Vector2f(image.width()/2, image.height()/2);
    for (int j = 0; j < image.height(); j++)
        for (int i = 0; i < image.width(); i++)
        {
            double x, y, xCor, yCor;
            x = (i - center(0))/f(0);
            y = (j - center(1))/f(1);
            double r2 = x*x + y*y;
            double radialTerm = (1 + k(0)*r2 + k(1)*pow(r2, 2) + p(0)*pow(r2, 3) + p(1)*pow(r2, 4));

            xCor = x*radialTerm /*+ (2*p(0)*x*y + p(1)*(r2 + 2*x*x))*/;
            yCor = y*radialTerm /*+ (p(0)*(r2 + 2*y*y) + 2*p(1)*x*y)*/;

            xCor = xCor*f(0) + center(0);
            yCor = yCor*f(1) + center(1);

            if (xCor >= 0 && xCor < image.width() && yCor >= 0 && yCor < image.height())
                corrected.setPixel(i, j, image.pixel(xCor, yCor));
        }
    return corrected;
}


