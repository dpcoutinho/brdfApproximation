#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    meshView = new GLWidget(this);
    meshView->setFixedSize(QSize(1350, 900));
    meshView->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    connect(meshView, SIGNAL(correspondence(QPoint)), this, SLOT(drawCorrespondence(QPoint)));
    connect(meshView, SIGNAL(operation(uint)), this, SLOT(addOperation(uint)));
    photo = new CameraLabel;
    photo->setBackgroundRole(QPalette::Base);
    photo->setFixedSize(QSize(1350, 900));
    photo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    photo->setScaledContents(true);
    photo->hide();
    connect(photo, SIGNAL(message(const char*)), this, SLOT(message(const char*)));

    curveGraph = new CurveGLWidget(this);
    connect(meshView, SIGNAL(plot(const Vector2f*&,int,float*, int)), curveGraph, SLOT(plot(const Vector2f*&,int,float*, int)));
    connect(meshView, SIGNAL(plotLine(float)), curveGraph, SLOT(plotLine(float)));
    connect(meshView, SIGNAL(changedFov(float)), this, SLOT(changedFov(float)));
    connect(meshView, SIGNAL(message(const char*)), this, SLOT(message(const char*)));

    webcam = new WebcamThread;
    connect(webcam, SIGNAL(newFrame(QImage)), this, SLOT(updateFrame(QImage)));
    connect(photo, SIGNAL(zoomed(int, cv::Vec2f)), webcam, SLOT(zoomed(int, cv::Vec2f)));
    connect(this, SIGNAL(updated()), webcam, SLOT(release()));
    connect(webcam, SIGNAL(drawLightCenter(int, int)), this, SLOT(setLightCenter(int, int))); // problem here
    connect(webcam, SIGNAL(renderLightDirection(float,float,float)), this, SLOT(setLightDirection(float,float,float)));

    digitalCamera = new DigitalCamera;
    connect(digitalCamera, SIGNAL(drawLightCenter(int, int)), this, SLOT(setLightCenter(int, int))); // problem here ?
    connect(digitalCamera, SIGNAL(renderLightDirection(float,float,float)), this, SLOT(setLightDirection(float,float,float)));

    newProjectAction = new QAction(QIcon("Icons/newProject.png"), tr("&New Project"), this);
    connect(newProjectAction, SIGNAL(triggered()), this, SLOT(newProject()));
    loadMeshAction = new QAction(QIcon("Icons/addMesh.png"), tr("&Load Mesh"), this);
    connect(loadMeshAction, SIGNAL(triggered()), this, SLOT(loadMesh()));
    saveAction = new QAction(QIcon("Icons/save.png"), tr("&Save"), this);
    connect(saveAction, SIGNAL(triggered()), this, SLOT(save()));
    readFileAction = new QAction(QIcon("Icons/read.png"), tr("&Read File"), this);
    connect(readFileAction, SIGNAL(triggered()), this, SLOT(readFile()));
    undoAction = new QAction(QIcon("Icons/undo.png"), tr("&Undo"), this);
    connect(undoAction, SIGNAL(triggered()), this, SLOT(undoOperation()));
    takePhotoAction = new QAction(QIcon("Icons/camera.png"), tr("Take Photo"), this);
    connect(takePhotoAction, SIGNAL(triggered()), this, SLOT(takePhoto()));
    showAxisAction = new QAction(QIcon("Icons/showAxis.png"), tr("Show Trackball"), this);
    connect(showAxisAction, SIGNAL(triggered()), this, SLOT(showAxis()));
    showHemisphereAction = new QAction(QIcon("Icons/showHemisphere.png"), tr("Show Hemisphere Coverage"), this);
    connect(showHemisphereAction, SIGNAL(triggered()), this, SLOT(showHemisphere()));
    startAction = new QAction(QIcon("Icons/start.png"), tr("Start BRDF Approximation"), this);
    connect(startAction, SIGNAL(triggered()), this, SLOT(start()));
    exportMeshAction = new QAction(QIcon("Icons/exportMesh.png"), tr("Export Mesh"), this);
    connect(exportMeshAction, SIGNAL(triggered()), this, SLOT(exportMesh()));
    optionsAction = new QAction(QIcon("Icons/settings.png"), tr("Settings"), this);
    connect(optionsAction, SIGNAL(triggered()), this, SLOT(showSettings()));
    showCameraAction = new QAction(QIcon("Icons/show.png"), tr("Show Camera"), this);
    connect(showCameraAction, SIGNAL(triggered()), this, SLOT(showCamera()));
    fillNeighborsAction = new QAction(QIcon("Icons/neighbors.png"), tr("Fill Neighbors"), this);
    connect(fillNeighborsAction, SIGNAL(triggered()), this, SLOT(fillNeighbors()));
    testPhotosAction = new QAction(QIcon("Icons/testPhotos.png"), tr("Test Photos"), this);
    connect(testPhotosAction, SIGNAL(triggered()), this, SLOT(testPhotos()));
    uploadChartAction = new QAction(tr("Upload Chart"), this);
    connect(uploadChartAction, SIGNAL(triggered()), this, SLOT(uploadChart()));

    startCalibrationButton = new QPushButton(tr("&Calibrate"), this);
    connect(startCalibrationButton, SIGNAL(clicked()), this, SLOT(startCalibration()));
    suggestLightButton = new QPushButton(tr("Suggest &Light"), this);
    connect(suggestLightButton, SIGNAL(clicked()), this, SLOT(suggestLight()));
    fitCurvesButton = new QPushButton(tr("Fit Curves"), this);
    connect(fitCurvesButton, SIGNAL(clicked()), this, SLOT(fitCurves()));
    deletePhotoButton = new QPushButton(tr("Remove Photo"), this);
    connect(deletePhotoButton, SIGNAL(clicked()), this, SLOT(deletePhoto()));
    detectSphereButton = new QPushButton(tr("&Detect Sphere"), this);
    connect(detectSphereButton, SIGNAL(clicked()), this, SLOT(detectSphere()));
//    delimitAreaButton = new QPushButton(tr("Delimit area"), this);
//    connect(delimitAreaButton, SIGNAL(clicked()), this, SLOT(delimitArea()));
    finishSphereButton = new QPushButton(tr("Finish"), this);
    connect(finishSphereButton, SIGNAL(clicked()), this, SLOT(finishSphere()));
    detectChartButton = new QPushButton(tr("Detect Chart"), this);
    connect(detectChartButton, SIGNAL(clicked()), this, SLOT(detectChart()));
    clearChartButton = new QPushButton(tr("Clear Chart"), this);
    connect(clearChartButton, SIGNAL(clicked()), this, SLOT(clearChart()));
    acceptChartButton = new QPushButton(tr("Accept"), this);
    connect(acceptChartButton, SIGNAL(clicked()), this, SLOT(acceptChart()));
    finishChartButton = new QPushButton(tr("Finish"), this);
    connect(finishChartButton, SIGNAL(clicked()), this, SLOT(finishChart()));
    skipChartButton = new QPushButton(tr("Skip Chart"), this);
    connect(skipChartButton, SIGNAL(clicked()), this, SLOT(skipChart()));
    alignButton = new QPushButton(tr("Align"), this);
    connect(alignButton, SIGNAL(clicked()), this, SLOT(align()));
    setAlignButton = new QPushButton(tr("Set Camera"), this);
    connect(setAlignButton, SIGNAL(clicked()), this, SLOT(setAlign()));
    clearCorrespondencesButton = new QPushButton(tr("Clear Correspondences"));
    connect(clearCorrespondencesButton, SIGNAL(clicked()), this, SLOT(clearCorrespondences()));
    finishAlignButton = new QPushButton(tr("Finish"), this);
    connect(finishAlignButton, SIGNAL(clicked()), this, SLOT(finishAlign()));
    alignmentParametersButton = new QPushButton(tr("Parameters"), this);
    connect(alignmentParametersButton, SIGNAL(clicked()), this, SLOT(showParametersMessageBox()));
    updateAlignmentButton = new QPushButton(tr("Ok"), this);
    connect(updateAlignmentButton, SIGNAL(clicked()), this, SLOT(updateAlignmentConfiguration()));
    cancelAlignmentButton = new QPushButton(tr("Cancel"), this);
    connect(cancelAlignmentButton, SIGNAL(clicked()), this, SLOT(cancelAlignment()));
    acceptSettingsButton = new QPushButton(tr("Ok"), this);
    connect(acceptSettingsButton, SIGNAL(clicked()), this, SLOT(acceptSettings()));
    cancelSettingsButton = new QPushButton(tr("Cancel"), this);
    connect(cancelSettingsButton, SIGNAL(clicked()), this, SLOT(cancelSettings()));
    acceptUploadButton = new QPushButton(tr("Accept Chart"), this);
    connect(acceptUploadButton, SIGNAL(clicked()), this, SLOT(acceptUpload()));
    saveGraphButton = new QPushButton(tr("Save"), this);
    saveGraphButton->setMaximumWidth(40);
    connect(saveGraphButton, SIGNAL(clicked()), this, SLOT(saveGraph()));
    testPhotosButton = new QPushButton("Ok");
    connect(testPhotosButton, SIGNAL(clicked()), this, SLOT(acceptTestPhotos()));

    estimateFocalCheck = new QCheckBox;
    fineAlignmentCheck = new QCheckBox;
    rCheckBox = new QCheckBox("Red");
    connect(rCheckBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    rCheckBox->setChecked(true);
    gCheckBox = new QCheckBox("Green");
    connect(gCheckBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    bCheckBox = new QCheckBox("Blue");
    connect(bCheckBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    lumCheckBox = new QCheckBox("Luminance");
    connect(lumCheckBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    hueCheckBox = new QCheckBox("Hue");
    connect(hueCheckBox, SIGNAL(clicked()), this, SLOT(plotGraph()));
    saturationCheckBox = new QCheckBox("Saturation");
    connect(saturationCheckBox, SIGNAL(clicked()), this, SLOT(plotGraph()));
    valueCheckBox = new QCheckBox("Value");
    connect(valueCheckBox, SIGNAL(clicked()), this, SLOT(plotGraph()));

    pointBox = new QCheckBox("Points");
    pointBox->setChecked(true);
    connect(pointBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    curveBox = new QCheckBox("Curve");
    curveBox->setChecked(true);
    connect(curveBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    diffuseBox = new QCheckBox("Diffuse Angle");
    diffuseBox->setChecked(true);
    connect(diffuseBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));
    specularBox = new QCheckBox("Specular Angle");
    connect(specularBox, SIGNAL(clicked(bool)), this, SLOT(plotGraph()));

    iterationsSpinBox = new QSpinBox;
    iterationsSpinBox->setMaximum(1000);
    backgroundWeightSpinBox = new QSpinBox;
    toleranceSpinBox = new QDoubleSpinBox;
    varianceSpinBox = new QDoubleSpinBox;
    mutCorrSpinBox = new QDoubleSpinBox;
    mutCorrSpinBox->setMaximum(1.0);
    mutCorrSpinBox->setSingleStep(0.1);
    mutCorrSpinBox->setValue(0.9);
    connect(mutCorrSpinBox, SIGNAL(valueChanged(double)), this, SLOT(updateMutCorrSlider(double)));

    mutCorrSlider = new QSlider(Qt::Horizontal);
    mutCorrSlider->setMaximum(100);
    mutCorrSlider->setValue(90);
    connect(mutCorrSlider, SIGNAL(sliderMoved(int)), this, SLOT(updateMutCorrSpinBox(int)));

    estimateFocalLabel = new QLabel("Estimate focal Length:");
    fineAlignmentLabel = new QLabel("Fine Alignment:");
    numIterationsLabel = new QLabel("Number of iterations:");
    backgroundLabel = new QLabel("Background Weight:" );
    toleranceLabel = new QLabel("Tolerance:");
    varianceLabel = new QLabel("Expected Variance:");
    mutCorrLabel = new QLabel("Correspondences/MutualInfo Weight:");
    cameraNameLabel = new QLabel("Camera Model (only on Linux):");
    curveResolutionLabel = new QLabel("Curve Resolution");
    colorThresholdLabel = new QLabel("Threshold:");

    cameraName = new QTextEdit("Nikon DSC D80 (PTP mode)");
    cameraName->setFixedHeight(20);
    cameraName->setFixedWidth(250);

    colorThreshold = new QTextEdit("18");
    colorThreshold->setFixedHeight(20);
    colorThreshold->setFixedWidth(50);

    DSLRCameraButton = new QRadioButton("DSLR Camera");
    DSLRCameraButton->setChecked(true);
    webcamButton = new QRadioButton("Webcam");
    takePhotosRadioButton = new QRadioButton("Take Photo");
    uploadPhotosRadioButton = new QRadioButton("Upload Photos");
    uploadPhotosRadioButton->setChecked(true);

    QGridLayout* alignmentLayout = new QGridLayout;
    alignmentLayout->addWidget(estimateFocalLabel);
    alignmentLayout->addWidget(estimateFocalCheck, 0, 1);
    alignmentLayout->addWidget(fineAlignmentLabel, 1, 0);
    alignmentLayout->addWidget(fineAlignmentCheck, 1, 1);
    alignmentLayout->addWidget(numIterationsLabel, 2, 0);
    alignmentLayout->addWidget(iterationsSpinBox, 2, 1);
    alignmentLayout->addWidget(backgroundLabel, 3, 0);
    alignmentLayout->addWidget(backgroundWeightSpinBox, 3, 1);
    alignmentLayout->addWidget(toleranceLabel, 4, 0);
    alignmentLayout->addWidget(toleranceSpinBox, 4, 1);
    alignmentLayout->addWidget(varianceLabel, 5, 0);
    alignmentLayout->addWidget(varianceSpinBox, 5, 1);
    alignmentLayout->addWidget(mutCorrLabel, 6, 0);
    alignmentLayout->addWidget(mutCorrSlider, 6, 1);
    alignmentLayout->addWidget(mutCorrSpinBox, 7, 1);
    alignmentLayout->addWidget(updateAlignmentButton, 9, 0, Qt::AlignRight);
    alignmentLayout->addWidget(cancelAlignmentButton, 9, 1);

    QGridLayout* optionsLayout = new QGridLayout;
    optionsLayout->addWidget(DSLRCameraButton);
    optionsLayout->addWidget(webcamButton, 1, 0);
    optionsLayout->addWidget(cameraNameLabel, 2, 0);
    optionsLayout->addWidget(cameraName, 2, 1);
    optionsLayout->addWidget(colorThresholdLabel, 3, 0);
    optionsLayout->addWidget(colorThreshold, 3, 1);
    optionsLayout->addWidget(acceptSettingsButton, 5, 0, Qt::AlignRight);
    optionsLayout->addWidget(cancelSettingsButton, 5, 1);

    QGridLayout* curveLayout = new QGridLayout;
    curveLayout->addWidget(curveGraph);
    curveLayout->addWidget(curveResolutionLabel, 1, 0);
    curveLayout->addWidget(pointBox, 2, 0);
    curveLayout->addWidget(rCheckBox, 2, 1);
    curveLayout->addWidget(curveBox, 3, 0);
    curveLayout->addWidget(gCheckBox, 3, 1);
    curveLayout->addWidget(bCheckBox, 4, 1);
    curveLayout->addWidget(lumCheckBox, 5, 1);
    curveLayout->addWidget(diffuseBox, 6, 1);
    curveLayout->addWidget(specularBox, 7, 1);
    curveLayout->addWidget(hueCheckBox, 8, 1);
    curveLayout->addWidget(saturationCheckBox, 9, 1);
    curveLayout->addWidget(valueCheckBox, 10, 1);
    curveLayout->addWidget(saveGraphButton, 4, 0);

    QGridLayout* testPhotosLayout = new QGridLayout;
    testPhotosLayout->addWidget(takePhotosRadioButton);
    testPhotosLayout->addWidget(uploadPhotosRadioButton);
    testPhotosLayout->addWidget(testPhotosButton);


    optionsMessage = new QDialog;
    optionsMessage->setLayout(optionsLayout);

    alignmentOptionsMessage = new QDialog;
    alignmentOptionsMessage->setLayout(alignmentLayout);

    curveDialog = new QDialog;
    curveDialog->setMaximumHeight(1000);
    curveDialog->setLayout(curveLayout);

    testPhotosMessage = new QDialog;
    testPhotosMessage->setLayout(testPhotosLayout);

    fileMenu = menuBar()->addMenu("File");
    fileMenu->addAction(newProjectAction);
    fileMenu->addAction(loadMeshAction);
    fileMenu->addAction(saveAction);
    fileMenu->addAction(readFileAction);
    fileMenu->addAction(exportMeshAction);
    fileMenu->addAction(uploadChartAction);

    toolsMenu = menuBar()->addMenu("Tools");
    toolsMenu->addAction(optionsAction);

    fileToolBar = new QToolBar();
    fileToolBar->addAction(newProjectAction);
    fileToolBar->addAction(loadMeshAction);
    fileToolBar->addAction(saveAction);
    fileToolBar->addAction(readFileAction);
    fileToolBar->addAction(takePhotoAction);
    fileToolBar->addAction(showAxisAction);
    fileToolBar->addAction(undoAction);
    fileToolBar->addAction(showHemisphereAction);
    fileToolBar->addAction(startAction);
    fileToolBar->addAction(exportMeshAction);
    fileToolBar->addAction(showCameraAction);
    fileToolBar->addAction(fillNeighborsAction);
    fileToolBar->addAction(testPhotosAction);

    calibrationToolBar = new QToolBar("Calibration");
    startCalibrationAction = calibrationToolBar->addWidget(startCalibrationButton);
    suggestLightAction = calibrationToolBar->addWidget(suggestLightButton);
    fitCurvesAction = calibrationToolBar->addWidget(fitCurvesButton);
    deletePhotoAction = calibrationToolBar->addWidget(deletePhotoButton);
    detectSphereAction = calibrationToolBar->addWidget(detectSphereButton);
//    delimitAreaAction = calibrationToolBar->addWidget(delimitAreaButton);
    finishSphereAction = calibrationToolBar->addWidget(finishSphereButton);
    detectChartAction = calibrationToolBar->addWidget(detectChartButton);
    clearChartAction = calibrationToolBar->addWidget(clearChartButton);
    acceptChartAction = calibrationToolBar->addWidget(acceptChartButton);
    finishChartAction = calibrationToolBar->addWidget(finishChartButton);
    skipChartAction = calibrationToolBar->addWidget(skipChartButton);
    alignAction = calibrationToolBar->addWidget(alignButton);
    setAlignAction = calibrationToolBar->addWidget(setAlignButton);
    clearCorrespondencesAction = calibrationToolBar->addWidget(clearCorrespondencesButton);
    finishAlignAction = calibrationToolBar->addWidget(finishAlignButton);
    alignmentParametersAction = calibrationToolBar->addWidget(alignmentParametersButton);
    acceptUploadAction = calibrationToolBar->addWidget(acceptUploadButton);

    addToolBar(fileToolBar);
    addToolBar(Qt::RightToolBarArea, calibrationToolBar);
    fileToolBar->setMovable(false);
//    calibrationToolBar->setMovable(false);
    calibrationToolBar->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    calibrationToolBar->setMaximumSize(QSize(clearCorrespondencesButton->width(), calibrationToolBar->height()));
//    this->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
//    this->setMaximumSize(this->size());

    messageStatusBar = new QStatusBar;
    setStatusBar(messageStatusBar);

    detectSphereAction->setVisible(false);
//    delimitAreaAction->setVisible(false);
    finishSphereAction->setVisible(false);
    detectChartAction->setVisible(false);
    clearChartAction->setVisible(false);
    acceptChartAction->setVisible(false);
    finishChartAction->setVisible(false);
    skipChartAction->setVisible(false);
    alignAction->setVisible(false);
    setAlignAction->setVisible(false);
    clearCorrespondencesAction->setVisible(false);
    finishAlignAction->setVisible(false);
    alignmentParametersAction->setVisible(false);
    acceptUploadAction->setVisible(false);

    centralWidget = new QStackedWidget;

    centralWidget->addWidget(meshView);
    centralWidget->addWidget(photo);

    setCentralWidget(centralWidget);

    this->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    calibrating = false;
    cameraOpen = false;
    changedSettings = false;
    testing = false;

    photoDir = "photos/";

    indexPhoto = 0;
    chosenCamera = 0;
}

MainWindow::~MainWindow()
{
    delete webcam;
    delete digitalCamera;
}

void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        qApp->exit();
    if (e->key() == Qt::Key_H && e->modifiers() & Qt::ControlModifier)
    {
        meshView->restartCamera();
    }
    if (e->key() == Qt::Key_Z && e->modifiers() & Qt::ControlModifier)
        undoOperation();
    if (e->key() == Qt::Key_R)
    {
        meshView->reset();
        meshView->updateGL();
    }
    if (e->key() == Qt::Key_S)
        meshView->reloadShaders();
    if (e->key() == Qt::Key_D && meshView->hasPhoto())
        meshView->nextView();
    if (e->key() == Qt::Key_A && meshView->hasPhoto())
        meshView->previousView();
    if (e->key() == Qt::Key_U)
        meshView->setUseColorWeights();
    if (e->key() == Qt::Key_G)
        meshView->deleteTextures();
    if (e->key() == Qt::Key_C)
    {
        curveDialog->setFocusPolicy(Qt::NoFocus);
        curveDialog->show();
        meshView->getPlotData();
    }
    if (e->key() == Qt::Key_B)
        meshView->loadBestPhoto();
    if (e->key() == Qt::Key_T)
        meshView->acceptBestPhoto();
    if (e->key() == Qt::Key_P)
    {
        if (e->modifiers() & Qt::ControlModifier)
            meshView->printStatistics();
        else
            meshView->printDebugData();
    }
    if (e->key() == Qt::Key_E)
        meshView->printError();
    if (e->key() == Qt::Key_N)
        if (e->modifiers() & Qt::ControlModifier)
            meshView->calculateNewPhotos();
    if (e->key() == Qt::Key_M)
        saveChoices();
    if (e->key() == Qt::Key_X)
        meshView->setSave();
    if (e->key() == Qt::Key_Right)
        passTestPhoto();
    if (e->key() == Qt::Key_F)
        readBonnXML();
    if (e->key() == Qt::Key_I)
        meshView->renderAxes();
    if (e->key() == Qt::Key_Up)
        meshView->changeThresholdAngle(1);
    if (e->key() == Qt::Key_Down)
        meshView->changeThresholdAngle(-1);
}

void MainWindow::newProject()
{
    if (!QDir("photos").exists())
    {
        QDir().mkdir("photos");
    }
    meshView->clear();
    photo->clearAll();
    webcam->clear();
    digitalCamera->clear();
}

void MainWindow::loadMesh()
{
    meshFile = QFileDialog::getOpenFileName(this, tr("Choose Mesh"), QDir::currentPath(), tr("Ply(*.ply);;All(*.*)"));
    if (meshFile.length() == 0)
        return;
    meshView->loadMesh(meshFile.toStdString().c_str());
}

void MainWindow::save()
{
    QString saveFile = QFileDialog::getSaveFileName(this, tr("File Name"), QDir::currentPath(), tr("XML(*.xml);;All(*.*)"));
    if (saveFile.length() == 0)
        return;
    if (!saveFile.contains(".xml"))
        saveFile.append(".xml");
    QFile file(saveFile);
    file.open(QIODevice::WriteOnly);
    QXmlStreamWriter xml(&file);

    xml.setAutoFormatting(true);
    xml.writeDTD("<!DOCTYPE CalibrationDocument>");
    xml.writeStartElement("BRDFProject");
    xml.writeStartElement("Mesh");
    xml.writeAttribute("file", meshFile);
    xml.writeEndElement(); // mesh
    for (unsigned int i = 0; i < cameras.size(); i++)
    {
        xml.writeStartElement("View");
        xml.writeStartElement("Calibration");
        xml.writeStartElement("SpherePosition");
//        Vector3f sphereCenter = photo->getCenter();
        cout << "centers size: " << centers.size() << endl;
        cout << "color charts: " << colorCharts.size() << endl;
        Vector3f sphereCenter = Vector3f(0, 0, 0) /*centers[i]*/;
        vector<Vector3f> colorChart =  vector<Vector3f>()/*colorCharts[i]*/;
        QString sphereS = QString::number(sphereCenter[0]) + " " + QString::number(sphereCenter[1]);
        QString radiusS = QString::number(sphereCenter[2]);
        QString colorChartS;
        for (unsigned int i = 0; i < colorChart.size(); i++)
            colorChartS += QString::number(colorChart[i][0]) + "," + QString::number(colorChart[i][1]) + "," + QString::number(colorChart[i][2]) + "|";
        xml.writeAttribute("center", sphereS);
        xml.writeAttribute("radius", radiusS);
        xml.writeEndElement(); // sphere position

        xml.writeStartElement("ColorChart");
        xml.writeAttribute("colors", colorChartS);
        xml.writeEndElement(); // color chart

        PhotoCamera camera = cameras[i];
        xml.writeStartElement("Camera");
        Matrix4f r = camera.getRotation();
        QString rotationS;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
            {
                rotationS += QString::number(r(i, j));
                if (i != 3 || j != 3)
                    rotationS += " ";
            }

        xml.writeAttribute("RotationMatrix", rotationS);

        Vector4f t = camera.getTranslation().col(3);
        QString translationS;
        for (int i = 0; i < 4; i++)
        {
           translationS += QString::number(t(i));
           if (i != 3)
               translationS += " ";
        }
        xml.writeAttribute("TranslationVector", translationS);

        Vector2f radialDistortion = camera.getRadialDistortion();
        QString distortionS;
        distortionS += QString::number(radialDistortion(0)) + " " + QString::number(radialDistortion(1));
        xml.writeAttribute("LensDistortion", distortionS);

        Vector2f pixelSize = camera.getPixelSize();
        QString pixelSizeS;
        pixelSizeS += QString::number(pixelSize(0)) + " " + QString::number(pixelSize(1));
        xml.writeAttribute("PixelSizeMm", pixelSizeS);

        Vector2i viewport;
        viewport << camera.getWidth(), camera.getHeight();
        QString viewportS;
        viewportS += QString::number(viewport(0)) + " " + QString::number(viewport(1));
        xml.writeAttribute("ViewportPx", viewportS);

        Vector2i center = camera.getPrincipalPoint();
        QString centerS;
        centerS += QString::number(center(0)) + " " + QString::number(center(1));
        xml.writeAttribute("CenterPx", centerS);

        QString focalS = QString::number(camera.getFocalLength());
        xml.writeAttribute("FocalMm", focalS);
        xml.writeEndElement(); // camera

        xml.writeEndElement(); // calibration part

        xml.writeStartElement("Photos");
        for (unsigned int j = 0; j < photos[i].size(); j++)
        {
            xml.writeStartElement("Photo");
            xml.writeAttribute("file", photos[i][j]);
            Vector3f currLD = lightDirections[i][j];
            QString ldS = QString::number(currLD(0)) + " " + QString::number(currLD(1)) + " " + QString::number(currLD(2));
            xml.writeAttribute("lightDirection", ldS);
            xml.writeEndElement();
        }
        xml.writeEndElement(); //Photos

        xml.writeEndElement(); //View
    }

    xml.writeEndElement();

    file.close();
}

void MainWindow::readFile()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Choose XML"), QDir::currentPath(), tr("XML(*.xml);;ALL(*.*"));
    if (filename.length() == 0)
        return;
    QDomElement root;
    QDomDocument doc;
    QDomNode node;
    QDomNode viewNode;
    QFile file(filename);

    file.open(QIODevice::ReadOnly);
    doc.setContent(&file);
    root = doc.documentElement();
    node = root.firstChild();

    meshFile = node.attributes().namedItem("file").nodeValue();
    viewNode = root.firstChild().nextSibling();
    int cameraIndex = 0;

    meshView->loadMesh(meshFile.toStdString().c_str());

    while(!viewNode.isNull())
    {
        node = viewNode.firstChild().firstChild();
        vector<QString> newCameraPosition;
        photos.push_back(newCameraPosition);
        indexPhoto = 0;
        while(!node.isNull())
        {
            if (node.nodeName().compare("SpherePosition") == 0)
            {
                Vector3f sphereCenter;
                QString attr = node.attributes().namedItem("center").nodeValue();
                QStringList list = attr.split(" ");
                sphereCenter[0] = list[0].toFloat();
                sphereCenter[1] = list[1].toFloat();
                attr = node.attributes().namedItem("radius").nodeValue();
                sphereCenter[2] = attr.toFloat();
                photo->setCenter(sphereCenter);
                centers.push_back(sphereCenter);
                if (chosenCamera == 0)
                    digitalCamera->setCenter(sphereCenter);

                node = node.nextSibling();
            }
            if (node.nodeName().compare("ColorChart") == 0)
            {
                QString colors = node.attributes().namedItem("colors").nodeValue();
                QStringList colorsList = colors.split("|");
                vector<Vector3f> colorChart;
                cout << "size: "<<  colorsList.size() << endl;
                if (colorsList.size() > 1)
                {
                    for (int i = 0; i < colorsList.size(); i++)
                    {
                        if (colorsList[i].length() == 0)
                            break;
                        QStringList currentColor = colorsList[i].split(",");

                        Vector3f averageColor;
                        averageColor << currentColor[0].toFloat(), currentColor[1].toFloat(), currentColor[2].toFloat();
                        colorChart.push_back(averageColor);
                    }
                    photo->setColorChart(colorChart);
//                    photo->acceptChart();
                    meshView->setColorWeights(photo->getColorWeights());
                }
                colorCharts.push_back(colorChart);
                node = node.nextSibling();
            }
            if (node.nodeName().compare("Camera") == 0)
            {
                PhotoCamera newCamera;
                Matrix4f rotation;
                Vector4f translation;
                Vector2d viewport;
                Vector2i center;
                float distortionX, distortionY;
                double dx, dy;
                double focal;

                QString attrString = node.attributes().namedItem("TranslationVector").nodeValue();
                QStringList attrList = attrString.split(" ");
                for (int i = 0; i < attrList.size(); i++)
                    translation(i) = attrList[i].toFloat();

                attrString = node.attributes().namedItem("LensDistortion").nodeValue();
                attrList = attrString.split(" ");
                distortionX = attrList[0].toDouble();
                distortionY = attrList[1].toDouble();

                attrString = node.attributes().namedItem("ViewportPx").nodeValue();
                attrList = attrString.split(" ");
                viewport(0) = attrList[0].toDouble();
                viewport(1) = attrList[1].toDouble();

                attrString = node.attributes().namedItem("PixelSizeMm").nodeValue();
                attrList = attrString.split(" ");
                dx = attrList[0].toDouble();
                dy = attrList[1].toDouble();

                attrString = node.attributes().namedItem("CenterPx").nodeValue();
                attrList = attrString.split(" ");
                center(0) = attrList[0].toInt();
                center(1) = attrList[1].toInt();

                attrString = node.attributes().namedItem("FocalMm").nodeValue();
                focal = attrString.toDouble();

                attrString = node.attributes().namedItem("RotationMatrix").nodeValue();
                attrList = attrString.split(" ");
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 4; j++)
                    {
                        rotation(i, j) = attrList[i*4 + j].toFloat();
                    }

                Matrix4f t = Matrix4f::Identity();
                t.col(3) = translation;

                newCamera.setPixelSize(dx, dy);
                newCamera.setPrincipalPoint(center);
                newCamera.setFocalLength(focal);
                newCamera.setRotation(rotation);
                newCamera.setTranslation(t);
                newCamera.setRadialDistortion(distortionX, distortionY);
                newCamera.setWidthHeight(viewport(0), viewport(1));

                cameras.push_back(newCamera);

                node = node.nextSibling();
            }
        }

        node = viewNode.firstChild().nextSibling();
        vector<QString> photosFile;
        vector<Vector3f> newLightDirections;
        QDomNode photoNode = node.firstChild();

        QString dirAttr("");
        if (photoNode.nodeName().compare("Photodir") == 0)
        {
            dirAttr = photoNode.attributes().namedItem("dir").nodeValue();
            photoNode = photoNode.nextSibling();
        }

        while(!photoNode.isNull())
        {
            QString fileAttr(dirAttr);
            fileAttr.append(photoNode.attributes().namedItem("file").nodeValue());
            photosFile.push_back(fileAttr);
            Vector3f lightDirection;
            QString lightAttr = photoNode.attributes().namedItem("lightDirection").nodeValue();
            QStringList list = lightAttr.split(" ");
            lightDirection << list[0].toFloat(), list[1].toFloat(), list[2].toFloat();
            newLightDirections.push_back(lightDirection);
            photoNode = photoNode.nextSibling();
            int index = photos.size() - 1;
            photos[index].push_back(fileAttr);
            indexPhoto++;
        }


        for (unsigned int i = 0; i < photosFile.size(); i++)
        {
            meshView->createCamera(cameras[cameraIndex], photosFile[i].toStdString().c_str(), newLightDirections[i]);
        }
        lightDirections.push_back(newLightDirections);
        viewNode = viewNode.nextSibling();
        cameraIndex++;
    }
    meshView->nextView();

    if (chosenCamera == 0)
        if (cameraName->toPlainText().size() > 0)
            digitalCamera->start(cameraName->toPlainText().toStdString().c_str());
    file.close();
}

void MainWindow::addOperation(unsigned int newOperation)
{
    operations.push(newOperation);
}

void MainWindow::undoOperation()
{
    if (operations.size() == 0)
        return;
    unsigned int lastOperation = operations.top();
    operations.pop();
    switch(lastOperation)
    {
        case 1:
            meshView->undoRotation();
            break;
        case 2:
            meshView->undoTranslation();
            break;
        case 3:
            meshView->undoScale();
            break;
        case 4:
            meshView->removeCorrespondence();
            break;
        case 5:
            QImage updatedImage = photo->removeCorrespondence();
            meshView->setAlign(true, updatedImage);
            meshView->updateGL();
            break;
    }
}

void MainWindow::takePhoto()
{
    if (chosenCamera == 1)
        webcam->pause();
    int index = photos.size() - 1;
    cout << "index: " << index << endl;

    if (index < 0)
    {
        statusBar()->showMessage("Cannot take photo before calibration");
        return;
    }

    if (!webcam->isRunning() && chosenCamera == 1)
    {
        statusBar()->showMessage("Webcam not initialized");
        return;
    }

    QImage newImage;
    Vector3f lightDirection;
    QString photoName;
    string completeFilename;
    cout << "chosen camera: " << chosenCamera << endl;
    cout << "calibrating: " << calibrating << endl;
    if (chosenCamera == 0)
    {
        if (!calibrating)
        {
            photoName = QString("photo_000" + QString::number(index) + "_" + QString::number(indexPhoto)+".JPG");
            cout << "photoname: " << photoName.toStdString() << endl;
            newImage = digitalCamera->getPhoto(photoName.toStdString().c_str());
            lightDirection = digitalCamera->getLightDirection();
            completeFilename = photoDir + photoName.toStdString();
            cout << "with light direction: " << lightDirection.transpose() << endl;
        }
        else
        {
            newImage = digitalCamera->getPhoto();
        }
    }
    else
    {
        cv::Mat currentFrame = webcam->getFrame();
        newImage = matToQImage(currentFrame);
        if (!calibrating)
        {
            photoName = QString("photo_000" + QString::number(index) + "_" + QString::number(indexPhoto)+".png");
            completeFilename = photoDir + photoName.toStdString();
            cv::Vec3f lightDirectionCV = webcam->getLightDirection();
            lightDirection = Vector3f(lightDirectionCV[0], lightDirectionCV[1], lightDirectionCV[2]);
            newImage.save(QString::fromStdString(completeFilename));
        }
    }
    if (calibrating)
    {
        currentPhoto = newImage/*.scaled(meshView->width(), meshView->height())*/;
        photo->setOriginal(currentPhoto);
        photo->setPixmap(QPixmap::fromImage(currentPhoto));

        if (alignAction->isVisible())
            takePhotoAlign();
    }
    else
    {
        if (testing)
        {
            meshView->testPhoto(completeFilename.c_str(), lightDirection);
        }
        else
        {
            lightDirections[index].push_back(lightDirection);
            indexPhoto++;
            photos[index].push_back(QString::fromStdString(completeFilename));

            meshView->addPhoto(completeFilename.c_str(), lightDirection);
        }
    }
    if (chosenCamera == 1)
        webcam->resume();

}

void MainWindow::showAxis()
{
    meshView->drawTrackball();
}

void MainWindow::showHemisphere()
{
    meshView->drawHemisphereCoverage();
}

void MainWindow::start()
{
    meshView->start();
}

void MainWindow::exportMesh()
{
    QString exportFile = QFileDialog::getSaveFileName(this, tr("File Name"), QDir::currentPath(), tr("Ply(*.ply);;All(*.*)"));
     if (exportFile.length() == 0)
         return;
    meshView->exportMesh(exportFile.toStdString().c_str());
}

void MainWindow::startCalibration()
{
    if (!cameraOpen)
    {
        if (chosenCamera == 0)
        {
            if (cameraName->toPlainText().size() > 0)
                digitalCamera->start(cameraName->toPlainText().toStdString().c_str());
            else
            {
                statusBar()->showMessage("No camera model");
                return;
            }
            currentPhoto = digitalCamera->getPhoto()/*.scaled(meshView->width(), meshView->height())*/;
            photo->setPixmap(QPixmap::fromImage(currentPhoto));
            photo->adjustSize();
        }
        else
            webcam->start(QThread::LowPriority);
        cameraOpen = true;
    }
    else
        if (chosenCamera == 1)
            webcam->resume();
    centralWidget->setCurrentIndex(1);
    startCalibrationAction->setVisible(false);
    showCameraAction->setVisible(false);
    suggestLightAction->setVisible(false);
    fitCurvesAction->setVisible(false);
    deletePhotoAction->setVisible(false);
    detectSphereAction->setVisible(true);
//    delimitAreaAction->setVisible(true);
    finishSphereAction->setVisible(true);

    indexPhoto = 0;
    calibrating = true;
    vector<QString> newCameraPosition;
    photos.push_back(newCameraPosition);
    vector<Vector3f> newLightDirection;
    lightDirections.push_back(newLightDirection);
    photo->setDetectingSphere(true);
}

void MainWindow::showCamera()
{
    if (chosenCamera == 0)
    {

    }
    else
    {
        if (!cameraOpen || webcam->isPaused() || webcam->isHidden())
        {
            if (cameraOpen)
                webcam->resume();
            else
            {
                webcam->start(QThread::LowPriority);
                cameraOpen = true;
            }
            centralWidget->setCurrentIndex(1);
        }
        else
        {
            webcam->pause();
            centralWidget->setCurrentIndex(0);
        }
    }

}

void MainWindow::suggestLight()
{
    meshView->calculateNewLight();
}

void MainWindow::detectSphere()
{
    cv::Mat newMat;
    QImage newPhoto;
    if (chosenCamera == 0)
        newMat = qImageToMat(currentPhoto);
    else
    {
        cv::Mat currentFrame = webcam->getFrame();
        newMat = currentFrame;
//        cv::resize(currentFrame, newMat, cv::Size(meshView->width(), meshView->height()));
    }
    vector<Vector3f> centers = detectSphereHough(newMat);
    if (centers.size() > 0)
    {
        if (chosenCamera == 1)
        {
            webcam->pause();
            newPhoto = matToQImage(newMat);
        }
        else
            newPhoto = currentPhoto;

        photo->setOriginal(newPhoto);
        photo->setPixmap(QPixmap::fromImage(newPhoto));
        photo->drawCenters(centers);
    }
    else
        statusBar()->showMessage("No spheres found");
    centers.clear();
}

void MainWindow::delimitArea()
{
    photo->delimitArea(true);
}

void MainWindow::finishSphere()
{
    detectSphereAction->setVisible(false);
//    delimitAreaAction->setVisible(false);
    finishSphereAction->setVisible(false);

    detectChartAction->setVisible(true);
    clearChartAction->setVisible(true);
    acceptChartAction->setVisible(true);
    finishChartAction->setVisible(true);
    skipChartAction->setVisible(true);

    Vector3f center = photo->getCenter();
    if (chosenCamera == 0)
    {
//        Vector3f centerR;
//        centerR(0) = (center(0)*digitalCamera->width())/photo->width();
//        centerR(1) = (center(1)*digitalCamera->height())/photo->height();
//        centerR(2) = (center(2)*digitalCamera->width())/photo->width();
        digitalCamera->setCenter(center);
    }
    else
    {
        cv::Vec3f centerCV;
//        centerCV[0] = (center(0)*webcam->getFrame().cols)/photo->width();
//        centerCV[1] = (center(1)*webcam->getFrame().rows)/photo->height();
//        centerCV[2] = (center(2)*webcam->getFrame().cols)/photo->width();
        centerCV[0] = center(0);
        centerCV[1] = center(1);
        centerCV[2] = center(2);
        webcam->setCenter(centerCV);
        webcam->resume();
    }
    photo->reset();
    photo->setDetectingChart(true);
}

void MainWindow::detectChart()
{
    QImage image;
    if (chosenCamera == 0)
        image = currentPhoto;
    else
    {
        cv::Mat currentFrame = webcam->getFrame();
//        cv::Mat resizedFrame;
//        cv::resize(currentFrame, resizedFrame, cv::Size(meshView->width(), meshView->height()));
        image = matToQImage(currentFrame);
    }
    photo->setOriginal(image);
    currentPhoto = image;
    photo->setPixmap(QPixmap::fromImage(image));

    int threshold = colorThreshold->toPlainText().toInt();
    photo->detectChart(threshold);

    if (webcam->isRunning() && chosenCamera == 1)
    {
        webcam->pause();
    }
}

void MainWindow::clearChart()
{
    photo->clearChart();
}

void MainWindow::acceptChart()
{
    photo->acceptChart();
}

void MainWindow::finishChart(bool accepted)
{
    detectChartAction->setVisible(false);
    clearChartAction->setVisible(false);
    acceptChartAction->setVisible(false);
    finishChartAction->setVisible(false);
    skipChartAction->setVisible(false);

    alignAction->setVisible(true);
    setAlignAction->setVisible(true);
    clearCorrespondencesAction->setVisible(true);
    finishAlignAction->setVisible(true);
    alignmentParametersAction->setVisible(true);

    photo->setDetectingChart(false);

    photo->reset();
    if (chosenCamera == 1)
    {
        webcam->resume();
        takePhoto();
    }
    environment = readOptimizationXML("Mutualinfo/filter_mutualinfo.xml");

    // update interface variables
    estimateFocalCheck->setChecked(environment.estimateFocal);
    fineAlignmentCheck->setChecked(environment.fineAlignment);
    backgroundWeightSpinBox->setValue(environment.backgroundWeight);
    iterationsSpinBox->setValue(environment.numIterations);
    varianceSpinBox->setValue(environment.expectedVariance);
    toleranceSpinBox->setValue(environment.tolerance);
    mutCorrSpinBox->setValue(environment.mIweight);

    photo->setOriginal(currentPhoto);
    photo->setPixmap(QPixmap::fromImage(currentPhoto));
    if (accepted)
        meshView->setColorWeights(photo->getColorWeights());
    meshView->setAlign(true, currentPhoto);
    centralWidget->setCurrentIndex(0);
}

void MainWindow::skipChart()
{
    finishChart(false);
}

void MainWindow::align()
{
    meshView->startAlign(photo->getOriginal(), environment);
}

void MainWindow::takePhotoAlign()
{
    if (chosenCamera == 1)
    {
        cv::Mat currentFrame = webcam->getFrame();
//        cv::Mat resizedFrame;
//        cv::resize(currentFrame, resizedFrame, cv::Size(meshView->width(), meshView->height()));
        currentPhoto = matToQImage(currentFrame);
    }

    if (webcam->isRunning() && chosenCamera == 1)
        webcam->pause();
    meshView->setAlign(true, currentPhoto);
    centralWidget->setCurrentIndex(0);
}

void MainWindow::setAlign()
{
    meshView->setCamera();
}

void MainWindow::clearCorrespondences()
{
    meshView->clearCorrespondences();
    photo->clearCorrespondences();
    meshView->setAlign(true, photo->getOriginal());
    statusBar()->showMessage("");
}

void MainWindow::finishAlign()
{
    alignAction->setVisible(false);
    setAlignAction->setVisible(false);
    clearCorrespondencesAction->setVisible(false);
    finishAlignAction->setVisible(false);
    alignmentParametersAction->setVisible(false);
    startCalibrationAction->setVisible(true);
    showCameraAction->setVisible(true);
    suggestLightAction->setVisible(true);
    fitCurvesAction->setVisible(true);
    deletePhotoAction->setVisible(true);

    meshView->clearCorrespondences();
    photo->clearCorrespondences();
    photo->reset();
    if (chosenCamera == 1)
        webcam->hide();

    meshView->setAlign(false);
    centralWidget->setCurrentIndex(0);
    meshView->updateGL();

    calibrating = false;
    cameras.push_back(meshView->getCurrentCamera());
    centers.push_back(photo->getCenter());
    colorCharts.push_back(photo->getColorChart());
}

void MainWindow::updateFrame(const QImage &frame)
{
    photo->setPixmap(QPixmap::fromImage(frame)/*.scaled(meshView->width(), meshView->height())*/);
    photo->adjustSize();
    show();
    emit(updated());
}

void MainWindow::drawCorrespondence(QPoint p)
{
    QImage updatedImage = photo->addCorrespondence(p);
    meshView->setAlign(true, updatedImage);
    operations.push(5);
}

void MainWindow::showParametersMessageBox()
{
    alignmentOptionsMessage->show();
}

void MainWindow::updateMutCorrSlider(double position)
{
    mutCorrSlider->setValue(position*100);
}

void MainWindow::updateAlignmentConfiguration()
{
    alignmentOptionsMessage->accept();

    // Update OptEnvironment variable
    environment.estimateFocal = estimateFocalCheck->isChecked();
    environment.fineAlignment = fineAlignmentCheck->isChecked();
    environment.backgroundWeight = backgroundWeightSpinBox->value();
    environment.numIterations = iterationsSpinBox->value();
    environment.expectedVariance = varianceSpinBox->value();
    environment.tolerance = toleranceSpinBox->value();
    environment.mIweight = mutCorrSpinBox->value();
}

void MainWindow::cancelAlignment()
{
    alignmentOptionsMessage->reject();
}

void MainWindow::acceptSettings()
{
    optionsMessage->accept();
    chosenCamera = DSLRCameraButton->isChecked()?0:1;
    int threshold = colorThreshold->toPlainText().toInt();
    photo->setColorThreshold(threshold);
}

void MainWindow::cancelSettings()
{
    optionsMessage->reject();
}

void MainWindow::setLightCenter(int x, int y)
{
    Vector3i newLight;
//    if (chosenCamera == 0)
//    {
//        newLight[0] = ((float)x*photo->width())/digitalCamera->width();
//        newLight[1] = ((float)y*photo->height())/digitalCamera->height();
//        newLight[2] = 2;
//    }
//    else
//    {
//        newLight[0] = ((float)x*photo->width())/webcam->getFrame().cols;
//        newLight[1] = ((float)y*photo->height())/webcam->getFrame().rows;
//        newLight[2] = 2;
//    }
    newLight[0] = x;
    newLight[1] = y;
    newLight[2] = 2;
    photo->setLightCenter(newLight);
}

void MainWindow::setLightDirection(float x, float y, float z)
{
    Vector3f newLight = Vector3f(x, y, z);
    meshView->setWebcamLightDirection(newLight);
}

void MainWindow::showSettings()
{
    cameraName->setText("");
    optionsMessage->show();
}

void MainWindow::plotGraph()
{
    curveGraph->plot(rCheckBox->isChecked(), gCheckBox->isChecked(), bCheckBox->isChecked(), lumCheckBox->isChecked(),
                     pointBox->isChecked(), curveBox->isChecked(), diffuseBox->isChecked(), specularBox->isChecked(),
                     hueCheckBox->isChecked(), saturationCheckBox->isChecked(), valueCheckBox->isChecked());
}

void MainWindow::saveGraph()
{
    curveGraph->save();
}

void MainWindow::uploadChart()
{
    QString imageFile = QFileDialog::getOpenFileName(this, tr("Choose Photo"), QDir::currentPath(), tr("JPG(*.jpg);;PNG(*.png);;All(*.*)"));

    if (imageFile.isEmpty())
    {
        statusBar()->showMessage("Empty file");
        return;
    }
    QImage image;
    image.load(imageFile);
    image = image.scaled(meshView->width(), meshView->height());

    centralWidget->setCurrentIndex(1);

    photo->setOriginal(image);
    currentPhoto = image;
    photo->setPixmap(QPixmap::fromImage(image));

    clearChartAction->setVisible(true);

    int threshold = colorThreshold->toPlainText().toInt();
    photo->detectChart(threshold);
    acceptUploadAction->setVisible(true);

    if (webcam->isRunning() && chosenCamera == 1)
    {
        webcam->pause();
    }
}

void MainWindow::acceptUpload()
{
    photo->acceptChart();
    meshView->setColorWeights(photo->getColorWeights());

    clearChartAction->setVisible(false);
    acceptUploadAction->setVisible(false);

    centralWidget->setCurrentIndex(0);
}

void MainWindow::changedFov(float newFov)
{
    QString fovString;
    fovString.setNum(newFov);
    statusBar()->showMessage(QString("FOV: ") + fovString);
}

void MainWindow::message(const char* message)
{
    QString text(message);
    statusBar()->showMessage(text);
}

void MainWindow::fillNeighbors()
{
    meshView->useNeighbors();
}

void MainWindow::testPhotos()
{
//    meshView->testPhoto(testingCameras[0], (testingPhotos[0][0]).toStdString().c_str(), testingLightDirections[0][0]);
    testPhotosMessage->show();
}

void MainWindow::acceptTestPhotos()
{
    testPhotosMessage->accept();
    if (takePhotosRadioButton->isChecked())
        testing = true;
    else
    {
        testing = false;

        // Open directory and choose photos

        QString filename = QFileDialog::getOpenFileName(this, tr("Choose XML"), QDir::currentPath(), tr("XML(*.xml);;ALL(*.*"));
        if (filename.length() == 0)
            return;
        QDomElement root;
        QDomDocument doc;
        QDomNode node;
        QDomNode viewNode;
        QFile file(filename);

        file.open(QIODevice::ReadOnly);
        doc.setContent(&file);
        root = doc.documentElement();
        node = root.firstChild();

        viewNode = node;

        testingIndex = Vector2i(0, 0);
        cout << "view node: " << viewNode.nodeName().toStdString() << endl;

        while(!viewNode.isNull())
        {
            node = viewNode.firstChild().firstChild();
            while(!node.isNull())
            {
                cout << "node: " << node.nodeName().toStdString() << endl;
                if (node.nodeName().compare("Camera") == 0)
                {
                    PhotoCamera newCamera;
                    Matrix4f rotation;
                    Vector4f translation;
                    Vector2d viewport;
                    Vector2i center;
                    float distortionX, distortionY;
                    double dx, dy;
                    double focal;

                    QString attrString = node.attributes().namedItem("TranslationVector").nodeValue();
                    QStringList attrList = attrString.split(" ");
                    for (int i = 0; i < attrList.size(); i++)
                        translation(i) = attrList[i].toFloat();

                    attrString = node.attributes().namedItem("LensDistortion").nodeValue();
                    attrList = attrString.split(" ");
                    distortionX = attrList[0].toDouble();
                    distortionY = attrList[1].toDouble();

                    attrString = node.attributes().namedItem("ViewportPx").nodeValue();
                    attrList = attrString.split(" ");
                    viewport(0) = attrList[0].toDouble();
                    viewport(1) = attrList[1].toDouble();

                    attrString = node.attributes().namedItem("PixelSizeMm").nodeValue();
                    attrList = attrString.split(" ");
                    dx = attrList[0].toDouble();
                    dy = attrList[1].toDouble();

                    attrString = node.attributes().namedItem("CenterPx").nodeValue();
                    attrList = attrString.split(" ");
                    center(0) = attrList[0].toInt();
                    center(1) = attrList[1].toInt();

                    attrString = node.attributes().namedItem("FocalMm").nodeValue();
                    focal = attrString.toDouble();

                    attrString = node.attributes().namedItem("RotationMatrix").nodeValue();
                    attrList = attrString.split(" ");
                    for (int i = 0; i < 4; i++)
                        for (int j = 0; j < 4; j++)
                        {
                            rotation(i, j) = attrList[i*4 + j].toFloat();
                        }

                    Matrix4f t = Matrix4f::Identity();
                    t.col(3) = translation;

                    newCamera.setPixelSize(dx, dy);
                    newCamera.setPrincipalPoint(center);
                    newCamera.setFocalLength(focal);
                    newCamera.setRotation(rotation);
                    newCamera.setTranslation(t);
                    newCamera.setRadialDistortion(distortionX, distortionY);
                    newCamera.setWidthHeight(viewport(0), viewport(1));

                    testingCameras.push_back(newCamera);

                }
                node = node.nextSibling();
            }

            node = viewNode.firstChild().nextSibling();
            vector<QString> photosFile;
            vector<QString> newPhotos;
            vector<Vector3f> newLightDirections;
            QDomNode photoNode = node.firstChild();

            QString dirAttr("");
            if (photoNode.nodeName().compare("Photodir") == 0)
            {
                dirAttr = photoNode.attributes().namedItem("dir").nodeValue();
                photoNode = photoNode.nextSibling();
            }

            while(!photoNode.isNull())
            {
                QString fileAttr(dirAttr);
                fileAttr.append(photoNode.attributes().namedItem("file").nodeValue());
                photosFile.push_back(fileAttr);
                Vector3f lightDirection;
                QString lightAttr = photoNode.attributes().namedItem("lightDirection").nodeValue();
                QStringList list = lightAttr.split(" ");
                lightDirection << list[0].toFloat(), list[1].toFloat(), list[2].toFloat();
                newLightDirections.push_back(lightDirection);
                photoNode = photoNode.nextSibling();
                newPhotos.push_back(fileAttr);
            }
            cout << "pushing" << endl;
            if (newPhotos.size() > 0)
            {
                testingPhotos.push_back(newPhotos);
                testingLightDirections.push_back(newLightDirections);
            }

            viewNode = viewNode.nextSibling();
            cout << "next view node" << endl;
        }
        cout << (testingPhotos[0][0]).toStdString().c_str() << endl;
        cout << testingLightDirections[0][0] << endl;
        meshView->setRenderingBonn(false);
        meshView->testPhoto(testingCameras[0], (testingPhotos[0][0]).toStdString().c_str(), testingLightDirections[0][0]);
    }
}

void MainWindow::readBonnXML()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Choose XML"), QDir::currentPath(), tr("XML(*.xml);;ALL(*.*"));
    if (filename.length() == 0)
        return;
    QDomElement root;
    QDomDocument doc;
    QDomNode node;
    QDomNode viewNode;
    QFile file(filename);

    file.open(QIODevice::ReadOnly);
    doc.setContent(&file);
    root = doc.documentElement();
    node = root.firstChild();

    viewNode = root.firstChild().nextSibling();
    QDomNode cameraNode = viewNode.firstChild();

    QStringList fileList = filename.split("/");
    QString dir = "/";
    for (int i = 1; i < fileList.size() - 1; i++)
        dir += fileList[i] + "/";

    meshFile = dir + node.firstChild().attributes().namedItem("value").nodeValue();
    meshFile += ".ply";
    meshView->loadMesh(meshFile.toStdString().c_str());

    vector<Vector3f> cameraAttr;
    while (!cameraNode.isNull())
    {
        QDomNode calibrationNode = cameraNode.firstChild();
        while (calibrationNode.nodeName().compare("calib3d"))
            calibrationNode = calibrationNode.nextSibling();

        calibrationNode = calibrationNode.firstChild();
        for (int i = 0; i < 5; i++)
            calibrationNode = calibrationNode.nextSibling();

        QString calibration = calibrationNode.firstChild().nodeValue();
        QStringList calibList = calibration.split("\n");

        Matrix3d K;
        for (int i = 0; i < 3; i++)
        {
            QStringList rowList = calibList[i+3].split(" ");
            int ii = 0;
            if (i == 0)
                ii = 1;
            for (int j = ii; j < rowList.size(); j++)
                K(i, j-ii) = rowList[j].toDouble();
        }
        QStringList dString = calibList[6].split(" ");
        QStringList tString = calibList[7].split(" ");
        QStringList rString = calibList[8].split(" ");
        Vector4d d;
        Vector3d t, r;
        for (int i = 0; i < 4; i++)
        {
            d[i] = dString[i].toDouble();
        }
        for (int i = 0; i < 3; i++)
        {
            t[i] = tString[i].toDouble();
            r[i] = rString[i].toDouble();
        }

        // BUILD CAMERA
        PhotoCamera newCamera;
        newCamera.setWidthHeight(4000, 3000);
        newCamera.setPixelSize(K(1, 1) /K(0, 0), 1.0);
        Vector2i center(K(0, 2), K(1, 2));
        newCamera.setPrincipalPoint(center);
        newCamera.setFocalLength(0.0);
        newCamera.setIntrinsic(K.cast<float>());
        newCamera.setRadialDistortion(d(0), d(1));
        newCamera.setTangentialDistortion(d(2), d(3));

        Matrix4f ext = Matrix4f::Identity();

        cv::Mat r3(3, 3, CV_64F);
        CvMat R(r3);
        cv::Mat cvR(3, 1, CV_64F);
        cvR.at<double>(0, 0) = r(0);
        cvR.at<double>(1, 0) = r(1);
        cvR.at<double>(2, 0) = r(2);
        CvMat cvMat(cvR);
        cvRodrigues2(&cvMat, &R);

        Matrix3d rotationMatrix;

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                rotationMatrix(i, j) = CV_MAT_ELEM(R, double, i, j);

        ext.topLeftCorner(3, 3) = rotationMatrix.cast<float>();
        ext.col(3).head(3) = t.cast<float>();

        newCamera.setExtrinsic(ext);

        int cameraIndex = cameraNode.attributes().namedItem("camera").nodeValue().toInt();
        float cameraPhi = cameraNode.attributes().namedItem("phi").nodeValue().toFloat();
        float cameraTheta = cameraNode.attributes().namedItem("theta").nodeValue().toFloat();
        Vector3f cameraAttributes = Vector3f(cameraIndex, cameraPhi, cameraTheta);

        cameras.push_back(newCamera);
        cameraAttr.push_back(cameraAttributes);
//        testingCameras.push_back(newCamera);

        cameraNode = cameraNode.nextSibling();
    }
    for (int j = 0; j < cameras.size(); j++)
    {
        int cameraIndex = cameraAttr[j][0];
        QStringList filenameList = filename.split("/");
        QString photosFolder;
        for (int i = 0; i < filenameList.size() - 1; i++)
            photosFolder += filenameList[i] + "/";
        QString cameraIndexStr = QString::number(cameraIndex);
        if (cameraIndex < 10)
            cameraIndexStr.push_front("00");
        else
            if (cameraIndex < 100)
                cameraIndexStr.push_front("0");
        photosFolder += cameraIndexStr + "/";

        // READ ALL PHOTOS AND EXTRACT LIGHT INFORMATION FROM THEM

        QStringList imagesFilter("*.jpg");
        QDir directory(photosFolder);
        QStringList images = directory.entryList(imagesFilter);
        for (int i = 0; i < images.size(); i++)
        {
//            cout << images[i].toStdString() << endl;
            // cv001_tv000.00_pv000.00_cl001_tl000.00_pl000.00_ISO100_FQ0
            QStringList photonameList = images[i].split("_");
            float phi, theta;
            float phiDegree, thetaDegree;
            for (int j = 0; j < photonameList.size(); j++)
            {
                if (photonameList[j].contains("tl"))
                {
                    string thetaStr = photonameList[j].toStdString().substr(2, photonameList[j].size() - 2);
                    replace(thetaStr.begin(), thetaStr.end(), '.', ',');
                    thetaDegree = stof(thetaStr);
                }
                if (photonameList[j].contains("pl"))
                {
                    string phiStr = photonameList[j].toStdString().substr(2, photonameList[j].size() - 2);
                    replace(phiStr.begin(), phiStr.end(), '.', ',');
                    phiDegree = stof(phiStr);
                }
            }
            Vector3f newLightDirection;
            theta = (thetaDegree*PI)/180.0;
            phi = (phiDegree*PI)/180.0;

            Eigen::Vector3f cartesian = Eigen::Vector3f (sin(theta) * cos(phi), -sin(theta) * sin(phi), -cos(theta));
            Eigen::Quaternionf light_view = Eigen::Quaternionf::FromTwoVectors(cartesian, Eigen::Vector3f::UnitZ());
            Affine3f lightViewMatrix = Affine3f::Identity();
            lightViewMatrix.rotate(light_view);

            Affine3f cvToGl = Affine3f::Identity();
            cvToGl.rotate (AngleAxisf(M_PI, Vector3f::UnitX()));

            Vector4f z = Vector4f(0, 0, 1, 0);
            Vector4f lightDirection4d = cvToGl*cameras[j].getExtrinsic()*lightViewMatrix.matrix().inverse()*z;
            newLightDirection << lightDirection4d[0], lightDirection4d[1], lightDirection4d[2];
            newLightDirection.normalize();

            int cameraIndex = 0;
            for (int k = 0; k < cameraAttr.size(); k++)
                if (phiDegree == cameraAttr[k][1] && thetaDegree == cameraAttr[k][2])
                    cameraIndex = k;

            Matrix4f adjLightViewMatrix = cameras[cameraIndex].getExtrinsic();
            Vector4f flashPos = Vector4f((0.946+3.15)*0.5, -(3.64+3.19)*0.5, -2.5, 0);
            adjLightViewMatrix.col(3) += flashPos;

            QString newPhotoFile = photosFolder + images[i];
            if (cameraIndexStr == "060")
            {
                testingCameras.push_back(cameras[j]);
                vector<QString> newPhoto;
                newPhoto.push_back(newPhotoFile);
                testingPhotos.push_back(newPhoto);
                vector<Vector3f> newLightDirections;
                newLightDirections.push_back(newLightDirection);
                testingLightDirections.push_back(newLightDirections);
            }
            else
                meshView->createCamera(cameras[j], newPhotoFile.toStdString().c_str(), newLightDirection, adjLightViewMatrix);
        }
    }
    meshView->setRenderingBonn(true);
    meshView->nextView();
}

void MainWindow::updateMutCorrSpinBox(int position)
{
    mutCorrSpinBox->setValue(position/(float)100);
}

OptEnvironment MainWindow::readOptimizationXML(const char *filename)
{
    if (filename == 0)
        return OptEnvironment();
    QDomElement root;
    QDomDocument doc;
    QDomNode node;
    QFile file(filename);

    OptEnvironment environment;

    file.open(QIODevice::ReadOnly);
    doc.setContent(&file);
    root = doc.documentElement();
    node = root.firstChild();

    node = node.childNodes().at(0);
    node = node.childNodes().at(1);
    while(!node.isNull())
    {
        node = node.nextSibling();
        QString parName = node.attributes().namedItem("parName").nodeValue();
        if (!parName.compare("RenderingMode"))
        {
            int value = node.attributes().namedItem("parDefault").nodeValue().toInt();
            environment.renderingMode = (RenderingMode)value;
        }
        if (!parName.compare("Shot"))
        {
            int value = node.attributes().namedItem("parDefault").nodeValue().toInt();
//             environment.shot = value
        }
        if (!parName.compare("EstimateFocal"))
        {
            QString value = node.attributes().namedItem("parDefault").nodeValue();
            bool valueB = true;
            if (!value.compare("false"))
                valueB = false;
            environment.estimateFocal = valueB;
        }
        if (!parName.compare("Fine"))
        {
            QString value = node.attributes().namedItem("parDefault").nodeValue();
            bool valueB = true;
            if (!value.compare("false"))
                valueB = false;
            environment.fineAlignment = valueB;
        }
        if (!parName.compare("NumOfIterations"))
        {
            int value = node.attributes().namedItem("parDefault").nodeValue().toInt();
            environment.numIterations = value;
        }
        if (!parName.compare("Tolerance"))
        {
            float value = node.attributes().namedItem("parDefault").nodeValue().toFloat();
            environment.tolerance = value;
        }
        if (!parName.compare("ExpectedVariance"))
        {
            float value = node.attributes().namedItem("parDefault").nodeValue().toFloat();
            environment.expectedVariance = value;
        }
        if (!parName.compare("BackgroundWeight"))
        {
            int value = node.attributes().namedItem("parDefault").nodeValue().toInt();
            environment.backgroundWeight = value;
        }
    }

    file.close();
    return environment;
}

void MainWindow::saveChoices()
{
    vector<int> chosenPhotos = meshView->getChosenPhotos();
    vector<QString> newPhotos;
    vector<Vector3f> newLightDirections;
    for (unsigned int i = 0; i < chosenPhotos.size(); i++)
    {
        newPhotos.push_back(photos[0][chosenPhotos[i]]);
        newLightDirections.push_back(lightDirections[0][chosenPhotos[i]]);
    }
    photos.clear();
    photos.push_back(newPhotos);
    lightDirections.clear();
    lightDirections.push_back(newLightDirections);
}

void MainWindow::passTestPhoto()
{
    cout << "passing test photo" << endl;
    if (testingIndex[1] < testingPhotos[testingIndex[0]].size() - 1)
        testingIndex[1]++;
    else
    {
        testingIndex[1] = 0;
        testingIndex[0]++;
    }

    if (testingIndex[0] == testingPhotos.size())
        testingIndex[0] = 0;

    int cameraIndex = testingIndex[0];
    int photoIndex = testingIndex[1];

    meshView->testPhoto(testingCameras[cameraIndex], (testingPhotos[cameraIndex][photoIndex]).toStdString().c_str(), testingLightDirections[cameraIndex][photoIndex]);
}

void MainWindow::fitCurves()
{
    meshView->fitBRDF();
}

void MainWindow::deletePhoto()
{
    meshView->removePhoto();
}
