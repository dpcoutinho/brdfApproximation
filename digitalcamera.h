#ifndef DIGITALCAMERA_H
#define DIGITALCAMERA_H


#include <QImage>
#include <QColor>
#include <QDir>
#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <stdio.h>

#include <iostream>

using namespace Eigen;
using namespace std;

class DigitalCamera : public QObject
{
    Q_OBJECT

    int indexPhoto;
    QImage currentPhoto;
    Vector3f sphereCenter;
    Vector3f lightDirection;
    int photoWidth;
    int photoHeight;
    bool onLinux;
public:
    explicit DigitalCamera(QObject *parent = 0);
    void clear();
    QImage getPhoto(const char* newName = 0);
    void setCenter(const Vector3f&);
    void start(const char*);
    Vector3f getLightDirection();
    Vector3f detectLightDirection(Vector3f, Vector2i&);
    void formatCamera();
    int width();
    int height();
signals:
    void drawLightCenter(int, int);
    void renderLightDirection(float, float, float);
};

#endif // DIGITALCAMERA_H
