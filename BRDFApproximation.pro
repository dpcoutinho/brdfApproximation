#-------------------------------------------------
#
# Project created by QtCreator 2014-02-10T10:10:57
#
#-------------------------------------------------

VCGLIBDIR = ../vcglib
GLEWDIR   = ../glew
ANTDIR    = ../AntTweakBar
RPLYDIR = ../rply
SHADERLIBDIR = ../shader-lib
CIMGDIR = ../cimg
OPENCVDIR = ../openCV
EIGENDIR = ../eigen

QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#QT_CONFIG -= no-pkg-config
#CONFIG += link_pkgconfig
#PKGCONFIG += opencv

unix{
LIBS += `pkg-config opencv --libs`
LIBS +=  -lGL -lGLU -lGLEW $$ANTDIR/lib/libAntTweakBar.so -L/usr/X11R6/lib -lm -lpthread -lX11 #-lv4l2

# Compile glew
DEFINES += GLEW_STATIC
INCLUDEPATH += $$GLEWDIR/include
SOURCES += $$GLEWDIR/src/glew.c
}
win32{
INCLUDEPATH += $$OPENCVDIR/build/include
LIBS += -L$$OPENCVDIR/build/x64/vc11/lib
LIBS += -lopencv_core249
LIBS += -lopencv_contrib249
LIBS += -lopencv_highgui249
LIBS += -lopencv_imgproc249
LIBS += -lopencv_calib3d249

LIBS += $$ANTDIR/lib/AntTweakBar.lib $$ANTDIR/lib/AntTweakBar64.lib

INCLUDEPATH += $$CIMGDIR/

DEFINES += GLEW_STATIC
INCLUDEPATH += $$GLEWDIR/include
#LIBS += -L$$GLEWDIR/lib/Release/x64 -lglew32
SOURCES += ../glew/src/glew.c
}

TARGET = BRDFApproximation
TEMPLATE = app


SOURCES += main.cpp\
    glwidget.cpp \
    camera.cpp \
    model.cpp \
    photo.cpp \
    utils.cpp \
    webcamthread.cpp \
    cameralabel.cpp \
    mainwindow.cpp \
    Mutualinfo/alignset.cpp \
    Mutualinfo/filter_mutualinfo.cpp \
    Mutualinfo/levmarmethods.cpp \
    Mutualinfo/mutual.cpp \
    Mutualinfo/parameters.cpp \
    Mutualinfo/pointCorrespondence.cpp \
    Mutualinfo/solver.cpp \
    digitalcamera.cpp \
    curveglwidget.cpp

HEADERS  += utils.h \
    glwidget.h \
    camera.h \
    model.h \
    photo.h \
    vertex.h \
    webcamthread.h \
    cameralabel.h \
    mainwindow.h \
    Mutualinfo/alignset.h \
    Mutualinfo/filter_mutualinfo.h \
    Mutualinfo/levmarmethods.h \
    Mutualinfo/mutual.h \
    Mutualinfo/parameters.h \
    Mutualinfo/pointCorrespondence.h \
    Mutualinfo/shutils.h \
    Mutualinfo/solver.h \
    includes.h \
    digitalcamera.h \
    curveglwidget.h

QT       += core gui opengl xml

CONFIG += c++14



INCLUDEPATH += $$SHADERLIBDIR/include
INCLUDEPATH += $$VCGLIBDIR
INCLUDEPATH += $$GLEWDIR/include
INCLUDEPATH += $$ANTDIR/include
INCLUDEPATH += $$RPLYDIR/
INCLUDEPATH += $$EIGENDIR/

SOURCES += $$VCGLIBDIR/wrap/ply/plylib.cpp
SOURCES += $$VCGLIBDIR/wrap/qt/anttweakbarMapper.cpp

SOURCES += $$RPLYDIR/rply.c \
    $$SHADERLIBDIR/src/effect.cpp \
    $$SHADERLIBDIR/src/framebuffer.cpp \
    $$SHADERLIBDIR/src/mesh.cpp \
    $$SHADERLIBDIR/src/shader.cpp \
    $$SHADERLIBDIR/src/texture.cpp \
    $$SHADERLIBDIR/src/textureManager.cpp \
    $$SHADERLIBDIR/src/trackball.cpp

HEADERS += $$SHADERLIBDIR/include/effect.cpp \
    $$SHADERLIBDIR/include/framebuffer.hpp \
    $$SHADERLIBDIR/include/mesh.hpp \
    $$SHADERLIBDIR/include/shader.hpp \
    $$SHADERLIBDIR/include/texture.hpp \
    $$SHADERLIBDIR/include/textureManager.hpp \
    $$SHADERLIBDIR/include/trackball.hpp

OTHER_FILES += \
    shaders/draw.vert \
    shaders/draw.frag \
    shaders/coord.vert \
    shaders/coord.frag \
    texture.vert \
    shaders/texture.vert \
    shaders/texture.frag \
    shaders/mixtexture.vert \
    shaders/mixtexture.frag \
    clearbuffer.vert \
    shaders/clearbuffer.frag \
    shaders/clearbuffer.vert \
    shaders/render.frag \
    shaders/render.vert \
    shaders/light.vert \
    shaders/light.frag \
    shaders/normals.frag \
    shaders/normals.vert \
    shaders/normals.frag \
    shaders/normals.vert \
    shaders/render.geom \
    shaders/token.vert \
    shaders/token.frag \
    Mutualinfo/filter_mutualinfo.xml \
    shaders/sphere.vert \
    shaders/sphere.frag \
    shaders/utils.vert \
    shaders/calculateLight.vert \
    shaders/calculateLight.frag \
    shaders/curveShader.vert \
    shaders/curveShader.frag \
    shaders/pointShader.vert \
    shaders/pointShader.frag \
    shaders/axistexture.frag \
    shaders/axistexture.vert \
    shaders/fitting.vert \
    shaders/fitting.frag \
    shaders/utilsfunc.vert \
    shaders/neighbors.vert \
    shaders/neighbors.frag \
    shaders/axis.frag \
    shaders/axis.vert \
    shaders/cookTorrance.vert \
    shaders/cookTorrance.frag \
    shaders/cookTorrance.geom \
    shaders/fitCookTorrance.frag \
    shaders/renderBonn.vert \
    shaders/renderBonn.frag

SUBDIRS += \
    Mutualinfo/filter_mutualinfoxml.pro






