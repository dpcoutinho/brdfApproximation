#include "pointCorrespondence.h"

PointCorrespondence::PointCorrespondence()
{
    numofItems=0;
}

PointCorrespondence::~PointCorrespondence()
{
}

vcg::Point2f PointCorrespondence::getPoint2d()
{
    return pointImage;
}

vcg::Point3f PointCorrespondence::getPoint3d()
{
    return pointMesh;
}

void PointCorrespondence::addPoint(const vcg::Point3f &newPt3d, const vcg::Point2f &newPt2d)
{
    pointMesh = newPt3d;
    pointImage = newPt2d;
    numofItems++;
}

