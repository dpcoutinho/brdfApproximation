#ifndef POINTCORRESPONDENCE_H
#define POINTCORRESPONDENCE_H

#include <iostream>

#include <vcg/complex/complex.h>
#include <vcg/complex/algorithms/update/bounding.h>
#include <vcg/complex/algorithms/update/color.h>
#include <vcg/complex/algorithms/update/normal.h>
#include <vcg/complex/algorithms/create/platonic.h>

class PointCorrespondence
{

public:

    PointCorrespondence();
    ~PointCorrespondence();

    int	numofItems;

    void addPoint(const vcg::Point3f &newPt3d, const vcg::Point2f &newPt2d);
    vcg::Point2f getPoint2d();
    vcg::Point3f getPoint3d();

private:

    vcg::Point3f pointMesh;
    vcg::Point2f pointImage;

};


#endif // POINTCORRESPONDENCE_H
