/****************************************************************************
* MeshLab                                                           o o     *
* A versatile mesh processing toolbox                             o     o   *
*                                                                _   O  _   *
* Copyright(C) 2005                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/

#ifndef FILTER_MUTUALINFO_H
#define FILTER_MUTUALINFO_H
#include <QObject>
#include "alignset.h"

class MutualInfoPlugin : public QGLWidget
{
    Q_OBJECT
//	MESHLAB_PLUGIN_IID_EXPORTER(MESHLAB_FILTER_INTERFACE_IID)
public:
    MutualInfoPlugin(QWidget *parent = 0);
    bool applyFilter( const QString& filterName, CMesh& mesh, QImage& image, const vector<PointCorrespondence*>& correspondences, const OptEnvironment&, vcg::CallBackPos * cb );
private:
	AlignSet align;
protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
signals:
    void updateRender(vcg::Shot<float>);
};
#endif
