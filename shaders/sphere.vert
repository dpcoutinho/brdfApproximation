#version 430

in vec3 position;

out vec4 vert;
out vec4 vPosition;
out vec4 vNormal;
out vec4 light;

uniform float radius;

uniform mat4 projectionMatrix;
uniform mat4 translationMatrix;
uniform mat4 trackballMatrix;


void main(void)
{
    vec4 l = vec4(0, 0, 1, 0);
    vec4 pos = vec4(position, 1);
    vert = pos;
    light = trackballMatrix*l;
    vNormal = pos/radius;

    vPosition = translationMatrix*vert;
    gl_Position = vPosition;
}
