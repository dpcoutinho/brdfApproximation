#version 430

in vec3 vPosition;
in vec4 vert;
in vec3 vNormal;
flat in uint vIndex;
flat in float depth;
flat in int vAdjacencyStart;
flat in int vNumAdjacents;

uniform sampler2D depthTexture;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform sampler2DArray textureArray;
uniform ivec2 viewportSize;
uniform int numPhotos;

uniform ivec2 totalTextureSize;
uniform ivec2 textureBlockCoord;
uniform bool useTextureBlock;

uniform float BBDiagonal;

out vec4 fragColor;

layout( std430, binding=1 ) buffer positionBuffer
{
    float  positions[];
};

layout( std430, binding=2 ) buffer normalBuffer
{
    float  normals[];
};

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout( std430, binding=5 ) buffer adjacency
{
    float adjacents[];
};

layout( std430, binding=10 ) buffer LightPositions
{
    float lightPos[];
};

layout( std430, binding=11 ) buffer fillBuffer
{
    float fill[];
};

#include shaders/utils.vert

void main()
{
    if (fill[vIndex] < 0)
    {
//        discard;
        fragColor = vec4(1, 1, 1, 0.4);
    }
    else
    {
        int numPhotosUsed = 0;
        float avg = 0;
        float sd = 0;
        int i, j, ii;
        for (i = 0; i < numPhotos; i++)
        {
            if (curve[5*size*vIndex + 5*i] < 10)
            {
                numPhotosUsed++;
                avg += curve[5*size*vIndex + 5*i];
            }
        }
        avg /= numPhotosUsed;
        for (int i = 0; i < numPhotos; i++)
        {
            if (curve[5*size*vIndex + 5*i] < 10)
            {
                sd += pow(curve[5*size*vIndex + 5*i] - avg, 2);
            }
        }
        sd /= numPhotosUsed;
        sd = sqrt(sd);
        float ratio = float(numPhotosUsed)/float(numPhotos);

        bool hasOneNeighbor = false;
        fragColor = vec4(1, 0, 0, 1);
        if (ratio < 0.5)
        {
            fragColor = vec4(1, 0, 1, 1);
            for (i = 0; i < numPhotos; i++)
            {
                if (curve[5*size*vIndex + 5*i] == 10)
                {
                    vec3 light = vec3(lightPos[3*i], lightPos[3*i+1], lightPos[3*i+2]);

                    float thresholdAngle = 30 *(PI/180.0);

                    float maxSd = -1;
                    int indMax = -1;
                    for (ii = vAdjacencyStart; ii < vAdjacencyStart+vNumAdjacents; ii++)
                    {
                        int index = int(adjacents[ii]);
                        float x = curve[5*size*index+5*i];

//                        vec3 neighborNormal = vec3(normals[3*index], normals[3*index+1], normals[3*index+2]);
//                        float dotNormals = dot(vNormal, neighborNormal);
//                        float angle = acos(dotNormals);
//                        angle = angle*(180.0/PI);

                        if(x < 10)
                        {
                            int originalIndex = int(curve[5*size*index+5*i + 4]);
                            vec3 originalNormal = vec3(normals[3*originalIndex], normals[3*originalIndex+1], normals[3*originalIndex+2]);
                            float dotNormals = dot(vNormal, originalNormal);
                            dotNormals = clamp(dotNormals, -1, 1);
                            float angle = acos(dotNormals);

                            if (angle > thresholdAngle)
                                continue;

                            vec3 originalPosition = vec3(positions[3*originalIndex], positions[3*originalIndex+1], positions[3*originalIndex+2]);
                            float distance = length(vPosition - originalPosition);
                            if (distance > 0.1*BBDiagonal)
                                continue;

                            float newAvg = (avg*numPhotosUsed + x)/(numPhotosUsed+1);
                            float newSd = 0;
                            for (j = 0; j < numPhotos; j++)
                            {
                                if (curve[5*size*vIndex + 5*j] < 10)
                                    newSd += pow(curve[5*size*vIndex + 5*j] - newAvg, 2);
                            }
                            newSd += pow(x-newAvg, 2);
                            newSd /= (numPhotosUsed + 1);
                            newSd = sqrt(newSd);
                            if (newSd > maxSd)
                            {
                                maxSd = newSd;
                                indMax = index;
                            }
                        }
                    }
                    if (indMax > -1)
                    {
                        curve[5*size*vIndex + 5*i] = curve[5*size*indMax + 5*i];
                        curve[5*size*vIndex + 5*i + 1] = curve[5*size*indMax + 5*i + 1];
                        curve[5*size*vIndex + 5*i + 2] = curve[5*size*indMax + 5*i + 2];
                        curve[5*size*vIndex + 5*i + 3] = curve[5*size*indMax + 5*i + 3];
                        curve[5*size*vIndex + 5*i + 4] = curve[5*size*indMax + 5*i + 4];

                        hasOneNeighbor = true;
                    }
                }
            }
            if (hasOneNeighbor)
            {
                fill[vIndex] = 1.0;
                fragColor = vec4(1, 1, 0, 1);
            }
        }
        else
        {
            fill[vIndex] = -1.0;
            fragColor = vec4(0, 1, 0, 1);
        }
    }
    memoryBarrier();
}


