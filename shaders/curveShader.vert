#version 430

in vec2 position;

uniform mat4 projection;


void main(void)
{
    vec4 posWorld = vec4(position, 1, 1);
    gl_Position = projection*posWorld;
}
