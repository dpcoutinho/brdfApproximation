#version 430

uniform sampler2D imageTexture;
uniform ivec2 viewportSize;

out vec4 fragColor;


void main(void)
{
    vec2 texCoord = vec2(gl_FragCoord.x/viewportSize[0], gl_FragCoord.y/viewportSize[1]);
    vec3 result = texture(imageTexture, texCoord).rgb;

    fragColor = vec4(result, 1.0);
}
