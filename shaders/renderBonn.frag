#version 430

in vec3 fNormal;
in vec4 vert;
flat in uint fIndex;

uniform vec3 light;
uniform mat3 lightModelMatrix;
uniform int numPhotos;

uniform sampler2D testTexture;
uniform sampler2DArray textureArray;
uniform ivec2 viewportSize;

out vec4 out_Color;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout( std430, binding=4 ) buffer polynomialBuffer
{
    float polynomial[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};

layout( std430, binding=10 ) buffer LightPositions
{
    float lightPos[];
};

#include shaders/utils.vert

vec4 calculateColor(in vec4 v, in vec3 n, in vec3 diffuseColor, in vec3 lightColor, in vec3 light, in float kd, in float ks, in float shininess,  inout float d, inout float s);

vec4 calculatePolyColor(in uint index, in vec3 n, in vec3 light, in vec4 v);

void getData(in uint index, inout vec3 color, inout float kd, inout float ks, inout float shininess, inout bool hasPolynomial);

void main(void)
{
    vec3 lightDirection = normalize(light);

    vec3 c;
    float kd, ks, n, d, s;
    bool p;

    getData(fIndex, c, kd, ks, n, p);

    vec3 lightColor = vec3(1, 1, 1);
    vec4 color = calculateColor(vert, fNormal, c, lightColor, lightDirection, kd, ks, n, d, s);

    if (p)
        color = calculatePolyColor(fIndex, fNormal, lightDirection, vert);
    out_Color = color;

    ivec3 texSize = textureSize(textureArray, 0);
    vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
    vec2 pixelCoord = texCoord*texSize.xy;
    vec3 pixel = texelFetch(textureArray, ivec3(pixelCoord, 0), 0).rgb;
    vec4 tex = vec4(pixel, 1);


    int numPhotosUsed = 0;
    float avg = 0;
    float sd = 0;
    int i, j, ii;
    for (i = 0; i < numPhotos; i++)
    {
        if (curve[5*size*fIndex + 5*i] < 10)
        {
            numPhotosUsed++;
            avg += curve[5*size*fIndex + 5*i];
        }
    }
    avg /= numPhotosUsed;
    for (int i = 0; i < numPhotos; i++)
    {
        if (curve[5*size*fIndex + 5*i] < 10)
        {
            sd += pow(curve[5*size*fIndex + 5*i] - avg, 2);
        }
    }
    sd /= numPhotosUsed;
    sd = sqrt(sd);
    float ratio = float(numPhotosUsed)/float(numPhotos);



//    if (lighted[fIndex] < 0)
//        color = vec4(0);
    vec4 pixelDifference = color - tex;
    float error = pow(length(pixelDifference), 2);
//    out_Color = vec4(0.7+error, 1-error, 0.5-error, 1)*d;
//    out_Color = vec4(d);
    out_Color = color;
//    if (ratio < 0.5)
//        out_Color = vec4(1, 0, 0, 1);
//    out_Color = vec4(fNormal, 1);
}

vec4 calculateColor(in vec4 v, in vec3 n, in vec3 color, in vec3 lightColor, in vec3 light, in float kd, in float ks, in float shininess, inout float d, inout float s)
{
    vec3 posLight = normalize(light);
    vec3 lightDirection = lightModelMatrix * posLight;
    lightDirection = normalize(lightDirection);

    vec3 l = lightDirection;
    vec3 reflection = reflect(-l, n);
    vec3 eye  = normalize(-v.xyz);

    float diffuse = /*max(*/dot(n, l)/*, 0)*/;
    float specular = /*max(*/dot(reflection, eye)/*, 0)*/;
    d = diffuse;
    s = specular;

    vec3 diffuseTerm = (color*diffuse);
    vec3 specularTerm = lightColor;

    vec3 diffuseColor = kd*diffuseTerm;
    vec3 specularColor = ks*specularTerm*pow(specular, shininess);

    vec4 finalColor = vec4(diffuseColor + specularColor, 1);
    return finalColor;
}

vec4 calculatePolyColor(in uint index, in vec3 n, in vec3 light, in vec4 v)
{
    vec3 posLight = normalize(light);
    vec3 lightDirection = lightModelMatrix * posLight;
    lightDirection = normalize(lightDirection);

    vec3 l = lightDirection;
    float ang = dot(n, l);
    vec3 e = -v.xyz;
    e = normalize(e);
    vec3 r = reflect(-l, n);
    r = normalize(r);
    float s = dot(r, e);

//    float a = acos(d);
    float a = acos(s);
    vec4 polyR = vec4(polynomial[12*index], polynomial[12*index+1], polynomial[12*index+2], polynomial[12*index+3]);
    vec4 polyG = vec4(polynomial[12*index+4], polynomial[12*index+5], polynomial[12*index+6], polynomial[12*index+7]);
    vec4 polyB = vec4(polynomial[12*index+8], polynomial[12*index+9], polynomial[12*index+10], polynomial[12*index+11]);

    vec4 color = vec4(1);
    color.r = polyR.x*pow(a, 3) + polyR.y*pow(a, 2) + polyR.z*a + polyR.w;
    color.g = polyG.x*pow(a, 3) + polyG.y*pow(a, 2) + polyG.z*a + polyG.w;
    color.b = polyB.x*pow(a, 3) + polyB.y*pow(a, 2) + polyB.z*a + polyB.w;

    if (ang < 0)
        color = vec4(0);
    return color;
}

void getData(in uint index, inout vec3 color, inout float kd, inout float ks, inout float shininess, inout bool hasPolynomial)
{
    uint indexR = 4*index;
    uint indexG = indexR + 1;
    uint indexB = indexR + 2;
    uint indexT = indexR + 3;

    color = vec3(colors[indexR], colors[indexG], colors[indexB]);
    color /= colors[indexT];

    kd = 1;
    ks = 0;
    shininess = 10;

    if (polynomial[12*index] != 0 || polynomial[12*index+1] != 0 || polynomial[12*index+2] != 0 || polynomial[12*index+3] != 0)
        hasPolynomial = true;
}
