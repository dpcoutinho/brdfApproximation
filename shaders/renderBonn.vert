#version 430

in vec3 position;
in vec3 normal;
in float index;

out vec3 fNormal;
out vec4 vert;
flat out uint fIndex;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 cvToGl;

uniform mat4 trackballMatrix;
uniform mat4 centerTranslation;
uniform mat4 iCenterTranslation;

// radial distortion
uniform vec2 k;

// tangential distortion
uniform vec2 p;

uniform ivec2 image_size;

void main(void)
{
    fIndex = uint(index);

    mat4 modelViewMatrix = viewMatrix * modelMatrix;

    mat4 invModelViewMatrix = cvToGl*modelViewMatrix;
    mat4 normalMatrix = transpose(inverse(invModelViewMatrix));
    vec4 normal4 = vec4(normal, 0);
    fNormal = normalize(vec3(normalMatrix * vec4(normal4)).xyz);

    vec4 pos = vec4(position, 1);
    pos = pos;
    vert = modelViewMatrix * pos;

    // the code bellow follows exactly the OpenCV explanation in this link:
    // http://docs.opencv.org/2.4.8/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#void Rodrigues(InputArray src, OutputArray dst, OutputArray jacobian)

    // vertice in camera space
    vec4 vproj = iCenterTranslation*trackballMatrix*centerTranslation*vert;
    vproj = vproj/vproj.z;

    float r2 = vproj.x*vproj.x + vproj.y*vproj.y;

    // apply distortion
    vec2 vcorrected;
    vcorrected.x = vproj.x * (1.0 + k.x*r2 + k.y*r2*r2) + 2.0*p.x*vproj.x*vproj.y + p.y*(r2 + 2.0*vproj.x*vproj.x);
    vcorrected.y = vproj.y * (1.0 + k.x*r2 + k.y*r2*r2) + p.x*(r2 + 2.0*vproj.y*vproj.y) + 2.0*p.y*vproj.x*vproj.y;

    // project using matrix K
    vproj.x = projectionMatrix[0].x*vcorrected.x + projectionMatrix[2].x;
    vproj.y = projectionMatrix[1].y*vcorrected.y + projectionMatrix[2].y;

    // OpenCV explanation finishes here
    // now put coordinates in range [-1,+1] and let OpenGL do the rest
    vproj.xy /= vec2(image_size.xy);

    // OBS: invert y axis since OpenGL origin is top left and OpenCV is bottom left
    vproj.x = vproj.x*2.0 - 1.0;
    vproj.y = (1.0 - vproj.y)*2.0 - 1.0;
    vproj.z = vert.z / 1000.0; // divide by some arbitraty far plane constant
    vproj.w = 1.0;

    gl_Position = vproj;
}
