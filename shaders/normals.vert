#version 430


in vec3 position;
in float index;
in vec3 color;

out vec4 vert;
out vec4 vPosition;
flat out uint vIndex;
flat out float depth;
out vec4 vColor;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;


uniform mat4 trackballMatrix;
uniform mat4 centerTranslation;
uniform mat4 iCenterTranslation;




void main()
{
    int iIndex = int(index);
    vec4 pos = vec4(position, 1.0);
    vec4 aux = iCenterTranslation*trackballMatrix*centerTranslation*pos;
    vert = modelViewMatrix*aux;
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;

    vPosition = modelViewProjectionMatrix*aux;
    gl_Position = vPosition;
    vIndex = iIndex;

    vColor = vec4(color, 1);


    float far=gl_DepthRange.far; float near=gl_DepthRange.near;

    vec4 eye_space_pos = modelViewMatrix * pos;
    vec4 clip_space_pos = projectionMatrix * eye_space_pos;

    float ndc_depth = clip_space_pos.z / clip_space_pos.w;

    depth = (((far-near) * ndc_depth) + near + far) / 2.0;


    gl_PointSize = 0.5;

//    uint indexR = 3*iIndex;
//    uint indexG = indexR + 1;
//    uint indexB = indexR + 2;

//    colors[6*iIndex] = 0;
//    colors[6*iIndex+1] = 0;
//    colors[6*iIndex+2] = 1;



}
