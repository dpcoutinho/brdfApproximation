#version 430

flat in uint vIndex;
flat in float depth;

uniform sampler2D depthTexture;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform sampler2DArray textureArray;
uniform ivec2 viewportSize;
uniform int numPhotos;

uniform ivec2 totalTextureSize;
uniform ivec2 textureBlockCoord;
uniform bool useTextureBlock;

out vec4 fragColor;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout( std430, binding=4 ) buffer polynomialBuffer
{
    float polynomial[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};

#include shaders/utils.vert

void main()
{
    ivec3 texSize = textureSize(textureArray, 0);
    vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
    vec2 pixelCoord = texCoord*texSize.xy;

    if (useTextureBlock)
    {
        vec2 globalPixelCoord = texCoord * totalTextureSize;
        if (globalPixelCoord.x < textureBlockCoord.x || globalPixelCoord.x >= textureBlockCoord.x + texSize.x ||
                globalPixelCoord.y < textureBlockCoord.y || globalPixelCoord.y >= textureBlockCoord.y + texSize.y)
            discard;
        else
            pixelCoord = globalPixelCoord - textureBlockCoord;
    }

//    if (texColor.x >= 1 || texColor.y >= 1 || texColor.z >= 1)
//        discard;

    vec3 depthPixel = texture(depthTexture, texCoord).rgb;

    if ( abs(depth - depthPixel.x) > 1e-4)
    {
        discard;
    }
    else
    {
        dvec4 A[size];
        dvec3 b[size];
        dmat4 AtA = dmat4(0);
        dvec4 Atbr = dvec4(0);
        dvec4 Atbg = dvec4(0);
        dvec4 Atbb = dvec4(0);
        dvec4 p;
        float x;
        int i, j, k;
        dvec4 a;
        int index = 0;
        if (curve[5*size*vIndex] < 0)
        {
            vec3 color;
            uint indexR = 4*vIndex;
            uint indexG = indexR + 1;
            uint indexB = indexR + 2;
            uint indexT = indexR + 3;

            color = vec3(colors[indexR], colors[indexG], colors[indexB]);
            color /= colors[indexT];

            polynomial[12*vIndex] = 0;
            polynomial[12*vIndex+1] = 0;
            polynomial[12*vIndex+2] = 0;
            polynomial[12*vIndex+3] = color.r;

            polynomial[12*vIndex+4] = 0;
            polynomial[12*vIndex+5] = 0;
            polynomial[12*vIndex+6] = 0;
            polynomial[12*vIndex+7] = color.g;

            polynomial[12*vIndex+8] = 0;
            polynomial[12*vIndex+9] = 0;
            polynomial[12*vIndex+10] = 0;
            polynomial[12*vIndex+11] = color.b;
        }
        else
        {
            for (i = 0; i < numPhotos; i++)
            {
                if (curve[5*size*vIndex + 5*i] < 10 || curve[5*size*vIndex + 5*i+1] > 0 || curve[5*size*vIndex + 5*i+2] > 0 || curve[5*size*vIndex + 5*i+3] > 0)
                {
                    x = curve[5*size*vIndex + 5*i];
                    A[index] = dvec4(pow(x, 3), pow(x, 2), x, 1);
                    b[index] = dvec3(curve[5*size*vIndex + 5*i+1], curve[5*size*vIndex + 5*i+2], curve[5*size*vIndex + 5*i+3]);
                    index++;
                }
            }
            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                    for (k = 0; k < index; k++)
                    {
                        a = A[k];
                        AtA[i][j] += a[i]*a[j];
                    }

            dmat4 AtAI = inverse(AtA);
            for (j = 0; j < 4; j++)
                for (k = 0; k < index; k++)
                {
                    Atbr[j] += A[k][j]*b[k][0];
                    Atbg[j] += A[k][j]*b[k][1];
                    Atbb[j] += A[k][j]*b[k][2];
                }

            p = AtAI * Atbr;
            polynomial[12*vIndex] = float(p.x);
            polynomial[12*vIndex+1] = float(p.y);
            polynomial[12*vIndex+2] = float(p.z);
            polynomial[12*vIndex+3] = float(p.w);

            p = AtAI * Atbg;
            polynomial[12*vIndex+4] = float(p.x);
            polynomial[12*vIndex+5] = float(p.y);
            polynomial[12*vIndex+6] = float(p.z);
            polynomial[12*vIndex+7] = float(p.w);

            p = AtAI * Atbb;
            polynomial[12*vIndex+8] = float(p.x);
            polynomial[12*vIndex+9] = float(p.y);
            polynomial[12*vIndex+10] = float(p.z);
            polynomial[12*vIndex+11] = float(p.w);
        }
    }
    fragColor = vec4(1);
    memoryBarrier();
}


