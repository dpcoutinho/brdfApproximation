#version 430

in vec4 vPosition;
flat in uint vIndex;
//flat in uint vIndexFace;
in float depth;

uniform sampler2D depthTexture;
uniform ivec2 viewportSize;
uniform bool renderLight;

out vec4 fragColor;

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};


void main()
{
    ivec2 texSize = textureSize(depthTexture, 0);

    vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
    vec3 pixel = texture(depthTexture, texCoord).rgb;
    gl_FragDepth = depth;

//    if (lighted[vIndex] != 0)
//    {
//        if (lighted[vIndex] < 0)
//            discard;
//        else
//            fragColor = vec4(1);
//    }
//    else
//    {
        if (abs(depth - pixel.x) > 1e-4)
        {
            lighted[vIndex] = -1.0;
            fragColor = vec4(1, 0, 0, 1);
        }
        else
        {
            lighted[vIndex] = 1.0;
            fragColor = vec4(1, 1, 1, 1);
        }
//    }



    memoryBarrier();
    //fragColor = vpos;
}
