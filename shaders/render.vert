#version 430

in vec3 position;
in vec3 normal;
in float index;
in float adjacencyStart;
in float numAdjacents;

out vec4 vert;
out vec4 vPosition;
out vec3 vNormal;
flat out int distantNeighbor;
flat out uint vIndex;
flat out float depth;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 trackballMatrix;
uniform mat4 centerTranslation;
uniform mat4 iCenterTranslation;

uniform float thresholdNormalAngle;


layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};

layout( std430, binding=1 ) buffer positionBuffer
{
    float positions[];
};

layout( std430, binding=2 ) buffer normalBuffer
{
    float normals[];
};

layout( std430, binding=5 ) buffer adjacency
{
    float adjacents[];
};

float PI = 3.14159265358979323846264;

void main()
{
    vIndex = uint(index);
    vec4 pos = vec4(position, 1);
    vec4 aux = pos;
    vert = iCenterTranslation*trackballMatrix*centerTranslation*modelViewMatrix*aux;
    mat4 modelViewProjectionMatrix = projectionMatrix * iCenterTranslation*trackballMatrix*centerTranslation * modelViewMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix*trackballMatrix));

    vPosition = modelViewProjectionMatrix*aux;
    gl_Position = vPosition;

    vec3 inNormal = normal;
    vNormal = normalize(vec3(normalMatrix * vec4(inNormal,0.0)).xyz);

    distantNeighbor = -1;
    for (int i = int(adjacencyStart); i < int(adjacencyStart+numAdjacents); i++)
    {
        int index = int(adjacents[i]);
        vec3 neighborNormal = vec3(normals[3*index], normals[3*index+1], normals[3*index+2]);

        float dotNormals = dot(normal, neighborNormal);
        float angle = acos(dotNormals);
        angle = angle*(180.0/PI);

//        if (abs(angle) >= thresholdNormalAngle)
//            distantNeighbor = 1;
    }

    if (numAdjacents > 8.0)
        distantNeighbor = 1;


    float far=gl_DepthRange.far; float near=gl_DepthRange.near;

    vec4 eye_space_pos = modelViewMatrix * pos;
    vec4 clip_space_pos = projectionMatrix * eye_space_pos;

    float ndc_depth = clip_space_pos.z / clip_space_pos.w;

    depth = (((far-near) * ndc_depth) + near + far) / 2.0;

    //gl_PointSize = 0.5;
    //memoryBarrier();

}
