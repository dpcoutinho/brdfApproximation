#version 430

in vec3 position;
in vec3 color;

out vec4 vColor;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 trackballMatrix;


void main(void)
{
    vec4 pos = vec4(position, 1);
    vec4 aux = trackballMatrix*pos;
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;

    vec4 vPosition = modelViewProjectionMatrix*aux;
    vColor = vec4(color, 1);
    gl_Position = vPosition;
}
