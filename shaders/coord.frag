#version 430

#extension GL_NV_gpu_shader5 : enable

in vec4 vert;
in vec4 vNormal;
flat in uint vIndex;
in float depth;

out vec4 fragColor;

uniform bool renderPosition;
uniform bool renderNormal;
uniform bool renderProjection;

void main()
{
    fragColor = vec4(depth, 0, vIndex, 1);
    if (renderPosition)
        fragColor = vec4(vert);
    if (renderNormal)
        fragColor = vec4(vNormal);
}
