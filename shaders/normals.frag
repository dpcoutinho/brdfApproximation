#version 430

in vec4 vert;
in vec4 vPosition;
flat in uint vIndex;
in vec4 vColor;

uniform vec3 light;

uniform mat3 lightModelMatrix;

out vec4 fragColor;
//out float gl_FragDepth;


layout( std430, binding=0 ) buffer colorBuffer
{
    float colors[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};


void main()
{
    vec3 posLight = normalize(light);
    vec3 lightDirection = lightModelMatrix * posLight;
    lightDirection = normalize(lightDirection);

    if (colors[4*vIndex+3] == 0)
        discard;

    fragColor = vec4(vColor);
}



