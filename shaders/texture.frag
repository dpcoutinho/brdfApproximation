#version 430

//uniform sampler2D imageTexture;
uniform sampler2DArray textureArray;
uniform sampler2D depthTexture;
uniform sampler2D lightTexture;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform ivec2 viewportSize;

uniform int texIndex;

uniform vec2 mousePos;
uniform float radius;

out vec4 fragColor;

uniform vec3 colorWeights;
uniform bool useColorWeights;

#include shaders/utilsfunc.vert

void main(void)
{
    ivec3 texSize = textureSize(textureArray, 0);

    vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
    vec2 pixelCoord = texCoord*texSize.xy;

    vec3 result = texelFetch(textureArray, ivec3(pixelCoord, texIndex), 0).rgb;
//    vec3 result = texture(lightTexture, texCoord).rgb;

    if (useColorWeights)
        result *= colorWeights;

//    fragColor = bilateralFilter(pixelCoord, 25);
    fragColor = vec4(result, 1.0);

    if (pixelCoord.x > 500 && pixelCoord.x < 1500 && pixelCoord.y > 600 && pixelCoord.y < 700)
    {
        float error = pixelCoord.x - 500;
        error /= 1000;
        fragColor = vec4(0.7+error*0.3, 1-error, 0.5-error*0.5, 1);
    }
//    else
//	fragColor = vec4(0);
}
