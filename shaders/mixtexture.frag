#version 430

uniform sampler2D textures[2];
uniform ivec2 viewportSize;
uniform float blendFactor;


out vec4 fragColor;

void main(void)
{
    vec2 texCoord = vec2(gl_FragCoord.x/viewportSize[0], gl_FragCoord.y/viewportSize[1]);
    vec3 result = mix(texture(textures[0], texCoord).rgb,texture(textures[1], texCoord).rgb, blendFactor);
    fragColor = vec4(result, 1.0);
}
