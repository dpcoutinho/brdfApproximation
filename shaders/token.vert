#version 430

in vec3 position;

out vec4 vert;
out vec4 vPosition;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 trackballMatrix;
uniform mat4 centerTranslation;
uniform mat4 iCenterTranslation;


void main(void)
{
    vec4 pos = vec4(position, 1);
    vec4 aux = iCenterTranslation*trackballMatrix*centerTranslation*pos;
    vert = modelViewMatrix*aux;
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix*trackballMatrix));

    vPosition = modelViewProjectionMatrix*aux;
    gl_Position = vPosition;
}
