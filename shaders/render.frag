#version 430

in vec4 fVert;
in vec4 fPosition;
in vec3 fNormal;
flat in uint fIndex;
flat in float fDepth;
flat in uvec3 verticesIndices;
in vec3 baricentricCoordinates;
flat in int fDistantNeighbor;

flat in vec3 n1;
flat in vec3 n2;
flat in vec3 n3;
flat in vec4 v1;
flat in vec4 v2;
flat in vec4 v3;

uniform vec3 light;
uniform vec3 testLight;
uniform bool renderDepth;
uniform bool renderDifferP;
uniform bool renderDifferM;
uniform bool renderPixel;
uniform bool useLAB;
uniform bool aligning;
uniform bool testing;
uniform float baseShininess;
uniform int lightIndex;
uniform int numPhotos;

uniform mat3 lightModelMatrix;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform sampler2D depthTexture;
uniform sampler2D testTexture;
uniform sampler2DArray textureArray;
uniform ivec2 viewportSize;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform vec3 colorWeights;
uniform bool useColorWeights;

out vec4 fragColor;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};

layout( std430, binding=1 ) buffer positionBuffer
{
    float positions[];
};

layout( std430, binding=2 ) buffer normalBuffer
{
    float normals[];
};

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout( std430, binding=4 ) buffer polynomialBuffer
{
    float polynomial[];
};

layout ( std430, binding=6 ) buffer feedback
{
    float error[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};

layout( std430, binding=8 ) buffer Anglebuffer
{
    float angles[];
};

layout( std430, binding=9 ) buffer renderInPhotos
{
    double photos[];
};

layout( std430, binding=10 ) buffer LightPositions
{
    float lightPos[];
};

layout( std430, binding=11 ) buffer fillBuffer
{
    float fill[];
};

vec4 calculateColor(in vec4 v, in vec3 n, in vec3 diffuseColor, in vec3 lightColor, in vec3 light, in float kd, in float ks, in float shininess,  inout float d, inout float s);

vec4 calculatePolyColor(in uint index, in vec3 n, in vec3 light, in vec4 v);

void getData(in uint index, inout vec3 color, inout float kd, inout float ks, inout float shininess, inout bool hasPolynomial);

vec3 rgbToXyz(in vec3 c);

vec3 xyzToRgb(in vec3 c);

vec3 xyzToLab(in vec3 c);

vec3 labToXyz(in vec3 c);

vec3 rgbToLab(in vec3 c);

vec3 labToRgb(in vec3 c);

float f(in float t);

float fi(in float t);

#include shaders/utils.vert

void main()
{
    bool hasPhoto = true;
    vec4 texture;
    ivec3 texSize = textureSize(textureArray, 0);

    vec4 testPixel;
    if (texSize.z == 0)
        hasPhoto = false;
    else
    {
        vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
        vec2 pixelCoord = texCoord*texSize.xy;
        vec3 pixel = texelFetch(textureArray, ivec3(pixelCoord, 0), 0).rgb;
        texture = vec4(pixel, 1);

        if (testing)
        {
            vec3 pixel2 =texelFetch(testTexture, ivec2(pixelCoord), 0).rgb;
            testPixel = vec4(pixel2, 1);
        }
    }

    vec3 lightColor = vec3(1.0);

    vec3 normal = fNormal;
    normal /= length(fNormal);

    vec3 color;
    float coefficientD, coefficientS, n;

    vec3 c1, c2, c3;
    float kd1, kd2, kd3;
    float ks1, ks2, ks3;
    float shininess1, shininess2, shininess3;
    float d1, d2, d3, dF, s1, s2, s3, sF;
    bool p1 = false, p2 = false, p3 = false;
    bool sp1 = false, sp2 = false, sp3 = false;

    // get color and coefficients of verticesIndices[0], verticesIndices[1], verticesIndices[2]
    getData(verticesIndices[0], c1, kd1, ks1, shininess1, p1);
    getData(verticesIndices[1], c2, kd2, ks2, shininess2, p2);
    getData(verticesIndices[2], c3, kd3, ks3, shininess3, p3);

    vec3 currentLight = light;
    if (hasPhoto)
        currentLight = vec3(lightPos[3*lightIndex], lightPos[3*lightIndex+1], lightPos[3*lightIndex+2]);

    if (testing)
        currentLight = testLight;

    if (hasPhoto == false || aligning)
    {
        c1 = vec3(1);
        c2 = vec3(1);
        c3 = vec3(1);
        currentLight = vec3(0, 0, 1);
    }

    vec4 color1 = calculateColor(v1, n1, c1, lightColor, currentLight, kd1, ks1, shininess1, d1, s1);
    vec4 color2 = calculateColor(v2, n2, c2, lightColor, currentLight, kd2, ks2, shininess2, d2, s2);
    vec4 color3 = calculateColor(v3, n3, c3, lightColor, currentLight, kd3, ks3, shininess3, d3, s3);

    vec4 dColor1 = color1;
    vec4 dColor2 = color2;
    vec4 dColor3 = color3;

    if (p1 && !aligning)
        color1 = calculatePolyColor(verticesIndices[0], n1, currentLight, v1);
    if (p2 && !aligning)
        color2 = calculatePolyColor(verticesIndices[1], n2, currentLight, v2);
    if (p3 && !aligning)
        color3 = calculatePolyColor(verticesIndices[2], n3, currentLight, v3);


    vec4 finalColor = baricentricCoordinates.x * color1 + baricentricCoordinates.y * color2 + baricentricCoordinates.z * color3;
    vec4 dFinalColor = baricentricCoordinates.x * dColor1 + baricentricCoordinates.y * dColor2 + baricentricCoordinates.z * dColor3;
    coefficientD = baricentricCoordinates.x * kd1 + baricentricCoordinates.y * kd2 + baricentricCoordinates.z * kd3;
    coefficientS = baricentricCoordinates.x * ks1 + baricentricCoordinates.y * ks2 + baricentricCoordinates.z * ks3;

    n = baricentricCoordinates.x * shininess1 + baricentricCoordinates.y * shininess2 + baricentricCoordinates.z * shininess3;

    dF = baricentricCoordinates.x * d1 + baricentricCoordinates.y * d2 + baricentricCoordinates.z * d3;
    sF = baricentricCoordinates.x * s1 + baricentricCoordinates.y * s2 + baricentricCoordinates.z * s3;

    vec3 baseColor = baricentricCoordinates.x * c1 + baricentricCoordinates.y * c2 + baricentricCoordinates.z * c3;
    vec4 baseColor4 = vec4(baseColor, 1);

//    if (baricentricCoordinates.x < 0.01 || baricentricCoordinates.y < 0.01 || baricentricCoordinates.z < 0.01)
//        finalColor = vec4(0);
//    else
//        finalColor = vec4(1);

    vec3 eye  = -normalize(fVert.xyz);

//    if (lighted[fIndex] < 0 && !aligning)
//        finalColor = vec4(0);
//        finalColor *= 0.1;
//       discard;

    vec3 pixelDifference;
    if (useLAB)
        pixelDifference = rgbToLab(texture.xyz) - rgbToLab(finalColor.xyz);
    else
        pixelDifference = texture.xyz - finalColor.xyz;

    if (!isnan(pow(length(pixelDifference), 2)) && !isinf(pow(length(pixelDifference), 2)))
    {
        if (dot(eye, normal) <= 0.5)
        {
            error[fIndex] = -1;
        }
        else
        {
            if (lighted[fIndex] > 0)
            {
                error[fIndex] = pow(length(pixelDifference),2);
            }
            else
                error[fIndex] = 0;
        }
    }

    if (renderDifferP || renderDifferM || renderPixel)
    {
        if (renderDifferP && lighted[fIndex] > 0)
        {
            fragColor = texture - finalColor;
        }
        if (renderDifferM && lighted[fIndex] > 0)
            fragColor = finalColor - texture;
        if (renderPixel)
            fragColor = texture;
    }
    else
    {
        fragColor = finalColor;

        if (testing)
            fragColor = finalColor/* - testPixel*/;

//        if (fDepth > 0.999765)
//            fragColor = vec4(fDepth);

//        fragColor = vec4(abs(sF));
//        if (dF < 0.2)
//            fragColor = vec4(1, 0, 0, 1);

//        if (fDistantNeighbor > 0)
//            fragColor += vec4(0, 1, 0, 1);
    }
}


vec4 calculateColor(in vec4 v, in vec3 n, in vec3 color, in vec3 lightColor, in vec3 light, in float kd, in float ks, in float shininess, inout float d, inout float s)
{
    vec3 posLight = normalize(light);
    vec3 lightDirection = lightModelMatrix * posLight;
    lightDirection = normalize(lightDirection);

    vec3 l = lightDirection;
    vec3 reflection = reflect(-l, n);
    vec3 eye  = normalize(-v.xyz);

    float diffuse = /*max(*/dot(n, l)/*, 0)*/;
    float specular = /*max(*/dot(reflection, eye)/*, 0)*/;
    d = diffuse;
    s = specular;


    vec3 diffuseTerm = (color*diffuse);
    vec3 specularTerm = lightColor;

    vec3 diffuseColor = kd*diffuseTerm;
    vec3 specularColor = ks*specularTerm*pow(specular, shininess);

    vec4 finalColor = vec4(diffuseColor + specularColor, 1);
    return finalColor;
}

vec4 calculatePolyColor(in uint index, in vec3 n, in vec3 light, in vec4 v)
{
    vec3 posLight = normalize(light);
    vec3 lightDirection = lightModelMatrix * posLight;
    lightDirection = normalize(lightDirection);

    vec3 l = lightDirection;
    float ang = dot(n, l);
    vec3 e = -v.xyz;
    e = normalize(e);
    vec3 r = reflect(-l, n);
    r = normalize(r);
    float s = dot(r, e);

//    float a = acos(d);
    float a = acos(s);
    vec4 polyR = vec4(polynomial[12*index], polynomial[12*index+1], polynomial[12*index+2], polynomial[12*index+3]);
    vec4 polyG = vec4(polynomial[12*index+4], polynomial[12*index+5], polynomial[12*index+6], polynomial[12*index+7]);
    vec4 polyB = vec4(polynomial[12*index+8], polynomial[12*index+9], polynomial[12*index+10], polynomial[12*index+11]);

    vec4 color = vec4(1);
    color.r = polyR.x*pow(a, 3) + polyR.y*pow(a, 2) + polyR.z*a + polyR.w;
    color.g = polyG.x*pow(a, 3) + polyG.y*pow(a, 2) + polyG.z*a + polyG.w;
    color.b = polyB.x*pow(a, 3) + polyB.y*pow(a, 2) + polyB.z*a + polyB.w;

//    if (ang < 0)
//        color = vec4(0);
    return color;
}


void getData(in uint index, inout vec3 color, inout float kd, inout float ks, inout float shininess, inout bool hasPolynomial)
{
    uint indexR = 4*index;
    uint indexG = indexR + 1;
    uint indexB = indexR + 2;
    uint indexT = indexR + 3;

    color = vec3(colors[indexR], colors[indexG], colors[indexB]);
//    color = vec3(curve[5*size*index+1], curve[5*size*index+2], curve[5*size*index+3]);
    color /= colors[indexT];

    kd = 1;
    ks = 0;
    shininess = baseShininess;

    if (polynomial[12*index] != 0 || polynomial[12*index+1] != 0 || polynomial[12*index+2] != 0 || polynomial[12*index+3] != 0)
        hasPolynomial = true;
}


vec3 rgbToXyz(in vec3 c)
{
    vec3 convertedColor;
    mat3 matrixConversion;
    float constant = 1/0.17697;

    matrixConversion[0][0] = 0.49;
    matrixConversion[0][1] = 0.31;
    matrixConversion[0][2] = 0.20;
    matrixConversion[1][0] = 0.17697;
    matrixConversion[1][1] = 0.81240;
    matrixConversion[1][2] = 0.01063;
    matrixConversion[2][0] = 0.00;
    matrixConversion[2][1] = 0.01;
    matrixConversion[2][2] = 0.99;

    matrixConversion *= constant;
    convertedColor = matrixConversion*c;
    return convertedColor;
}

vec3 xyzToRgb(in vec3 c)
{
    vec3 convertedColor;
    mat3 matrixConversion;

    matrixConversion[0][0] = 0.41847;
    matrixConversion[0][1] = -0.15866;
    matrixConversion[0][2] = -0.082835;
    matrixConversion[1][0] = -0.091169;
    matrixConversion[1][1] = 0.25243;
    matrixConversion[1][2] = 0.015708;
    matrixConversion[2][0] = 0.00092090;
    matrixConversion[2][1] = -0.0025498;
    matrixConversion[2][2] = 0.17860;

    convertedColor = matrixConversion*c;
    return convertedColor;
}

vec3 xyzToLab(in vec3 c)
{
    vec3 convertedColor;
    float xw, yw, zw;
    xw = 0.9642;
    yw = 1.0;
    zw = 0.8249;

    convertedColor.x = 116*f(c.y/yw) - 16;
    convertedColor.y = 500*(f(c.x/xw) - f(c.y/yw));
    convertedColor.z = 200*(f(c.y/yw) - f(c.z/zw));

    return convertedColor;
}

vec3 labToXyz(in vec3 c)
{
    vec3 convertedColor;
    float xw, yw, zw;
    xw = 0.9642;
    yw = 1.0;
    zw = 0.8249;
    float constant = (1.0/116.0) *(c.x + 16);

    convertedColor.y = yw*fi(constant);
    convertedColor.x = xw*fi(constant + c.y/500.0);
    convertedColor.z = zw*fi(constant - c.z/200.0);

    return convertedColor;
}

vec3 rgbToLab(in vec3 c)
{
    vec3 colorXYZ = rgbToXyz(c);
    return xyzToLab(colorXYZ);
}

vec3 labToRgb(in vec3 c)
{
    vec3 colorXYZ = labToXyz(c);
    return xyzToRgb(colorXYZ);
}

float f(in float t)
{
    if (t > 0.0088564516790)
        return pow(t, 1.0/3.0);
    else
        return (1.0/3.0*pow(29.0/6.0, 2)*t + 4.0/29.0);
}

float fi(in float t)
{
    if (t > 0.2068965517241)
        return pow(t, 3);
    else
        return (3*pow(6.0/29.0, 2)*(t-4.0/29.0));
}
