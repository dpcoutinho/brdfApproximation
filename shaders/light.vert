#version 430

in vec3 position;
in float index;

out vec4 vPosition;
flat out uint vIndex;
out float depth;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform bool renderBonn;
uniform mat4 cvToGl;
uniform vec2 k;
uniform vec2 p;
uniform ivec2 imageSize;

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};


void main()
{
    int iIndex = int(index);
    vec4 pos = vec4(position, 1);
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
    vec4 vert = modelViewMatrix*pos;

    vPosition = modelViewProjectionMatrix*pos;
    gl_Position = vPosition;
    vIndex = iIndex;
    float far=gl_DepthRange.far; float near=gl_DepthRange.near;

    vec4 eye_space_pos = modelViewMatrix * pos;
    vec4 clip_space_pos = projectionMatrix * eye_space_pos;

    float ndc_depth = clip_space_pos.z / clip_space_pos.w;

    depth = (((far-near) * ndc_depth) + near + far) / 2.0;

    if (renderBonn)
    {
        vec4 vproj = vert;
        vproj = vproj/vproj.z;

        float r2 = vproj.x*vproj.x + vproj.y*vproj.y;

        // apply distortion
        vec2 vcorrected;
        vcorrected.x = vproj.x * (1.0 + k.x*r2 + k.y*r2*r2) + 2.0*p.x*vproj.x*vproj.y + p.y*(r2 + 2.0*vproj.x*vproj.x);
        vcorrected.y = vproj.y * (1.0 + k.x*r2 + k.y*r2*r2) + p.x*(r2 + 2.0*vproj.y*vproj.y) + 2.0*p.y*vproj.x*vproj.y;

        // project using matrix K
        vproj.x = projectionMatrix[0].x*vcorrected.x + projectionMatrix[2].x;
        vproj.y = projectionMatrix[1].y*vcorrected.y + projectionMatrix[2].y;

        // OpenCV explanation finishes here
        // now put coordinates in range [-1,+1] and let OpenGL do the rest
        vproj.xy /= vec2(imageSize.xy);

        // OBS: invert y axis since OpenGL origin is top left and OpenCV is bottom left
        vproj.x = vproj.x*2.0 - 1.0;
        vproj.y = (1.0 - vproj.y)*2.0 - 1.0;
        vproj.z = vert.z / 1000.0; // divide by some arbitraty far plane constant
        vproj.w = 1.0;

        depth = vproj.z;

        gl_Position = vproj;
    }
}
