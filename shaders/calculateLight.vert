#version 430

in vec3 position;
in vec3 normal;
in float index;

flat out vec4 vert;
flat out vec4 vPosition;
flat out vec3 vNormal;
flat out uint vIndex;
flat out float depth;

uniform ivec2 viewportSize;
uniform float blendFactor;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 trackballMatrix;
uniform mat4 centerTranslation;
uniform mat4 iCenterTranslation;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};



void main()
{
    vIndex = uint(index);
    vec4 pos = vec4(position, 1);
    vec4 aux = iCenterTranslation*trackballMatrix*centerTranslation*pos;
    vert = modelViewMatrix*aux;
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix*trackballMatrix));

    vPosition = modelViewProjectionMatrix*aux;
    gl_Position = vPosition;
    vec3 inNormal = normal;
    vNormal = normalize(vec3(normalMatrix * vec4(inNormal,0.0)).xyz);

    float far=gl_DepthRange.far; float near=gl_DepthRange.near;

    vec4 eye_space_pos = modelViewMatrix * pos;
    vec4 clip_space_pos = projectionMatrix * eye_space_pos;

    float ndc_depth = clip_space_pos.z / clip_space_pos.w;

    depth = (((far-near) * ndc_depth) + near + far) / 2.0;

    gl_PointSize = 0.45;
}
