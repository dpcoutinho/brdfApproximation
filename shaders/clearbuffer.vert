#version 430

in vec3 position;

out vec4 vPosition;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform sampler2DArray textureArray;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout( std430, binding=4 ) buffer polynomialBuffer
{
    float polynomial[];
};

layout ( std430, binding=6 ) buffer feedback
{
    float error[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};

layout( std430, binding=8 ) buffer angleBuffer
{
    float angles[];
};

layout( std430, binding=9 ) buffer renderInPhotos
{
    double photos[];
};

layout( std430, binding=11 ) buffer fillBuffer
{
    float fill[];
};


#include shaders/utils.vert


void main(void)
{

    vec4 pos = vec4(position, 1.0);
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix));

    vPosition = modelViewProjectionMatrix*pos;
    gl_Position = vPosition;

    int iIndex = gl_VertexID;
    colors[4*iIndex] = 0;
    colors[4*iIndex+1] = 0;
    colors[4*iIndex+2] = 0;
    colors[4*iIndex+3] = 0;

    error[iIndex] = 0;

    lighted[iIndex] = 1;

    angles[5*iIndex] = -1;
    angles[5*iIndex+1] = 0;
    angles[5*iIndex+2] = 0;
    angles[5*iIndex+3] = 0;
    angles[5*iIndex+4] = 0;

    photos[iIndex] = 0;

    fill[iIndex] = 0;

    for (int i = 0; i < size; i++)
    {
        curve[5*size*iIndex + (5*i)] = 10;
        curve[5*size*iIndex + (5*i)+1] = 0;
        curve[5*size*iIndex + (5*i)+2] = 0;
        curve[5*size*iIndex + (5*i)+3] = 0;
        curve[5*size*iIndex + (5*i)+4] = 0;
    }

    for (int i = 0; i < 12; i++)
    {
        polynomial[12*iIndex + i] = 0;
    }

}
