
float PI = 3.14159265358979323846264;
float sigma = 1;
float sigmaI = 10;
float sigmaC = 0.5;
float sigmaP = 20;
float sigmaN = 5;
int filterStep = 7;

vec4 bilateralFilter(in vec2 pixelCoord, in int windowSize)
{
    vec4 colorPixel = texelFetch(textureArray, ivec3(pixelCoord, 0), 0);
    vec4 positionPixel = texelFetch(vertexMap, ivec2(pixelCoord), 0);
    vec4 normalPixel = texelFetch(normalMap, ivec2(pixelCoord), 0);
    vec4 result = vec4(0.0);
    float weightSum = 0;

    if (colorPixel == vec4(0.0))
        discard;
    bool hasNoDiff = false;

    for (int i = -windowSize/2; i <= windowSize/2; i++)
    {
        for (int j = -windowSize/2; j <= windowSize/2; j++)
        {
            vec2 offset = ivec2(i, j);
            vec4 colorPixelAux = texelFetch(textureArray, ivec3(pixelCoord + offset, 0), 0);
            vec4 positionPixelAux = texelFetch(vertexMap, ivec2(pixelCoord+offset), 0);
            vec4 normalPixelAux = texelFetch(normalMap, ivec2(pixelCoord+offset), 0);

            vec4 pixelDiff = colorPixel - colorPixelAux;
            vec4 pixelDistance = positionPixel - positionPixelAux;
            float distance = length(pixelDistance);
            float angle = acos(dot(normalPixel, normalPixelAux));

            float alpha = 1.0/(2.0*PI*sigma*sigma);

            float weight = alpha*exp(
                        -(offset.x*offset.x + offset.y*offset.y)/(2*sigmaI*sigmaI)
                        -(length(pixelDiff)*length(pixelDiff))/(2*sigmaC*sigmaC)
                        -(distance*distance)/(2*sigmaP*sigmaP)
//                        -(angle*angle)/(2*sigmaN*sigmaN)
                        );
            result += colorPixelAux*weight;
            weightSum += weight;
        }
    }
    result /= weightSum;
    return result;
}

float trilateralWeight(in vec2 pixelCoord, in vec2 offset)
{
    vec4 colorPixel = texelFetch(textureArray, ivec3(pixelCoord, 0), 0);
    vec4 positionPixel = texelFetch(vertexMap, ivec2(pixelCoord), 0);
    vec4 normalPixel = texelFetch(normalMap, ivec2(pixelCoord), 0);

    if (colorPixel == vec4(0.0))
        discard;

    vec4 colorPixelAux = texelFetch(textureArray, ivec3(pixelCoord + offset, 0), 0);
    vec4 positionPixelAux = texelFetch(vertexMap, ivec2(pixelCoord+offset), 0);
    vec4 normalPixelAux = texelFetch(normalMap, ivec2(pixelCoord+offset), 0);

    vec4 pixelDiff = colorPixel - colorPixelAux;
    vec4 pixelDistance = positionPixel - positionPixelAux;
    float distance = length(pixelDistance);
    float angle = acos(dot(normalPixel, normalPixelAux));

    float alpha = 1.0/(2.0*PI*sigma*sigma);

    float weight = alpha*exp(
                -(offset.x*offset.x + offset.y*offset.y)/(2*sigmaI*sigmaI)
                -(length(pixelDiff)*length(pixelDiff))/(2*sigmaC*sigmaC)
                -(distance*distance)/(2*sigmaP*sigmaP)
//                        -(angle*angle)/(2*sigmaN*sigmaN)
                );

    return weight;
}

vec3 rgbToHsv(in vec3 c)
{
    vec3 cHsv;
    float min, max, delta;
    int index = 0;
    if (c.r > c.g && c.r < c.b)
    {
        min = c.r;
    }
    if (c.g < c.r && c.g < c.b)
    {
        min = c.g;
    }
    if (c.b < c.r && c.b < c.g)
    {
        min = c.b;
    }
    if (c.r > c.g && c.r > c.b)
    {
        max = c.r;
        index = 0;
    }
    if (c.g > c.r && c.g > c.b)
    {
        max = c.g;
        index = 1;
    }
    if (c.b > c.r && c.b > c.g)
    {
        max = c.b;
        index = 2;
    }
    delta = max - min;
    cHsv.b = max;

    if (max != 0)
        cHsv.g = delta/max;
    else
    {
        cHsv.g = 0;
        cHsv.r = -1;
        return cHsv;
    }

    if (index == 0)
    {
        cHsv.r = mod((c.g - c.b)/delta, 6);
    }
    else
    {
        if (index == 1)
        {
            cHsv.r = 2 + (c.b - c.r)/delta;
        }
        else
        {
            cHsv.r = 4 + (c.r - c.g)/delta;
        }
    }
    if (cHsv.r < 0)
        cHsv.r += 6;

    return cHsv;
}

vec3 hsvToRgb(in vec3 c)
{
    vec3 cRgb;
    float vs = c.b*c.g;
    float m = c.b - vs;
    float x = vs*(1 - abs(mod(c.r,2) - 1));

    vec3 rgb1;
    int h = int(c.r);
    switch(h)
    {
        case 0:
            rgb1 = vec3(vs, x, 0);
            break;
        case 1:
            rgb1 = vec3(x, vs, 0);
            break;
        case 2:
            rgb1 = vec3(0, vs, x);
            break;
        case 3:
            rgb1 = vec3(0, x, vs);
            break;
        case 4:
            rgb1 = vec3(x, 0, vs);
            break;
        case 5:
            rgb1 = vec3(vs, 0, x);
            break;
    }

    cRgb = rgb1;
    cRgb.r += m;
    cRgb.g += m;
    cRgb.b += m;

    return cRgb;
}

//vec3 rgbToXyz(in vec3 c)
//{
//    vec3 convertedColor;
//    mat3 matrixConversion;
//    float constant = 1/0.17697;

//    matrixConversion[0][0] = 0.49;
//    matrixConversion[0][1] = 0.31;
//    matrixConversion[0][2] = 0.20;
//    matrixConversion[1][0] = 0.17697;
//    matrixConversion[1][1] = 0.81240;
//    matrixConversion[1][2] = 0.01063;
//    matrixConversion[2][0] = 0.00;
//    matrixConversion[2][1] = 0.01;
//    matrixConversion[2][2] = 0.99;

//    matrixConversion *= constant;
//    convertedColor = matrixConversion*c;
//    return convertedColor;
//}

//vec3 xyzToRgb(in vec3 c)
//{
//    vec3 convertedColor;
//    mat3 matrixConversion;

//    matrixConversion[0][0] = 0.41847;
//    matrixConversion[0][1] = -0.15866;
//    matrixConversion[0][2] = -0.082835;
//    matrixConversion[1][0] = -0.091169;
//    matrixConversion[1][1] = 0.25243;
//    matrixConversion[1][2] = 0.015708;
//    matrixConversion[2][0] = 0.00092090;
//    matrixConversion[2][1] = -0.0025498;
//    matrixConversion[2][2] = 0.17860;

//    convertedColor = matrixConversion*c;
//    return convertedColor;

//}

//vec3 xyzToLab(in vec3 c)
//{
//    vec3 convertedColor;
//    float xw, yw, zw;
//    xw = 0.9642;
//    yw = 1.0;
//    zw = 0.8249;

//    convertedColor.x = 116*f(c.y/yw) - 16;
//    convertedColor.y = 500*(f(c.x/xw) - f(c.y/yw));
//    convertedColor.z = 200*(f(c.y/yw) - f(c.z/zw));

//    return convertedColor;
//}

//vec3 labToXyz(in vec3 c)
//{
//    vec3 convertedColor;
//    float xw, yw, zw;
//    xw = 0.9642;
//    yw = 1.0;
//    zw = 0.8249;
//    float constant = (1.0/116.0) *(c.x + 16);

//    convertedColor.y = yw*fi(constant);
//    convertedColor.x = xw*fi(constant + c.y/500.0);
//    convertedColor.z = zw*fi(constant - c.z/200.0);

//    return convertedColor;
//}

//vec3 rgbToLab(in vec3 c)
//{
//    vec3 colorXYZ = rgbToXyz(c);
//    return xyzToLab(colorXYZ);
//}

//vec3 labToRgb(in vec3 c)
//{
//    vec3 colorXYZ = labToXyz(c);
//    return xyzToRgb(colorXYZ);
//}

//float f(in float t)
//{
//    if (t > 0.0088564516790)
//        return pow(t, 1.0/3.0);
//    else
//        return ((1.0/3.0)*pow(29.0/6.0, 2)*t + 4.0/29.0);
//}

//float fi(in float t)
//{
//    if (t > 0.2068965517241)
//        return pow(t, 3);
//    else
//        return (3*pow(6.0/29.0, 2)*(t-4.0/29.0));
//}


