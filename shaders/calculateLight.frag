#version 430

flat in vec4 vert;
flat in vec4 vPosition;
flat in vec3 vNormal;
flat in uint vIndex;
flat in float depth;

uniform vec3 light;

uniform bool analyzingLight;
uniform bool discardBadVertices;
uniform vec3 currentLight;

uniform sampler2D depthTexture;
uniform sampler2DArray textureArray;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform ivec2 viewportSize;
uniform int numPhotos;

uniform mat3 lightModelMatrix;

uniform ivec2 totalTextureSize;
uniform ivec2 textureBlockCoord;
uniform bool useTextureBlock;

out vec4 fragColor;

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};


layout( std430, binding=8 ) buffer Anglebuffer
{
    float angles[];
};

layout( std430, binding=11 ) buffer printBuffer
{
    float print[];
};


vec3 rotationAxis(vec3 v1, vec3 v2);

vec3 rotate(float angle, vec3 v, vec3 axis);

#include shaders/utils.vert

void main()
{
    ivec3 texSize = textureSize(textureArray, 0);
    vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
    vec2 pixelCoord = texCoord*texSize.xy;
    if (useTextureBlock)
    {
        vec2 globalPixelCoord = texCoord * totalTextureSize;
        if (globalPixelCoord.x < textureBlockCoord.x || globalPixelCoord.x >= textureBlockCoord.x + texSize.x ||
                globalPixelCoord.y < textureBlockCoord.y || globalPixelCoord.y >= textureBlockCoord.y + texSize.y)
            discard;
        else
            pixelCoord = globalPixelCoord - textureBlockCoord;
    }

    vec3 depthPixel = texture(depthTexture, texCoord).rgb;


    fragColor = vec4(1);

    int i, j;
    float aux;

    if ( abs(depth - depthPixel.x) > 1e-6 || isnan(vNormal.x) || isnan(vNormal.y) || isnan(vNormal.z) && !discardBadVertices)
    {
        angles[5*vIndex] = 0;
        discard;
    }
    else
    {
        vec3 eye = -vert.xyz;
        eye = normalize(eye);
        vec3 reflection = reflect(-light, vNormal);
        reflection = normalize(reflection);
        float en = dot(eye, vNormal);

        if (en <= 0.5 && !discardBadVertices)
        {
            angles[5*vIndex] = 0;
        }
        else
        {
            float angleEN = acos(en);

            float points[size+2];
            float dist[size+1];
            points[0] = 0;
            points[1] = (PI)/2 + angleEN;
            int numSamples = 0;
            for (i = 0; i < size; i++)
            {
                points[i+2] = curve[5*size*vIndex + 5*i];
                if (points[i+2] < 10)
                    numSamples++;
            }
            for (i = 0; i < size+1; i++)
            {
                bool changed = false;
                for (j = 1; j < size+2-i; j++)
                {
                    if (points[j] < points[j-1])
                    {
                        aux = points[j];
                        points[j] = points[j-1];
                        points[j-1] = aux;
                        changed = true;
                    }
                }
                if (!changed)
                    break;
            }
            float maxDist = 0;
            int indMax = -1;
            int lastIndex = -1;
            for (i = 0; i < size;i++)
            {
                if (points[i+1] > 9)
                {
                    dist[i] = 0;
                    if (lastIndex == -1)
                        lastIndex = i-1;
                }
                else
                    dist[i] = points[i+1] - points[i];
                if (dist[i] > maxDist)
                {
                    maxDist = dist[i];
                    indMax = i;
                }
            }
            if (analyzingLight)
            {
                if (lighted[vIndex] < 0)
                    angles[5*vIndex] = 0;
                else
                {
                    reflection = reflect(-currentLight, vNormal);
                    reflection = normalize(reflection);

                    float currentAngle = acos(dot(reflection, eye));

                    int indCurrent = 0;
                    for (i = 0; i < size+2; i++)
                        if (currentAngle > points[i])
                            indCurrent = i;

                    float dist1 = dist[indCurrent];
                    float dist2 = dist[indCurrent+1];
                    dist1 = abs(currentAngle - dist1);
                    dist2 = abs(currentAngle - dist2);
                    float minDist = min(dist1, dist2);

                    angles[5*vIndex] = 1;
                    angles[5*vIndex+4] = minDist;
                }
            }
            else
            {
                if (discardBadVertices)
                {
//                    if (maxDist > ((PI)/2 + angleEN)/3 || numSamples < 6)
//                        curve[5*size*vIndex] = -1;
                }
                else
                {
                    float angle = 0;
                    if (indMax > -1)
                    {
                        angle = points[indMax] + dist[indMax]/2;
                    }
                    if (indMax == 0)
                        angle = 0;
                    if (indMax == lastIndex)
                        angle = (PI)/2 + angleEN;

                    vec3 axis = rotationAxis(vNormal, eye);
                    vec3 rr = rotate(angle, eye, axis);
                    vec3 newLight = reflect(-rr, vNormal);

                    if (newLight.z < 0)
                    {
                        angles[5*vIndex] = 0;
                    }
                    else
                    {
                        angles[5*vIndex] = 1;
                        angles[5*vIndex+1] = newLight.x;
                        angles[5*vIndex+2] = newLight.y;
                        angles[5*vIndex+3] = newLight.z;
                        angles[5*vIndex+4] = maxDist;
                    }
                }
            }


    //            vec3 newReflection = reflect(-newLight, vNormal);
    //            if (abs(acos(dot(newReflection, eye)) - angle) > 0.00001)
    //            {
    //                newLight = vec3(1, 0, 0);
    //            }
    //            else
    //                newLight = vec3(0, 1, 0);


        }
    }
    memoryBarrier();
}



vec3 rotationAxis(vec3 v1, vec3 v2)
{
    vec3 v2N = normalize(v2);
    vec3 v1Projection = dot(v1, v2N)*v2N;
    vec3 vOrtho = v1 - v1Projection;
    vec3 vOrthoN = normalize(vOrtho);

    vec3 axis = cross(v2N, vOrthoN); // HAVE TO WATCH OUT FOR NUMERICAL ERROR
    return normalize(axis);
}

vec3 rotate(float angle, vec3 v, vec3 axis)
{
    vec3 axisN = normalize(axis);
    vec4 v4 = vec4(v, 0);

    float c = cos(angle);
    float s = sin(angle);
    float cI = (1-c);

    float rx, ry, rz;
    rx = axisN.x;
    ry = axisN.y;
    rz = axisN.z;

    vec4 c1, c2, c3, c4;
    c1 = vec4(c+cI*rx*rx, cI*rx*ry+rz*s, cI*rx*rz-ry*s, 0);
    c2 = vec4(cI*rx*ry-rz*s, c+cI*ry*ry, cI*ry*rz+rx*s, 0);
    c3 = vec4(cI*rx*rz+ry*s, cI*ry*rz-rx*s, c+cI*rz*rz, 0);
    c4 = vec4(0, 0, 0, 1);
    mat4 r = mat4(c1, c2, c3, c4);

    vec4 rV4 = r*v4;
    vec3 rV = vec3(rV4.x, rV4.y, rV4.z);
    return rV;
}

