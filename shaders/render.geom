#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec4 vert[];
in vec4 vPosition[];
in vec3 vNormal[];
flat in uint vIndex[];
flat in float depth[];
flat in int distantNeighbor[];

out vec4 fVert;
out vec4 fPosition;
out vec3 fNormal;
out vec3 baricentricCoordinates;
flat out uint fIndex;
flat out float fDepth;
flat out uvec3 verticesIndices;
flat out vec3 n1;
flat out vec3 n2;
flat out vec3 n3;
flat out vec4 v1;
flat out vec4 v2;
flat out vec4 v3;
flat out int fDistantNeighbor;

void main()
{
    gl_Position = gl_in[0].gl_Position;
    fVert = vert[0];
    fPosition = vPosition[0];
    fNormal = vNormal[0];
    fIndex = vIndex[0];
    fDepth = depth[0];
    fDistantNeighbor = distantNeighbor[0];
    baricentricCoordinates = vec3(1, 0, 0);
    verticesIndices = uvec3(vIndex[0], vIndex[1], vIndex[2]);
    n1 = vNormal[0];
    n2 = vNormal[1];
    n3 = vNormal[2];
    v1 = vert[0];
    v2 = vert[1];
    v3 = vert[2];
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    fVert = vert[1];
    fPosition = vPosition[1];
    fNormal = vNormal[1];
    fIndex = vIndex[1];
    fDepth = depth[1];
    fDistantNeighbor = distantNeighbor[1];
    baricentricCoordinates = vec3(0, 1, 0);
    verticesIndices = uvec3(vIndex[0], vIndex[1], vIndex[2]);
    n1 = vNormal[0];
    n2 = vNormal[1];
    n3 = vNormal[2];
    v1 = vert[0];
    v2 = vert[1];
    v3 = vert[2];
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    fVert = vert[2];
    fPosition = vPosition[2];
    fNormal = vNormal[2];
    fIndex = vIndex[2];
    fDepth = depth[2];
    fDistantNeighbor = distantNeighbor[2];
    baricentricCoordinates = vec3(0, 0, 1);
    verticesIndices = uvec3(vIndex[0], vIndex[1], vIndex[2]);
    n1 = vNormal[0];
    n2 = vNormal[1];
    n3 = vNormal[2];
    v1 = vert[0];
    v2 = vert[1];
    v3 = vert[2];
    EmitVertex();


};
