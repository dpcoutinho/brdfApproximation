#version 430

in vec3 position;
in vec3 normal;
in float index;

flat out vec4 vert;
flat out vec3 vNormal;
flat out uint vIndex;
flat out float depth;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 trackballMatrix;
uniform mat4 centerTranslation;
uniform mat4 iCenterTranslation;

uniform ivec2 viewportSize;

uniform bool renderBonn;
uniform mat4 cvToGl;
uniform vec2 k;
uniform vec2 p;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};


void main()
{
    vIndex = uint(index);
    vec4 pos = vec4(position, 1);
    vert = modelViewMatrix*pos;
    mat4 modelViewProjectionMatrix = projectionMatrix * modelViewMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix));

    gl_Position = modelViewProjectionMatrix*pos;
    vec3 inNormal = normal;
    vNormal = normalize(vec3(normalMatrix * vec4(inNormal,0.0))).xyz;

    float far=gl_DepthRange.far; float near=gl_DepthRange.near;

    vec4 eye_space_pos = modelViewMatrix * pos;
    vec4 clip_space_pos = projectionMatrix * eye_space_pos;

    float ndc_depth = clip_space_pos.z / clip_space_pos.w;

    depth = (((far-near) * ndc_depth) + near + far) / 2.0;

    if (renderBonn)
    {

        mat4 invModelViewMatrix = cvToGl*modelViewMatrix;
        normalMatrix = transpose(inverse(invModelViewMatrix));
        vec4 normal4 = vec4(normal, 0);
        vNormal = normalize(vec3(normalMatrix * vec4(normal4)).xyz);

        vec4 vproj = vert;
        vproj = vproj/vproj.z;

        float r2 = vproj.x*vproj.x + vproj.y*vproj.y;

        // apply distortion
        vec2 vcorrected;
        vcorrected.x = vproj.x * (1.0 + k.x*r2 + k.y*r2*r2) + 2.0*p.x*vproj.x*vproj.y + p.y*(r2 + 2.0*vproj.x*vproj.x);
        vcorrected.y = vproj.y * (1.0 + k.x*r2 + k.y*r2*r2) + p.x*(r2 + 2.0*vproj.y*vproj.y) + 2.0*p.y*vproj.x*vproj.y;

        // project using matrix K
        vproj.x = projectionMatrix[0].x*vcorrected.x + projectionMatrix[2].x;
        vproj.y = projectionMatrix[1].y*vcorrected.y + projectionMatrix[2].y;

        // OpenCV explanation finishes here
        // now put coordinates in range [-1,+1] and let OpenGL do the rest
        vproj.xy /= vec2(viewportSize.xy);

        // OBS: invert y axis since OpenGL origin is top left and OpenCV is bottom left
        vproj.x = vproj.x*2.0 - 1.0;
        vproj.y = (1.0 - vproj.y)*2.0 - 1.0;
        vproj.z = vert.z / 1000.0; // divide by some arbitraty far plane constant
        vproj.w = 1.0;

        depth = vproj.z;

        gl_Position = vproj;
    }


    gl_PointSize = 0.45;


}
