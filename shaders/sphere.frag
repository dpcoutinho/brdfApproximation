#version 430

in vec4 vert;
in vec4 vPosition;
in vec4 vNormal;
in vec4 light;

out vec4 fragColor;

uniform int startingLight;
uniform int finalLight;
uniform int numLights;
uniform bool drawSuggestedLight;
uniform vec3 suggestedLight;

uniform vec3 currentLight;
uniform mat3 lightModelMatrix;


layout( std430, binding=10 ) buffer LightPositions
{
    float lightPos[];
};

void main(void)
{
//    vec3 lN = light.xyz / length(light.xyz);
    vec3 z = vec3(0, 0, 1);
    float lightedValue = 0;
    int num = 0;

    vec3 light = lightModelMatrix*currentLight;
    light = normalize(light);

    int indexFinal = numLights;
    if (finalLight > 0)
        indexFinal = finalLight;
    for (int i = startingLight; i < indexFinal; i++)
    {
        vec3 auxLight = vec3(lightPos[3*i], lightPos[3*i+1], lightPos[3*i+2]);
        if (dot(vNormal.xyz, auxLight) > 0.99)
        {
            lightedValue += max(dot(vNormal.xyz, auxLight),0);
            num += 1;
        }
    }
    lightedValue /= num;

    fragColor = 0.1*vec4(dot(vNormal.xyz, z));
    if (drawSuggestedLight)
    {
        if (dot(vNormal.xyz, suggestedLight) > 0.99)
            fragColor += pow(max(dot(vNormal.xyz, suggestedLight),0), 50)*vec4(1, 0, 0, 1);
    }
//    else
    {
        if (lightedValue > 0)
            fragColor += 0.8*pow(lightedValue, 20)*vec4(0, 1, 0, 1);
    }

//    if (dot(vNormal.xyz, suggestedLight) > 0.95)
        fragColor += pow(max(dot(vNormal.xyz, light),0), 50)*vec4(1, 1, 1, 1);
}
