#version 430

flat in vec4 vert;
flat in vec3 vNormal;
flat in uint vIndex;
flat in float depth;

uniform vec3 light;
uniform vec3 newLight;

uniform bool getPixel;
uniform bool deletePhoto;
uniform bool newPhoto;
uniform bool specularlyCovered;
uniform sampler2D depthTexture;
uniform sampler2D vertexMap;
uniform sampler2D normalMap;
uniform sampler2DArray textureArray;
uniform ivec2 viewportSize;
uniform int texIndex;
uniform int numPhotos;

uniform mat3 lightModelMatrix;

uniform vec3 colorWeights;
uniform bool useColorWeights;

uniform ivec2 totalTextureSize;
uniform ivec2 textureBlockCoord;
uniform bool useTextureBlock;

out vec4 fragColor;

layout( std430, binding=0 ) buffer ssbo
{
    float colors[];
};

layout( std430, binding=1 ) buffer positionBuffer
{
    float positions[];
};

layout( std430, binding=2 ) buffer normalBuffer
{
    float  normals[];
};

layout( std430, binding=3 ) buffer curveBuffer
{
    float curve[];
};

layout ( std430, binding=6 ) buffer feedback
{
    float error[];
};

layout( std430, binding=7 ) buffer lightBuffer
{
    float lighted[];
};

layout( std430, binding=8 ) buffer Anglebuffer
{
    float angles[];
};

layout( std430, binding=9 ) buffer renderInPhotos
{
    double photos[];
};

layout( std430, binding=10 ) buffer LightPositions
{
    float lightPos[];
};

layout( std430, binding=11 ) buffer printBuffer
{
    double print[];
};

#include shaders/utils.vert


void calculateCoefficients(in vec3 l, inout float diffuse, inout float specular, inout vec3 eye, inout vec3 reflection);


void main()
{
    vec3 lightColor = vec3(1.0);
    uint indexR = 4*vIndex;
    uint indexG = indexR + 1;
    uint indexB = indexR + 2;
    uint indexT = indexR+3;

    vec3 color = vec3(colors[indexR], colors[indexG], colors[indexB]);
    if (colors[indexT] > 0)
        color /= colors[indexT];

    ivec3 texSize = textureSize(textureArray, 0);
    vec2 texCoord = vec2((gl_FragCoord.xy)/vec2(viewportSize.xy));
    vec2 pixelCoord = texCoord*texSize.xy;
    if (useTextureBlock)
    {
        vec2 globalPixelCoord = texCoord * totalTextureSize;
        if (globalPixelCoord.x < textureBlockCoord.x || globalPixelCoord.x >= textureBlockCoord.x + texSize.x ||
                globalPixelCoord.y < textureBlockCoord.y || globalPixelCoord.y >= textureBlockCoord.y + texSize.y)
            discard;
        else
            pixelCoord = globalPixelCoord - textureBlockCoord;
    }

    vec3 pixel = texelFetch(textureArray, ivec3(pixelCoord, 0), 0).rgb;
    if (useColorWeights)
        pixel *= colorWeights;

    vec4 texColor = vec4(pixel, 1);

//    if (texColor.x >= 1 || texColor.y >= 1 || texColor.z >= 1)
//        discard;

    vec3 depthPixel = texture(depthTexture, texCoord).rgb;

    vec3 baseColor = color;
    float diffuse, specular;
    vec3 reflection, eye;
    calculateCoefficients(light, diffuse, specular, eye, reflection);

    vec3 diffuseColor = (baseColor*diffuse);

    vec3 newLightDirection = lightModelMatrix * newLight;
    newLightDirection = normalize(newLightDirection);

    if (newPhoto && lighted[vIndex] > 0)
        photos[vIndex] += pow(2, texIndex);


    if ( abs(depth - depthPixel.x) > 1e-4)
    {
        discard;
    }
    else
    {
        if (getPixel)
        {

            vec3 lightDirection = lightModelMatrix * light;
            lightDirection = normalize(lightDirection);
            float direct = dot(vNormal, lightDirection);
            float spec = dot(reflection, eye);
            spec = clamp(spec, -1, 1);

            if (lighted[vIndex] > 0/* && diffuse > 0.2*/)
            {
                colors[indexR] += texColor.r;
                colors[indexG] += texColor.g;
                colors[indexB] += texColor.b;
                colors[indexT] += 1;

                float pixelGray = 0.2126*pixel.r + 0.7152*pixel.g + 0.0722*pixel.b;
                curve[5*size*vIndex + 5*texIndex] = acos(spec);
                curve[5*size*vIndex + 5*texIndex+1] = pixel.r;
                curve[5*size*vIndex + 5*texIndex+2] = pixel.g;
                curve[5*size*vIndex + 5*texIndex+3] = pixel.b;
                curve[5*size*vIndex + 5*texIndex+4] = vIndex;
            }
        }
        if (deletePhoto)
        {
            if (lighted[vIndex] > 0)
            {

                colors[indexR] -= texColor.r;
                colors[indexG] -= texColor.g;
                colors[indexB] -= texColor.b;
                colors[indexT] -= 1;

                for (int i = texIndex; i < numPhotos-1; i++)
                {
                    curve[5*size*vIndex + 5*i] = curve[5*size*vIndex + 5*(i+1)];
                    curve[5*size*vIndex + 5*i+1] = curve[5*size*vIndex + 5*(i+1)+1];
                    curve[5*size*vIndex + 5*i+2] = curve[5*size*vIndex + 5*(i+1)+2];
                    curve[5*size*vIndex + 5*i+3] = curve[5*size*vIndex + 5*(i+1)+3];
                    curve[5*size*vIndex + 5*i+4] = curve[5*size*vIndex + 5*(i+1)+4];
                }
            }
        }
    }

    fragColor = vec4(diffuseColor, 1);
    memoryBarrier();
}



void calculateCoefficients(in vec3 l, inout float diffuse, inout float specular, inout vec3 eye, inout vec3 reflection)
{
    vec3 lightDirection = lightModelMatrix * l;
    lightDirection = normalize(lightDirection);

    reflection = reflect(-lightDirection, vNormal);
    reflection = normalize(reflection);
    eye  = -normalize(vert.xyz);
    eye = normalize(eye);

    diffuse = max(dot(vNormal, lightDirection), 0);
    specular = max(dot(reflection, eye), 0);
}


