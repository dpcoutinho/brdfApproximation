#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    loadCamerasButton = new QPushButton("Load &Camera");
    connect(loadCamerasButton, SIGNAL(clicked()), this, SLOT(loadCameras()));

    loadLightsButton = new QPushButton("Load &Lights");
    connect(loadLightsButton, SIGNAL(clicked()), this, SLOT(loadLights()));

    loadMeshButton = new QPushButton("Load Mes&h");
    connect(loadMeshButton, SIGNAL(clicked()), this, SLOT(loadMesh()));

    takePhotoButton = new QPushButton("&Take Photo");
    connect(takePhotoButton, SIGNAL(clicked()), this, SLOT(takePhoto()));

    startButton = new QPushButton("&Start");
    connect(startButton, SIGNAL(clicked()), this, SLOT(start()));

    exitButton = new QPushButton("&Exit");
    connect(exitButton, SIGNAL(clicked()), this, SLOT(exit()));

    renderDifferButton = new QPushButton("Render &Differ");
    connect(renderDifferButton, SIGNAL(clicked()), this, SLOT(renderDiffer()));

    saveButton = new QPushButton("S&ave");
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveImage()));

    errorButton = new QPushButton("Err&or");
    connect(errorButton, SIGNAL(clicked()), this, SLOT(printError()));

    updateButton = new QPushButton("&Update");
    connect(updateButton, SIGNAL(clicked()), this, SLOT(updateGL()));

    calculateLightButton = new QPushButton("Calculate Ne&w Photos");
    connect(calculateLightButton, SIGNAL(clicked()), this, SLOT(calculateNewPhotos()));
//    connect(calculateLightButton, SIGNAL(clicked()), this, SLOT(calculateLight()));

    calculateLightSButton = new QPushButton("Calculate &Specular Photos");
    connect(calculateLightSButton, SIGNAL(clicked()), this, SLOT(calculateNewSpecularPhotos()));

    loadBestPhotoButton = new QPushButton("Load &Best");
    connect(loadBestPhotoButton, SIGNAL(clicked()), this, SLOT(loadBestPhoto()));

    acceptBestPhotoButton = new QPushButton("Acce&pt Best");
    connect(acceptBestPhotoButton, SIGNAL(clicked()), this, SLOT(acceptBestPhoto()));

    showCameraButton = new QPushButton("&Show Camera");
    connect(showCameraButton, SIGNAL(clicked()), this, SLOT(showCamera()));

    detectSphereButton = new QPushButton("&Detect Sphere");
    connect(detectSphereButton, SIGNAL(clicked()), this, SLOT(detectSphere()));

    alignButton = new QPushButton("&Align");
    connect(alignButton, SIGNAL(clicked()), this, SLOT(align()));

    leftButton = new QPushButton;
    connect(leftButton, SIGNAL(clicked()), this, SLOT(leftPhoto()));
    QPixmap leftPixmap("Icons/left.png");
    QIcon leftIcon(leftPixmap);
    leftButton->setIcon(leftIcon);
    leftButton->setIconSize(leftPixmap.rect().size()/3);

    rightButton = new QPushButton;
    connect(rightButton, SIGNAL(clicked()), this, SLOT(rightPhoto()));
    QPixmap rightPixmap("Icons/right.png");
    QIcon rightIcon(rightPixmap);
    rightButton->setIcon(rightIcon);
    rightButton->setIconSize(rightPixmap.rect().size()/3);

    reloadShaderButton = new QPushButton("&Reload Shaders");
    connect(reloadShaderButton, SIGNAL(clicked()), this, SLOT(reloadShaders()));

    readImagesFile = new QCheckBox("Read images &from mlp");
    //readImagesFile->setChecked(true);

    loadingTab = new QWidget;
    actionTab = new QWidget;
    controlTab = new QWidget;
    cameraTab = new QWidget;

    tabs = new QTabWidget;

    tabs->addTab(cameraTab, "Camera");
    tabs->addTab(loadingTab, "Data");
    tabs->addTab(actionTab, "Actions");
    tabs->addTab(controlTab, "Tests");

    kdSlider = new QSlider(Qt::Vertical);
    kdSlider->setValue(0);
    connect(kdSlider, SIGNAL(sliderMoved(int)), this, SLOT(kdSlide(int)));
    ksSlider = new QSlider(Qt::Vertical);
    ksSlider->setValue(0);
    connect(ksSlider, SIGNAL(sliderMoved(int)), this, SLOT(ksSlide(int)));
    shininessSlider = new QSlider(Qt::Vertical);
    shininessSlider->setValue(0);
    connect(shininessSlider, SIGNAL(sliderMoved(int)), this, SLOT(shininessSlide(int)));


    rSlider = new QSlider(Qt::Vertical);
    rSlider->setValue(0);
    connect(rSlider, SIGNAL(sliderMoved(int)), this, SLOT(rSlide(int)));
    gSlider = new QSlider(Qt::Vertical);
    gSlider->setValue(0);
    connect(gSlider, SIGNAL(sliderMoved(int)), this, SLOT(gSlide(int)));
    bSlider = new QSlider(Qt::Vertical);
    bSlider->setValue(0);
    connect(bSlider, SIGNAL(sliderMoved(int)), this, SLOT(bSlide(int)));


    QLabel* kdLabel = new QLabel("Kd", this);
    QLabel* ksLabel = new QLabel("Ks", this);
    QLabel* shininessLabel = new QLabel("shininess", this);
    QLabel* rLabel = new QLabel("R", this);
    QLabel* gLabel = new QLabel("G", this);
    QLabel* bLabel = new QLabel("B", this);

    QGridLayout* slidersGrid = new QGridLayout;
    slidersGrid->addWidget(kdLabel, 0, 0);
    slidersGrid->addWidget(kdSlider, 0, 1);
    slidersGrid->addWidget(ksLabel, 0, 2);
    slidersGrid->addWidget(ksSlider, 0, 3);
    slidersGrid->addWidget(shininessLabel, 0, 4);
    slidersGrid->addWidget(shininessSlider, 0, 5);

    slidersGrid->addWidget(rLabel, 1, 0);
    slidersGrid->addWidget(rSlider, 1, 1);
    slidersGrid->addWidget(gLabel, 1, 2);
    slidersGrid->addWidget(gSlider, 1, 3);
    slidersGrid->addWidget(bLabel, 1, 4);
    slidersGrid->addWidget(bSlider, 1, 5);


    cameraView = new GLWidget(this);

    webcam = new WebcamThread;
    connect(webcam, SIGNAL(newFrame(QImage)), this, SLOT(updateFrame(QImage)));
    connect(this, SIGNAL(updated()), webcam, SLOT(release()));

    cameraLabel = new CameraLabel;
    cameraLabel->setBackgroundRole(QPalette::Base);
    cameraLabel->setFixedSize(QSize(900, 600));
    cameraLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    cameraLabel->setScaledContents(true);
    cameraLabel->hide();
    connect(cameraLabel, SIGNAL(clicked(QPoint)), this, SLOT(setCircle(QPoint)));

    QVBoxLayout* vbox1 = new QVBoxLayout;
    vbox1->addWidget(loadCamerasButton);
    vbox1->addWidget(loadMeshButton);
    vbox1->addWidget(loadLightsButton);
    vbox1->addWidget(readImagesFile);

    loadingTab->setLayout(vbox1);

    QVBoxLayout* vbox2 = new QVBoxLayout;
    vbox2->addWidget(calculateLightButton);
//    vbox2->addWidget(calculateLightSButton);
//    vbox2->addWidget(loadBestPhotoButton);
//    vbox2->addWidget(acceptBestPhotoButton);
//    vbox2->addWidget(mixButton);
    vbox2->addWidget(updateButton);
    vbox2->addWidget(reloadShaderButton);
    vbox2->addWidget(leftButton);
    vbox2->addWidget(rightButton);
    vbox2->addWidget(errorButton);
    vbox2->addWidget(renderDifferButton);
    vbox2->addLayout(slidersGrid);

    actionTab->setLayout(vbox2);

    tabs->setCurrentIndex(1);


    QVBoxLayout* vbox3 = new QVBoxLayout;
    vbox3->addWidget(startButton);
    vbox3->addWidget(exitButton);
    vbox3->addWidget(saveButton);
    controlTab->setLayout(vbox3);

    QVBoxLayout* vbox4 = new QVBoxLayout;
    vbox4->addWidget(showCameraButton);
    vbox4->addWidget(takePhotoButton);
    vbox4->addWidget(detectSphereButton);
    vbox4->addWidget(alignButton);
    cameraTab->setLayout(vbox4);

    QGridLayout* grid = new QGridLayout;
    grid->addWidget(cameraView);
    grid->addWidget(cameraLabel, 0, 1);
    grid->addWidget(tabs, 0, 2);

    setLayout(grid);
    this->adjustSize();

    cameraOpen = false;
    detecting = false;

//    readImagesFile->setChecked(true);

//    fileDir = string("Buddha/");
//    cameraView->loadMesh(string(fileDir + "Buddhan_3M_ricardo.ply").c_str());
//    loadLights(string(fileDir + "project.lp").c_str());
//    loadCameras(string(fileDir + "Buddha.mlp").c_str());

//    fileDir = string("primodataset/");
//    cameraView->loadMesh(string(fileDir + "Fragolnana_2M_Ricardo.ply").c_str());
//    loadLights(string(fileDir + "project.lp").c_str());
//    loadCameras(string(fileDir + "nanas_ric.mlp").c_str());

//    fileDir = string("secondodataset/");
//    cameraView->loadMesh(string(fileDir + "Fragolnana_2M_Ricardo.ply").c_str());
//    loadLights(string(fileDir + "project.lp").c_str());
//    loadCameras(string(fileDir + "nanas_ric.mlp").c_str());

//    fileDir = string("Gertrud1/");
//    cameraView->loadMesh(string(fileDir + "GertFin.ply").c_str());
//    loadLights(string(fileDir + "Gertrud_OriginalSet.lp").c_str());
//    loadCameras(string(fileDir + "Gertrud1.mlp").c_str());

//    fileDir = string("Gertrud2/");
//    cameraView->loadMesh(string(fileDir + "GertFin2.ply").c_str());
//    loadLights(string(fileDir + "GertrudCloseup_OriginalSet.lp").c_str());
//    loadCameras(string(fileDir + "Gert2_fin.mlp").c_str());

//    fileDir = string("Goats/");
//    cameraView->loadMesh(string(fileDir + "Capre_final.ply").c_str());
//    loadLights(string(fileDir + "Goats_OriginalSet.lp").c_str());
//    loadCameras(string(fileDir + "Goats_aligned2.mlp").c_str());

//    fileDir = string("primodatasetRedux8/");
//    cameraView->loadMesh(string(fileDir + "Fragolnana_2M_Ricardo.ply").c_str());
//    loadLights(string(fileDir + "project.lp").c_str());
//    loadCameras(string(fileDir + "nanas_ric.mlp").c_str());

//    fileDir = string("secondodatasetRedux8/");
//    cameraView->loadMesh(string(fileDir + "Fragolnana_2M_Ricardo.ply").c_str());
//    loadLights(string(fileDir + "project.lp").c_str());
//    loadCameras(string(fileDir + "nanas_ric.mlp").c_str());

//    fileDir = string("BuddhaRedux8/");
//    cameraView->loadMesh(string(fileDir + "Buddhan_3M_ricardo.ply").c_str());
//    loadLights(string(fileDir + "project.lp").c_str());
//    loadCameras(string(fileDir + "Buddha.mlp").c_str());

//    fileDir = string("Gertrud2Redux8/");
//    cameraView->loadMesh(string(fileDir + "GertFin2.ply").c_str());
//    loadLights(string(fileDir + "GertrudCloseup_OriginalSet.lp").c_str());
//    loadCameras(string(fileDir + "Gert2_fin.mlp").c_str());

//    fileDir = string("GoatsRedux8/");
//    cameraView->loadMesh(string(fileDir + "Capre_final.ply").c_str());
//    loadLights(string(fileDir + "Goats_OriginalSet.lp").c_str());
//    loadCameras(string(fileDir + "Goats_aligned2.mlp").c_str());
}

Widget::~Widget()
{
    for (unsigned int i = 0; i < photoFiles.size(); i++)
        delete[] photoFiles[i];

    delete webcam;
}

void Widget::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        qApp->exit();
    if (e->key() == Qt::Key_R)
    {
        cameraView->reset();
        cameraView->updateGL();
    }
//    if (e->key() == Qt::Key_C)
//        cameraView->changeLight();
    if (e->key() == Qt::Key_N)
        cameraView->calculateNumNonCovered();
    if (e->key() == Qt::Key_B)
        cameraView->chooseBestPhoto();
    if (e->key() == Qt::Key_L)
        cameraView->renderNormal();
    if (e->key() == Qt::Key_S)
        cameraView->calculateSpecularity();
    if (e->key() == Qt::Key_P)
        cameraView->calculateNewPhotos();
//    if (e->modifiers() & Qt::ControlModifier)
//        cameraView->pressedMouse();
}

void Widget::keyReleaseEvent(QKeyEvent *e)
{
//    if (e->key() == Qt::Key_Control)
//        cameraView->releasedMouse();
}

void Widget::arguments(char * argument)
{
    if (!strcmp(argument, "t"))
    {
        cameraView->start();
    }
}

void Widget::arguments(int argc, char *argv[])
{
    cout << "calling arguments function: " << atof(argv[1]) << " " << atoi(argv[2]) << " " << atof(argv[3]) << " " << atof(argv[4]) << endl;
    if (argc > 4)
    {
        cameraView->addTestParameters(atof(argv[1]), atoi(argv[2]), atof(argv[3]), atof(argv[4]));
    }
}


void Widget::loadCameras()
{
    QFileDialog fileDialog;
    QString file = fileDialog.getOpenFileName(this, tr("Choose Cameras"), QDir::currentPath(), tr("MLP(*.mlp)"));

    fileDir = QFileInfo(file).absoluteDir().absolutePath().toStdString();
    fileDir.append("/");

    loadCameras(file.toStdString().c_str());
    cameraView->createPhotoTexture();
}

void Widget::loadLights()
{
    QFileDialog fileDialog;
    QString file = fileDialog.getOpenFileName(this, tr("Choose Cameras"), QDir::currentPath(), tr("LP(*.lp)"));

    loadLights(file.toStdString().c_str());

}

void Widget::loadMesh()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Choose Mesh"), QDir::currentPath(), tr("Ply(*.ply);;All(*.*)"));
    cameraView->loadMesh(file.toStdString().c_str());
}

void Widget::takePhoto()
{
    cv::Vec3f l = webcam->getLightDirection();
    Vector3f light = Vector3f(l[0], l[1], l[2]);

    QImage newPhoto = cameraLabel->pixmap()->toImage();
    QDir dir(QString(fileDir.c_str()));
    QStringList filters("*.jpg");
    filters.push_back("*.png");
    QStringList files = dir.entryList(filters);
    QString newString = QString(fileDir.c_str()) + files[files.size()-1];
    int carry = 1;
    for (int i = newString.size() - 1; i>= 0; i--)
    {
        QChar cI = newString[i];
        if (cI.isNumber())
        {
            if (cI.digitValue() == 9)
            {
                newString[i].unicode() -= 9;
                carry = 1;
            }
            else
            {
                newString[i].unicode() += carry;
                carry = 0;
            }
        }
    }

    newPhoto.save(newString);
    cameraView->addPhoto(newString.toStdString().c_str());
}

void Widget::updateFrame(const QImage& frame)
{
    cameraLabel->setPixmap(QPixmap::fromImage(frame).scaled(cameraLabel->width(), cameraLabel->height()));
    cameraLabel->adjustSize();
    show();
    emit(updated());
}

void Widget::showCamera()
{
    if (!cameraOpen)
    {
        webcam->start(QThread::LowPriority);
        showCameraButton->setText("Close Camera");
        cameraLabel->show();
        cameraOpen = true;
    }
    else
    {
        cameraLabel->hide();
        cameraOpen = false;
        showCameraButton->setText("Show Camera");
        adjustSize();
        webcam->stop();
        webcam->wait();
    }
}

void Widget::start()
{
    cameraView->start();
}

void Widget::exit()
{
    qApp->exit();
}

void Widget::leftPhoto()
{
    cameraView->previousView(true);
}

void Widget::rightPhoto()
{
    cameraView->nextView(true);
}

void Widget::renderDiffer()
{
    cameraView->renderDiffer();
}

void Widget::saveImage()
{
    QImage rendering = cameraView->grabFrameBuffer();
    rendering.save("currentFrame.png", "PNG");
}

void Widget::printError()
{
    cameraView->printError();
}

void Widget::updateGL()
{
    cameraView->update();
    cameraView->updateGL();
}

void Widget::reloadShaders()
{
    cameraView->reloadShaders();
}

void Widget::calculateLight()
{
    cameraView->calculateNewLight();
}

void Widget::loadBestPhoto()
{
    cameraView->loadBestPhoto();
}

void Widget::acceptBestPhoto()
{
    cameraView->acceptBestPhoto();
}

void Widget::calculateNewPhotos()
{
    cameraView->calculateNewPhotos();
    //    cameraView->calculateSpecularity();
}

void Widget::calculateNewSpecularPhotos()
{
    cameraView->calculateSpecularity();
}

void Widget::detectSphere()
{
    if (detecting)
    {
        detecting = false;
        detectSphereButton->setText("Detect Sphere");
    }
    else
    {
        detecting = true;
        detectSphereButton->setText("Stop Detecting");
    }
    webcam->detectSphere(detecting);
}

void Widget::align()
{
    cameraOpen = false;
    webcam->stop();
    webcam->wait();

    cameraLabel->setAlign(true);
    cameraView->setAlign(true);
}

void Widget::setCircle(const QPoint& p)
{
    webcam->setCameraCircle(p, cameraLabel->size());
}

void Widget::loadCameras(const char *filename)
{
    if (newCameras.size() != 0)
    {
        newCameras.clear();
    }
    if (photoFiles.size() != 0)
    {
        for (unsigned int i = 0; i < photoFiles.size(); i++)
            delete[] photoFiles[i];
        photoFiles.clear();
    }
    ifstream cameraFile(filename);
    char* line = new char[500];
    int pos;
    int filePos = 0;
    if (!readImagesFile->isChecked())
        readAllImages();
    while (cameraFile.getline(line, 500))
    {
        string lineString(line);

        int currentIndex = cameraFile.tellg();
        cameraFile.seekg(filePos);

        if (readImagesFile->isChecked())
        {
            pos = lineString.find("MLRaster label=");
            if (pos != -1)
            {
                pos = lineString.find('"');
                int pos2 = lineString.rfind('"');
                pos++;
                string file = lineString.substr(pos, (pos2-pos));
                string absolutPhotoDir = fileDir+file;
                char* photoFile = new char[absolutPhotoDir.size()];
                strcpy(photoFile, absolutPhotoDir.c_str());
//                cout << "image: " << photoFile << endl;
                photoFiles.push_back(photoFile);
            }
        }
        pos = lineString.find("TranslationVector=");
        if (pos != -1)
        {
            PhotoCamera newCamera;

            //cout << endl << "camera: " << endl;
            pos += 19;
            cameraFile.seekg(filePos+pos);
            Vector4f translation;
            cameraFile >> translation(0) >> translation(1) >> translation(2) >> translation(3);


            pos = lineString.find("LensDistortion=");
            pos += 16;
            cameraFile.seekg(filePos+pos);
            double distortionX, distortionY;
            cameraFile >> distortionX >> distortionY;

            pos = lineString.find("ViewportPx=");
            pos += 12;
            cameraFile.seekg(filePos+pos);
            Vector2d viewport;
            cameraFile >> viewport(0) >> viewport(1);

            pos = lineString.find("PixelSizeMm=");
            pos += 13;
            cameraFile.seekg(filePos+pos);
            double dx, dy;
            cameraFile >> dx >> dy;

            pos = lineString.find("CenterPx=");
            pos += 10;
            cameraFile.seekg(filePos+pos);
            Vector2i center;
            cameraFile >> center(0) >> center(1);

            pos = lineString.find("FocalMm=");
            pos += 9;
            cameraFile.seekg(filePos+pos);
            double focal;
            cameraFile >> focal;

            pos = lineString.find("RotationMatrix=");
            pos += 16;
            cameraFile.seekg(filePos+pos);
            Matrix4f rotation;
            cameraFile >> rotation(0, 0) >> rotation(0, 1) >> rotation(0, 2) >> rotation(0, 3) >>
                          rotation(1, 0) >> rotation(1, 1) >> rotation(1, 2) >> rotation(1, 3) >>
                          rotation(2, 0) >> rotation(2, 1) >> rotation(2, 2) >> rotation(2, 3) >>
                          rotation(3, 0) >> rotation(3, 1) >> rotation(3, 2) >> rotation(3, 3);

            Matrix4f t = Matrix4f::Identity();

            t.col(3) = translation;

            double distortion = distortionX*distortionX + distortionY*distortionY;

            newCamera.setPixelSize(dx, dy);
            newCamera.setPrincipalPoint(center);
            newCamera.setFocalLength(focal);
            newCamera.setRotation(rotation);
            newCamera.setTranslation(t);
            newCamera.setRadialDistortion(distortion);

//            cout << "translation: " << endl << translation << endl;
//            cout << "distortion: " << distortionX << ", " << distortionY << endl;
//            cout << "viewport: " << viewport << endl;
//            cout << "pixel size: " << dx << ", " << dy << endl;
//            cout << "center: " << center << endl;
//            cout << "focal: " << focal << endl;
//            cout << "rotation: " << endl << rotation << endl;

            newCameras.push_back(newCamera);

        }

        cameraFile.seekg(currentIndex);
        filePos = currentIndex;
    }
    delete[] line;
    cameraFile.close();

    createCameras();

}

void Widget::loadLights(const char *filename)
{
    ifstream lightFile(filename);
    int numLights;
    lightFile >> numLights;

    for (int i = 0; i < numLights; i++)
    {
        string imageFile;
        double x, y, z;
        lightFile >> imageFile >> x >> y >> z;
        Vector3f newLight(x, y, z);
        lightPositions.push_back(newLight);

//        cout << "new light: " << x << " " << y << " " << z << endl;
    }
}

void Widget::readAllImages()
{
    QStringList imagesFilter("*.jpg");
    imagesFilter.push_back("*.png");
    QString fileDirString(fileDir.c_str());
    QDir directory(fileDirString);
    QStringList images = directory.entryList(imagesFilter);
    QStringList::iterator it = images.begin();
    while (it != images.end())
    {
        char* photoFile = new char[150];
        string absolutePhotoDir = fileDir + (*it).toStdString();
        strcpy(photoFile, absolutePhotoDir.c_str());
//        cout << "image: " << photoFile << endl;
        photoFiles.push_back(photoFile);
        it++;
    }
}

void Widget::createCameras()
{
//    cout << "create cameras" << endl;
//    cout << "cameras: " << newCameras.size() << endl;
//    cout << "photos: " << photoFiles.size() << endl;
    if (newCameras.size() == photoFiles.size())
    {
        cout << "equal size" << endl;
        for (unsigned int i = 0; i < newCameras.size(); i++)
        {
            Vector3f light;
            if (lightPositions.size() == 0)
                light = Vector3f();
            else
                light = lightPositions[i];
            cameraView->createCamera(newCameras[i], photoFiles[i], light);
        }
    }
    else
    {
//        cout << "here" << endl;
        for (unsigned int i = 0; i < photoFiles.size(); i++)
        {

            Vector3f light;
            if (lightPositions.size() == 0)
                light = Vector3f();
            else
                light = lightPositions[i];
            cameraView->createCamera(newCameras[0], photoFiles[i], light);
        }
    }
//    cout << "created" << endl;
}

