/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "glwidget.h"

#define BUFFER_OFFSET(bytes)  ((GLubyte*) NULL + (bytes))

Point2f image[] = {Point2f(-1.0, -1.0), Point2f(1.0, -1.0), Point2f(-1.0, 1.0), Point2f(1.0, 1.0)};
GLuint elementData[] = {0, 1, 2, 3};

#define MAX_PHOTOS 151

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    sizePoint3f = sizeof(Point);
    sizeFloat = sizeof(float);
    stride = LAVertex::stride();
    initializedGL = false;
    drDataCurve = new float[4];
    dgDataCurve = new float[4];
    dbDataCurve = new float[4];

    initialSetup();

    hSlices = 50;
    vSlices = 50;
    hemisphere = sphere(0.2, vSlices, hSlices, hemisphereIndices);
    axis = createAxes(0.02, vSlices, hSlices, axisIndices);

    connect(&mutualInfo, SIGNAL(updateRender(vcg::Shot<float>)), this, SLOT(updateCamera(vcg::Shot<float>)));
}

void GLWidget::initialSetup()
{
    projectionMatrix = Matrix4f::Identity();
    viewMatrix = Matrix4f::Identity();
    modelMatrix = Matrix4f::Identity();
    meshScaleMatrix = Matrix4f::Identity();
    cvToGl = Affine3f::Identity();
    cvToGl.rotate (AngleAxisf(M_PI, Vector3f::UnitX()));
    initialRender = true;
    getPixel = true;
    deletePhoto = false;
    fitCurves = false;
    fillNeighbors = false;
    renderDifferP = false;
    renderDifferM = false;
    renderPixel = false;
    mixTextures = false;
    showMesh = true;
    readDiffer = false;
    renderLight = false;
    showError = false;
    calculatingNewLight = false;
    getClickedPosition = false;
    aligningRendering = false;
    aligned = false;
    renderTrackball = false;
    showHemisphereCoverage = false;
    drawSuggestedLight = false;
    changeTextureBlock = false;
    startedOptimization = false;
    finishedOptimization = false;
    useColorWeights = false;
    printBuffer = false;
    aligning = false;
    discardBadVertices = false;
    save = false;
    renderAxis = false;
    testing = false;
    renderingBonn = false;
    blendFactor = 0.5;
    indexPhoto = 0;
    xStep = -1;
    yStep = 0;
    numXSteps = 10;
    numYSteps = 10;
    currentPhoto = -1;
    bestLight = -1;
    textureArray = 0;
    texIndex = -1;
    numPhotos = 0;
    newLight << 0, 0, 1;
    chosenPhotos.push_back(0);
    cameras.push_back(0);
    numVertices = 0;
    numNonCovered = 0;
    correspondenceIndices = 0;
    tokens = 0;
    hemisphere = 0;
    cameraFov = 40;
    _near = 0.1;
    _far = 1000;
    ak = 1;
    drDataPoints = 0;
    dgDataPoints = 0;
    dbDataPoints = 0;
    dlDataPoints = 0;
    photoAlignment = 0;
    fbo = 0;
    meshTexture = 0;
    photoTexture = 0;
    startingLight = 0;
    finalLight = -1;
    currentLight = -1;
    previousNumNegatives = 0;
    thresholdNormalAngle = 30;
    numNewPhotos = 40;
    samplesVertices = 0;

    setMouseTracking(true);
}

GLWidget::~GLWidget()
{
    if (numVertices > 0)
    {
        delete[] vertices;
        delete[] faceIndices;
    }

    for (unsigned int i = 0; i < textures.size(); i++)
        delete[] textures[i];

    if (tokens != 0)
    {
        delete[] tokens;
        delete[] correspondenceIndices;
    }
    if (hemisphere != 0)
    {
        delete[] hemisphere;
        delete[] hemisphereIndices;
    }
    if (axis != 0)
    {
        delete[] axis;
        delete[] axisIndices;
    }
    if (samplesVertices != 0)
        delete[] samplesVertices;

    if (drDataCurve)
        delete[] drDataCurve;
    if (dgDataCurve)
        delete[] dgDataCurve;
    if (dbDataCurve)
        delete[] dbDataCurve;

    if (drDataPoints)
        delete[] drDataPoints;
    if (dgDataPoints)
        delete[] dgDataPoints;
    if (dbDataPoints)
        delete[] dbDataPoints;

    delete cameraTrackball;
    delete lightTrackball;
}

void GLWidget::initializeGL ()
{
    //Glew Initialization:
    GLenum glewInitResult = glewInit();

    //Check Glew Initialization:
    if (GLEW_OK != glewInitResult) {
        cerr << "Error: " << glewGetErrorString(glewInitResult) << endl;
        exit(EXIT_FAILURE);
    }

    glClearColor(0, 0, 0, 1);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_FRAMEBUFFER_SRGB);
    glLineWidth(1);

    glActiveTexture(GL_TEXTURE1);
    glGenTextures(1, &meshTexture);
    glBindTexture(GL_TEXTURE_2D, meshTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // automatic mipmap generation included in OpenGL v1.4
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, this->width(), this->height() , 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

    glActiveTexture(GL_TEXTURE2);
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->width(), this->height(), 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glGenTextures(1,&textureArray);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY,textureArray);

    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

    glActiveTexture(GL_TEXTURE4);
    glGenTextures(1, &lightTexture);
    glBindTexture(GL_TEXTURE_2D, lightTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glActiveTexture(GL_TEXTURE5);
    glGenTextures(1, &testTexture);
    glBindTexture(GL_TEXTURE_2D, testTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glGenBuffers(1, &tbo);
    glBindBuffer(GL_ARRAY_BUFFER, tbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(image), image, GL_STATIC_DRAW);

    glGenBuffers(1, &cbo);
    glBindBuffer(GL_ARRAY_BUFFER, cbo);

    glGenBuffers(1, &sbo);
    glBindBuffer(GL_ARRAY_BUFFER, sbo);

    glGenBuffers(1, &abo);
    glBindBuffer(GL_ARRAY_BUFFER, abo);

    compileShaders(getPixelProgram, "shaders/draw.vert", "shaders/draw.frag");
    compileShaders(fittingProgram, "shaders/fitting.vert", "shaders/fitting.frag");
    compileShaders(suggestLightProgram, "shaders/calculateLight.vert", "shaders/calculateLight.frag");
    compileShaders(coordProgram, "shaders/coord.vert", "shaders/coord.frag");
    compileShaders(textureProgram, "shaders/texture.vert", "shaders/texture.frag");
    compileShaders(mixProgram, "shaders/mixtexture.vert", "shaders/mixtexture.frag");
    compileShaders(clearProgram, "shaders/clearbuffer.vert", "shaders/clearbuffer.frag");
    compileShaders(renderProgram, "shaders/render.vert", "shaders/render.frag", "shaders/render.geom");
    compileShaders(lightProgram, "shaders/light.vert", "shaders/light.frag");
    compileShaders(correspondenceProgram, "shaders/token.vert", "shaders/token.frag");
    compileShaders(sphereProgram, "shaders/sphere.vert", "shaders/sphere.frag");
    compileShaders(neighborsProgram, "shaders/neighbors.vert", "shaders/neighbors.frag");
    compileShaders(axisProgram, "shaders/axis.vert", "shaders/axis.frag");
    compileShaders(renderBonnProgram, "shaders/renderBonn.vert", "shaders/renderBonn.frag");

    glGenBuffers(1, &textureBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textureBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementData), elementData, GL_STATIC_DRAW);

    glGenBuffers(1, &faceBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBuffer);

    glGenBuffers(1, &correspondenceBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, correspondenceBuffer);

    glGenBuffers(1, &sphereBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereBuffer);

    glGenBuffers(1, &axisBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, axisBuffer);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    if (model.numVertices() > 0)
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(LAVertex), vertices, GL_STATIC_DRAW);
        createSSBO();
    }

    if (photos.size() > 0 && textures.size() == 0)
    {
        createPhotoTexture();
        loadTexture();
    }

    cameraTrackball = new Trackball;
    lightTrackball = new Trackball;
    cameraTrackball->initOpenGLMatrices();
    lightTrackball->initOpenGLMatrices();
    cameraTrackball->initializeBuffers();
    cameraTrackball->loadShader();

    initializedGL = true;
    currentCamera.setWidthHeight(this->width(), this->height());

}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(900, 900);
}

void GLWidget::resizeGL (int w, int h)
{
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
}

void GLWidget::paintGL ()
{
    makeCurrent();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (hasPhoto() && !showMesh && !mixTextures)
    {
        renderTexture();
    }

    cameraViewMatrix = cameraTrackball->getViewMatrix().matrix();
    lightModelMatrix = lightTrackball->getViewMatrix().linear();

    if(model.numVertices()>0 && showMesh)
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        if (hasPhoto())
        {
            if (initialRender)
            {
                clearSSBO();
                initialRender = false;
                coordRendering(false);
            }
            if (renderLight)
            {
                coordRendering(true);
                renderLight = false;
                lightRendering();
                coordRendering(false);
            }
            if (showError)
            {
                renderMesh();
                printError();
            }
            if (getPixel || deletePhoto)
            {
                getPixelRender();
            }
            if (calculatingNewLight || discardBadVertices)
            {
                suggestLightRender();
            }
            if (fitCurves)
            {
                fittingRender();
            }
            if (fillNeighbors)
            {
                fillNeighborsRender();
            }
        }
        if (renderingBonn)
            renderBonn();
        else
            renderMesh();

        if (readDiffer)
            readDiffer = false;

        errorCheckFunc(__FILE__, __LINE__);
    }
    if (getClickedPosition)
    {
        getClickedPosition = false;
        // render coordinates and read point and normals
        // read point
        Vector3f p;
        Vector3f n;
        coordRendering(false, true);
        p = readPixel(clickedPoint.x(), this->height() - clickedPoint.y());
        // read normal
        coordRendering(false, false, true);
        n = readPixel(clickedPoint.x(), this->height() - clickedPoint.y());
        addCorrespondence(p, n);
    }
    if (printBuffer)
    {
        printBuffer = false;
        Vector3f d;
        coordRendering(false);
        d = readPixel(clickedPoint.x(), this->height() - clickedPoint.y());
//        cout << "index: " << d(2) << endl;
//        readCurveBuffer(d(2));
//        readPrintBuffer(d(2));
    }

    if (correspondences.size() > 0 && blendFactor > 0)
        correspondenceRendering();

    if (mixTextures)
    {
        renderMeshTexture();
    }
    if (showHemisphereCoverage && showMesh && !aligning)
        sphereRendering();

    if (renderAxis)
        axisRendering();

    if (renderTrackball)
    {
        glClear(GL_DEPTH_BUFFER_BIT);
        cameraTrackball->render();
    }

    if (aligningRendering)
    {
        aligningRendering = false;
        this->grabFrameBuffer().save(QString("AligningRendering") + QString::number(ak) + QString(".jpg"));
        ak++;
    }
    if (save)
    {
        save = false;
        ak++;
    }
}


void GLWidget::setGlProjectionMatrix()
{
    PhotoCamera camera = getCurrentCamera();

    // TODO Change according to rotation
    if (model.numVertices() > 0 && hasPhoto())
    {
        Vector4f center4;
        center4 << center(0), center(1), center(2), 1;

        center4 = viewMatrix*modelMatrix*center4;
        center4 /= center4(3);

        _near = (-center4(2) - model.bbDiag());
        _far = (-center4(2) + model.bbDiag());
    }

    _near = 0.1;
    _far = 10000;

    float fov = ((2*atan((camera.getHeight()/2.0)/(-camera.intrinsic(1, 1)))*(180.0/PI)));
    float ratio = camera.getWidth()/(float)camera.getHeight() * (camera.getPixelSize()(0)/camera.getPixelSize()(1));
//    cout << "fov: " << fov << " | " << camera.getWidth()/(float)camera.getHeight() << " || " << (camera.getPixelSize()(0)/camera.getPixelSize()(1)) << endl;
//    cout << "ratio: " << ratio << endl;

    if (renderingBonn)
        projectionMatrix = camera.getIntrinsic();
    else
        projectionMatrix = perspective(fov, ratio, _near, _far);

    float angle = (fov/2)*(PI/180.0);

    Vector4f center4;
    center4 << center(0), center(1), center(2), 0;
    center4 = viewMatrix*modelMatrix*center4;

    float z = -center4(2);
    float y = 2*(z)*tan(angle);
    float x = ratio * y;

//    cout << "xyz: " << x << " " << y << " " << z << endl;
//    lightProjectionMatrix = orthographic(-0.5*x, 0.5*x, -0.5*y, 0.5*y, -(_near), -(_far));
    lightProjectionMatrix = orthographic(-100, 100, -100, 100, -(_near), -(_far));
}

void GLWidget::setWorldTransformation()
{
    viewMatrix = currentCamera.getExtrinsic();
}

void GLWidget::loadTexture()
{
    if (usedPhotos[currentPhoto] == false)
    {
        getPixel = true;
        usedPhotos[currentPhoto] = true;
    }
    if (textures.size() > 0)
    {
        delete[] textures[0];
        textures.clear();
    }
    if (photos[currentPhoto].getFile() != 0)
    {
        presentPhoto.load(photos[currentPhoto].getFile());
    }
    photoSize << presentPhoto.width(), presentPhoto.height();
    if (fbo == 0)
        createFbo();

    if (currentCamera.applyDistortion())
    {
        presentPhoto = correctDistortion(presentPhoto, currentCamera.getRadialDistortion(), currentCamera.getTangentialDistortion(), currentCamera.getFocalInPixels());
        presentPhoto.save("corrected.png");
    }

    QImage interleaved = presentPhoto.mirrored();
//        QImage blurred = blur(interleaved, 10, 10);

    GLubyte* newMappedTextures = new GLubyte[interleaved.height()*interleaved.width()*4];
    makeTexture(newMappedTextures, interleaved, interleaved.width(), interleaved.height(), 4);


    xSize = presentPhoto.width()/numXSteps;
    ySize = presentPhoto.height()/numYSteps;

    textures.push_back(newMappedTextures);

    if (getPixel)
    {
        numPhotos++;
        addLightSSBO(getLight());
    }

    if (startedOptimization)
    {
        if (textureArray)
            glDeleteTextures(1, &textureArray);

        glGenTextures(1,&textureArray);
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D_ARRAY,textureArray);
        glTexImage3D( GL_TEXTURE_2D_ARRAY, 0, GL_SRGB, xSize, ySize, textures.size(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

        startedOptimization = false;
    }
    if (finishedOptimization)
    {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D_ARRAY,textureArray);
        glTexImage3D( GL_TEXTURE_2D_ARRAY, 0, GL_SRGB, presentPhoto.width(), presentPhoto.height(), 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

        finishedOptimization = false;
    }

    if (changeTextureBlock)
    {
        for (unsigned int i = 0; i < textures.size(); i++)
        {
            GLubyte* resizedMappedTexture = new GLubyte[xSize*ySize*4];
            makeResizedTexture(textures[i], resizedMappedTexture, presentPhoto.width(), xSize, ySize, 4, xStep*xSize, yStep*ySize);
            glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, xSize, ySize, 1, GL_RGBA, GL_UNSIGNED_BYTE, resizedMappedTexture);
            delete[] resizedMappedTexture;
        }
    }
    else
    {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D_ARRAY,textureArray);
        glTexImage3D( GL_TEXTURE_2D_ARRAY, 0, GL_SRGB, presentPhoto.width(), presentPhoto.height(), 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, textures[0/*currentPhoto*/]);
//        texIndex = currentPhoto;
//        cout << "current photo: " << currentPhoto << endl;
    }
    errorCheckFunc(__FILE__, __LINE__);

    renderLight = true;

//    float angle = acos(dot(getLight(), currentNormal));
//    emit plotLine(angle);
    reset();
}

void GLWidget::createFbo()
{
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    glGenRenderbuffers(1, &vertexRbo);
    glBindRenderbuffer(GL_RENDERBUFFER, vertexRbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA32F, photoSize(0), photoSize(1));
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, vertexRbo);

    glGenRenderbuffers(1, &depth);
    glBindRenderbuffer(GL_RENDERBUFFER, depth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, photoSize(0), photoSize(1));
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLWidget::createPhotoTexture()
{
    cout << "creating photo texture" << endl;
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &photoTexture);
    glBindTexture(GL_TEXTURE_2D, photoTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

void GLWidget::mousePressEvent (QMouseEvent * e)
{
    e->accept();
    setFocus();

    //Position vector in [0,viewportWidth] domain:
    Eigen::Vector2i pos(e->pos().x(), e->pos().y());

    //Convert the mouse coordinates vector to normalized device coordinates system
    Eigen::Vector2f positionInTrackballSystem;
    positionInTrackballSystem = convertToNormalizedDeviceCoordinates(pos, this->width(), this->height());

    if (e->button() == Qt::LeftButton)
    {
        initialRotation = positionInTrackballSystem;
        cameraTrackball->setInitialRotationPosition(positionInTrackballSystem[0],positionInTrackballSystem[1]);
        cameraTrackball->beginRotation();
    }

    if (e->button() == Qt::MidButton)
    {
        if (aligning)
        {
            Vector4f p, pp;
            p << e->pos().x(), (this->height()- e->pos().y()), 1, 1;
            pp = (projectionMatrix*viewMatrix*modelMatrix).inverse()*p;
            Vector2f t;
            t << pp(0), pp(1);
            initialTranslation = t;
            cameraTrackball->setInitialTranslationPosition(t);
            cameraTrackball->beginTranslation(t);
        }
        else
        {
            lightTrackball->setInitialRotationPosition(positionInTrackballSystem[0],positionInTrackballSystem[1]);
            lightTrackball->beginRotation();
        }
    }
    updateGL();
}

void GLWidget::mouseMoveEvent (QMouseEvent * e)
{
    //Position vector in [0,viewportWidth] domain:
    Eigen::Vector2i pos(e->pos().x(),e->pos().y());

    //Convert to Normalized Device Coordinates System:
    Eigen::Vector2f positionInTrackballSystem;
    positionInTrackballSystem = convertToNormalizedDeviceCoordinates(pos, this->width(), this->height());

    //It's important to remember that it only makes sense to calculate a rotation if initial and final positions vector are different.

    //Camera Rotating:
    if(cameraTrackball->isRotating()) {
        Eigen::Vector3f initialPosition = cameraTrackball->getInitialRotationPosition();
        if(positionInTrackballSystem != Eigen::Vector2f(initialPosition[0], initialPosition[1])) {
            cameraTrackball->setFinalRotationPosition(positionInTrackballSystem[0],positionInTrackballSystem[1]);
            cameraTrackball->rotateCamera();
            cameraTrackball->setInitialRotationPosition(positionInTrackballSystem[0],positionInTrackballSystem[1]);
        }
    }

    //Light Rotating:
    else if(lightTrackball->isRotating()) {
        Eigen::Vector3f initialPosition = lightTrackball->getInitialRotationPosition();
        if(positionInTrackballSystem != Eigen::Vector2f(initialPosition[0], initialPosition[1])) {
            lightTrackball->setFinalRotationPosition(positionInTrackballSystem[0],positionInTrackballSystem[1]);
            lightTrackball->rotateCamera();
            lightTrackball->setInitialRotationPosition(positionInTrackballSystem[0],positionInTrackballSystem[1]);
        }
    }

    //Camera Translating:
    else if(cameraTrackball->isTranslating()) {
        Eigen::Vector2f initialPosition = cameraTrackball->getInitialTranslationPosition();
        if(positionInTrackballSystem != Eigen::Vector2f(initialPosition[0], initialPosition[1])) {
            Vector4f p, pp;
            p << e->pos().x(), (this->height()- e->pos().y()), 1, 1;
            pp = (projectionMatrix*viewMatrix*modelMatrix).inverse()*p;
            Vector2f t;
            t << pp(0), pp(1);
            cameraTrackball->setFinalTranslationPosition(t);
            cameraTrackball->translateCamera();
            cameraTrackball->setInitialTranslationPosition(t);
        }
    }
    updateGL();
}

void GLWidget::mouseDoubleClickEvent (QMouseEvent * e)
{
    if (e->button() == Qt::LeftButton && hasPhoto() && !aligning)
        showMesh = !showMesh;
    updateGL();
}

void GLWidget::mouseReleaseEvent (QMouseEvent * e)
{

    if (e->button() == Qt::RightButton)
    {
        if (renderDifferP == true || renderDifferM == true || renderPixel == true)
        {
            if (renderDifferP)
            {
                renderDifferP = false;
                renderDifferM = true;
            }
            else
            {
                if (renderDifferM)
                {
                    renderDifferM = false;
                    renderPixel = true;
                }
                else
                {
                    renderPixel = false;
                    renderDifferP = true;
                    readDiffer = true;
                }
            }
        }
    }
    if (e->button() == Qt::LeftButton)
    {
        bool noModifier = true;
        if (aligning)
        {
            if (e->modifiers() & Qt::ShiftModifier)
            {
                noModifier = false;
                getClickedPosition = true;
                clickedPoint = e->pos();
                emit operation(4);
            }
            if (e->modifiers() & Qt::ControlModifier)
            {
                noModifier = false;
                // add to imageCorrespondences
                Vector2f pp;
                pp(0) = 2*(e->pos().x()/(float)this->width()) - 1;
                pp(1) = 2*((this->height()-e->pos().y())/(float)this->height()) - 1;
//                cout << "pp: " << pp << endl;

                imageCorrespondences.push_back(pp);
                emit correspondence(e->pos());
            }
        }
        else
        {
            if (e->modifiers() & Qt::ControlModifier)
            {
//                cout << "pixel: " << e->pos().x() << " " << e->pos().y() << endl;
                printBuffer = true;
                clickedPoint = e->pos();
            }
        }
        if (noModifier)
            emit operation(1);
        cameraTrackball->endRotation();

    }

    if (e->button() == Qt::MidButton)
    {
        if (aligning)
        {
            cameraTrackball->endTranslation();
            emit operation(2);
        }
        else
            lightTrackball->endRotation();
    }

    updateGL();
}

void GLWidget::wheelEvent (QWheelEvent * e)
{
    const int WHEEL_STEP = 120;

    if (!showMesh)
    {
        texIndex++;
        if (texIndex == textures.size())
            texIndex = 0;
    }
    if (showMesh &&  e->modifiers() & Qt::ControlModifier)
    {
        float pos = e->delta () / float (WHEEL_STEP);

        initialScale = cameraTrackball->getZoom();
        if(pos > 0)
            cameraTrackball->increaseZoom(1.01);
        else if(pos < 0)
            cameraTrackball->increaseZoom(1.0/1.01);
        emit operation(3);
    }
    else
    {
        if (e->modifiers() & Qt::ShiftModifier)
        {
            float pos = e->delta () / float (WHEEL_STEP);

            if(pos < 0)
            {
                cameraFov += 0.6;
            }
            else if(pos > 0)
            {
                cameraFov -= 0.6;
            }
            currentCamera.setDefault(cameraFov, currentCamera.getWidth(), currentCamera.getHeight());
            updateCameraMatrices();
            emit changedFov(cameraFov);
        }
        else
        {
            blendFactor += 0.05 * (e->delta()/WHEEL_STEP);
            clamp(blendFactor, 0.0, 1.0);
//            cout << "blend factor: " << blendFactor << endl;
        }
    }

    updateGL();
}


void GLWidget::createCamera(const PhotoCamera& newCamera)
{
    currentCamera.build(newCamera);
    modelMatrix = Matrix4f::Identity();
}

void GLWidget::createCamera(const PhotoCamera& newCamera, const char* filename, const Vector3f& lightPosition)
{
    currentCamera.build(newCamera);
    modelMatrix = Matrix4f::Identity();
    createPhoto(newCamera, filename, lightPosition);
    vector<GLfloat> newErrors;
    errorsPerPhoto.push_back(newErrors);
}

void GLWidget::createCamera(const PhotoCamera& newCamera, const char* filename, const Vector3f& lightPosition, const Matrix4f& newlightViewMatrix)
{
    currentCamera.build(newCamera);
    modelMatrix = Matrix4f::Identity();
    createPhoto(newCamera, filename, lightPosition, newlightViewMatrix);
    vector<GLfloat> newErrors;
    errorsPerPhoto.push_back(newErrors);
}

void GLWidget::loadMesh(const char* filename)
{
    model.loadMesh(filename);

    numVertices = model.getVertices().size();
    vertices = new LAVertex[numVertices];

    numIndices = model.getFaceIndices().size();
    faceIndices = new GLuint[numIndices];

    for (int i = 0; i < numVertices; i++)
    {
        vertices[i] = model.getVertices()[i];
    }

    for (int i = 0; i < numIndices; i++)
    {
        faceIndices[i] = model.getFaceIndices()[i];
    }

    sizeAdjacencyList = 0;
    for (unsigned int i = 0; i < model.getAdjacencies().size(); i++)
    {
        vector< GLuint > newAdjacencyList;
        for (unsigned int j = 0; j < model.getAdjacencies()[i].size(); j++)
            newAdjacencyList.push_back(model.getAdjacencies()[i][j]);
        adjacencyList.push_back(newAdjacencyList);
        sizeAdjacencyList += newAdjacencyList.size();
    }

    // TODO GET ADJACENCIES

    center = model.bbCenter();
    cout << "mesh center: " << center.transpose() << endl;
    Vector3f bBox = model.bBox();
    if (center(2) > 0 || ((center+bBox)(2)*(center-bBox)(2)) < 0)
    {
        float maxDim = qMax(qMax(bBox(0), bBox(1)), bBox(2));
        modelMatrix = translation(0, 0, -center(2)-3*maxDim);
    }
    // project center

    Matrix4f fullProjection = projectionMatrix*viewMatrix*modelMatrix;
    Vector4f projectedCenter = fullProjection*Vector4f(center(0), center(1), center(2), 1);
    projectedCenter /= projectedCenter(3);
    if (fabs(projectedCenter(0)) > 0.1 ||  fabs(projectedCenter(1)) > 0.1)
    {
        projectedCenter -= Vector4f(projectedCenter(0), projectedCenter(1), 0, 0);
        Vector4f newCenter = fullProjection.inverse()*projectedCenter;
        newCenter /= newCenter(3);
        Matrix4f newTranslation = translation(newCenter(0)-center(0), newCenter(1)-center(1), 0);
        modelMatrix = newTranslation*modelMatrix;
    }

    backupModelMatrix = modelMatrix;

    Vector4f centerP;
    centerP << center(0), center(1), center(2), 0;

    Vector4f lookEigen = modelMatrix*centerP;
    look << lookEigen(0), lookEigen(1), lookEigen(2);
//    meshScaleMatrix = model.applyScale();
//    look = eigenToVcg(model.scaledBBCenter());

    model.cleanVertices();

    if (initializedGL)
        createSSBO();

    translate = Matrix4f::Identity();
    translate.col(3) << -center(0), -center(1), -center(2), 1;
    iTranslate = Matrix4f::Identity();
    iTranslate.col(3) << center(0), center(1), center(2), 1;

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(LAVertex), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(GLuint), faceIndices, GL_STATIC_DRAW);

    cout << "MODEL MATRIX: " << modelMatrix << endl;

    updateCameraMatrices();
}

Vector3f GLWidget::readPixel(int x, int y)
{
    int xRes, yRes;
    xRes = ((float)x/this->width()) * photoSize(0);
    yRes = ((float)y/this->height()) * photoSize(1);
    GLfloat data[4];
    Vector3f d;
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glReadPixels(xRes, yRes, 1, 1, GL_RGBA, GL_FLOAT, &data[0]);
    d << data[0], data[1], data[2];

    errorCheckFunc(__FILE__, __LINE__);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return d;
}


void GLWidget::acquirePolynomial(int index, Vector4f &pR, Vector4f &pG, Vector4f &pB)
{
    unsigned long int bufferSize = sizeof(GLfloat)*numVertices;
    bufferSize *= 12;
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, polynomialSsbo);
    GLfloat *polynomialBuffer = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, bufferSize, GL_MAP_READ_BIT);

    unsigned long int finalIndex = 12*index;

    pR << polynomialBuffer[finalIndex], polynomialBuffer[finalIndex+1], polynomialBuffer[finalIndex+2], polynomialBuffer[finalIndex+3];
    pG << polynomialBuffer[finalIndex+4], polynomialBuffer[finalIndex+5], polynomialBuffer[finalIndex+6], polynomialBuffer[finalIndex+7];
    pB << polynomialBuffer[finalIndex+8], polynomialBuffer[finalIndex+9], polynomialBuffer[finalIndex+10], polynomialBuffer[finalIndex+11];

    if (isnan(pR(0)))
    {
        cout << "index: " << index << " of " << numVertices << endl;
        cout << "buffersize: " << bufferSize << endl;
    }

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase( GL_SHADER_STORAGE_BUFFER, 0, 0);
}

void GLWidget::addLightSSBO(const Vector3f& lightPos)
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, lightPosSsbo);
    GLfloat *lightBuffer = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, MAX_PHOTOS*3*sizeof(GLfloat),
                                                         GL_MAP_WRITE_BIT);
    int index = 3*(numPhotos-1);
    lightBuffer[index] = lightPos(0);
    lightBuffer[index + 1] = lightPos(1);
    lightBuffer[index + 2] = lightPos(2);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, 0);
}

void GLWidget::removeLightSSBO()
{
    cout << "remove light ssbo: " << endl;
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, lightPosSsbo);
    GLfloat *lightBuffer = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, MAX_PHOTOS*3*sizeof(GLfloat),
                                                         GL_MAP_WRITE_BIT);

    for (int i = currentPhoto; i < numPhotos-1; i++)
    {
        lightBuffer[3*i] = lightBuffer[3*(i+1)];
        lightBuffer[3*i + 1] = lightBuffer[3*(i+1) + 1];
        lightBuffer[3*i + 2] = lightBuffer[3*(i+1) + 2];
    }
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, 0);
}

void GLWidget::addCorrespondence(const Vector3f &point, const Vector3f &normal)
{
    Point pointVcg = eigenToVcg(point);
    Vector4f vert, aux, pos;
    vert << pointVcg.X(), pointVcg.Y(), pointVcg.Z(), 1;
    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;
    aux = modelviewMatrix.inverse()*vert;
    pos = (iTranslate*cameraViewMatrix*translate).inverse()*aux;
    pointVcg = Point(pos(0), pos(1), pos(2));
    Vector3f pointN = vcgToEigen(pointVcg);

    Matrix4f normalMatrix = modelviewMatrix.inverse().transpose();
    Vector4f n, vNormal;
    n << normal(0), normal(1), normal(2), 0;
    vNormal = normalMatrix * n;
    Vector3f normalN;
    normalN << vNormal(0), vNormal(1), vNormal(2);

    correspondences.push_back(tuple<Vector3f, Vector3f>(pointN, normalN));
    if (correspondenceIndices != 0)
        delete[] correspondenceIndices;
    if (tokens != 0)
        delete[] tokens;

    numIndicesCorrespondences = correspondences.size()*6*3;
    correspondenceIndices = new GLuint[numIndicesCorrespondences];
    int index = 0;
    for (unsigned int i = 0; i < correspondences.size(); i++)
    {
        int base = 5*i;
        correspondenceIndices[index] = base;
        correspondenceIndices[index+1] = base+1;
        correspondenceIndices[index+2] = base+2;
        index+=3;
        correspondenceIndices[index] = base;
        correspondenceIndices[index+1] = base+2;
        correspondenceIndices[index+2] = base+4;
        index+=3;
        correspondenceIndices[index] = base;
        correspondenceIndices[index+1] = base+4;
        correspondenceIndices[index+2] = base+3;
        index+=3;
        correspondenceIndices[index] = base;
        correspondenceIndices[index+1] = base+3;
        correspondenceIndices[index+2] = base+1;
        index+=3;
        correspondenceIndices[index] = base+4;
        correspondenceIndices[index+1] = base+2;
        correspondenceIndices[index+2] = base+1;
        index+=3;
        correspondenceIndices[index] = base+1;
        correspondenceIndices[index+1] = base+3;
        correspondenceIndices[index+2] = base+4;
        index+=3;
    }

    tokens = new Point[correspondences.size()*5];
    index = 0;
    for (unsigned int i = 0; i < correspondences.size(); i++)
    {
        Point b0 = eigenToVcg(get<0>(correspondences[i]));

        Point b1, b2, b3, b4;
        b1 = b0 + Point(-1, 2, -1);
        b2 = b0 + Point(1, 2, -1);
        b3 = b0 + Point(-1, 2, 1);
        b4 = b0 + Point(1, 2, 1);
        tokens[index] = b0;
        tokens[index+1] = b1;
        tokens[index+2] = b2;
        tokens[index+3] = b3;
        tokens[index+4] = b4;
        index+=5;
    }
}

void GLWidget::getPixelRender()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(getPixelProgram.id);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
//    GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT1 };
//    glDrawBuffers(1, drawBuffers);

//     glBindFramebuffer(GL_FRAMEBUFFER, 0);

     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glViewport(0, 0, presentPhoto.width(), presentPhoto.height());

     GLuint MVLocation, PLocation, lightLocation, newLightLocation, getPixelLocation, numPhotosLocation,
            depthTextureLocation, viewportLocation, useColorWeightsLocation, totalTextureSizeLocation, deletePhotoLocation,
            lightModelMatrixLocation, cameraViewMatrixLocation, translationCenterMatrixLocation, iTranslationCenterMatrixLocation, colorWeightsLocation,
            useTextureBlockLocation, textureArrayLocation, texIndexLocation, newPhotoLocation, textureBlockCoordLocation, cvToGlLocation, renderBonnLocation,
            kLocation, pLocation;

     getUniformLocation(getPixelProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(lightLocation, "light"), tUIC(newLightLocation, "newLight"),
                        tUIC(getPixelLocation, "getPixel"), tUIC(depthTextureLocation, "depthTexture"), tUIC(viewportLocation, "viewportSize"),
                        tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"), tUIC(deletePhotoLocation, "deletePhoto"),
                        tUIC(lightModelMatrixLocation, "lightModelMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix"), tUIC(translationCenterMatrixLocation, "centerTranslation"),
                        tUIC(textureArrayLocation, "textureArray"), tUIC(numPhotosLocation, "numPhotos"),
                        tUIC(texIndexLocation, "texIndex"), tUIC(newPhotoLocation, "newPhoto"),
                        tUIC(useColorWeightsLocation, "useColorWeights"), tUIC(colorWeightsLocation, "colorWeights"), tUIC(totalTextureSizeLocation, "totalTextureSize"),
                        tUIC(useTextureBlockLocation, "useTextureBlock"), tUIC(textureBlockCoordLocation, "textureBlockCoord"),
                        tUIC(cvToGlLocation, "cvToGl"), tUIC(renderBonnLocation, "renderBonn"), tUIC(kLocation, "k"), tUIC(pLocation, "p")
                        });

    GLuint positionLocation, normalLocation, indexLocation, numAdjacentsLocation, adjacencyStartLocation;
    getLocation(getPixelProgram.id, { tUIC(positionLocation, "position"), tUIC(normalLocation, "normal"), tUIC(indexLocation, "index"),
                                 tUIC(numAdjacentsLocation, "numAdjacents"), tUIC(adjacencyStartLocation, "adjacencyStart")});
    enable({positionLocation, normalLocation, indexLocation/*, numAdjacentsLocation, adjacencyStartLocation*/});

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);
    glUniform1i(textureArrayLocation, 3);

    Vector3f l = getLight();

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;
    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(cvToGlLocation, 1, GL_FALSE, cvToGl.matrix().data());
    glUniform3fv(lightLocation, 1, l.data());
    glUniform3fv(newLightLocation, 1, newLight.data());
    glUniform3fv(colorWeightsLocation, 1, colorWeights.data());
    glUniform1i(getPixelLocation, getPixel);
    glUniform1i(deletePhotoLocation, deletePhoto);
    glUniform1i(useTextureBlockLocation, changeTextureBlock);
    glUniform2i(viewportLocation, presentPhoto.width(), presentPhoto.height());
    glUniform2i(totalTextureSizeLocation, presentPhoto.width(), presentPhoto.height());
    glUniform2i(textureBlockCoordLocation, xStep*xSize, yStep*ySize);
    glUniform1i(texIndexLocation, texIndex);
    glUniform1i(useColorWeightsLocation, useColorWeights);
    glUniform1i(newPhotoLocation, getPixel);
    glUniform1i(numPhotosLocation, numPhotos);
    glUniform1i(renderBonnLocation, renderingBonn);
    glUniformMatrix3fv(lightModelMatrixLocation, 1, GL_FALSE,lightModelMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE,translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE,iTranslate.data());
    glUniform2fv(kLocation, 1, currentCamera.getRadialDistortion().data());
    glUniform2fv(pLocation, 1, currentCamera.getTangentialDistortion().data());

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizePoint3f));
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(0, colorSsbo), tIUI(1, positionSsbo), tIUI(2, normalSsbo), tIUI(3, curveSsbo),
                   tIUI(6, feedbackSsbo), tIUI(7, lightSsbo), tIUI(8, angleSsbo), tIUI(9, photosSsbo), tIUI(10, lightPosSsbo)});

    errorCheckFunc(__FILE__, __LINE__);

    glDrawArrays(GL_POINTS, 0, numVertices);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glFinish();

//    this->grabFrameBuffer().save(QString("DrawRendering") + QString::number(currentPhoto) + QString("-") + QString::number(ak) + QString(".jpg"));
//    ak++;
    enable({positionLocation, normalLocation, indexLocation/*, numAdjacentsLocation, adjacencyStartLocation*/}, true);

    errorCheckFunc(__FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});

    glUseProgram(0);
    glViewport(0, 0, this->width(), this->height());
    getPixel = false;
}

void GLWidget::suggestLightRender()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(suggestLightProgram.id);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glViewport(0, 0, presentPhoto.width(), presentPhoto.height());

     GLuint MVLocation, PLocation, lightLocation, numPhotosLocation, analyzeLightLocation, discardLocation,
            depthTextureLocation, viewportLocation, totalTextureSizeLocation, currentLightLocation,
            lightModelMatrixLocation, cameraViewMatrixLocation, translationCenterMatrixLocation, iTranslationCenterMatrixLocation,
            useTextureBlockLocation, textureArrayLocation, textureBlockCoordLocation;

     getUniformLocation(suggestLightProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(lightLocation, "light"),
                        tUIC(depthTextureLocation, "depthTexture"), tUIC(viewportLocation, "viewportSize"), tUIC(currentLightLocation, "currentLight"),
                        tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"), tUIC(analyzeLightLocation, "analyzingLight"),
                        tUIC(lightModelMatrixLocation, "lightModelMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix"), tUIC(translationCenterMatrixLocation, "centerTranslation"),
                        tUIC(textureArrayLocation, "textureArray"), tUIC(numPhotosLocation, "numPhotos"), tUIC(totalTextureSizeLocation, "totalTextureSize"),
                        tUIC(useTextureBlockLocation, "useTextureBlock"), tUIC(textureBlockCoordLocation, "textureBlockCoord"), tUIC(discardLocation, "discardBadVertices")
                        });

    GLuint positionLocation, normalLocation, indexLocation;
    getLocation(suggestLightProgram.id, { tUIC(positionLocation, "position"), tUIC(normalLocation, "normal"), tUIC(indexLocation, "index")});
    enable({positionLocation, normalLocation, indexLocation});

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);
    glUniform1i(textureArrayLocation, 3);

    Vector3f l = getLight();

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;
    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniform3fv(lightLocation, 1, l.data());
    glUniform3fv(currentLightLocation, 1, newLight.data());
    glUniform1i(useTextureBlockLocation, changeTextureBlock);
    glUniform1i(analyzeLightLocation, analyzingLight);
    glUniform1i(discardLocation, discardBadVertices);
    glUniform2i(viewportLocation, presentPhoto.width(), presentPhoto.height());
    glUniform2i(totalTextureSizeLocation, presentPhoto.width(), presentPhoto.height());
    glUniform2i(textureBlockCoordLocation, xStep*xSize, yStep*ySize);
    glUniform1i(numPhotosLocation, numPhotos);
    glUniformMatrix3fv(lightModelMatrixLocation, 1, GL_FALSE,lightModelMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE,translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE,iTranslate.data());

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizePoint3f));
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(3, curveSsbo), tIUI(7, lightSsbo), tIUI(8, angleSsbo), tIUI(11, printSsbo)});

    glDrawArrays(GL_POINTS, 0, numVertices);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glFinish();

//    this->grabFrameBuffer().save(QString("SuggestingRendering") + QString::number(currentPhoto) + QString("-") + QString::number(ak) + QString(".jpg"));
//    ak++;
    enable({positionLocation, normalLocation, indexLocation}, true);

    errorCheckFunc(__FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {3, 7, 8, 11});

    glUseProgram(0);
    glViewport(0, 0, this->width(), this->height());
}

void GLWidget::fittingRender()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(fittingProgram.id);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glViewport(0, 0,presentPhoto.width(), presentPhoto.height()); // 4000, 3000);

     GLuint MVLocation, PLocation, lightLocation, numPhotosLocation, depthTextureLocation, viewportLocation, totalTextureSizeLocation,
            lightModelMatrixLocation, cameraViewMatrixLocation, translationCenterMatrixLocation, iTranslationCenterMatrixLocation,
            useTextureBlockLocation, textureArrayLocation, textureBlockCoordLocation, cvToGlLocation, renderBonnLocation,
            kLocation, pLocation;

     getUniformLocation(fittingProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(lightLocation, "light"),
                        tUIC(depthTextureLocation, "depthTexture"), tUIC(viewportLocation, "viewportSize"),tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"),
                        tUIC(lightModelMatrixLocation, "lightModelMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix"), tUIC(translationCenterMatrixLocation, "centerTranslation"),
                        tUIC(numPhotosLocation, "numPhotos"), tUIC(textureArrayLocation, "textureArray"),
                        tUIC(totalTextureSizeLocation, "totalTextureSize"), tUIC(useTextureBlockLocation, "useTextureBlock"), tUIC(textureBlockCoordLocation, "textureBlockCoord"),
                                            tUIC(cvToGlLocation, "cvToGl"), tUIC(renderBonnLocation, "renderBonn"), tUIC(kLocation, "k"), tUIC(pLocation, "p")
                        });

    GLuint positionLocation, indexLocation;
    getLocation(fittingProgram.id, {tUIC(positionLocation, "position"), tUIC(indexLocation, "index")});
    enable({positionLocation, indexLocation});

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);
    glUniform1i(textureArrayLocation, 3);

    Vector3f l = getLight();

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;
    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(cvToGlLocation, 1, GL_FALSE, cvToGl.matrix().data());
    glUniform3fv(lightLocation, 1, l.data());
    glUniform1i(useTextureBlockLocation, changeTextureBlock);
    glUniform1i(numPhotosLocation, numPhotos);
    glUniform1i(renderBonnLocation, renderingBonn);
    glUniform2i(viewportLocation, presentPhoto.width(), presentPhoto.height()); //4000, 3000);
    glUniform2i(totalTextureSizeLocation, presentPhoto.width(), presentPhoto.height());
    glUniform2i(textureBlockCoordLocation, xStep*xSize, yStep*ySize);
    glUniformMatrix3fv(lightModelMatrixLocation, 1, GL_FALSE,lightModelMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE,translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE,iTranslate.data());
    glUniform2fv(kLocation, 1, currentCamera.getRadialDistortion().data());
    glUniform2fv(pLocation, 1, currentCamera.getTangentialDistortion().data());

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(0, colorSsbo), tIUI(1, positionSsbo),tIUI(3, curveSsbo), tIUI(4, polynomialSsbo), tIUI(7, lightSsbo)});

    glDrawArrays(GL_POINTS, 0, numVertices);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glFinish();

//    this->grabFrameBuffer().save(QString("DrawRendering") + QString::number(currentPhoto) + QString("-") + QString::number(ak) + QString(".jpg"));
//    ak++;
    enable({positionLocation, indexLocation}, true);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {0, 1, 3, 4, 7});

    fitCurves = false;
    glUseProgram(0);
    glViewport(0, 0, this->width(), this->height());
}

void GLWidget::fillNeighborsRender()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(neighborsProgram.id);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//     glViewport(0, 0, presentPhoto.width(), presentPhoto.height());
    glViewport(0, 0, this->width(), this->height());

    GLuint MVLocation, PLocation, numPhotosLocation, BBDiagonalLocation,
            depthTextureLocation, viewportLocation, totalTextureSizeLocation,
            cameraViewMatrixLocation, translationCenterMatrixLocation, iTranslationCenterMatrixLocation,
            useTextureBlockLocation, textureArrayLocation, textureBlockCoordLocation,
            cvToGlLocation, renderBonnLocation, kLocation, pLocation, imageSizeLocation;

    getUniformLocation(neighborsProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"),
                        tUIC(depthTextureLocation, "depthTexture"), tUIC(viewportLocation, "viewportSize"), tUIC(imageSizeLocation, "imageSize"),
                        tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"),
                        tUIC(cameraViewMatrixLocation, "trackballMatrix"), tUIC(translationCenterMatrixLocation, "centerTranslation"),
                        tUIC(textureArrayLocation, "textureArray"), tUIC(numPhotosLocation, "numPhotos"),
                        tUIC(totalTextureSizeLocation, "totalTextureSize"), tUIC(BBDiagonalLocation, "BBDiagonal"),
                        tUIC(useTextureBlockLocation, "useTextureBlock"), tUIC(textureBlockCoordLocation, "textureBlockCoord"),
                        tUIC(cvToGlLocation, "cvToGl"), tUIC(renderBonnLocation, "renderBonn"), tUIC(kLocation, "k"), tUIC(pLocation, "p")
                        });

    GLuint positionLocation, normalLocation, indexLocation, numAdjacentsLocation, adjacencyStartLocation;
    getLocation(neighborsProgram.id, { tUIC(positionLocation, "position"),  tUIC(indexLocation, "index"), tUIC(normalLocation, "normal"),
                                 tUIC(numAdjacentsLocation, "numAdjacents"), tUIC(adjacencyStartLocation, "adjacencyStart")});

    enable({positionLocation, normalLocation, indexLocation, numAdjacentsLocation, adjacencyStartLocation});

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);
    glUniform1i(textureArrayLocation, 3);

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;
    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(cvToGlLocation, 1, GL_FALSE, cvToGl.matrix().data());
    glUniform2i(viewportLocation, this->width(), this->height());
    glUniform2i(imageSizeLocation, 4000, 3000);
    glUniform2i(totalTextureSizeLocation, presentPhoto.width(), presentPhoto.height());
    glUniform2i(textureBlockCoordLocation, xStep*xSize, yStep*ySize);
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE,translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE,iTranslate.data());
    glUniform1f(BBDiagonalLocation, model.bbDiag());
    glUniform1i(useTextureBlockLocation, changeTextureBlock);
    glUniform1i(renderBonnLocation, renderingBonn);
    glUniform1i(numPhotosLocation, numPhotos);
    glUniform2fv(kLocation, 1, currentCamera.getRadialDistortion().data());
    glUniform2fv(pLocation, 1, currentCamera.getTangentialDistortion().data());

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizePoint3f));
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));
    glVertexAttribPointer(adjacencyStartLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f + sizeFloat));
    glVertexAttribPointer(numAdjacentsLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f + 2*sizeFloat));

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(1, positionSsbo), tIUI(2, normalSsbo), tIUI(3, curveSsbo), tIUI(5, adjacencySsbo), tIUI(10, lightSsbo), tIUI(11, fillSsbo)});

    glDrawArrays(GL_POINTS, 0, numVertices);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glFinish();

    this->grabFrameBuffer().save(QString("FillRendering") + QString::number(currentPhoto) + QString("-") + QString::number(ak) + QString(".jpg"));
    ak++;
    enable({positionLocation, normalLocation, indexLocation, numAdjacentsLocation, adjacencyStartLocation}, true);

    errorCheckFunc(__FILE__, __LINE__);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {1, 2, 3, 5, 10, 11});

    if (analyzeNeighbors() == false)
    {
//        fitCurves = true;
        fillNeighbors = false;

        fitBRDF();
        emit message("Ended neighbor phase");

        clock_t neighborsEnd;
        neighborsEnd = clock();
        timeNeighbors = (float)(neighborsEnd-neighborsStart)/CLOCKS_PER_SEC;
        cout << "time neighbors phase: " << timeNeighbors << endl;
    }
    else
        updateGL();

    glUseProgram(0);
    glViewport(0, 0, this->width(), this->height());
}

void GLWidget::renderMesh()
{
    glUseProgram(renderProgram.id);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLuint MVLocation, PLocation, lightLocation, lightModelMatrixLocation, useLABLocation, numPhotosLocation, newLightLocation, testingLocation, testTextureLocation,
           cameraViewMatrixLocation, translationCenterMatrixLocation, iTranslationCenterMatrixLocation, imageTextureLocation, depthTextureLocation, viewportLocation,
           renderDifferPLocation, renderDifferMLocation, renderPixelLocation, baseShininessLocation, lightIndexLocation, textureArrayLocation, aligningLocation, thresholdNormalAngleLocation;

    getUniformLocation(renderProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(useLABLocation, "useLAB"), tUIC(aligningLocation, "aligning"),
                       tUIC(lightLocation, "light"), tUIC(numPhotosLocation, "numPhotos"), tUIC(testingLocation, "testing"), tUIC(newLightLocation, "testLight"), tUIC(thresholdNormalAngleLocation, "thresholdNormalAngle"),
                       tUIC(lightModelMatrixLocation, "lightModelMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix"), tUIC(translationCenterMatrixLocation, "centerTranslation"),
                       tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"), tUIC(imageTextureLocation, "imageTexture"), tUIC(depthTextureLocation, "depthTexture"),
                       tUIC(viewportLocation, "viewportSize"), tUIC(renderDifferPLocation, "renderDifferP"), tUIC(renderDifferMLocation, "renderDifferM"), tUIC(renderPixelLocation, "renderPixel"),
                       tUIC(baseShininessLocation, "baseShininess"), tUIC(lightIndexLocation, "lightIndex"), tUIC(textureArrayLocation, "textureArray"), tUIC(testTextureLocation, "testTexture")
                       });

    GLuint positionLocation, indexLocation, normalLocation, numAdjacentsLocation, adjacencyStartLocation;
    getLocation(renderProgram.id, {tUIC(positionLocation, "position"), tUIC(indexLocation, "index"), tUIC(normalLocation, "normal"),
                tUIC(numAdjacentsLocation, "numAdjacents"), tUIC(adjacencyStartLocation, "adjacencyStart")});
    errorCheckFunc(__FILE__, __LINE__);
    enable({ positionLocation, normalLocation, indexLocation, numAdjacentsLocation, adjacencyStartLocation});

    errorCheckFunc(__FILE__, __LINE__);
    glViewport(0, 0, this->width(), this->height());
    glUniform2i(viewportLocation, this->width(), this->height());

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;

//    cout << "model: " << endl << modelMatrix  << endl;
//    cout << "view: " << endl << viewMatrix  << endl;
//    cout << "projection: " << endl << projectionMatrix << endl;
//    cout << "mesh scale: " << endl << meshScaleMatrix << endl;
//    cout << "translate" << endl << translate << endl;
//    cout << "itranslate" << endl << iTranslate << endl;
//    cout << "trackball" << endl << cameraViewMatrix << endl;

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);
    glUniform1i(textureArrayLocation, 3);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, testTexture);
    glUniform1i(testTextureLocation, 5);

    Vector3f l = Vector3f(0, 0, 1);
    if (hasPhoto())
        l = getLight();

    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE, cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE, translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE, iTranslate.data());
    glUniformMatrix3fv(lightModelMatrixLocation, 1, GL_FALSE, lightModelMatrix.data());
    glUniform3fv(lightLocation, 1, l.data());
    glUniform3fv(newLightLocation, 1, newLight.data());
    glUniform1i(lightIndexLocation, texIndex);
    glUniform1i(renderDifferPLocation, renderDifferP);
    glUniform1i(renderDifferMLocation, renderDifferM);
    glUniform1i(renderPixelLocation, renderPixel);
    glUniform1i(useLABLocation, false);
    glUniform1f(baseShininessLocation, 10);
    glUniform1i(aligningLocation, aligning);
    glUniform1i(numPhotosLocation, numPhotos);
    glUniform1i(testingLocation, testing);
    errorCheckFunc(__FILE__, __LINE__);
    glUniform1f(thresholdNormalAngleLocation, thresholdNormalAngle);
    errorCheckFunc(__FILE__, __LINE__);

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizePoint3f));
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));
    errorCheckFunc(__FILE__, __LINE__);
    glVertexAttribPointer(adjacencyStartLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f + sizeFloat));

    errorCheckFunc(__FILE__, __LINE__);
    glVertexAttribPointer(numAdjacentsLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f + 2*sizeFloat));

    errorCheckFunc(__FILE__, __LINE__);

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(0, colorSsbo), tIUI(1, positionSsbo), tIUI(2, normalSsbo), tIUI(3, curveSsbo), tIUI(4, polynomialSsbo),
                   tIUI(6, feedbackSsbo), tIUI(7, lightSsbo), tIUI(8, angleSsbo), tIUI(9, photosSsbo), tIUI(10, lightPosSsbo), tIUI(11, fillSsbo)});

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBuffer);
    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);

    errorCheckFunc(__FILE__, __LINE__);
    enable({positionLocation, normalLocation, indexLocation, numAdjacentsLocation, adjacencyStartLocation}, true);

    errorCheckFunc(__FILE__, __LINE__);
    if (mixTextures)
    {
        glBindTexture(GL_TEXTURE_2D, meshTexture);
        glCopyTexImage2D(GL_TEXTURE_2D,0, GL_SRGB, 0, 0, this->width(), this->height(), 0);
    }

    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});

    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    glFinish();
    glUseProgram(0);
    glViewport(0, 0, this->width(), this->height());

    if (currentLight >= 0)
        analyzeLight();
    if (save)
        this->grabFrameBuffer().save(QString("r") + QString::number(ak) + QString(".png"));
//    ak++;

    errorCheckFunc(__FILE__, __LINE__);
}

void GLWidget::renderTexture()
{
    // render texture
    glUseProgram(textureProgram.id);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);


    GLuint position, imageTextureLocation, viewportLocation, mousePosLocation, radiusLocation, textureArrayLocation, texIndexLocation, useColorWeightsLocation, colorWeightsLocation,
           depthTextureLocation, lightTextureLocation;
    getUniformLocation(textureProgram.id, {tUIC(imageTextureLocation, "imageTexture"), tUIC(viewportLocation, "viewportSize"),
                       tUIC(mousePosLocation, "mousePos"), tUIC(radiusLocation, "radius"), tUIC(depthTextureLocation, "depthTexture"), tUIC(lightTextureLocation, "lightTexture"),
                       tUIC(textureArrayLocation, "textureArray"), tUIC(texIndexLocation, "texIndex"), tUIC(useColorWeightsLocation, "useColorWeights"), tUIC(colorWeightsLocation, "colorWeights")
                       });

    getLocation(textureProgram.id, {tUIC(position, "position")});


    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    glUniform1i(textureArrayLocation, 3);
    glUniform1i(texIndexLocation, 0/*texIndex*/);
    glUniform1i(useColorWeightsLocation, useColorWeights);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, lightTexture);
    glUniform1i(lightTextureLocation, 6);

    glUniform1i(imageTextureLocation, 0);
    glUniform2i(viewportLocation, this->width(), this->height());
    glUniform3fv(colorWeightsLocation, 1, colorWeights.data());

    glBindBuffer(GL_ARRAY_BUFFER, tbo);

    glEnableVertexAttribArray(position);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textureBuffer);

    glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, (GLvoid*)0);

    glDisableVertexAttribArray(position);
    glUseProgram(0);

    if (save)
        this->grabFrameBuffer().save(QString("textureRendering") + QString::number(currentPhoto) + QString("-") + QString::number(ak) + QString(".png"));
}

void GLWidget::renderMeshTexture()
{
    glUseProgram(mixProgram.id);
    glClear(GL_DEPTH_BUFFER_BIT);

    GLuint position, texture0, texture1, viewportLocation, blendFactorLocation;
    getUniformLocation(mixProgram.id, {tUIC(texture0, "textures[0]"), tUIC(texture1, "textures[1]"), tUIC(viewportLocation, "viewportSize"), tUIC(blendFactorLocation, "blendFactor")});
    getLocation(mixProgram.id, {tUIC(position, "position")});

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, photoTexture);
    glUniform1i(texture0, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, meshTexture);
    glUniform1i(texture1, 1);

    glUniform2i(viewportLocation, this->width(), this->height());
    glUniform1f(blendFactorLocation, blendFactor);

    glBindBuffer(GL_ARRAY_BUFFER, tbo);

    glEnableVertexAttribArray(position);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textureBuffer);

    glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, (GLvoid*)0);

    glDisableVertexAttribArray(position);
    errorCheckFunc(__FILE__, __LINE__);
}


void GLWidget::coordRendering(bool renderFromLight, bool renderPosition, bool renderNormal)
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, drawBuffers);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(coordProgram.id);
    glViewport(0, 0, photoSize(0), photoSize(1));
//    cout << "viewport: " << photoSize.transpose() << endl;

    GLuint positionLocation, normalLocation, indexLocation;
    getLocation(coordProgram.id, {tUIC(positionLocation, "position"), tUIC(normalLocation, "normal"), tUIC(indexLocation, "index")});

    GLuint MVLocation, PLocation, renderPositionLocation, renderNormalLocation, cameraViewMatrixLocation,
            translationCenterMatrixLocation, iTranslationCenterMatrixLocation, cvToGlLocation, renderBonnLocation,
            viewportLocation, kLocation, pLocation;
    getUniformLocation(coordProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(renderPositionLocation, "renderPosition"),
                                    tUIC(renderNormalLocation, "renderNormal"),tUIC(cameraViewMatrixLocation, "trackballMatrix"),
                                    tUIC(translationCenterMatrixLocation, "centerTranslation"), tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"),
                                    tUIC(cvToGlLocation, "cvToGl"), tUIC(renderBonnLocation, "renderBonn"), tUIC(viewportLocation, "viewportSize"),
                                    tUIC(kLocation, "k"), tUIC(pLocation, "p")});
    enable({positionLocation, normalLocation, indexLocation});

    if (renderFromLight)
    {
        if (renderingBonn)
        {
            lightViewMatrix = photos[currentPhoto].getLightViewMatrix();
            modelviewMatrix = lightViewMatrix*modelMatrix*meshScaleMatrix;
        }
        else
            modelviewMatrix = lightViewMatrix*viewMatrix*modelMatrix*meshScaleMatrix;
        glUniformMatrix4fv(PLocation, 1, GL_FALSE, /*lightProjectionMatrix*/projectionMatrix.data());
    }
    else
    {
        modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;
        glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    }

    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE,translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE,iTranslate.data());
    glUniformMatrix4fv(cvToGlLocation, 1, GL_FALSE, cvToGl.matrix().data());
    glUniform2i(viewportLocation, photoSize(0), photoSize(1));
    glUniform1i(renderPositionLocation, renderPosition);
    glUniform1i(renderNormalLocation, renderNormal);
    glUniform1i(renderBonnLocation, renderingBonn);
    glUniform2fv(kLocation, 1, currentCamera.getRadialDistortion().data());
    glUniform2fv(pLocation, 1, currentCamera.getTangentialDistortion().data());

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizePoint3f));
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBuffer);
    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);

    if (renderFromLight)
    {
        glBindTexture(GL_TEXTURE_2D, lightTexture);
        glCopyTexImage2D(GL_TEXTURE_2D,0, GL_RGBA32F, 0, 0, photoSize(0), photoSize(1), 0);
    }
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glCopyTexImage2D(GL_TEXTURE_2D,0, GL_RGBA32F, 0, 0, photoSize(0), photoSize(1), 0);

    enable({positionLocation, normalLocation, indexLocation}, true);

    glFinish();

    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glViewport(0, 0, this->width(), this->height());
    errorCheckFunc(__FILE__, __LINE__);
}

void GLWidget::lightRendering()
{
//    cout << "quantas vezes renderizo da luz?" << endl;
    errorCheckFunc(__FILE__, __LINE__);
    glUseProgram(lightProgram.id);
//    glViewport(0, 0, photoSize(0), photoSize(1));

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLuint positionLocation, indexLocation;
    getLocation(lightProgram.id, {tUIC(positionLocation, "position"), tUIC(indexLocation, "index")});

    GLuint MVLocation, PLocation, depthTextureLocation, renderLightLocation, viewportLocation, cvToGlLocation, renderBonnLocation,
            imageSizeLocation, kLocation, pLocation;
    getUniformLocation(lightProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(depthTextureLocation, "depthTexture"),
                        tUIC(renderLightLocation, "renderLight"), tUIC(viewportLocation, "viewportSize"), tUIC(imageSizeLocation, "imageSize"),
                       tUIC(cvToGlLocation, "cvToGl"), tUIC(renderBonnLocation, "renderBonn"), tUIC(kLocation, "k"), tUIC(pLocation, "p")});

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glUniform1i(depthTextureLocation, 2);

    if (renderingBonn)
    {
        lightViewMatrix = photos[currentPhoto].getLightViewMatrix();
        modelviewMatrix = lightViewMatrix*modelMatrix*meshScaleMatrix;
    }
    else
        modelviewMatrix = lightViewMatrix*viewMatrix*modelMatrix*meshScaleMatrix;

//    cout << "light model: " << endl << modelMatrix << endl;
//    cout << "light view: " << endl << lightViewMatrix << endl;
//    cout << "light projection: " << endl << projectionMatrix << endl;
    glUniform1i(renderLightLocation, renderLight);
    glUniform1i(renderBonnLocation, renderingBonn);
    glUniform2i(viewportLocation, this->width(), this->height());
    glUniform2i(imageSizeLocation, 4000, 3000);

    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, /*lightProjectionMatrix*/projectionMatrix.data());
    glUniformMatrix4fv(cvToGlLocation, 1, GL_FALSE, cvToGl.matrix().data());
    glUniform2fv(kLocation, 1, currentCamera.getRadialDistortion().data());
    glUniform2fv(pLocation, 1, currentCamera.getTangentialDistortion().data());

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(7, lightSsbo)});

    enable({positionLocation, indexLocation});
    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));

    glDrawArrays(GL_POINTS, 0, numVertices);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glFinish();

//    this->grabFrameBuffer().save(QString("lightRendering") + QString::number(ak++) + QString(".png"));
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {4, 7});

    enable({positionLocation, indexLocation}, true);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
//    glViewport(0, 0, this->width(), this->height());

    errorCheckFunc(__FILE__, __LINE__);
}


void GLWidget::correspondenceRendering()
{
    glUseProgram(correspondenceProgram.id);
    glClear(GL_DEPTH_BUFFER_BIT);

    glBindBuffer(GL_ARRAY_BUFFER, cbo);
    glBufferData(GL_ARRAY_BUFFER, correspondences.size()*5*sizeof(Point), tokens, GL_STATIC_DRAW);

    GLuint positionLocation;
    getLocation(correspondenceProgram.id, {tUIC(positionLocation, "position")});
    enable({positionLocation});

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint MVLocation, PLocation, cameraViewMatrixLocation, translationCenterMatrixLocation, iTranslationCenterMatrixLocation;
    getUniformLocation(correspondenceProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix"),
                                              tUIC(translationCenterMatrixLocation, "centerTranslation"), tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation")});

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;

    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE,translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE,iTranslate.data());

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, correspondenceBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndicesCorrespondences*sizeof(GLuint), correspondenceIndices, GL_STATIC_DRAW);
    glDrawElements(GL_TRIANGLES, numIndicesCorrespondences, GL_UNSIGNED_INT, 0);

    enable({positionLocation}, true);
    errorCheckFunc(__FILE__, __LINE__);

    glFinish();
    glBindTexture(GL_TEXTURE_2D, meshTexture);
    glCopyTexImage2D(GL_TEXTURE_2D,0, GL_SRGB, 0, 0, this->width(), this->height(), 0);
    glUseProgram(0);
}

void GLWidget::axisRendering()
{
    glUseProgram(axisProgram.id);
    glClear(GL_DEPTH_BUFFER_BIT);

    glBindBuffer(GL_ARRAY_BUFFER, abo);
    glBufferData(GL_ARRAY_BUFFER, 3*hSlices*(vSlices+1)*sizeof(Axis), axis, GL_STATIC_DRAW);

    GLuint positionLocation, colorLocation;
    getLocation(axisProgram.id, {tUIC(positionLocation, "position"), tUIC(colorLocation, "color")});
    enable({positionLocation, colorLocation});

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 2*sizePoint3f, 0);
    glVertexAttribPointer(colorLocation, 3, GL_FLOAT, GL_FALSE, 2*sizePoint3f, BUFFER_OFFSET(sizePoint3f));

    GLuint MVLocation, PLocation, cameraViewMatrixLocation;
    getUniformLocation(axisProgram.id, {tUIC(MVLocation, "modelViewMatrix"), tUIC(PLocation, "projectionMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix")});

    Matrix4f axisViewMatrix = Matrix4f::Identity();
    axisViewMatrix.topLeftCorner(3, 3) = viewMatrix.topLeftCorner(3, 3);
    modelviewMatrix = axisViewMatrix*modelMatrix*meshScaleMatrix;

    cout << "model: " << endl << modelMatrix  << endl;
    cout << "view: " << endl << axisViewMatrix  << endl;
    cout << "projection: " << endl << projectionMatrix << endl;

    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());

    glDrawArrays(GL_POINTS, 0, 3*hSlices*(vSlices+1));

//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, axisBuffer);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3*hSlices*3*2*(vSlices+1)*sizeof(GLuint), axisIndices, GL_STATIC_DRAW);
//    glDrawElements(GL_TRIANGLES, 3*hSlices*3*2*(vSlices+1), GL_UNSIGNED_INT, 0);

    enable({positionLocation, colorLocation}, true);
    errorCheckFunc(__FILE__, __LINE__);

    glFinish();
    glUseProgram(0);
}

void GLWidget::sphereRendering()
{
    glUseProgram(sphereProgram.id);
    glClear(GL_DEPTH_BUFFER_BIT);

    glBindBuffer(GL_ARRAY_BUFFER, sbo);
    glBufferData(GL_ARRAY_BUFFER, (vSlices*hSlices + 2)*sizeof(Point), hemisphere, GL_STATIC_DRAW);

    GLuint positionLocation;
    getLocation(sphereProgram.id, {tUIC(positionLocation, "position")});
    enable({positionLocation});

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint translationMatrixLocation, cameraViewMatrixLocation, numLightsLocation, drawLightLocation, suggestedLightLocation, currentLightLocation,
            radiusLocation, startingLightLocation, finalLightLocation, lightModelMatrixLocation;
    getUniformLocation(sphereProgram.id, {tUIC(translationMatrixLocation, "translationMatrix"), tUIC(cameraViewMatrixLocation, "trackballMatrix"),
                       tUIC(drawLightLocation, "drawSuggestedLight"), tUIC(suggestedLightLocation, "suggestedLight"), tUIC(numLightsLocation, "numLights"),
                       tUIC(currentLightLocation, "currentLight"), tUIC(radiusLocation, "radius"), tUIC(startingLightLocation, "startingLight"),
                       tUIC(finalLightLocation, "finalLight"), tUIC(lightModelMatrixLocation, "lightModelMatrix")});

    Matrix4f t = translation(0.75, -0.70, -0.5);
    t(0, 0) = this->height()/(float)this->width();

    Vector3f l = Vector3f(0, 0, 1);
    if (hasPhoto())
        l = getLight();

    glUniformMatrix4fv(translationMatrixLocation, 1, GL_FALSE, t.data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE,cameraViewMatrix.data());
    glUniformMatrix3fv(lightModelMatrixLocation, 1, GL_FALSE,lightModelMatrix.data());
    glUniform3fv(suggestedLightLocation, 1, newLight.data());
    glUniform3fv(currentLightLocation, 1, l.data());
    glUniform1i(numLightsLocation, numPhotos);
    glUniform1i(drawLightLocation, drawSuggestedLight);
    glUniform1i(startingLightLocation, startingLight);
    glUniform1i(finalLightLocation, finalLight);
    glUniform1f(radiusLocation, 0.2);

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(10, lightPosSsbo)});

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2*hSlices*(3*vSlices + 1)*sizeof(GLuint), hemisphereIndices, GL_STATIC_DRAW);
    glDrawElements(GL_TRIANGLES, 2*hSlices*(3*vSlices + 1), GL_UNSIGNED_INT, 0);

    if (save)
        this->grabFrameBuffer().save(QString("sphereRendering") + QString::number(currentPhoto) + QString("-") + QString::number(ak) + QString(".png"));

    enable({positionLocation}, true);
    errorCheckFunc(__FILE__, __LINE__);

    glFinish();
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {10});
    glUseProgram(0);
}

void GLWidget::renderBonn()
{
    glUseProgram(renderBonnProgram.id);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLuint mLocation, vLocation, PLocation, lightLocation, imSizeLocation, kLocation,
           pLocation, cvToGlLocation, lightModelMatrixLocation, cameraViewMatrixLocation,
           translationCenterMatrixLocation, iTranslationCenterMatrixLocation, numPhotosLocation,
            textureArrayLocation, testTextureLocation, viewportLocation;

    getUniformLocation(renderBonnProgram.id, {tUIC(mLocation, "modelMatrix"), tUIC(vLocation, "viewMatrix"), tUIC(PLocation, "projectionMatrix"),
                       tUIC(lightLocation, "light"), tUIC(imSizeLocation, "image_size"), tUIC(kLocation, "k"), tUIC(pLocation, "p"),
                       tUIC(cvToGlLocation, "cvToGl"), tUIC(lightModelMatrixLocation, "lightModelMatrix"),
                       tUIC(cameraViewMatrixLocation, "trackballMatrix"), tUIC(translationCenterMatrixLocation, "centerTranslation"),
                       tUIC(iTranslationCenterMatrixLocation, "iCenterTranslation"), tUIC(numPhotosLocation, "numPhotos"),
                       tUIC(textureArrayLocation, "textureArray"), tUIC(testTextureLocation, "testTexture"), tUIC(viewportLocation, "viewportSize")
                       });

    GLuint positionLocation, normalLocation, indexLocation;
    getLocation(renderBonnProgram.id, {tUIC(positionLocation, "position"), tUIC(normalLocation, "normal"), tUIC(indexLocation, "index")});
    errorCheckFunc(__FILE__, __LINE__);
    enable({ positionLocation, normalLocation, indexLocation});

    errorCheckFunc(__FILE__, __LINE__);
    glViewport(0, 0, this->width(), this->height());
    glUniform2i(imSizeLocation, currentCamera.getWidth(), currentCamera.getHeight());
    glUniform2i(viewportLocation, this->width(), this->height());

//    cout << "model: " << endl << modelMatrix  << endl;
//    cout << "view: " << endl << viewMatrix  << endl;
//    cout << "projection: " << endl << projectionMatrix << endl;
//    cout << "image size: " << photoSize.transpose() << endl;
//    cout << "radial: " << currentCamera.getRadialDistortion() << endl;
//    cout << "tangential: " << currentCamera.getTangentialDistortion() << endl;

//    cout << "cvToGL" << cvToGl.matrix() << endl;

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);
    glUniform1i(textureArrayLocation, 3);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, testTexture);
    glUniform1i(testTextureLocation, 5);

    Vector3f l = Vector3f(0, 0, 1);
    if (hasPhoto())
        l = getLight();

    if (testing)
    {
        l = newLight;
        cout << "new light: " << l.transpose() << endl;
    }

    glUniformMatrix4fv(mLocation, 1, GL_FALSE, modelMatrix.data());
    glUniformMatrix4fv(vLocation, 1, GL_FALSE, viewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(cvToGlLocation, 1, GL_FALSE, cvToGl.matrix().data());
    glUniformMatrix4fv(cameraViewMatrixLocation, 1, GL_FALSE, cameraViewMatrix.data());
    glUniformMatrix4fv(translationCenterMatrixLocation, 1, GL_FALSE, translate.data());
    glUniformMatrix4fv(iTranslationCenterMatrixLocation, 1, GL_FALSE, iTranslate.data());
    glUniformMatrix3fv(lightModelMatrixLocation, 1, GL_FALSE, lightModelMatrix.data());
    glUniform3fv(lightLocation, 1, l.data());
    glUniform2fv(kLocation, 1, currentCamera.getRadialDistortion().data());
    glUniform2fv(pLocation, 1, currentCamera.getTangentialDistortion().data());
    glUniform1i(numPhotosLocation, numPhotos);

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(sizePoint3f));
    glVertexAttribPointer(indexLocation, 1, GL_FLOAT, GL_FALSE, stride, BUFFER_OFFSET(2*sizePoint3f));
    errorCheckFunc(__FILE__, __LINE__);

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(0, colorSsbo), tIUI(3, curveSsbo), tIUI(4, polynomialSsbo), tIUI(7, lightSsbo), tIUI(10, lightPosSsbo)});


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceBuffer);
    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);

    errorCheckFunc(__FILE__, __LINE__);
    enable({positionLocation, normalLocation, indexLocation}, true);

    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {0, 3, 4, 7, 10});

    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    glFinish();
    glUseProgram(0);
    glViewport(0, 0, this->width(), this->height());

    errorCheckFunc(__FILE__, __LINE__);
}


void GLWidget::createSSBO()
{
    unsigned long int sizeFloatVertices = sizeof(GLfloat)* numVertices;
    unsigned long int curveSize;
    unsigned long int printSize;
    unsigned long int polySize = sizeFloatVertices;
    polySize *= 12;

    genSSBO(0, colorSsbo, 4*sizeFloatVertices);
    GLfloat* buffer_data = (GLfloat*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 4*sizeFloatVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data, 0, 4*sizeFloatVertices);
    glUnmapBuffer( GL_SHADER_STORAGE_BUFFER);

    genSSBO(1, positionSsbo, 3*sizeFloatVertices);
    GLfloat* buffer_data1 = (GLfloat*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 3*sizeFloatVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT );

    unsigned int index = 0;
    for (unsigned int i = 0; i < numVertices; i++)
    {
        Point currentPosition = vertices[i].position;
        buffer_data1[index] = currentPosition.X();
        buffer_data1[index+1] = currentPosition.Y();
        buffer_data1[index+2] = currentPosition.Z();
        index += 3;
    }

    genSSBO(2, normalSsbo, 3*sizeFloatVertices);
    GLfloat* buffer_data2 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 3*sizeFloatVertices, GL_MAP_WRITE_BIT |GL_MAP_INVALIDATE_BUFFER_BIT);

    index = 0;
    for (unsigned int i = 0; i < numVertices; i++)
    {
        Point currentNormal = vertices[i].normal;
        buffer_data2[index] = currentNormal.X();
        buffer_data2[index+1] = currentNormal.Y();
        buffer_data2[index+2] = currentNormal.Z();
        index += 3;
    }

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    curveSize = sizeFloatVertices;
    curveSize *= 5*MAX_PHOTOS;
    genSSBO(3, curveSsbo, curveSize);
    GLfloat *buffer_data3 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, curveSize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data3, 0, curveSize);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(4, polynomialSsbo, polySize);
    GLfloat *buffer_data4 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, polySize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data4, 0, polySize);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(5, adjacencySsbo, sizeAdjacencyList*sizeof(GLfloat));
    GLfloat* buffer_data5 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeAdjacencyList*sizeof(GLfloat), GL_MAP_WRITE_BIT |GL_MAP_INVALIDATE_BUFFER_BIT);
    index = 0;
    for (unsigned int i = 0; i < adjacencyList.size(); i++)
    {
        for (unsigned int j = 0; j < adjacencyList[i].size(); j++)
        {
            buffer_data5[index] = adjacencyList[i][j];
            index++;
        }
    }
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(6, feedbackSsbo, sizeFloatVertices);
    GLfloat *buffer_data6 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeFloatVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data6, 0, sizeFloatVertices);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(7, lightSsbo, sizeFloatVertices);
    GLfloat *buffer_data7 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeFloatVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data7, 0, sizeFloatVertices);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(8, angleSsbo, 5*sizeFloatVertices);
    GLfloat *buffer_data8 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 5*sizeFloatVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data8, 0, 5*sizeFloatVertices);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(9, photosSsbo, sizeof(GLdouble)*numVertices);
    GLdouble *buffer_data9 = (GLdouble*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GLdouble)*numVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data9, 0, sizeof(GLdouble)*numVertices);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(10, lightPosSsbo, MAX_PHOTOS*3*sizeof(GLfloat));
    GLfloat *buffer_data10 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, MAX_PHOTOS*3*sizeof(GLfloat), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data10, 0, MAX_PHOTOS*3*sizeof(GLfloat));
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    genSSBO(11, fillSsbo, sizeFloatVertices);
    GLfloat *buffer_data11 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeFloatVertices, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memset(buffer_data11, 0, sizeFloatVertices);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

//    printSize = sizeFloatVertices;
//    printSize *= 2*MAX_PHOTOS;
//    genSSBO(11, printSsbo, printSize);
//    GLdouble *buffer_data11 = (GLdouble*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, printSize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
//    memset(buffer_data11, 0, printSize);
//    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
}

void GLWidget::clearSSBO()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(clearProgram.id);

    GLuint position;
    getLocation(clearProgram.id, {tUIC(position, "position")});

    GLuint MVLocation = glGetUniformLocation(clearProgram.id, "modelViewMatrix");
    GLuint PLocation = glGetUniformLocation(clearProgram.id, "projectionMatrix");

    modelviewMatrix = viewMatrix*modelMatrix*meshScaleMatrix;

    glUniformMatrix4fv(MVLocation, 1, GL_FALSE, modelviewMatrix.data());
    glUniformMatrix4fv(PLocation, 1, GL_FALSE, projectionMatrix.data());

    bindBufferBase(GL_SHADER_STORAGE_BUFFER, {tIUI(0, colorSsbo), tIUI(3, curveSsbo), tIUI(4, polynomialSsbo),
                   tIUI(6, feedbackSsbo), tIUI(7, lightSsbo), tIUI(8, angleSsbo), tIUI(9, photosSsbo), tIUI(11, fillSsbo)});

    enable({position});
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glDrawArrays(GL_POINTS, 0, numVertices);

    glMemoryBarrier (GL_SHADER_STORAGE_BARRIER_BIT);
    glFinish();

    enable({position}, true);
    unbindBufferBase(GL_SHADER_STORAGE_BUFFER, {0, 3, 4, 6, 7, 8, 9, 11});
    glUseProgram(0);

    errorCheckFunc(__FILE__, __LINE__);
}

GLfloat GLWidget::readBuffer(GLuint ssbo, unsigned int shaderIndex, unsigned int bufferSize)
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, shaderIndex, ssbo);
    GLfloat* buffer_data = (GLfloat*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GLfloat)*bufferSize,
                            GL_MAP_READ_BIT);

    GLfloat sumError = 0;
    int numVerticesPhoto = 0;
    for (unsigned int i = 0; i < bufferSize; i++)
    {
        GLfloat aux = buffer_data[i];

        if (aux > 0)
        {
            sumError += aux;
            numVerticesPhoto++;
        }
    }

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase( GL_SHADER_STORAGE_BUFFER, shaderIndex, 0);

    return (sumError/numVerticesPhoto);
}

void GLWidget::readCurveBuffer(unsigned int index)
{
    unsigned long long int curveSize = sizeof(GLfloat)*numVertices;
    curveSize *= 5*MAX_PHOTOS;

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, curveSsbo);
    GLfloat* buffer_data1 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, curveSize, GL_MAP_READ_BIT);

    vector<Vector4f> points;
    vector<float> lum;

    cout << "index: " << index << endl;
    for (unsigned int i = 0; i < numPhotos; i++)
    {
        float x = buffer_data1[5*MAX_PHOTOS*index + 5*i];
        float yr = buffer_data1[5*MAX_PHOTOS*index + 5*i+1];
        float yg = buffer_data1[5*MAX_PHOTOS*index + 5*i+2];
        float yb = buffer_data1[5*MAX_PHOTOS*index + 5*i+3];
//        float yl = buffer_data1[5*MAX_PHOTOS*index + 5*i+4];
        float yl = 0.2126*yr + 0.7152*yg + 0.0722*yb;
//        cout << x << " " << yr << " " << yg << endl;

        if (x < 10 || yr > 0 )
        {
            Vector4f newPoint = Vector4f(x, yr, yg, yb);
            points.push_back(newPoint);
            lum.push_back(yl);
        }
    }

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, polynomialSsbo);
    GLfloat* buffer_data2 = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 12*sizeof(GLfloat)*numVertices, GL_MAP_READ_BIT);

    cout << "red: " << buffer_data2[12*index] << "*x**3 + " << buffer_data2[12*index+1] << "*x**2 +" << buffer_data2[12*index+2] << "*x +" << buffer_data2[12*index+3] << endl;
    cout << "green: " << buffer_data2[12*index+4] << "*x**3 + " << buffer_data2[12*index+5] << "*x**2 +" << buffer_data2[12*index+6] << "*x +" << buffer_data2[12*index+7] << endl;
    cout << "blue: " << buffer_data2[12*index+8] << "*x**3 + " << buffer_data2[12*index+9] << "*x**2 +" << buffer_data2[12*index+10] << "*x +" << buffer_data2[12*index+11] << endl << endl;

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase( GL_SHADER_STORAGE_BUFFER, 4, 0);

    numDataPoints = points.size();
    if (drDataPoints != 0)
        delete[] drDataPoints;
    if (dgDataPoints != 0)
        delete[] dgDataPoints;
    if (dbDataPoints != 0)
        delete[] dbDataPoints;
    if (dlDataPoints != 0)
        delete[] dlDataPoints;

    drDataPoints = new Vector2f[numDataPoints];
    dgDataPoints = new Vector2f[numDataPoints];
    dbDataPoints = new Vector2f[numDataPoints];
    dlDataPoints = new Vector2f[numDataPoints];
    for (int i = 0; i < numDataPoints; i++)
    {
        drDataPoints[i] = Vector2f(points[i](0), points[i](1));
        dgDataPoints[i] = Vector2f(points[i](0), points[i](2));
        dbDataPoints[i] = Vector2f(points[i](0), points[i](3));
        dlDataPoints[i] = Vector2f(points[i](0), lum[i]);
    }

    drDataCurve[0] = buffer_data2[12*index];
    drDataCurve[1] = buffer_data2[12*index+1];
    drDataCurve[2] = buffer_data2[12*index+2];
    drDataCurve[3] = buffer_data2[12*index+3];

    dgDataCurve[0] = buffer_data2[12*index+4];
    dgDataCurve[1] = buffer_data2[12*index+5];
    dgDataCurve[2] = buffer_data2[12*index+6];
    dgDataCurve[3] = buffer_data2[12*index+7];

    dbDataCurve[0] = buffer_data2[12*index+8];
    dbDataCurve[1] = buffer_data2[12*index+9];
    dbDataCurve[2] = buffer_data2[12*index+10];
    dbDataCurve[3] = buffer_data2[12*index+11];
}

void GLWidget::readPrintBuffer(unsigned int index)
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, printSsbo);
    unsigned long int printSize;
    printSize = sizeof(GLfloat)*numVertices;
    printSize *= 2*MAX_PHOTOS;
    GLfloat* buffer = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, printSize, GL_MAP_READ_BIT);
    float* points = new float[MAX_PHOTOS];
    float* dist = new float[MAX_PHOTOS];
    for (unsigned int i = 0; i < MAX_PHOTOS; i++)
    {
        points[i] = buffer[2*MAX_PHOTOS*index + i];
        dist[i] = buffer[2*MAX_PHOTOS*index + MAX_PHOTOS +i];
    }
    cout << "angles: " << endl;
    for (unsigned int i = 0; i < MAX_PHOTOS; i++)
    {
        cout << points[i] << " ";
    }
    cout << endl << endl;
    for (unsigned int i = 0; i < MAX_PHOTOS; i++)
    {
        cout << dist[i] << " ";
    }
    cout << endl << endl;
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, 0);
    delete[] points;
    delete[] dist;
}

void GLWidget::calculateNewLight()
{
    clock_t before, after;
    before = clock();
    calculatingNewLight = true;
    updateGL();
    reset();

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, angleSsbo);
    GLfloat* buffer_data = (GLfloat*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 5*sizeof(GLfloat)*numVertices,
                         GL_MAP_READ_BIT);

    if (allLights.size() > 0)
        allLights.clear();
    bestLight = -1;
    greatestWeight = 0;

    if (centralLights.size() != 0)
        centralLights.clear();

    float angleThreshold = 10;

    int size = 360/angleThreshold;
    int numBins = size*size;

    int numDirections = 0;
    vector <vector<Vector3f> > bins;
    float * weights = new float[numBins];
    for (int i = 0; i < numBins; i++)
    {
        vector<Vector3f> newBins;
        bins.push_back(newBins);
        weights[i] = 0;
    }

    GLfloat x, y, z, w, aux;
    for (int i = 0; i < numVertices; i++)
    {
        aux = buffer_data[5*i];
        if (aux > 0)
        {
            numDirections++;
            x = buffer_data[5*i+1];
            y = buffer_data[5*i+2];
            z = buffer_data[5*i+3];
            w = buffer_data[5*i+4];
            Vector3f currentLight = Vector3f(x, y, z);

            if (isnan(x) || isnan(y) || isnan(z))
                continue;

            Vector3f projection = currentLight;
            projection(1) = 0;
            float latitude = stableAngle(currentLight, projection);
            if (currentLight(1) < 0)
                latitude = 360 - latitude;

            int latitudePosition = latitude / angleThreshold;
            if (latitudePosition == size)
                latitudePosition = 0;

            float longitude = atan2(projection(0), projection(2));
            longitude *= 180.0/PI;

            if (longitude < 0)
                longitude += 360;

            int longitudePosition = longitude / angleThreshold;
            if (longitudePosition == size)
                longitudePosition = 0;

            int index = latitudePosition*size+longitudePosition;

            float bestAngle = angleThreshold + 1;
            int bestIndex;

            if (bins[index].size() != 0)
            {
                bins[index].push_back(currentLight);
                bins[index][1] += currentLight;
                Vector3f averageLight = bins[index][1];
                averageLight /= (bins[index].size()-2);
                averageLight /= averageLight.norm();

                bins[index][0] = averageLight;
                weights[index] += w;
            }
            else
            {
                bins[index].push_back(currentLight);
                bins[index].push_back(currentLight);
                bins[index].push_back(currentLight);
                weights[index] += w;
            }


            for (int j = -1; j <= 1; j++)
            {
                for (int k = -1; k <= 1; k++)
                {
                    int currentLatitude = latitudePosition + j;
                    if (currentLatitude < 0)
                        currentLatitude = size-1;
                    if (currentLatitude == 90/angleThreshold ||  currentLatitude == 260/angleThreshold)
                        continue;
                    if (currentLatitude == size)
                        currentLatitude = 0;

                    int currentLongitude = longitudePosition + k;
                    if (currentLongitude < 0)
                        currentLongitude = size - 1;
                    if (currentLongitude == size)
                        currentLongitude = 0;

                    int currentIndex = currentLatitude*size+currentLongitude;

                    if (bins[currentIndex].size() != 0)
                    {
                        float distance = stableAngle(currentLight, bins[currentIndex][0]);
                        if (distance < angleThreshold && distance < bestAngle)
                        {
                            bestAngle = distance;
                            bestIndex = currentIndex;
                        }
                    }
                }
            }

            if (bins[index].size() == 0)
            {
                if (bestAngle < angleThreshold/2)
                {

                    bins[bestIndex].push_back(currentLight);
                    bins[bestIndex][1] += currentLight;
                    Vector3f averageLight = bins[bestIndex][1];
                    averageLight /= (bins[bestIndex].size()-2);
                    averageLight /= averageLight.norm();

                    bins[bestIndex][0] = averageLight;
                    weights[bestIndex] += w;
                }
                else
                {
                    bins[index].push_back(currentLight);
                    bins[index].push_back(currentLight);
                    bins[index].push_back(currentLight);
                    weights[index] += w;
                }
            }
            else
            {
                bins[bestIndex].push_back(currentLight);
                bins[bestIndex][1] += currentLight;
                Vector3f averageLight = bins[bestIndex][1];
                averageLight /= (bins[bestIndex].size()-2);
                averageLight /= averageLight.norm();

                bins[bestIndex][0] = averageLight;
                weights[bestIndex] += w;
            }
        }
    }

    for (int i = 0; i < numBins; i++)
    {
        if (bins[i].size() != 0)
        {
            tuple <vector<Vector3f>, float> newTuple(bins[i], weights[i]);
            centralLights.push_back(newTuple);
        }
    }

    int moreLights = 0;
    int index = -1;

    sort(centralLights.begin(), centralLights.end(), vectorOperator);

    for (unsigned int i = 0; i < 10 /*centralLights.size()*/; i++)
    {
        tuple<Vector3f, int> newLightTuple{get<0>(centralLights[i])[0], i};
        allLights.push_back(newLightTuple);
        if ((get<0>(centralLights[i]).size()-2) > moreLights)
        {
            moreLights = get<0>(centralLights[i]).size()-2;
            index = i;
        }
    }
    cout << endl;

    after = clock();
    float timeNewLight = (float)(after-before)/CLOCKS_PER_SEC;
    timeCalculateLights.push_back(timeNewLight);

//    cout << "new light: " << newLight.transpose() << endl;
//    cout << "weight ratio: " << weightRatio << endl;

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase( GL_SHADER_STORAGE_BUFFER, 8, 0);

    drawSuggestedLight = true;
    analyzingLight = true;
    changeLight();
    calculatingNewLight = false;
    delete[] weights;
    updateGL();
}

void GLWidget::changeLight()
{
    for (currentLight = 0; currentLight < allLights.size(); currentLight++)
    {
        cout << "changing light: " << (currentLight+1) << " of " << allLights.size() << " lights"  << endl;
        newLight = get<0>(allLights[currentLight]);
        renderLight = true;
        updateGL();
    }
    newLight = get<0>(allLights[bestLight]);
    cout << "best light: " << (bestLight+1) << " of " << allLights.size() << " with weight: " << greatestWeight << endl;
    currentLight = -1;
    analyzingLight = false;
    updateGL();
}

void GLWidget::analyzeLight()
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, angleSsbo);
     GLfloat* buffer_data = (GLfloat*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 5*sizeof(GLfloat)*numVertices, GL_MAP_READ_BIT);
     float weight = 0;
     int numCovered;
     for (int i = 0; i < numVertices; i++)
     {
         GLfloat aux = buffer_data[5*i];
         if (aux > 0)
         {
             GLfloat aux2 = buffer_data[5*i + 4];
             weight += aux2;
             numCovered++;
         }
     }
     cout << "current weight: " << weight << endl;
     if (weight > greatestWeight)
     {
         greatestWeight = weight;
         bestLight = currentLight;
     }
     glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
     glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, 0);
}

bool GLWidget::analyzeNeighbors()
{
    cout << "analyzing" << endl;
    int numNegatives = 0;

    unsigned long int sizeFloatVertices = sizeof(GLfloat)* numVertices;

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 11, fillSsbo);
    GLfloat* buffer_data = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeFloatVertices, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT);

    for (int i = 0; i < numVertices; i++)
        if (buffer_data[i] > 0)
            numNegatives++;

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 11, 0);

    cout << "num negative: " << numNegatives << endl;

    if (numNegatives > 0 && numNegatives != previousNumNegatives)
    {
        previousNumNegatives = numNegatives;
        return true;
    }
    else
        return false;
}

void GLWidget::loadBestPhoto()
{
    chooseBestPhoto();
    if (bestPhoto == -1)
    {
        emit message("No more photos");
        return;
    }
    newLight = photos[bestPhoto].getLight();
    updateGL();
}

void GLWidget::acceptBestPhoto()
{
    chosenPhotos.push_back(bestPhoto);
    nextView();
    drawSuggestedLight = false;

    updateGL();
}


void GLWidget::calculateNewPhotos()
{
//    int i = 0;
//    while(i < numNewPhotos)
    {
//        cout << "iteration: " << i << endl; i++;
        calculateNewLight();
        loadBestPhoto();
        if (bestPhoto == -1)
            return;
        acceptBestPhoto();
    }
//    cout << "Num chosen photos: " << i << endl;
}

void GLWidget::chooseBestPhoto()
{
    bestPhoto = -1;
    float minimumAngle = std::numeric_limits<float>::max();
    for (unsigned int i = 0; i < photos.size(); i++)
    {
        float diff = stableAngle(photos[i].getLight(), newLight);

        if (diff < minimumAngle && i != currentPhoto)
        {
            bool used = false;
            for (unsigned int j = 0; j < chosenPhotos.size(); j++)
            {
                if (chosenPhotos[j] == i)
                {
                    used = true;
                    break;
                }
            }
            if (!used)
            {
                minimumAngle = diff;
                bestPhoto = i;
            }
        }
    }
    cout << "Next best photo: " << bestPhoto << " - " << minimumAngle << endl;
}

void GLWidget::addPhoto(const char* filename, const Vector3f& lightDirection)
{
    currentPhoto = photos.size()-1;
    texIndex = photos.size()-1;
    if (finalLight != -1)
        finalLight = -1;
    createPhoto(getCurrentCamera(), filename, lightDirection);
    nextView();
    drawSuggestedLight = false;
}

void GLWidget::testPhoto(const char *filename, const Vector3f &lightDirection)
{
    testPhoto(getCurrentCamera(), filename, lightDirection);
}

void GLWidget::testPhoto(const PhotoCamera& camera, const char *filename, const Vector3f &lightDirection)
{
    testing = true;

    currentCamera.build(camera);
    updateCameraMatrices();

    QImage testingPhoto(filename);
    QImage interleaved = testingPhoto.mirrored();

    cout << "testing photo: " << filename << endl;

    GLubyte* newMappedTextures = new GLubyte[interleaved.height()*interleaved.width()*4];
    makeTexture(newMappedTextures, interleaved, interleaved.width(), interleaved.height(), 4);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, testTexture);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_SRGB, testingPhoto.width(), testingPhoto.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, newMappedTextures);


    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D_ARRAY,textureArray);
    glTexImage3D( GL_TEXTURE_2D_ARRAY, 0, GL_SRGB, testingPhoto.width(), testingPhoto.height(), 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, newMappedTextures);

    newLight = lightDirection;
    updateGL();
}

void GLWidget::setAlign(const bool &newAligning, const QImage& alignmentImage)
{
    aligning = newAligning;

    QImage interleaved = alignmentImage.mirrored();

    photoAlignment = new GLubyte[interleaved.height()*interleaved.width()*4];
    makeTexture(photoAlignment, interleaved, interleaved.width(), interleaved.height(), 4);

    if (aligning)
    {
        if (photoTexture == 0)
            createPhotoTexture();
        else
        {
            glDeleteTextures(1, &photoTexture);
            createPhotoTexture();
        }

        photoSize << interleaved.width(), interleaved.height();
        currentCamera.setDefault(cameraFov, photoSize(0), photoSize(1));
        updateCameraMatrices();

        if (fbo == 0)
            createFbo();
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,photoTexture);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_SRGB, interleaved.width(), interleaved.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, photoAlignment );
    delete[] photoAlignment;
    mixTextures = aligning;
    if (aligning == false)
    {
        glClear(GL_COLOR_BUFFER_BIT);

        glDeleteTextures(1, &photoTexture);
        photoTexture = 0;
    }
}

void GLWidget::startAlign(const QImage& alignmentImage, const OptEnvironment& env)
{
    OptEnvironment environment = env;
    presentPhoto = alignmentImage;
    if (correspondences.size() != imageCorrespondences.size())
    {
        emit message("Not the same number of correspondences");
        return;
    }

    vcg::CallBackPos* cb = 0;
    vector<PointCorrespondence*> pointCorrespondences;
    for (unsigned int i = 0; i < correspondences.size(); i++)
    {
        PointCorrespondence* newPointCorrespondence = new PointCorrespondence;
        newPointCorrespondence->addPoint(eigenToVcg(get<0>(correspondences[i])), Point2f(imageCorrespondences[i](0), imageCorrespondences[i](1)));
        pointCorrespondences.push_back(newPointCorrespondence);
    }
    environment.shot.Intrinsics.ViewportPx = vcg::Point2<int>(photoSize(0), photoSize(1));
    environment.shot.Intrinsics.CenterPx = vcg::Point2<float>(photoSize(0)/2, photoSize(1)/2);
    environment.shot.Intrinsics.FocalMm = currentCamera.getFocalLength();
    environment.shot.Intrinsics.PixelSizeMm = vcg::Point2<float>(currentCamera.getPixelSize()(0), currentCamera.getPixelSize()(1));

    // PLEASE REMEMBER THAT I TOOK OUT THE MESH SCALE MATRIX

    Matrix4f ext = viewMatrix*modelMatrix*iTranslate*cameraTrackball->getViewMatrix().matrix()*translate;
    // Complete version, but in our case I don't think there will be need of trackball's modelMatrix
//    Matrix4f ext = viewMatrix*modelMatrix*iTranslate*(cameraTrackball->getViewMatrix().matrix()*cameraTrackball->getModelMatrix().matrix())*translate;

//    cout << "ext: " << ext << endl;
    Matrix3f r = ext.topLeftCorner(3, 3);
//    float zoom = cameraTrackball->getZoom();

    Vector3f tra = r.inverse()*Vector3f(ext(0, 3), ext(1, 3), ext(2, 3));
//    cout << "tra: " << endl << tra << endl;

    vcg::Matrix44<float> rot;
    rot.SetIdentity();
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            rot.ElementAt(i, j) = r(i, j);

    vcg::Point3<float> t = eigenToVcg(-tra);

    environment.shot.Extrinsics.SetRot(rot);
    environment.shot.Extrinsics.SetTra(t);
//    environment.shot.RescalingWorld(cameraTrackball->getZoom(), true);
    QImage alignImage = alignmentImage;
    mutualInfo.applyFilter("Image alignment: Mutual Information", model.mesh(), alignImage, pointCorrespondences, environment, cb);
    cameraTrackball->reset();
    this->makeCurrent();
    aligned = true;
    updateGL();

}

void GLWidget::setCamera()
{
    Matrix4f ext = viewMatrix*modelMatrix*iTranslate*cameraTrackball->getViewMatrix().matrix()*translate;
    Matrix4f rotation = Matrix4f::Identity();
    rotation.topLeftCorner(3, 3) = ext.topLeftCorner(3, 3);
    Vector3f t = rotation.topLeftCorner(3, 3).inverse()*Vector3f(ext(0, 3), ext(1, 3), ext(2, 3));
    Matrix4f translation = Matrix4f::Identity();
    translation.col(3) << t[0], t[1], t[2], 1;

    currentCamera.setTranslation(translation);
    currentCamera.setRotation(rotation);

    currentCamera.buildExtrinsic();

    modelMatrix = Matrix4f::Identity();
    cameraTrackball->reset();
    updateCameraMatrices();
}

void GLWidget::restartCamera()
{
    PhotoCamera newCamera;
    currentCamera = newCamera;
    currentCamera.setDefault(40, photoSize(0), photoSize(1));
    cameraFov = 40;
    emit changedFov(40);
    modelMatrix = backupModelMatrix;
    lightTrackball->reset();
    cameraTrackball->reset();
    updateCameraMatrices();
}

void GLWidget::clearCorrespondences()
{
    correspondences.clear();
    imageCorrespondences.clear();
    updateGL();
}

void GLWidget::removeCorrespondence()
{
    correspondences.pop_back();
    imageCorrespondences.pop_back();
    updateGL();
}

void GLWidget::undoScale()
{
    cameraTrackball->applyScaleToViewMatrix(initialScale);
    updateGL();
}

void GLWidget::undoRotation()
{
    cameraTrackball->setFinalRotationPosition(initialRotation);
    cameraTrackball->rotateCamera();
    updateGL();
}

void GLWidget::undoTranslation()
{
    cameraTrackball->setFinalTranslationPosition(initialTranslation);
    cameraTrackball->translateCamera();
    updateGL();
}

bool GLWidget::hasAligned()
{
    return aligned;
}

void GLWidget::drawTrackball()
{
    renderTrackball = !renderTrackball;
    updateGL();
}

void GLWidget::clear()
{
    photos.clear();
    allLights.clear();
    centralLights.clear();
    errorsPerPhoto.clear();
    adjacencyList.clear();
    model.clear();

    GLuint ssbos[10] = {colorSsbo, positionSsbo, normalSsbo, feedbackSsbo, lightSsbo, angleSsbo, photosSsbo, lightPosSsbo, curveSsbo};
    glDeleteBuffers(10, ssbos);

    if (textureArray != 0)
    {
        glDeleteTextures(1, &textureArray);
    }

    initialSetup();

    if (numVertices > 0)
    {
        delete[] vertices;
        delete[] faceIndices;
    }

    for (unsigned int i = 0; i < textures.size(); i++)
        delete[] textures[i];
    textures.clear();

    reset();
    clearCorrespondences();
}

void GLWidget::drawHemisphereCoverage()
{
    showHemisphereCoverage = !showHemisphereCoverage;
    updateGL();
}

void GLWidget::setWebcamLightDirection(const Vector3f& newWebcamLightDirection)
{
    webcamLightDirection = newWebcamLightDirection;
}

int GLWidget::exportMesh(const char *filename)
{
    p_ply output;
    output = ply_create(filename, PLY_ASCII, NULL, 0, NULL);
    if (!output)
        return 1;
    if (!ply_add_element(output, "vertex", numVertices)) return 2;
    if (!ply_add_property(output, "x", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "y", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "z", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "nx", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "ny", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "nz", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "ra", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "rb", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "rc", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "rd", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "ga", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "gb", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "gc", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "gd", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "ba", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "bb", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "bc", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_property(output, "bd", PLY_FLOAT, PLY_FLOAT, PLY_FLOAT)) return 3;
    if (!ply_add_element(output, "face", numIndices/3)) return 4;
    if (!ply_add_property(output, "vertex_indices", PLY_LIST, PLY_UCHAR, PLY_INT)) return 5;
    ply_write_header(output);
    for (int i = 0; i < numVertices; i++)
    {
        ply_write(output, vertices[i].position.X());
        ply_write(output, vertices[i].position.Y());
        ply_write(output, vertices[i].position.Z());
        ply_write(output, vertices[i].normal.X());
        ply_write(output, vertices[i].normal.Y());
        ply_write(output, vertices[i].normal.Z());
        Vector4f pR, pG, pB;

        acquirePolynomial(i, pR, pG, pB);

        ply_write(output, pR(0));
        ply_write(output, pR(1));
        ply_write(output, pR(2));
        ply_write(output, pR(3));
        ply_write(output, pG(0));
        ply_write(output, pG(1));
        ply_write(output, pG(2));
        ply_write(output, pG(3));
        ply_write(output, pB(0));
        ply_write(output, pB(1));
        ply_write(output, pB(2));
        ply_write(output, pB(3));

        if (i == 0)
        {
            cout << "pr: " << pR.transpose() << endl;
            cout << "pg: " << pG.transpose() << endl;
            cout << "pb: " << pB.transpose() << endl;
        }
    }
    for (int i = 0; i < numIndices/3; i++)
    {
        ply_write(output, 3);
        ply_write(output, faceIndices[3*i]);
        ply_write(output, faceIndices[3*i+1]);
        ply_write(output, faceIndices[3*i+2]);
    }
    ply_close(output);
    return 0;
}

void GLWidget::setColorWeights(const Vector3f& newColorWeights)
{
    colorWeights = newColorWeights;
    useColorWeights = true;
}

void GLWidget::setUseColorWeights()
{
    useColorWeights = !useColorWeights;
    updateGL();
}

void GLWidget::deleteTextures()
{
    if (textureArray != 0)
    {
        glDeleteTextures(1, &textureArray);
    }
}

void GLWidget::updateCamera(const vcg::Shot<float>& newShot)
{
    PhotoCamera newCamera;
    float ratio = presentPhoto.height() /(float) newShot.Intrinsics.ViewportPx[1];

    newCamera.setHeight(presentPhoto.height());
    newCamera.setWidth(presentPhoto.width());

    vcg::Point2<float> pixelSize = newShot.Intrinsics.PixelSizeMm;
    pixelSize /= ratio;
    newCamera.setPixelSize(pixelSize.X(), pixelSize.Y());

//    cout << "pixel size: " << pixelSize.X() << " " << pixelSize.Y() << endl;

    Vector2i principalPoint = Vector2i(newShot.Intrinsics.ViewportPx[0]/2.0, newShot.Intrinsics.ViewportPx[1]/2.0);
    newCamera.setPrincipalPoint(principalPoint);

    vcg::Matrix44<float> newRot = newShot.Extrinsics.Rot();
    vcg::Point3<float> newTra =  newShot.Extrinsics.Tra();

    Matrix4f t = Matrix4f::Identity();
    t.col(3) = Vector4f(-newTra.X(), -newTra.Y(), -newTra.Z(), 1);
    newCamera.setTranslation(t);

    newCamera.setFocalLength(newShot.Intrinsics.FocalMm);

//    cout << "new focal length: " << newCamera.getFocalLength() << endl;

    Matrix4f r;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            r(i, j) = newRot.ElementAt(i, j);
    newCamera.setRotation(r);

    currentCamera = newCamera;
    currentCamera.buildExtrinsic();
    currentCamera.buildIntrinsic();
    currentCamera.setWidthHeight(presentPhoto.width(), presentPhoto.height());

    modelMatrix = Matrix4f::Identity();

    updateCameraMatrices();
}


void GLWidget::moveTextureBlock()
{
    bool first = false;
    if (xStep == -1)
        first = true;
    xStep = (xStep+1)%numXSteps;
    if (xStep == 0 && !first)
        yStep = (yStep+1)%numYSteps;
    loadTexture();
    updateGL();
}

void GLWidget::getPlotData()
{
    emit plot(drDataPoints, numDataPoints, drDataCurve, 0);
    emit plot(dgDataPoints, numDataPoints, dgDataCurve, 1);
    emit plot(dbDataPoints, numDataPoints, dbDataCurve, 2);
    emit plot(dlDataPoints, numDataPoints, drDataCurve, 3);
}

void GLWidget::printStatistics()
{
    calculateAverageCoverage();
    calculateStandardDeviation();
    cout << "average coverage: " << averageCoverage << endl;
    cout << "standard deviation: " << standardDeviation << endl;
    ofstream out("statistics.txt");
    for (int i = 0; i < numVertices; i++)
        if (samplesVertices[i] > 0)
            out << samplesVertices[i] << " ";
    out.close();

    averageTimeLights();
}

void GLWidget::printDebugData()
{
    cout << "view matrix: " << endl << viewMatrix << endl;
    cout << "model matrix: " << endl << modelMatrix << endl;
    cout << "mesh scale: " << endl << meshScaleMatrix << endl;
    cout << "projection matrix: " << endl << projectionMatrix << endl;
    cout << "current photo size: " << presentPhoto.width() << " " << presentPhoto.height() << endl;
}

vector<int> GLWidget::getChosenPhotos()
{
    return chosenPhotos;
}

void GLWidget::setSave()
{
    save = true;
}

void GLWidget::renderAxes()
{
    renderAxis = true;
}

void GLWidget::removePhoto()
{
    deletePhoto = true;
    updateGL();
    deletePhoto = false;
    if (photos.size() == 0)
        return;
    vector<Photo>::iterator photosIt = photos.begin();
    vector<bool>::iterator usedPhotosIt = usedPhotos.begin();
    vector<int>::iterator camerasIt = cameras.begin();

    for (int i = 0; i < currentPhoto; i++)
    {
        photosIt++;
        usedPhotosIt++;
    }
    for (unsigned int i = 0; i < cameras.size(); i++)
    {
        if (cameras[i] == currentPhoto)
        {
            cameras.erase(camerasIt);
            break;
        }
        camerasIt++;
    }

    photos.erase(photosIt);
    usedPhotos.erase(usedPhotosIt);

    removeLightSSBO();

    if (currentPhoto == numPhotos-1)
        currentPhoto -= 1;
    numPhotos--;
    loadTexture();
    updateCameraMatrices();
    updateGL();
    emit message("Photo deleted");
}

void GLWidget::changeThresholdAngle(int increase)
{
    if (increase > 0)
        thresholdNormalAngle += 5;
    else
        thresholdNormalAngle -= 5;

    QString changedThresholdAngleMessage = QString("Threshold Angle: ") + QString::number(thresholdNormalAngle);

    emit message(changedThresholdAngleMessage.toStdString().c_str());

    updateGL();
}

void GLWidget::setRenderingBonn(bool rendering)
{
    renderingBonn = rendering;
}

void GLWidget::calculateAverageCoverage()
{
    averageCoverage = 0;
    samplesVertices = new float[numVertices];
    unsigned long long int numSamples = 0;
    unsigned long long int numVerticesCovered = 0;
    float x, yr, yg, yb;
    unsigned long long int curveSize = sizeof(GLfloat)*numVertices;
    curveSize *= 5*MAX_PHOTOS;

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, curveSsbo);
    GLfloat* buffer_data = (GLfloat*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, curveSize, GL_MAP_READ_BIT);


    for (int i = 0; i < numVertices; i++)
    {
        bool vertexCovered = false;
        int sampleVertex = 0;
        for (int j = 0; j < MAX_PHOTOS; j++)
        {
            x = buffer_data[5*MAX_PHOTOS*i + 5*j];
            yr = buffer_data[5*MAX_PHOTOS*i + 5*j+1];
            yg = buffer_data[5*MAX_PHOTOS*i + 5*j+2];
            yb = buffer_data[5*MAX_PHOTOS*i + 5*j+3];

            if (x < 10 || yr > 0  || yg > 0 || yb > 0)
            {
                numSamples++;
                sampleVertex++;
                vertexCovered = true;
            }
        }
        samplesVertices[i] = sampleVertex;
        if (vertexCovered)
            numVerticesCovered++;
    }
    averageCoverage = ((double) numSamples)/numVerticesCovered;

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
}

void GLWidget::calculateStandardDeviation()
{
    standardDeviation = 0;
    int numVerticesCovered = 0;
    for (int i = 0; i < numVertices; i++)
        if (samplesVertices[i] > 0)
        {
            standardDeviation += pow(samplesVertices[i] - averageCoverage, 2);
            numVerticesCovered++;
        }
    standardDeviation /= numVerticesCovered;
    standardDeviation = sqrt(standardDeviation);
}

void GLWidget::averageTimeLights()
{
    float totalTime = 0;
    cout << "times: " << endl;
    for (unsigned int i = 0; i < timeCalculateLights.size(); i++)
    {
        cout << timeCalculateLights[i] << " ";
        totalTime += timeCalculateLights[i];
    }
    cout << endl << "aveage time to calculate lights: " << totalTime/timeCalculateLights.size() << endl;
}

void GLWidget::updateCameraMatrices()
{
//    cout << "updating camera matrices" << endl;
    setWorldTransformation();
    setGlProjectionMatrix();
    Vector3f light;
    if (calculatingNewLight)
        light = getCurrentLight();
    else
    {
        if (hasPhoto())
            light = getLight();
        else
            light = Vector3f(0, 0, 1);
    }
    Vector4f newCenter = Vector4f(center(0), center(1), center(2), 1);
    newCenter = viewMatrix*modelMatrix*newCenter;
    newCenter /= newCenter(3);

    translate = Matrix4f::Identity();
    translate.col(3) << -newCenter(0), -newCenter(1), -newCenter(2), 1;
    iTranslate = Matrix4f::Identity();
    iTranslate.col(3) << newCenter(0), newCenter(1), newCenter(2), 1;

//    Matrix4f fullProjection = projectionMatrix*viewMatrix*modelMatrix;
//    Vector4f projectedCenter = fullProjection*Vector4f(center(0), center(1), center(2), 1);
//    projectedCenter /= projectedCenter(3);
//    Matrix4f newTranslation = Matrix4f::Identity();
//    if (fabs(projectedCenter(0)) > 0.1 ||  fabs(projectedCenter(1)) > 0.1)
//    {
//        projectedCenter -= Vector4f(projectedCenter(0), projectedCenter(1), 0, 0);
//        Vector4f newCenter = fullProjection.inverse()*projectedCenter;
//        newTranslation = translation(newCenter(0)-center(0), newCenter(1)-center(1), 0);
//    }

//    Vector4f center4 = Vector4f(center(0), center(1), center(2), 1);
//    Vector4f newCenter = newTranslation*viewMatrix*modelMatrix*center4;
//    newCenter /= newCenter(3);


//    Vector3f bBox = model.bBox();
//    float maxDim = qMax(qMax(bBox(0), bBox(1)), bBox(2));
//    Vector4f centerMax = newCenter + Vector4f(maxDim, maxDim, maxDim, 0);
//    Vector4f centerMin = newCenter - Vector4f(maxDim, maxDim, maxDim, 0);
//    Vector4f projectedCenterMax = projectionMatrix*newTranslation*viewMatrix*modelMatrix*centerMax;
//    projectedCenterMax /= projectedCenterMax(3);
//    Vector4f projectedCenterMin = projectionMatrix*newTranslation*viewMatrix*modelMatrix*centerMin;
//    projectedCenterMin /= projectedCenterMin(3);

//    if (fabs(projectedCenterMax(1)) > 1 || fabs(projectedCenterMin(1)) > 1)
//    {
//        float max = fabs(projectedCenterMax(1)) - 1;
//        if (fabs(projectedCenterMin(1)) - 1 > max)
//            max = fabs(projectedCenterMin(1)) -1;
//        int sign = -1;
//        if (center(2) < 0)
//            sign = 1;

//        Matrix4f translateZ = translation(0, 0, sign*center(2));
//        newTranslation *= translateZ;
//    }
//    newCenter = newTranslation*viewMatrix*modelMatrix*center4;
//    newCenter /= newCenter(3);

//    Matrix4f newCenterTranslation = translation(newCenter(0), newCenter(1), newCenter(2));
//    Matrix4f newCenterITranslation = translation(-newCenter(0), -newCenter(1), -newCenter(2));

//    Matrix4f rotationLight = lookAt((light), Vector3f(0, 0, 0), Vector3f(0, 1, 0));
//    lightViewMatrix = newCenterTranslation*rotationLight*newCenterITranslation*newTranslation;

    Matrix4f fullProjection = projectionMatrix*viewMatrix*modelMatrix;
    Vector4f projectedCenter = fullProjection*Vector4f(center(0), center(1), center(2), 1);
    projectedCenter /= projectedCenter(3);
    Matrix4f newTranslation = Matrix4f::Identity();
    Vector4f imageCenter = fullProjection.inverse()*Vector4f(0, 0, 0, 1);
    imageCenter /= imageCenter(3);
    Matrix4f rotationLight = lookAt((light), Vector3f(0, 0, 0), Vector3f(0, 1, 0));
    if (renderingBonn)
    {
        Eigen::Quaternionf light_view = Eigen::Quaternionf::FromTwoVectors(light, Eigen::Vector3f::UnitZ());
        Affine3f lightMatrix = Affine3f::Identity();
        lightMatrix.rotate(light_view);

        Vector4f newCenter = viewMatrix*modelMatrix*Vector4f(center(0), center(1), center(2), 1);
        newTranslation = translation(newCenter(0), newCenter(1), newCenter(2));
        Matrix4f newITranslation = translation(-newCenter(0), -newCenter(1), -newCenter(2));
        lightViewMatrix = newTranslation*lightMatrix.matrix()*newITranslation;
        cout << "light: " << light.transpose() << endl;
    }
    else
    {
        if (fabs(projectedCenter(0)) > 1 || fabs(projectedCenter(1)) > 1)
        {
            imageCenter(2) = center(2);
            newTranslation = translation(imageCenter(0), imageCenter(1), imageCenter(2));
            Matrix4f newITranslation = translation(-imageCenter(0), -imageCenter(1), -imageCenter(2));
            lightViewMatrix = newTranslation*rotationLight*newITranslation;
        }
        else
        {
            if (fabs(projectedCenter(0)) > 0.1 ||  fabs(projectedCenter(1)) > 0.1)
            {
                projectedCenter -= Vector4f(projectedCenter(0), projectedCenter(1), 0, 0);
                Vector4f newCenter = fullProjection.inverse()*projectedCenter;
                newCenter /= newCenter(3);
                newTranslation = translation(newCenter(0)-center(0), newCenter(1)-center(1), 0);
            }
            Vector4f center4 = Vector4f(center(0), center(1), center(2), 1);
            Vector4f newCenter = newTranslation*viewMatrix*modelMatrix*center4;
            newCenter /= newCenter(3);
            Matrix4f newCenterTranslation = translation(newCenter(0), newCenter(1), newCenter(2));
            Matrix4f newCenterITranslation = translation(-newCenter(0), -newCenter(1), -newCenter(2));

            lightViewMatrix = newCenterTranslation*rotationLight*newCenterITranslation*newTranslation;
        }
    }

//    cout << "light view matrix: " << endl << lightViewMatrix << endl;
}

void GLWidget::nextView(bool getCamera)
{
    if (getCamera)
        getNextCamera();
    loadTexture();

    texIndex++;
    if (texIndex == numPhotos)
        texIndex = 0;

    updateCameraMatrices();

    updateGL();
}

void GLWidget::previousView()
{
    getPreviousCamera();
    loadTexture();

    texIndex--;
    if (texIndex < 0)
        texIndex = numPhotos - 1;

    updateCameraMatrices();

    updateGL();
}

const PhotoCamera& GLWidget::getCurrentCamera()
{
    return currentCamera;
}

const PhotoCamera& GLWidget::getNextCamera()
{
    if (chosenPhotos.size() > 1)
    {
        indexPhoto++;
        if (indexPhoto == chosenPhotos.size())
            indexPhoto = 0;
        currentPhoto = chosenPhotos[indexPhoto];
    }
    else
    {
        if (photos.size() != 0)
        {
            currentPhoto++;
            if (currentPhoto == photos.size())
                currentPhoto = 0;
        }
    }

    changedView = false;
    if (currentCamera != photos[currentPhoto].getCamera())
        changedView = true;
    currentCamera = photos[currentPhoto].getCamera();
    if (changedView)
    {
        bool added = false;
        unsigned int index;
        for (unsigned int i = 0; i < cameras.size(); i++)
            if (cameras[i] == currentPhoto)
            {
                added = true;
                index = i;
            }
        if (!added)
            cameras.push_back(currentPhoto);
        else
            if (index != cameras.size()-1)
                finalLight = cameras[index+1];
            else
                finalLight = numPhotos;
        startingLight = currentPhoto;
    }

    return currentCamera;
}

const PhotoCamera& GLWidget::getPreviousCamera()
{
    if (chosenPhotos.size() > 1)
    {
        indexPhoto--;
        if (indexPhoto < 0)
            indexPhoto = chosenPhotos.size() - 1;
        currentPhoto = chosenPhotos[indexPhoto];
    }
    else
    {
        if (photos.size() != 0)
        {
            currentPhoto--;
            if (currentPhoto < 0)
                currentPhoto = photos.size() - 1;
        }
    }
    changedView = false;
    if (currentCamera != photos[currentPhoto].getCamera())
        changedView = true;
    currentCamera = photos[currentPhoto].getCamera();
    return currentCamera;
}

void GLWidget::createPhoto(const PhotoCamera& camera, const char *filename, const Vector3f& lightPosition)
{
    Photo newPhoto(filename);
    newPhoto.buildCamera(camera);
    newPhoto.setLight(lightPosition);

//    double invBBDiag = 2.0f/model.bbDiag();
//    newPhoto.applyScale(invBBDiag);

    photos.push_back(newPhoto);
    usedPhotos.push_back(false);
}

void GLWidget::createPhoto(const PhotoCamera& camera, const char *filename, const Vector3f& lightPosition, const Matrix4f& newLightViewMatrix)
{
    Photo newPhoto(filename);
    newPhoto.buildCamera(camera);
    newPhoto.setLight(lightPosition);
    newPhoto.setLightViewMatrix(newLightViewMatrix);

    photos.push_back(newPhoto);
    usedPhotos.push_back(false);
}

bool GLWidget::hasPhoto()
{
    return (photos.size() != 0);
}

const Vector3f& GLWidget::getLight()
{
    return photos[currentPhoto].getLight();
}

const Vector3f& GLWidget::getCurrentLight()
{
    return newLight;
}

void GLWidget::start()
{
    clock_t before, after;

    before = clock();

    for (unsigned int i = 0; i < photos.size() - 1; i++)
    {
        nextView();
    }
    getPixel = false;
    after = clock();
    timeFirstCycle = (float)(after-before)/CLOCKS_PER_SEC;
    cout << "time first cycle: " << timeFirstCycle << endl;

    fitBRDF();
}

void GLWidget::fitBRDF()
{
    clock_t before, after;

    for (unsigned int i = 0; i < cameras.size(); i++)
    {
        fitCurves = true;
        currentPhoto = cameras[i];
        currentCamera = photos[currentPhoto].getCamera();

//        discardBadVertices = true;
        before = clock();
        nextView(false);
        after = clock();
        timeOtherCycles = (float)(after-before)/CLOCKS_PER_SEC;
        cout << "time fitting: " << timeOtherCycles << endl;
//        discardBadVertices = false;
    }

    if (cameras.size() > 1)
    {
        currentPhoto = 0;
        currentCamera = photos[currentPhoto].getCamera();
        loadTexture();

        texIndex = 0;

        updateCameraMatrices();
    }
    QString fitMessage("Fit done with ");
    QString numPhotosString;
    numPhotosString.setNum(numPhotos);
    fitMessage += numPhotosString + " photos";
    emit message(fitMessage.toStdString().c_str());
    updateGL();
}

void GLWidget::useNeighbors()
{
    fillNeighbors = true;
    neighborsStart = clock();
}

void GLWidget::reset()
{
    lightTrackball->reset();
    cameraTrackball->reset();
//    updateGL();
}

void GLWidget::renderDiffer()
{
    if (renderDifferP == false && renderDifferM == false && renderPixel == false)
    {
        renderPixel = true;
    }
    else
    {
        renderDifferP = false;
        renderDifferM = false;
        renderPixel = false;
    }
    updateGL();
}

void GLWidget::printError()
{
    GLfloat error = readBuffer(feedbackSsbo, 6, numVertices);
    cout << "error: " << error << endl;
}

void GLWidget::reloadShaders()
{
    for (auto &s :{ref(getPixelProgram), ref(fittingProgram), ref(suggestLightProgram), ref(coordProgram), ref(textureProgram),ref(mixProgram), ref(axisProgram), ref(clearProgram),
                   ref(renderProgram), ref(lightProgram), ref(correspondenceProgram), ref(sphereProgram), ref(neighborsProgram), ref(renderBonnProgram)})
        detachShaders(s);

    compileShaders(getPixelProgram, "shaders/draw.vert", "shaders/draw.frag");
    compileShaders(fittingProgram, "shaders/fitting.vert", "shaders/fitting.frag");
    compileShaders(suggestLightProgram, "shaders/calculateLight.vert", "shaders/calculateLight.frag");
    compileShaders(coordProgram, "shaders/coord.vert", "shaders/coord.frag");
    compileShaders(textureProgram, "shaders/texture.vert", "shaders/texture.frag");
    compileShaders(mixProgram, "shaders/mixtexture.vert", "shaders/mixtexture.frag");
    compileShaders(clearProgram, "shaders/clearbuffer.vert", "shaders/clearbuffer.frag");
    compileShaders(renderProgram, "shaders/render.vert", "shaders/render.frag", "shaders/render.geom");
    compileShaders(lightProgram, "shaders/light.vert", "shaders/light.frag");
    compileShaders(correspondenceProgram, "shaders/token.vert", "shaders/token.frag");
    compileShaders(sphereProgram, "shaders/sphere.vert", "shaders/sphere.frag");
    compileShaders(neighborsProgram, "shaders/neighbors.vert", "shaders/neighbors.frag");
    compileShaders(axisProgram, "shaders/axis.vert", "shaders/axis.frag");
    compileShaders(renderBonnProgram, "shaders/renderBonn.vert", "shaders/renderBonn.frag");

    updateGL();
}
