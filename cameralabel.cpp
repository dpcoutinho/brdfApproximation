#include "cameralabel.h"

bool CameraLabel::detectChart(int threshold)
{
    colorThreshold = threshold;
    detectingChart = true;
    return false;
    for (int j = 0; j < original.height(); j++)
    {
        for (int i = 0; i < original.width(); i++)
        {
            int r, g, b;
            QColor pixelColor = original.pixel(i, j);
            pixelColor.getRgb(&r, &g, &b);
            Vector3f colorN = Vector3f(r, g, b);
            Vector3f diff = rgbToLab(chart[0][0]) - rgbToLab(colorN);
            float diffNorm = diff.norm();
            clamp(diffNorm, 0, 255);
            if (diffNorm < 35)
            {
                QPoint found(i, j);
                detectColors(found);
                return true;
            }
        }
    }
    QString missed("Failed to find color: 0 0");
    emit message(missed.toStdString().c_str());
    return false;
}

void CameraLabel::acceptChart()
{
    accepted = true;
    calculateAverageColors();
    linearRegression();
    vector<Vector3f> newAverages;
    for (unsigned int i = 0; i < averageColors.size(); i++)
    {
        QImage square = original.copy(colorSquares[i]);
        Vector3f average(0, 0, 0);
        for (int w = 0; w < square.width(); w++)
        {
            for (int h = 0; h < square.height(); h++)
            {
                int r, g, b;
                QColor pixelColor = square.pixel(w, h);
                pixelColor.getRgb(&r, &g, &b);
                float nr, ng, nb;
                nr = colorWeights(0)*r;
                ng = colorWeights(1)*g;
                nb = colorWeights(2)*b;
                Vector3f newAverage(nr, ng, nb);
                average += newAverage;
                QColor newColor(nr, ng, nb);
                square.setPixel(w, h, newColor.rgb());
            }
        }
        average /= (square.width()*square.height());
        cout << "average: " << average.transpose() << endl;
        newAverages.push_back(average);
    }
}


void CameraLabel::detectColors(const QPoint &pos)
{
    QImage sobelImage = sobel(original);
    sobelImage.save("sobel.jpg");
    QRect limitSquare;
    QRect square = fillSquare(sobelImage, pos, 0, 0, limitSquare);
    if (limitSquare.width() == 0)
        limitSquare = square;

    QPoint center = square.center();
    QPoint lastCenter = center;
    QPoint lastCenterY = center;
    QRect lastSquare = square;
    colorSquares.push_back(square);
    float threshold = 30;
    for (int i = 0; i < 4; i++)
    {
        if (i != 0)
        {
            QPoint found(-1, -1);
            for (int k = lastCenterY.y() + 1.20*lastSquare.height(); k < original.height(); k++)
            {
                QColor pixelColor = sobelImage.pixel(lastCenterY.x(), k);
                if (pixelColor.red() < threshold)
                {
                    found = QPoint(lastCenterY.x(), k);
                    break;
                }

            }
            if (found.x() == -1 || found.y() == -1)
            {
//                colorSquares.clear();

                QString iS;
                iS.setNum(i);
                QString missed("Failed to find color: " + iS + " 0");
                emit message(missed.toStdString().c_str());
                return;
            }
            QRect newSquare = fillSquare(sobelImage, found, i, 0, limitSquare);
            colorSquares.push_back(newSquare);
            lastCenterY = newSquare.center();
        }
        lastCenter = lastCenterY;

        for (int j = 1; j < 6; j++)
        {

            QPoint found(-1, -1);
            for (int k = lastCenter.x() + 1.20*lastSquare.width(); k < original.width(); k++)
            {
                QColor pixelColor = sobelImage.pixel(k, lastCenter.y());
                if (pixelColor.red() < threshold)
                {
                    found = QPoint(k, lastCenter.y());
                    break;
                }
            }
            if (found.x() == -1 || found.y() == -1)
            {
//                colorSquares.clear();
                QString iS;
                QString jS;
                iS.setNum(i);
                jS.setNum(j);
                QString missed("Failed to find color: " + iS + " " + jS);
                emit message(missed.toStdString().c_str());
                return;
            }

            QRect blackLimit;
            if (i == 3 && j == 5)
                blackLimit = limitSquare;
            QRect newSquare = fillSquare(sobelImage, found, i, j, blackLimit);
            colorSquares.push_back(newSquare);
            lastSquare = newSquare;
            lastCenter = newSquare.center();
        }
    }
}


QRect CameraLabel::fillSquare(const QImage& image, const QPoint &pos, const int& indexI, const int& indexJ, const QRect& limitSquare)
{
        QPoint max, min;
        min = QPoint(image.width()+1, image.height()+1);
        max = QPoint(-1, -1);
        int w = image.width();
        int h = image.height();
        bool** marked;
        marked = new bool*[w];
        for (int i = 0; i < w; i++)
        {
            marked[i] = new bool[h];
            for (int j = 0; j < h; j++)
                marked[i][j] = false;
        }
        stack<QPoint> squares;
        squares.push(pos);
        bool pushHorizontal = true;
        bool pushVertical = true;

        QRect square;
        while (true)
        {
            while(!squares.empty())
            {
                QPoint current = squares.top();
                squares.pop();
                if (marked[current.x()][current.y() == true])
                    continue;
                if (limitSquare.width() > 0 && (max.x() - min.x()) > limitSquare.width())
                    pushHorizontal = false;
                if (limitSquare.height() > 0 &&  (max.y() - min.y()) > limitSquare.height())
                    pushVertical = false;
                if (current.x() < 0 || current.x() >= image.width()  || current.y() < 0  || current.y() >= image.height())
                    continue;
                marked[current.x()][current.y()] = true;
                int r, g, b;
                QColor pixelColor = image.pixel(current);
                pixelColor.getRgb(&r, &g, &b);
                if (r < colorThreshold)
                {
                    if (current.x() < min.x() && pushHorizontal)
                        min.setX(current.x());
                    if (current.y() < min.y() && pushVertical)
                        min.setY(current.y());
                    if (current.x() > max.x() && pushHorizontal)
                        max.setX(current.x());
                    if (current.y() > max.y() && pushVertical)
                        max.setY(current.y());

                    if (current.x() > 0 && marked[current.x()-1][current.y()] == false && current.x() >= min.x() && current.x() <= max.x())
                        squares.push(QPoint(current.x()-1, current.y()));
                    if (current.x() < image.width()-1 && marked[current.x()+1][current.y()] == false && current.x() >= min.x() && current.x() <= max.x() )
                        squares.push(QPoint(current.x()+1, current.y()));
                    if (current.y() > 0 && marked[current.x()][current.y()-1] == false && current.y() >= min.y() && current.y() <= max.y() )
                        squares.push(QPoint(current.x(), current.y()-1));
                    if (current.y() < image.height()-1 && marked[current.x()][current.y()+1] == false && current.y() >= min.y() && current.y() <= max.y() )
                        squares.push(QPoint(current.x(), current.y()+1));
//                    original.setPixel(current, QColor(chart[indexI][indexJ](0), chart[indexI][indexJ](1), chart[indexI][indexJ](2)).rgb());
                }
            }
            if (max.x() != -1 && max.y() != -1)
                square = QRect(min, max);
            break;
        }
        for (int i = 0; i < w; i++)
            delete[] marked[i];
        delete[] marked;

        return square;
}

void CameraLabel::linearRegression()
{
    bool smaller[4][6];
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 6; j++)
        {
            float weight;
            for (int k = 0; k < 3; k++)
            {
                double xTx = pow(averageColors[i*6+j](k),2);
                double xTy = averageColors[i*6+j](k)*chart[i][j](k);
                weight = xTy / xTx;
            }
//            if (weight < 2.5)
                smaller[i][j] = true;
//            else
//                smaller[i][j] = false;
        }

    for (int k = 0; k < 3; k++)
    {
        float xTx = 0;
        float xTy = 0;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 6; j++)
            {
                if (smaller[i][j])
                {
                    xTx += pow(averageColors[i*6+j](k),2);
                    xTy += averageColors[i*6+j](k)*chart[i][j](k);
                }
            }
        cout << "color weight = " << xTy << " / " << xTx << endl;
        colorWeights(k) = xTy / xTx;
    }
}

void CameraLabel::calculateAverageColors()
{
    for (unsigned int i = 0; i < colorSquares.size(); i++)
    {
        QImage square = original.copy(colorSquares[i]);
        Vector3f newColor(0, 0, 0);
        averageColors.push_back(newColor);
        for (int w = 0; w < square.width(); w++)
        {
            for (int h = 0; h < square.height(); h++)
            {
                int r, g, b;
                QColor aux = square.pixel(w, h);
                aux.getRgb(&r, &g, &b);
                Vector3f pixelColor(r, g, b);
                averageColors[i] += pixelColor;
            }
        }
        averageColors[i] /= (square.width()*square.height());
    }
}


QImage CameraLabel::sobel(QImage image)
{
    QImage sobelImage(image);
    int sobelX[3][3], sobelY[3][3];
    sobelX[0][0] = 1;sobelX[0][1] = 0;sobelX[0][2] = -1;
    sobelX[1][0] = 2;sobelX[1][1] = 0;sobelX[1][2] = -2;
    sobelX[2][0] = 1;sobelX[2][1] = 0;sobelX[2][2] = -1;

    sobelY[0][0] = 1;sobelY[0][1] = 2;sobelY[0][2] = 1;
    sobelY[1][0] = 0;sobelY[1][1] = 0;sobelY[1][2] = 0;
    sobelY[2][0] =-1;sobelY[2][1] =-2;sobelY[2][2] =-1;
    Vector3f** matrix = new Vector3f*[image.width()];
    Vector3f** sobel1 = new Vector3f*[image.width()];
    Vector3f** sobel2 = new Vector3f*[image.width()];
    for (int i = 0; i < image.width(); i++)
    {
        matrix[i] = new Vector3f[image.height()];
        sobel1[i] = new Vector3f[image.height()];
        sobel2[i] = new Vector3f[image.height()];
    }
    for (int i = 0; i < image.width(); i++)
    {
        for (int j = 0; j < image.height(); j++)
        {
            matrix[i][j](0) = qRed(image.pixel(i,j));
            matrix[i][j](1) = qGreen(image.pixel(i,j));
            matrix[i][j](2) = qBlue(image.pixel(i,j));
        }
    }
    for (int i = 1; i < image.width()-1; i++)
    {
        for (int j = 1; j < image.height()-1; j++)
        {
            for (int k = 0; k < 3; k++)
            {
                sobel1[i][j](k) = sobelX[1][1]*matrix[i][j](k);
                sobel1[i][j](k) += sobelX[1][0]*matrix[i-1][j](k);
                sobel1[i][j](k) += sobelX[0][0]*matrix[i-1][j-1](k);
                sobel1[i][j](k) += sobelX[2][0]*matrix[i-1][j+1](k);
                sobel1[i][j](k) += sobelX[1][2]*matrix[i+1][j](k);
                sobel1[i][j](k) += sobelX[0][2]*matrix[i+1][j-1](k);
                sobel1[i][j](k) += sobelX[2][2]*matrix[i+1][j+1](k);
                sobel1[i][j](k) += sobelX[0][1]*matrix[i][j-1](k);
                sobel1[i][j](k) += sobelX[2][1]*matrix[i][j+1](k);
            }
        }
    }
    for (int i = 1; i < image.width()-1; i++)
    {
        for (int j = 1; j < image.height()-1; j++)
        {
            for (int k = 0; k < 3; k++)
            {
                sobel2[i][j](k) = sobelY[1][1]*matrix[i][j](k);
                sobel2[i][j](k) += sobelY[1][0]*matrix[i-1][j](k);
                sobel2[i][j](k) += sobelY[0][0]*matrix[i-1][j-1](k);
                sobel2[i][j](k) += sobelY[2][0]*matrix[i-1][j+1](k);
                sobel2[i][j](k) += sobelY[1][2]*matrix[i+1][j](k);
                sobel2[i][j](k) += sobelY[0][2]*matrix[i+1][j-1](k);
                sobel2[i][j](k) += sobelY[2][2]*matrix[i+1][j+1](k);
                sobel2[i][j](k) += sobelY[0][1]*matrix[i][j-1](k);
                sobel2[i][j](k) += sobelY[2][1]*matrix[i][j+1](k);
            }
        }
    }


    for (int i = 1; i < image.width()-1; i++)
    {
        for (int j = 1; j < image.height()-1; j++)
        {
            float value0 = sqrt(pow(sobel1[i][j](0),2) + pow(sobel2[i][j](0),2));
            float value1 = sqrt(pow(sobel1[i][j](1),2) + pow(sobel2[i][j](1),2));
            float value2 = sqrt(pow(sobel1[i][j](2),2) + pow(sobel2[i][j](2),2));
            clamp(value0, 0, 255);
            clamp(value1, 0, 255);
            clamp(value2, 0, 255);
            float value = qMax(qMax(value0, value1), value2);
            QColor newColor((int)value, (int)value, (int)value);
            sobelImage.setPixel(i,j, newColor.rgb());
        }
    }

    for (int i = 0; i < image.width(); i++)
    {
        delete[] matrix[i];
        delete[] sobel1[i];
        delete[] sobel2[i];
    }
    delete[] matrix;
    delete[] sobel1;
    delete[] sobel2;

    return sobelImage;
}

CameraLabel::CameraLabel(QWidget *parent) :
    QLabel(parent)
{
    changingRadius = false;
    sphereCenter = Vector3f(0, 0, 0);
    lightCenter = Vector3f(0, 0, 0);
    detectingSphere = false;
    detectingChart = false;
    delimitingArea = false;
    aligning = false;
    changingSquare = false;
    accepted = false;
    zoom = 0;
    accumZoom = 0;
    cntCorners = 0;
    areaStart = QPoint(0, 0);
    areaEnd = QPoint(0, 0);
    chart = new Vector3f*[4];
    colorDetected = new bool*[4];
    for (int i = 0; i < 4; i++)
    {
        chart[i] = new Vector3f[6];
        colorDetected[i] = new bool[6];
    }
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 6; j++)
            colorDetected[i][j] = false;
    chart[0][0] = Vector3f(115, 82, 68);
    chart[0][1] = Vector3f(194, 150, 130);
    chart[0][2] = Vector3f(98, 122, 157);
    chart[0][3] = Vector3f(87, 108, 67);
    chart[0][4] = Vector3f(133, 128, 177);
    chart[0][5] = Vector3f(103, 189, 170);
    chart[1][0] = Vector3f(214, 126, 44);
    chart[1][1] = Vector3f(80, 91, 166);
    chart[1][2] = Vector3f(193, 90, 99);
    chart[1][3] = Vector3f(94, 60, 108);
    chart[1][4] = Vector3f(157, 188, 64);
    chart[1][5] = Vector3f(224, 163, 46);
    chart[2][0] = Vector3f(56, 61, 150);
    chart[2][1] = Vector3f(70, 148, 73);
    chart[2][2] = Vector3f(175, 54, 60);
    chart[2][3] = Vector3f(231, 199, 31);
    chart[2][4] = Vector3f(187, 86, 149);
    chart[2][5] = Vector3f(8, 133, 161);
    chart[3][0] = Vector3f(243, 243, 242);
    chart[3][1] = Vector3f(200, 200, 200);
    chart[3][2] = Vector3f(160, 160, 160);
    chart[3][3] = Vector3f(122, 122, 121);
    chart[3][4] = Vector3f(85, 85, 85);
    chart[3][5] = Vector3f(52, 52, 52);
}

CameraLabel::~CameraLabel()
{
    for (int i = 0; i < 4; i++)
    {
        delete[] chart[i];
        delete[] colorDetected[i];
    }
    delete[] chart;
    delete[] colorDetected;
}

void CameraLabel::setDetectingSphere(const bool &newValue)
{
    detectingSphere = newValue;
}

void CameraLabel::setAlign(const bool &newAligning)
{
    aligning = newAligning;
}

void CameraLabel::drawCenters(const vector<Vector3f> &newCenters)
{
    detectingSphere = true;
    currentCenters = newCenters;
    for (unsigned int i = 0; i < currentCenters.size(); i++)
    {
        Vector3f newRadiusCenter = currentCenters[i];
        newRadiusCenter[0] += currentCenters[i][2];
        newRadiusCenter[2] = 4;
        radiusControls.push_back(newRadiusCenter);
    }
    if (currentCenters.size() == 1)
    {
        sphereCenter = currentCenters[0];
        radiusControl = radiusControls[0];
        currentCenters.clear();
        radiusControls.clear();
        centers.push_back(sphereCenter);
    }
}

void CameraLabel::setOriginal(QImage newOriginal)
{
    original = newOriginal;
}

QImage CameraLabel::getOriginal()
{
    return original;
}

void CameraLabel::setDetectingChart(const bool& newValue)
{
    detectingChart = newValue;
    delimitingArea = newValue;
}

void CameraLabel::reset()
{
    if (original.width() > 0)
        setPixmap(QPixmap::fromImage(original));
    original = QImage();
    detectingSphere = false;
    detectingChart = false;
    aligning = false;
}

bool CameraLabel::hasDetectedCenter()
{
    return (sphereCenter[2] > 0);
}

const Vector3f& CameraLabel::getCenter()
{
    return sphereCenter;
}

const vector<Vector3f> &CameraLabel::getCenters()
{
    return centers;
}

void CameraLabel::setCenter(const Vector3f &newCenter)
{
    sphereCenter = newCenter;
    radiusControl = sphereCenter;
    radiusControl[0] += sphereCenter[2];
    radiusControl[2] = 4;
}

bool CameraLabel::hasDetectedChart()
{
    return (averageColors.size() > 0);
}

QImage CameraLabel::addCorrespondence(const QPoint &newCorrespondence)
{
    QPoint newCorRes;
    newCorRes.setX(((float)newCorrespondence.x()/this->width()) * original.width());
    newCorRes.setY(((float)newCorrespondence.y()/this->height()) * original.height());

    correspondences.push_back(newCorRes);
    QImage tmp = original;
    QPainter painter(&tmp);
    QPen p(QColor(255, 0, 0));
    p.setWidth(1);
    painter.setPen(p);
    painter.setBrush(QBrush ( Qt::green,Qt::SolidPattern ));
    for (unsigned int i = 0; i < correspondences.size(); i++)
    {
        painter.drawEllipse(correspondences[i], 3*(original.width()/900.0), 3*(original.width()/900.0));
    }
    return tmp;
}

QImage CameraLabel::removeCorrespondence()
{
    correspondences.pop_back();
    QImage tmp = original;
    QPainter painter(&tmp);
    QPen p(QColor(255, 0, 0));
    p.setWidth(1);
    painter.setPen(p);
    painter.setBrush(QBrush ( Qt::green,Qt::SolidPattern ));
    for (unsigned int i = 0; i < correspondences.size(); i++)
    {
        painter.drawEllipse(correspondences[i], 3, 3);
    }
    return tmp;
}

const vector<Vector3f>& CameraLabel::getColorChart()
{
    return averageColors;
}

void CameraLabel::setColorChart(const vector<Vector3f> &newColors)
{
    if (averageColors.size() > 0)
        averageColors.clear();
    for (unsigned int i = 0; i < newColors.size(); i++)
        averageColors.push_back(newColors[i]);
    for (unsigned int i = 0; i < averageColors.size(); i++)
        cout << averageColors[i].transpose() << endl;
    accepted = true;
    linearRegression();
}

const vector<QPoint> &CameraLabel::getCorrespondences()
{
    return correspondences;
}

void CameraLabel::clearCorrespondences()
{
    correspondences.clear();
}

void CameraLabel::delimitArea(const bool &newDelimitingArea)
{
    delimitingArea = newDelimitingArea;
}

void CameraLabel::clearChart()
{
    averageColors.clear();
    colorSquares.clear();
}

void CameraLabel::clearAll()
{
    // clear sphere
    clearChart();
    clearCorrespondences();
}

void CameraLabel::mousePressEvent(QMouseEvent *ev)
{
    QPoint currentPos = ev->pos();
    currentPos.setX(((float)currentPos.x()/this->width()) * original.width());
    currentPos.setY(((float)currentPos.y()/this->height()) * original.height());
    previousPoint = currentPos;
    if (zoom > 0)
    {
        currentPos /= 2;
        QPoint startWindow;
        startWindow = zoomCenter - QPoint(original.width()/4, original.height()/4);
        currentPos += startWindow;
    }
    if (sphereCenter[2] != 0 && detectingSphere)
    {
        float dist = sqrt(pow(currentPos.x() - sphereCenter[0], 2) + pow(currentPos.y() - sphereCenter[1], 2)); // check if inside sphere
        float dist1 = dist;
        float dist2 = sqrt(pow(currentPos.x() - radiusControl[0], 2) + pow(currentPos.y() - radiusControl[1], 2));

        if (dist < sphereCenter[2] + radiusControl[2]*(original.width()/900.0))
        {
            if (dist1 < radiusControl[2]*(original.width()/900.0))
                movingCenter = true;
            else
                if (dist2 < radiusControl[2]*(original.width()/900.0))
                    changingRadius = true;
        }
    }
    if (detectingChart)
    {
        for (unsigned int i = 0; i < colorSquares.size(); i++)
        {
            if (colorSquares[i].contains(currentPos))
            {
                changingSquare = true;
                squareIndex = i;
                break;
            }
        }
    }
}

void CameraLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    QPoint evRes;
    evRes.setX(((float)ev->pos().x()/this->width()) * original.width());
    evRes.setY(((float)ev->pos().y()/this->height()) * original.height());

    QPoint clicked = evRes;
    if (detectingSphere)
    {
        if (currentCenters.size() > 1 || sphereCenter[2] == 0 || sphereCenter[2] == 60)
        {

            int index = 0;
            float minDist = numeric_limits<float>::max();
            for (unsigned int i = 0; i < currentCenters.size(); i++)
            {
                float dist = sqrt(pow(clicked.x() - currentCenters[i][0], 2) + pow(clicked.y() - currentCenters[i][1], 2));
                if (dist < minDist)
                {
                    index = i;
                    minDist = dist;
                }
            }
            if (currentCenters.size() != 0)
            {
                sphereCenter = currentCenters[index];
                radiusControl = radiusControls[index];
                currentCenters.clear();
                radiusControls.clear();
                centers.push_back(sphereCenter);
            }
            else
            {
                original = this->pixmap()->toImage();
                sphereCenter << clicked.x(), clicked.y(), 60;
                radiusControl << sphereCenter[0] + 60, sphereCenter[1], 4;
                centers.clear();
                centers.push_back(sphereCenter);
            }
        }

        movingCenter = false;
        changingRadius = false;
    }
    if (detectingChart)
    {
        if (colorSquares.size() != 0)
        {
            changingSquare = false;
            int r, g, b;
            QColor pixelColor = original.pixel(evRes.x(),evRes.y());
            pixelColor.getRgb(&r, &g, &b);
        }
        else
        {
            detectColors(evRes);
        }
    }
}

void CameraLabel::mouseMoveEvent(QMouseEvent *ev)
{
    QPoint evRes;
    evRes.setX(((float)ev->pos().x()/this->width()) * original.width());
    evRes.setY(((float)ev->pos().y()/this->height()) * original.height());
    if (changingRadius)
    {
        QPoint diff = evRes - previousPoint;
        previousPoint = evRes;
        float change = sqrt(pow(diff.x(), 2) + pow(diff.y(), 2));
        int sign = 1;
        if (diff.x() < 0)
            sign = -1;
        radiusControl[0] += sign*change;
        sphereCenter[2] = radiusControl[0] - sphereCenter[0];
    }
    if (movingCenter)
    {
        QPoint newPoint = evRes;
        if (zoom > 0)
        {
            newPoint /= 2;
            QPoint startWindow;
            startWindow = zoomCenter - QPoint(this->width()/4, this->height()/4);
            newPoint += startWindow;
        }
        sphereCenter[0] = newPoint.x();
        sphereCenter[1] = newPoint.y();
        radiusControl[0] = sphereCenter[0] + sphereCenter[2];
        radiusControl[1] = sphereCenter[1];
    }
    if (changingSquare)
    {
        QPoint topLeft, bottomRight, topRight, bottomLeft;
        topLeft = colorSquares[squareIndex].topLeft();
        bottomRight = colorSquares[squareIndex].bottomRight();
        topRight = colorSquares[squareIndex].topRight();
        bottomLeft = colorSquares[squareIndex].bottomLeft();
        float dist1, dist2, dist3, dist4;
        dist1 = sqrt(pow(evRes.x() - topLeft.x(), 2) + pow(evRes.y() - topLeft.y(), 2));
        dist2 = sqrt(pow(evRes.x() - bottomRight.x(), 2) + pow(evRes.y() - bottomRight.y(), 2));
        dist3 = sqrt(pow(evRes.x() - topRight.x(), 2) + pow(evRes.y() - topRight.y(), 2));
        dist4 = sqrt(pow(evRes.x() - bottomLeft.x(), 2) + pow(evRes.y() - bottomLeft.y(), 2));
        float distMin;
        distMin = qMin(qMin(qMin(dist1, dist2), dist3), dist4);
        if (distMin == dist1)
            colorSquares[squareIndex].setTopLeft(evRes);
        if (distMin == dist2)
            colorSquares[squareIndex].setBottomRight(evRes);
        if (distMin == dist3)
            colorSquares[squareIndex].setTopRight(evRes);
        if (distMin == dist4)
            colorSquares[squareIndex].setBottomLeft(evRes);
    }
//    if (delimitingArea)
//        areaEnd = evRes;

}

void CameraLabel::paintEvent(QPaintEvent *e)
{
    if (this->pixmap() == 0)
        return;
    QImage tmp;
    if (original.width() != 0)
        tmp = original;
    else
        tmp = this->pixmap()->toImage();
    QPainter painter(&tmp);
    QPen p(QColor(255, 0, 0));
    p.setWidth(3);
    p.setColor(QColor(255, 255, 255));
    painter.setBrush(QBrush(Qt::white, Qt::NoBrush));
    painter.setPen(p);
    if (detectingSphere)
    {
        for (unsigned int i = 0; i < currentCenters.size(); i++)
        {
            painter.drawEllipse(QPoint(currentCenters[i][0], currentCenters[i][1]), (int)currentCenters[i][2], (int)currentCenters[i][2]);
            painter.drawEllipse(QPoint(currentCenters[i][0], currentCenters[i][1]), (int)(2 * (original.width() / 900.0)), (int)(2 * (original.width() / 900.0)));
            painter.drawEllipse(QPoint(radiusControls[i][0], radiusControls[i][1]), (int)radiusControls[i][2], (int)radiusControls[i][2]);
        }
        if (sphereCenter[2] != 0)
        {
            painter.drawEllipse(QPoint(sphereCenter[0], sphereCenter[1]), (int)sphereCenter[2], (int)sphereCenter[2]);
            painter.drawEllipse(QPoint(sphereCenter[0], sphereCenter[1]), (int)(2 * (original.width() / 900.0)), (int)(2 * (original.width() / 900.0)));
            painter.drawEllipse(QPoint(radiusControl[0], radiusControl[1]), (int)radiusControl[2], (int)radiusControl[2]);
        }
    }
    if (detectingChart)
    {
        for (unsigned int i = 0; i < colorSquares.size(); i++)
        {
            painter.drawRect(colorSquares[i]);
        }
    }
    if (delimitingArea && areaStart != areaEnd && areaEnd != QPoint(0, 0) && colorSquares.size() == 0)
    {
//        p.setColor(QColor(0, 100, 0, 100));
//        painter.setPen(p);
//        painter.setBrush(QBrush(Qt::white, Qt::Dense6Pattern));
        painter.drawRect(QRect(areaStart, areaEnd));
    }
    if (lightCenter(2) != 0)
    {
        p.setColor(QColor(0, 255, 0));
        painter.setPen(p);
        painter.setBrush(QBrush(Qt::white, Qt::NoBrush));
        painter.drawEllipse(lightCenter(0), lightCenter(1), lightCenter(2)*(original.width()/1000.0), lightCenter(2)*(original.width()/1000.0));
    }

    setPixmap(QPixmap::fromImage(tmp));
    QLabel::paintEvent(e);
}

void CameraLabel::wheelEvent(QWheelEvent *e)
{
    const int WHEEL_STEP = 120;
    int zoomRate = e->delta()/WHEEL_STEP;
    cv::Vec2f newZoomCenter(e->pos().x()/(float)original.width(), e->pos().y()/(float)original.height());
    emit zoomed(zoomRate, newZoomCenter);
}

void CameraLabel::setLightCenter(const Vector3i &newLightCenter)
{
    lightCenter << newLightCenter(0), newLightCenter(1), newLightCenter(2);
}

const Vector3f &CameraLabel::getColorWeights()
{
    cout << "color weights: " << colorWeights.transpose() << endl;
    return colorWeights;
}

void CameraLabel::setColorThreshold(int threshold)
{
    colorThreshold = threshold;
}
