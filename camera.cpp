#include "camera.h"

PhotoCamera::PhotoCamera()
{
    intrinsicMatrix = Matrix4f::Identity();
    extrinsicMatrix = Matrix4f::Identity();
    rotationMatrix = Matrix4f::Identity();
    translationMatrix = Matrix4f::Identity();

    translationMatrix.col(3) << 0, 0, 0, 1;

    focalLength = 39.514 /*47.3606*/;
    pixelSize << 0.0319598, 0.0319598;
    oneOverDx = 1/pixelSize(0);
    oneOverDy = 1/pixelSize(1);
    radialDistortion = Vector2f(0.0, 0.0);
    tangentialDistortion = Vector2f(0.0, 0.0);
    buildIntrinsic();
    buildExtrinsic();
}

PhotoCamera::~PhotoCamera()
{
}

bool PhotoCamera::applyDistortion()
{
    if (radialDistortion(0) > 1e-6 || radialDistortion(1) > 1e-6 || tangentialDistortion(0) > 1e-6 || tangentialDistortion(1) > 1e-6)
        return true;
    return false;
}

void PhotoCamera::buildIntrinsic()
{
    Matrix3f Mi;
    Mi << -focalLength*oneOverDx, 0, principalPoint(0),
            0, -focalLength*oneOverDy, principalPoint(1),
            0, 0, 1;
    intrinsicMatrix.topLeftCorner(3, 3) = Mi;
}

void PhotoCamera::buildExtrinsic()
{
    extrinsicMatrix = rotationMatrix*translationMatrix;
}

void PhotoCamera::build(const PhotoCamera& newCamera)
{
    setPixelSize(newCamera.getPixelSize()(0), newCamera.getPixelSize()(1));
    setFocalLength(newCamera.getFocalLength());
    setRotation(newCamera.getRotation());
    setTranslation(newCamera.getTranslation());
    setPrincipalPoint(newCamera.getPrincipalPoint());
    setWidthHeight(newCamera.getWidth(), newCamera.getHeight());

    if (rotationMatrix == Matrix4f::Identity())
        extrinsicMatrix = newCamera.getExtrinsic();
    else
        buildExtrinsic();

    if (focalLength > 0)
        buildIntrinsic();
    else
        intrinsicMatrix = newCamera.getIntrinsic();

//    cout << "intrinsic: " << endl << intrinsicMatrix << endl;
//    cout << "extrinsic: " << endl << extrinsicMatrix << endl << endl;
    cameraCenter = center(intrinsicMatrix.topLeftCorner(3,4)*extrinsicMatrix);
}

void PhotoCamera::setDefault(const int &fov, const int &width, const int& height)
{
    setWidthHeight(width, height);

    float radianAngle = (180/PI);
    focalLength = (getHeight()/2.0) / tan((fov/radianAngle)/2) * (1/oneOverDx);
    buildIntrinsic();
}


const Matrix4f& PhotoCamera::getIntrinsic() const
{
    return intrinsicMatrix;
}

const Matrix4f& PhotoCamera::getExtrinsic() const
{
    return extrinsicMatrix;
}

MatrixXf PhotoCamera::getPseudoInverse()
{
    MatrixXf P = intrinsicMatrix*extrinsicMatrix;
    MatrixXf Pt = P.transpose();
    MatrixXf pseudoInverse = Pt*(P*Pt).inverse();
    return pseudoInverse;
}

double PhotoCamera::intrinsic(int i, int j)
{
    return intrinsicMatrix(i, j);
}

double PhotoCamera::extrinsic(int i, int j)
{
    return extrinsicMatrix(i, j);
}

const int& PhotoCamera::getHeight() const
{
    return height;
}

const int& PhotoCamera::getWidth() const
{
    return width;
}

float PhotoCamera::getFocalLength() const
{
    return focalLength;
}

Vector2f PhotoCamera::getFocalInPixels()
{
    Vector2f f = Vector2f(-intrinsicMatrix(0, 0), -intrinsicMatrix(1, 1));
    return f;
}

const Vector2f& PhotoCamera::getPixelSize() const
{
    return pixelSize;
}

const Vector2i& PhotoCamera::getPrincipalPoint() const
{
    return principalPoint;
}

Vector2f PhotoCamera::getRadialDistortion() const
{
    return radialDistortion;
}

Vector2f PhotoCamera::getTangentialDistortion() const
{
    return tangentialDistortion;
}

double PhotoCamera::lambda()
{
    return 1 + radialDistortion(0)*radialDistortion(0) + radialDistortion(1)*radialDistortion(1);
}

const Matrix4f& PhotoCamera::getRotation() const
{
    return rotationMatrix;
}

const Matrix4f& PhotoCamera::getTranslation() const
{
    return translationMatrix;
}

void PhotoCamera::setFov(const float &fov)
{
    float radianAngle = (180/PI);
    focalLength = (getHeight()/2.0) / tan((fov/radianAngle)/2) * (1/oneOverDx);
    buildIntrinsic();
}

void PhotoCamera::setHeight(const int& newHeight)
{
    height = newHeight;
}

void PhotoCamera::setWidth(const int& newWidth)
{
    width = newWidth;
}

void PhotoCamera::setWidthHeight(const int& newWidth, const int& newHeight)
{
    width = newWidth;
    height = newHeight;
}

void PhotoCamera::setFocalLength(const float& newFocalLength)
{
    focalLength = newFocalLength;
}

void PhotoCamera::setPixelSize(const float& newPx, const float &newPy)
{
    pixelSize << newPx, newPy;
    oneOverDx = 1.0/newPx;
    oneOverDy = 1.0/newPy;
}

void PhotoCamera::setRadialDistortion(float newDistortionX, float newDistortionY)
{
//    distortionX = newDistortionX;
//    distortionY = newDistortionY;
//    radialDistortion = distortionX*distortionX + distortionY*distortionY;
    radialDistortion = Vector2f(newDistortionX, newDistortionY);
}

void PhotoCamera::setTangentialDistortion(float newDistortionX, float newDistortionY)
{
    tangentialDistortion = Vector2f(newDistortionX, newDistortionY);
}

void PhotoCamera::setRotation(const Matrix4f& newRotation)
{
    rotationMatrix = newRotation;
}

void PhotoCamera::setTranslation(const Matrix4f& newTranslation)
{
    translationMatrix = newTranslation;
}

void PhotoCamera::setPrincipalPoint(const Vector2i& newPrincipalPoint)
{
    principalPoint = newPrincipalPoint;
}

void PhotoCamera::setIntrinsic(const Matrix3f M)
{
    intrinsicMatrix.topLeftCorner(3, 3) = M;
}

void PhotoCamera::setExtrinsic(const Matrix4f M)
{
    extrinsicMatrix = M;
}

Vector3f PhotoCamera::unproject(const Vector2d& pixel)
{
    Vector3f pixelH;
    pixelH << pixel(0), pixel(1), 1;
    MatrixXf pseudoInverse = getPseudoInverse();
    Vector4f XH = pseudoInverse*pixelH;
    Vector3f p = XH.hnormalized();
    Vector3f ray = p - cameraCenter.hnormalized();
    ray /= ray.norm();

    return ray;
}

Vector2f PhotoCamera::project(const Vector3d& point)
{
    MatrixXf P = intrinsicMatrix*extrinsicMatrix;
    Vector4f pointH;
    pointH << point(0), point(1), point(2), 1;
    Vector3f pixelH = P*pointH;
    return pixelH.hnormalized();

}

const Vector3d& PhotoCamera::getEulerAngles()
{
    return eulerAngles;
}

const Vector4f& PhotoCamera::getCenter() const
{
    return cameraCenter;
}

void PhotoCamera::applyScale(float s)
{
    Matrix4f scale = Matrix4f::Identity();
    scale(0, 0) = s;
    scale(1, 1) = s;
    scale(2, 2) = s;
    Vector4f newT = translationMatrix.col(3);
//    cout << "old t:" << endl << newT << endl;
    newT = scale*newT;
//    cout << "new t:" << endl << newT << endl;
    translationMatrix.col(3) = newT;
//    translationMatrix.col(3) << 0, 0, 0, 1;
    extrinsicMatrix = rotationMatrix*translationMatrix;
}

bool PhotoCamera::operator==(const PhotoCamera &camera)
{
    bool isIntrinsicEqual = (intrinsicMatrix == camera.getIntrinsic());
    bool isExtrinsicEqual = (extrinsicMatrix == camera.getExtrinsic());
    bool isRotationEqual = (rotationMatrix == camera.getRotation());
    bool isTranslationEqual = (translationMatrix == camera.getTranslation());
    bool isPointEqual = (principalPoint == camera.getPrincipalPoint());
    bool isPixelEqual = (pixelSize == camera.getPixelSize());
    bool isCenterEqual = (cameraCenter == camera.getCenter());
    return (isIntrinsicEqual && isExtrinsicEqual && isRotationEqual && isTranslationEqual && isPointEqual && isPixelEqual && isCenterEqual);
}

bool PhotoCamera::operator!=(const PhotoCamera &camera)
{
    return !(*this==camera);
}

void PhotoCamera::setPhi(float newPhi)
{
    phi = newPhi;
}

void PhotoCamera::setTheta(float newTheta)
{
    theta = newTheta;
}

float PhotoCamera::getPhi()
{
    return phi;
}

float PhotoCamera::getTheta()
{
    return theta;
}

Vector4f PhotoCamera::center(const MatrixXf& camera)
{
    Matrix3f C1, C2, C3, C4;
    C1 << camera.col(1), camera.col(2), camera.col(3);
    C2 << camera.col(0), camera.col(2), camera.col(3);
    C3 << camera.col(0), camera.col(1), camera.col(3);
    C4 << camera.col(0), camera.col(1), camera.col(2);

    Vector4f C;
    C << C1.determinant(), -C2.determinant(), C3.determinant(), -C4.determinant();
    return C;
}
