#ifndef CURVEGLWIDGET_H
#define CURVEGLWIDGET_H

#include "utils.h"

class CurveGLWidget : public QGLWidget
{
    Q_OBJECT

    GLuint pbo;
    GLuint cbo;
    GLuint tbo;
    GLuint axisTexture;
    GLuint textureBuffer;
    Program pointsProgram;
    Program curveProgram;
    Program textureProgram;
    Matrix4f projectionMatrix;
    vector<Vector4f> polynomial;
    vector<Vector2f*> dataCurve;
    vector<Vector2f*> dataPoints;
    QPoint mouseClick;

    int curveResolution;
    int numPoints;
    int numSPoints;
    float minX;
    float maxX;
    float minY;
    float maxY;
    float currentAngle;
    Vector2f xInterval;
    Vector2f yInterval;
    bool plotRed;
    bool plotGreen;
    bool plotBlue;
    bool plotLum;
    bool plotHue;
    bool plotSaturation;
    bool plotValue;
    bool plotPoints;
    bool plotCurves;
    bool plotDiffuse;
    bool plotSpecular;

    int k = 0;

    void createGraph(int);
    void createGraph(initializer_list<int>);
    void createSGraph(int);
    void createSGraph(initializer_list<int>);
    void drawCurve(unsigned int);
    void drawCurve(initializer_list<bool>);
    void drawPoints(unsigned int);
    void drawPoints(initializer_list<bool>);
    void drawSCurve(unsigned int);
    void drawSCurve(initializer_list<bool>);
    void drawSPoints(unsigned int);
    void drawSPoints(initializer_list<bool>);
    void clearCurves();
    void buildAxisImage();
    void renderAxisImage();


public:
    CurveGLWidget(QWidget *parent = 0);
    ~CurveGLWidget();
    void changeResolution(const int&);
    void reloadShaders();
    void plot(bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool);
    void reset();
    void save();

    QSize minimumSizeHint() const;
    void initializeGL();

protected:
    void paintGL();
    void resizeGL(int width, int height);
    void wheelEvent(QWheelEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void keyReleaseEvent(QKeyEvent *);

public slots:
    void plot(Vector2f*, int, float*, int);
    void plotLine(float);
};

#endif // CURVEGLWIDGET_H
