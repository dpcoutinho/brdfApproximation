#ifndef CAMERALABEL_H
#define CAMERALABEL_H


#include "utils.h"

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#include <QLabel>
#endif

#include <iostream>

using namespace std;

class CameraLabel : public QLabel
{
    Q_OBJECT

    vector<QPoint> correspondences;
    vector<Vector3f> centers;
    vector<Vector3f> currentCenters;
    vector<Vector3f> radiusControls;
    vector<Vector3f> averageColors;
    vector<QRect> colorSquares;
    Vector3f corners[4];
    Vector3f** chart;
    bool** colorDetected;
    Vector3f sphereCenter;
    Vector3f radiusControl;
    Vector3f lightCenter;
    QPoint areaStart, areaEnd;
    QPoint previousPoint;
    QPoint zoomCenter;
    QImage original;
    Matrix3f H, HI;
    Vector3f colorWeights;

    bool movingRadius;
    bool changingRadius;
    bool detectingSphere;
    bool delimitingArea;
    bool detectingChart;
    bool aligning;
    bool movingCenter;
    bool changingSquare;
    bool accepted;
    int squareIndex;
    int zoom;
    int accumZoom;
    int cntCorners;
    int colorThreshold;

    void detectColors(const QPoint&);
    QRect fillSquare(const QImage&, const QPoint&, const int&, const int&, const QRect& = QRect());
    void linearRegression();
    void calculateAverageColors();

    QImage sobel(QImage);
    void removePerspective();
public:
    explicit CameraLabel(QWidget *parent = 0);
    ~CameraLabel();

    void setDetectingSphere(const bool&);
    bool detectChart(int);
    void acceptChart();
    void setAlign(const bool&);
    void drawCenters(const vector<Vector3f>&);
    void setOriginal(QImage);
    QImage getOriginal();
    void setDetectingChart(const bool&);
    void reset();
    bool hasDetectedCenter();
    const Vector3f& getCenter();
    const vector<Vector3f>& getCenters();
    void setCenter(const Vector3f&);
    bool hasDetectedChart();
    QImage addCorrespondence(const QPoint&);
    QImage removeCorrespondence();
    const vector<Vector3f>& getColorChart();
    void setColorChart(const vector<Vector3f>&);
    const vector<QPoint>& getCorrespondences();
    void clearCorrespondences();
    void delimitArea(const bool&);
    void clearChart();
    void clearAll();
    void setLightCenter(const Vector3i&);
    const Vector3f& getColorWeights();
    void setColorThreshold(int);

protected:
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void paintEvent(QPaintEvent *);
    void wheelEvent(QWheelEvent *);

signals:
    void clicked(QPoint p);
    void zoomed(int zoomRate, cv::Vec2f);
    void message(const char*);
public slots:

};

#endif // CAMERALABEL_H
