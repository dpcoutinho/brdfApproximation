#ifndef WIDGET_H
#define WIDGET_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <QtGlobal>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSlider>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QAction>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QMenuBar>
#else
#include <QtGui>
#endif

#include "webcamthread.h"
#include "glwidget.h"
#include "cameralabel.h"

class Widget : public QWidget
{
    Q_OBJECT

    QPushButton* loadCamerasButton;
    QPushButton* loadLightsButton;
    QPushButton* loadMeshButton;
    QPushButton* takePhotoButton;
    QPushButton* acceptLightbutton;
    QPushButton* startButton;
    QPushButton* exitButton;
    QPushButton* leftButton;
    QPushButton* rightButton;
    QPushButton* renderDifferButton;
    QPushButton* saveButton;
    QPushButton* errorButton;
    QPushButton* updateButton;
    QPushButton* reloadShaderButton;
    QPushButton* calculateLightButton;
    QPushButton* calculateLightSButton;
    QPushButton* loadBestPhotoButton;
    QPushButton* acceptBestPhotoButton;
    QPushButton* showCameraButton;
    QPushButton* detectSphereButton;
    QPushButton* alignButton;

    CameraLabel* cameraLabel;

    QTabWidget* tabs;

    QWidget* loadingTab;
    QWidget* actionTab;
    QWidget* controlTab;
    QWidget* cameraTab;

    QSlider* kdSlider;
    QSlider* ksSlider;
    QSlider* shininessSlider;
    QSlider* rSlider;
    QSlider* gSlider;
    QSlider* bSlider;

    QMessageBox* lightMessageBox;
    QTextEdit* xLineEdit;
    QTextEdit* yLineEdit;
    QTextEdit* zLineEdit;

    QGroupBox* viewsGroupBox;

    GLWidget *cameraView;
    WebcamThread* webcam;

    vector<PhotoCamera> newCameras;
    vector<char*> photoFiles;
    vector<Vector3f> lightPositions;

    QCheckBox* readImagesFile;

    string fileDir;
    bool cameraOpen;
    bool detecting;

    PhotoCamera currentCamera;

    void loadCameras(const char*);
    void loadLights(const char*);
    void readAllImages();
    void createCameras();

public:
    Widget(QWidget *parent = 0);
    ~Widget();
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void arguments(char*);
    void arguments(int, char**);

signals:
    void updated();

private slots:
    void loadCameras();
    void loadLights();
    void loadMesh();
    void takePhoto();
    void updateFrame(const QImage& frame);
    void showCamera();
    void start();
    void exit();
    void leftPhoto();
    void rightPhoto();
    void renderDiffer();
    void saveImage();
    void printError();
    void updateGL();
    void reloadShaders();
    void calculateLight();
    void loadBestPhoto();
    void acceptBestPhoto();
    void calculateNewPhotos();
    void calculateNewSpecularPhotos();
    void detectSphere();
    void align();
    void setCircle(const QPoint& p);


};

#endif // WIDGET_H
