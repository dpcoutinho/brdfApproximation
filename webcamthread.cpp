#include "webcamthread.h"

WebcamThread::WebcamThread(QObject *parent) :
    QThread(parent)
{
    zoom = 0;
    capture = 0;
    sphereCenter[0] = -1;
    frameSize.setHeight(0);
    frameSize.setWidth(0);
    detecting = false;
    paused = false;
    hidden = true;
    lightDirection[0] = lightDirection[1] = lightDirection[2] = 0;
    firstSave = false;
}

void WebcamThread::stop()
{
    stopMutex.lock();
    capture.release();
    doStop = true;
    stopMutex.unlock();
}

void WebcamThread::start(Priority p)
{
    doStop = false;
    send = true;
    hidden = false;
    bool captured = capture.open(1);
    if (!captured)
        captured = capture.open(0);
    bool wDone, hDone;
//    wDone = capture.set(CV_CAP_PROP_FRAME_WIDTH, 1920);
//    hDone = capture.set(CV_CAP_PROP_FRAME_HEIGHT, 1080);
    wDone = capture.set(CV_CAP_PROP_FRAME_WIDTH, 3624);
    hDone = capture.set(CV_CAP_PROP_FRAME_HEIGHT, 2448);
    QThread::start(p);
}

void WebcamThread::setCameraCircle(const QPoint &pos, const QSize& size)
{
    QPoint convertedPos = convertSize(pos, size);
    sphereCenter[0] = convertedPos.x();
    sphereCenter[1] = convertedPos.y();
    sphereCenter[2] = 0;
}

void WebcamThread::detectSphere(const bool &newDetect)
{
    detecting = newDetect;
}

void WebcamThread::pause()
{
    paused = true;
}

void WebcamThread::resume()
{
    paused = false;
    hidden = false;
}

cv::Vec3f WebcamThread::getLightDirection()
{
    return lightDirection;
}

const cv::Mat& WebcamThread::getFrame()
{
    return currentFrame;
}

bool WebcamThread::isPaused()
{
    return paused;
}

bool WebcamThread::isHidden()
{
    return hidden;
}

void WebcamThread::hide()
{
    hidden = true;
}

void WebcamThread::setCenter(const cv::Vec3f &newCenter)
{
    sphereCenter = newCenter;
}

void WebcamThread::clear()
{

}

void WebcamThread::run()
{
    while (true)
    {
        if (paused)
            continue;
        stopMutex.lock();
        if (doStop)
        {
            doStop = false;
            stopMutex.unlock();
            break;
        }
        stopMutex.unlock();

        captureMutex.lock();
        if (capture.isOpened())
        {
            cv::Mat frame;
            bool read = capture.read(frame);
            captureMutex.unlock();
            cv::Mat gray;

            if (!read)
                continue;

            frameSize = QSize(frame.cols, frame.rows);
            if (!firstSave)
            {
                firstSave = true;
            }
            cvtColor(frame, gray, CV_BGR2GRAY );


            sendMutex.lock();
            currentFrame = frame.clone();
            if (sphereCenter[0] != -1)
            {
                cv::Vec2i lightCenter;
                lightDirection = detectLightDirection(sphereCenter, gray, lightCenter);
//                sendMutex.lock();
//                cv::circle(frame, lightCenter, 5, cv::Scalar(0, 255, 0));
//                imwrite("test.png", frame);
                emit drawLightCenter(lightCenter[0], lightCenter[1]);
                emit renderLightDirection(lightDirection[0], lightDirection[1], lightDirection[2]);
//                sendMutex.unlock();
            }
            sendMutex.unlock();
            if (send)
            {
                if (zoom > 0)
                {
                    int w = frame.cols/pow(2, zoom);
                    int h = frame.rows/pow(2, zoom);
                    int x = zoomCenter[0] - w/2;
                    int y = zoomCenter[1] - h/2;
                    if (x < 0)
                        x = 0;
                    if (y < 0)
                        y = 0;
                    if (x + w > frame.cols)
                        w = frame.cols - x;
                    if (y + h > frame.rows)
                        h = frame.rows - y;
                    cv::Mat zoomed(frame, cv::Rect(x, y, w, h));

                    const uchar* qImageBuffer = (const uchar*) zoomed.data;
                    img = QImage(qImageBuffer, zoomed.cols, zoomed.rows, zoomed.step, QImage::Format_RGB888);
                    img = img.rgbSwapped();
                }
                else
                {
                    const uchar* qImageBuffer = (const uchar*) frame.data;
                    img = QImage(qImageBuffer, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
                    img = img.rgbSwapped();
                }

                sendMutex.lock();
                emit newFrame(img);
                send = false;
                sendMutex.unlock();

            }
        }
        else
            captureMutex.unlock();
    }
}

void WebcamThread::release()
{
    sendMutex.lock();
    send = true;
    sendMutex.unlock();
}

void WebcamThread::zoomed(int zoomRate, cv::Vec2f newCenter)
{
    zoom += zoomRate;
    if (zoom < 0)
        zoom = 0;
    zoomCenter[0] = newCenter[0]*frameSize.width();
    zoomCenter[1] = newCenter[1]*frameSize.height();
}


QPoint WebcamThread::convertSize(const QPoint &p, const QSize& size)
{
    if (frameSize.width() == 0 || frameSize.height() == 0)
    {
        cout << "Frame size error" << endl;
        return QPoint(-1, -1);
    }
    if (size.width() == 0 || size.height() == 0)
    {
        cout << "Label size error" << endl;
        return QPoint(-1, -1);
    }
    QPoint newP;
    newP.setX(p.x() * ((float)frameSize.width()/size.width()));
    newP.setY(p.y() * ((float)frameSize.height()/size.height()));
    return newP;
}

cv::Vec3f WebcamThread::detectLightDirection(cv::Vec3i c, cv::Mat img, cv::Vec2i& lightCenter)
{
    cv::Vec2i current;
    int bigger = -1;
    int radius = c[2];
    for (int i = -radius; i <= radius; i++)
    {
        for (int j = -radius; j <= radius; j++)
        {
            unsigned char color = img.at<unsigned char>(cv::Point(c[0]+i,c[1]+j));
//            img.at<unsigned char>(cv::Point(c[0]+i,c[1]+j)) = 255;
            float distance = sqrt(pow(i, 2) + pow(j, 2));
            if ((int)color > bigger && distance <= c[2])
            {
                current = cv::Vec2i(c[0]+i, c[1]+j);
                bigger = img.at<unsigned char>(cv::Point(c[0]+i,c[1]+j));
            }
        }
    }
    // Maybe look a region of around 10 pixels in around the brightest pixel and catch the center. Look at that later

    lightCenter = current;

    cv::Vec3f r;
    r[0] = current[0] - c[0];
    r[1] = current[1] - c[1];
    r[1] *= -1;
    r[0] /= radius;
    r[1] /= radius;
    r[2] = sqrt(1 - pow(r[0], 2) - pow(r[1], 2));

    float phi = acos(r[2]);
    float phiL = 2*phi;
    float thetaL = asin(r[1]/sin(phi));

    cv::Vec3f l2;
    l2[0] = sin(phiL)*cos(thetaL);
    l2[1] = sin(phiL)*sin(thetaL);
    l2[2] = cos(phiL);

    cv::Vec3f v = cv::Vec3f(0, 0, -1);
    float dot = r[0]*v[0] + r[1]*v[1] + r[2]*v[2];

    cv::Vec3f l = v - 2*dot*r;
//    cout << "l1: " << l[0] << " " << l[1] << " " << l[2] << endl;
//    cout << "l2: " << l2[0] << " " << l2[1] << " " << l2[2] << endl;
    return l;
}
